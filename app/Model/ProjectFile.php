<?php
App::uses('AppModel', 'Model');
class ProjectFile extends AppModel {
	
	public $settings = array('location' => 'files/thumbnails', 'coverimage' => 'files/coverimages');	
	var $actsAs = array('common', 'folder', 'projectfile', 'attachable', 'thumb', 'cover');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'filename' => array(
            'rule' => array('extension',array('zip', 'rar', 'gif', 'jpeg', 'png', 'jpg', 'pdf', 'xlsm', 'xls', 'xlr', 'xlsx', 'docx', 'doc', 'pptx', 'ppt', 'pps', 'psd', 'csv')),
            //'required' => false,
            //'allowEmpty' => false,
            'message' => 'Invalid file extension.',
			'on' => 'create', // Limit validation to 'create' or 'update' operations
        ),
        'category_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'tags' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Category' => array(
			'className' => 'Category',
			'foreignKey' => 'category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Status' => array(
			'className' => 'Status',
			'foreignKey' => 'status_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
