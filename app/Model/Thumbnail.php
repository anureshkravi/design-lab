<?php
App::uses('AppModel', 'Model');

class Thumbnail extends AppModel {
	public $settings = array('location' => 'files/groupthumb');	
	var $actsAs = array('groupthumb');
}
