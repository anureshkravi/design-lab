<?php
//extending common, folder and projectfile behavior
class AttachableBehavior extends ModelBehavior {
	public $_defaults = array(
								'label' => 'filename',
								'errorMessages' => array(
									'isUploaded' => 'Malicious file upload.',
									'moveUploaded' => 'Error moving file upload.',
									'isValidType' => 'Invalid file type. Please check your file type and try again.',
									'isValidSize' => 'Invalid file size. Please check your file size and try again.'
								) ,
								'thumbQuality' => 80,
								'maxSize' => '103809024',

								'fields' => array(
									'filename' => 'filename'
								) ,
								'types' => array(
									'application/zip' => 'zip', 'application/octet-stream' => 'zip', 'application/x-zip-compressed' => 'zip', 'application/x-zip' => 'zip'
							),
	);

	public $_validThumbTypes = array('application/zip');
	public $_attachmentRoot = null;
	public $_errorMsg = "Error uploading file";
	public $settings = array();
	public $model = null;

	public function setup(Model $model, $config = array()) {
		$this->settings[$model->name] = array_merge($this->_defaults, $this->settings);
		$this->model = $model;
	}

	public function beforeSave(Model $model, $options = array()) {
		if(isset($model->data[$model->name]['project_folder'])){
			$this->_attachmentRoot = WWW_ROOT . str_replace(array('/','\\') , array(DS,DS) , 'files' . DS . $model->data[$model->name]['project_folder'] . DS . $model->data[$model->name]['category_folder'] . DS . $model->data[$model->name]['date_folder'] . DS);

			extract($this->settings[$model->name]);
			$attachment = $model->data[$model->name][$label];

			if (!empty($attachment['name'])) {
				$model->data[$model->name][$fields['filename']] = ($this->_fileExists($this->_attachmentRoot . $attachment['name'])) ? time() . '_' . $attachment['name'] : $attachment['name'];
				
				if (!$this->_isUploaded($attachment) || 
					!$this->_isValidSize($attachment['size']) || 
					!$this->_isValidType($attachment) || 
					!$this->_dirExists($this->_attachmentRoot) || 
					!$this->_moveUploaded($attachment['tmp_name'], $model->data[$model->name][$fields['filename']], $model->data[$model->name]['category_id'])
					) {
						$model->validationErrors[$label] = $this->_errorMsg;
						return false;
				}
			} else {
				$record = $model->findById($model->id);
				$model->data[$model->name][$fields['filename']] = $record[$model->name][$fields['filename']];
				return true;
			}
			return true;
		}
		return true;
	}

	protected function _moveUploaded($tmpName, $attachmentName, $category) {
		//not extracting assets category files
		if($category != 5) {
			//extracting zip folder
			$zip = new ZipArchive;
			if ($zip->open($tmpName) === TRUE) {
				$zip->extractTo($this->_attachmentRoot . $attachmentName);
				$zip->close();

				// copying files from zip folder and pasting into root folder
				$filename = explode('.zip', $attachmentName);
				$folder1 = new Folder($this->_attachmentRoot . $attachmentName . '\\' . $filename['0'] . '\\');

				// if someone create zip folder directly without creating a folder
				if(empty($folder1->path)) {
					$this->_errorMsg = 'Not a valid zip folder. Please check the folder structure';
					return false;
				} else {
					$folder1->copy($this->_attachmentRoot);
					// deleting .zip folder which comes after extraction inside the root folder
					$folder = new Folder($this->_attachmentRoot . $attachmentName);
					$folder->delete();
					return true;
				}
			} else {
				$this->_errorMsg = $this->settings[$this->model->name]['errorMessages']['moveUploaded'];
				return false;
			}
		} else {
			$this->_uploadZip($tmpName, $attachmentName);
			return true;
		}
	}

	protected function _uploadZip($tmpName, $attachmentName) {
		if (move_uploaded_file($tmpName, $this->_attachmentRoot . $attachmentName)) {
			return true;
		}
		$this->_errorMsg = $this->settings[$this->model->name]['errorMessages']['moveUploaded'];
		return false;
	}

	protected function _isUploaded($attachment) {
		if (isset($attachment['error']) && $attachment['error'] == 0 || !empty($attachment['tmp_name']) && $attachment['tmp_name'] != 'none') {
			return is_uploaded_file($attachment['tmp_name']);
		}
		$this->_errorMsg = $this->settings[$this->model->name]['errorMessages']['isUploaded'];
		return false;
	}

	protected function _isValidType($attachment) {
		//not validating assets category files
		if($this->model->data[$this->model->name]['category_id'] != 5) {
			$ext = substr($attachment['name'], strrpos($attachment['name'], '.') + 1);
			if (isset($this->settings[$this->model->name]['types'][$attachment['type']]) && $ext == $this->settings[$this->model->name]['types'][$attachment['type']]) {
				return true;
			}
			$this->_errorMsg = $this->settings[$this->model->name]['errorMessages']['isValidType'];
			return false;
		}
		return true;
	}

	protected function _isValidSize($size) {
		if ($size == 0) return false;
		if ($size <= $this->settings[$this->model->name]['maxSize']) {
			return true;
		}
		$this->_errorMsg = $this->settings[$this->model->name]['errorMessages']['isValidSize'];
		return false;
	}

	protected function _dirExists($dir) {
		if (!file_exists($dir)) {
			trigger_error(__("AttachableBehavior Error - Please create '{$dir}' directory and set permissions for uploading.", true) , E_USER_WARNING);
			return false;
		}
		return true;
	}

	protected function _fileExists($file) {
		if (file_exists($file)) {
			return true;
		}
		return false;
	}
}
?>
