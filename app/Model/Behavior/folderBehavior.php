<?php
	//extending common behavior
	class folderBehavior extends ModelBehavior{
		public function beforeSave(Model $model, $options = array()) {
			if(isset($model->data[$model->name]['project_folder']) && isset($model->data[$model->name]['category_folder'])){
				$projectFolder = 'files'.DS.$model->data[$model->name]['project_folder'];
				if( !file_exists($projectFolder) ){
					$folder = new Folder();
					$folder->create($projectFolder);
				}
	
				$categoryFolder = 'files'.DS.$model->data[$model->name]['project_folder'].DS.$model->data[$model->name]['category_folder'];
				if( !file_exists($categoryFolder) ){
					$folder = new Folder();
					$folder->create($categoryFolder);
				}
			}
			return true;
		}
	}
?>