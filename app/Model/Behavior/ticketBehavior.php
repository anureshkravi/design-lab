<?php
class ticketBehavior extends ModelBehavior {

    public  $_errorMsg = "Error uploading file";

    public function beforeSave(Model $model, $options = array()) {
        if(!empty($model->data[$model->name]['filename'][0]['name'])) {
            $location = WWW_ROOT.'documents/'.$model->data[$model->name]['project_folder'];
            $this->_dirExists($location);

            foreach($model->data[$model->name]['filename'] as $upload) {
                $file = ($this->_fileExists($location.'/'.$upload['name'])) ? time().'_'.$upload['name'] : $upload['name'];

                if($this->_isUploaded($upload) &&
                    $this->_moveUploaded($upload['tmp_name'], $location.'/'.$file)) {
                    $filename[] = $file;
                } else {
                    $model->validationErrors['filename'] = $this->_errorMsg;
                    return false;
                }
            }
            $model->data[$model->name]['filename'] = implode($filename, ',');
        } else {
            if(!isset($model->data[$model->name]['id'])) {
                $model->data[$model->name]['filename'] = '';
            }
        }
    	return true;
    }

    protected function _moveUploaded($tmpName, $attachmentName) {
        if (move_uploaded_file($tmpName, $attachmentName)) {
            return true;
        }
        $this->_errorMsg = 'Error moving file upload.';
        return false;
    }

    protected function _isUploaded($attachment) {
        if (isset($attachment['error']) && $attachment['error'] == 0 || !empty($attachment['tmp_name']) && $attachment['tmp_name'] != 'none') {
            return is_uploaded_file($attachment['tmp_name']);
        }
        $this->_errorMsg = 'Malicious file upload.';
        return false;
    }   

    protected function _dirExists($dir) {
        if (!file_exists($dir)) {
          mkdir($dir, 0777, true);
        }   
        return true;
    }

    protected function _fileExists($file) {
        if (file_exists($file)) {
          return true;
        }   
        return false;
    } 
}
?>