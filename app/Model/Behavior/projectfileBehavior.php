<?php
	//extending common and folder behavior
	class projectfileBehavior extends ModelBehavior {
		public function beforeSave(Model $model, $options = array()) {
			// Add Project
			//checking whether folder exists or not while adding any project
			if(!isset($model->data[$model->name]['id']) && isset($model->data[$model->name]['project_folder'])){
				$path = 'files'.DS.$model->data[$model->name]['project_folder'].DS.$model->data[$model->name]['category_folder'].DS.$model->data[$model->name]['date_folder'];
				if($this->fileExists($path) === false){
					$model->validationErrors['title'] = 'Folder with todays date already exists!!';
					return false;
				}
			}

			//Editing Project
			/*if(isset($model->data[$model->name]['id']) && isset($model->data[$model->name]['date_folder'])){
				//if user is editing the project (but not uploading files)
				if(empty($model->data[$model->name]['filename']['name'])){
					//checking whether folder exists or not while editing any project
					$project = $model->findById($model->data[$model->name]['id']);
					if($model->data[$model->name]['project_folder'] != $project['ProjectFile']['project_folder'] ||
					   $model->data[$model->name]['category_folder'] != $project['Category']['category_folder'] ||
					   $model->data[$model->name]['date_folder'] != $project['ProjectFile']['date_folder']
						){
						$path = 'files'.DS.$model->data[$model->name]['project_folder'].DS.$model->data[$model->name]['category_folder'].DS.$model->data[$model->name]['date_folder'];
						if($this->fileExists($path) === false){
							$model->validationErrors['title'] = 'Folder with todays date already exists!!';
							return false;
						}
						//transfering files from old path to new path
						$project = $model->findById($model->data[$model->name]['id']);
						$oldpath = 'files'.DS.$project['ProjectFile']['project_folder'].DS.$project['Category']['category_folder'].DS.$project['ProjectFile']['date_folder'];
						$newpath = 'files'.DS.$model->data[$model->name]['project_folder'].DS.$model->data[$model->name]['category_folder'].DS.$model->data[$model->name]['date_folder'];
						$folder = new Folder($oldpath);
						$folder->copy($newpath);
						//deleting old files
						$this->deleteFolder($oldpath);
					}
				} else {
					//if user is editing the project (with uploading files)
					$project = $model->findById($model->data[$model->name]['id']);
					$newpath = 'files'.DS.$model->data[$model->name]['project_folder'].DS.$model->data[$model->name]['category_folder'].DS.$model->data[$model->name]['date_folder'];
					$oldpath = 'files'.DS.$project['ProjectFile']['project_folder'].DS.$project['Category']['category_folder'].DS.$project['ProjectFile']['date_folder'];
					if($model->data[$model->name]['project_folder'] == $project['ProjectFile']['project_folder'] &&
					   $model->data[$model->name]['category_folder'] == $project['Category']['category_folder'] &&
					   $model->data[$model->name]['date_folder'] == $project['ProjectFile']['date_folder']
						){
						$this->deleteFiles($oldpath);
					} else {
						if($this->fileExists($newpath) === false){
							$model->validationErrors['title'] = 'Folder with todays date already exists!!';
							return false;
						}
						$this->deleteFolder($oldpath);
					}
				}
			}*/

			//edit project (updating files in the same folder)
			if(isset($model->data[$model->name]['id']) && isset($model->data[$model->name]['filename']['name'])) {
				$project = $model->findById($model->data[$model->name]['id']);

				//deleting old files so that new files can be uploaded
				//checking whether new files have been uploaded or not while doing editing
				if(!empty($model->data[$model->name]['filename']['name'])) {
					$path = 'files'.DS.$project['ProjectFile']['project_folder'].DS.$project['Category']['category_folder'].DS.$project['ProjectFile']['date_folder'];
					$this->deleteFiles($path);
				}

				//setting up variables for attachable behavior
				$model->data[$model->name]['project_folder'] = $project['ProjectFile']['project_folder'];
				$model->data[$model->name]['category_folder'] = $project['Category']['category_folder'];
				$model->data[$model->name]['category_id'] = $project['Category']['id'];
				$model->data[$model->name]['date_folder'] = $project['ProjectFile']['date_folder'];
			}

			//Deleting Project
			/*if(isset($model->data[$model->name]['id']) && isset($model->data[$model->name]['status_id']) && $model->data[$model->name]['status_id'] == 3){
				$model->recursive = 0;
				$project = $model->findById($model->data[$model->name]['id']);
				$path = 'files'.DS.$project['ProjectFile']['project_folder'].DS.$project['Category']['category_folder'].DS.$project['ProjectFile']['date_folder'];
				//$folder = new Folder($path);
				//$folder->delete();
				rename($path, $path.'-bk');
			}*/
			return true;
		}

		public function beforeDelete(Model $model, $cascade = true) {
			$project = $model->findById($model->id);
			$location = 'files'.DS.$project['ProjectFile']['project_folder'].DS.$project['Category']['category_folder'].DS.$project['ProjectFile']['date_folder'];
			$this->deleteFolder($location);
		}

		public function fileExists($path){
			if(!file_exists($path)) {
				$folder = new Folder();
				$folder->create($path);
			} else {
				return false;
			}
		}

		public function deleteFiles($path){
			$dir = new Folder($path);
			$files = $dir->read(true, array('files', ''));
			foreach ($files[0] as $file) {
				$folder = new Folder($path.'\\'.$file);
				$folder->delete();
			}		
			foreach ($files[1] as $file) {
				$value = new File($dir->pwd() . DS . $file);
				$value->delete();
			}
		}

		public function deleteFolder($path){
			$folder = new Folder($path);
			$folder->delete();
		}
	}
?>