<?php
	class commonBehavior extends ModelBehavior{
		public function beforeSave(Model $model, $options = array()) {
			//finding project and category folders
			if(isset($model->data[$model->name]['title']) && isset($model->data[$model->name]['category_id'])){
				//finding category folder with category ID
				App::import('Model', 'Category');
				$categoryModel = new Category();
				$categoryModel->recursive = -1;
				$category = $categoryModel->findById($model->data[$model->name]['category_id'], array('fields' => 'category_folder'));

				//defining folder names
				$model->data[$model->name]['category_folder'] = $category['Category']['category_folder'];
				$model->data[$model->name]['date_folder'] = date('Y-m-d-H-m-s');
			}
			return true;
		}
	}
?>