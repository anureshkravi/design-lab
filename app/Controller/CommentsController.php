<?php
App::uses('AppController', 'Controller');

class CommentsController extends AppController {

	public $uses = array('Comment', 'ProjectFile', 'User', 'CommentView');

	//for plotting comments count on different locations
	public function view($projectId, $thumbId, $cookieId) {
		$comments = $this->Comment->find('all', array('conditions'=>array('project_id' => $projectId, 'thumb_id' => $thumbId, 'status_id' => 1), 'fields' => array('id', 'x_axis', 'y_axis', 'cookie_id')));

		foreach($comments as $key => $comment) {
			$comments[$key]['Comment']['classname'] = 'unread';
			if($this->CommentView->find('first', array('conditions' => array('comment_id' => $comment['Comment']['id'], 'cookie_id' => $cookieId)))) {
				$comments[$key]['Comment']['classname'] = '';
			}
			if($comment['Comment']['cookie_id'] == $cookieId) {
				$comments[$key]['Comment']['classname'] = '';
			}
		}

		$finalComment = [];
		if($comments) {
			foreach($comments as $comment){
				$finalComment[$comment['Comment']['x_axis']][$comment['Comment']['y_axis']][] = $comment['Comment'];
			}
		}
		echo json_encode($finalComment);
		exit();
	}

	//returning comments when someone clicks on comment circle
	public function viewByAxis($x, $y, $projectId, $thumbId, $cookieId) {
		$comments = $this->Comment->find('all', array('conditions' => array('x_axis' => $x, 'y_axis' => $y, 'project_id' => $projectId, 'thumb_id' => $thumbId, 'status_id' => 1)));

		foreach($comments as $comment) {
			$comment['Comment']['time'] = date('h:i A', strtotime($comment['Comment']['created']));
			$date = date('M d, Y', strtotime($comment['Comment']['created']));
			$data[$date][] = $comment;
		}

		//storing comments into comment view table
		foreach($comments as $comment) {
			if($comment['Comment']['cookie_id'] != $cookieId) {
				if(!$this->CommentView->find('first', array('conditions' => array('comment_id' => $comment['Comment']['id'], 'cookie_id' => $cookieId)))) {
					$newView = array('CommentView' => array('comment_id' => $comment['Comment']['id'], 'cookie_id' => $cookieId));
					$this->CommentView->create();
					$this->CommentView->save($newView);
				}
			}
		}

		//returning comments
		echo json_encode($data);
		exit();
	}

	//for adding comment
	public function addComment() {
		if ($this->request->is('post')) {
			$this->request->data = $this->request->input('json_decode');
			$this->Comment->create();
			if ($this->Comment->save($this->request->data)) {
				$lastSavedRecord = $this->Comment->findById($this->Comment->id);
				$lastSavedRecord['Comment']['time'] = date('h:i A', strtotime($lastSavedRecord['Comment']['created']));
				$lastSavedRecord['Comment']['date'] = date('M d, Y', strtotime($lastSavedRecord['Comment']['created']));
				echo json_encode($lastSavedRecord);
				exit();
			}
		}
	}

	//returning counts of comments for each thumb in a project
	public function viewAll($projectId, $cookieId) {
		$comments = $this->Comment->find('all', array('conditions' => array('project_id' => $projectId, 'status_id' => 1), 'fields' => array('id', 'thumb_id', 'cookie_id')));
		foreach($comments as $key => $comment) {
			$comments[$key]['Comment']['classname'] = 'unread';
			if($this->CommentView->find('first', array('conditions' => array('comment_id' => $comment['Comment']['id'], 'cookie_id' => $cookieId)))) {
				$comments[$key]['Comment']['classname'] = '';
			}
			if($comment['Comment']['cookie_id'] == $cookieId) {
				$comments[$key]['Comment']['classname'] = '';
			}
		}

		$thumbId = array();
		if($comments) {
			foreach($comments as $comment) {
				$thumbId[$comment['Comment']['thumb_id']][] = $comment;
			}
		}
		echo json_encode($thumbId);
		exit();
	}

	//setting status when any user remove his/her comments
	public function setstatus($id = null, $status = null, $cookieId) {
		$comment = $this->Comment->findById($id);
		if($comment && $comment['Comment']['cookie_id'] == $cookieId) {
			$comment['Comment']['status_id'] = $status;
			$this->Comment->save($comment);
        	exit;
		}
	}

	//deleting comment only by admin
	public function admin_delete($x_axis, $y_axis, $projectId, $thumbId) {
		if($this->Auth->user('role_id') == 1) {
			$this->request->onlyAllow('post', 'delete');
			//counting total no of comments
			$this->Comment->recursive = -1;
			$count = $this->Comment->find('count', array('conditions' => array('project_id' => $projectId, 'thumb_id' => $thumbId, 'x_axis' => $x_axis, 'y_axis' => $y_axis)));
			//deleting all the comments
			if($this->Comment->deleteAll(array('project_id' => $projectId, 'thumb_id' => $thumbId, 'x_axis' => $x_axis, 'y_axis' => $y_axis))) {
				echo $count;
				exit();
			} else {
				echo 'Some error occurs please try again later';
				exit();
			}
		} else {
			echo 'You dont have permission to delete comments';
			exit();
		}
	}

	//for sending email
	public function sendNotification() {
		if ($this->request->is('post')) {
			//finding project
			$project = $this->ProjectFile->findById($this->request->data['projectId']);

			//finding email ids from comment table
			$userEmails = $this->Comment->find('list', array('conditions' => array('project_id' => $this->request->data['projectId'], 'status_id' => 1), 'fields' => array('id', 'email'), 'group' => array('email')));

			//unsetting if any key is empty
			foreach($userEmails as $key => $email) {
				if(empty($email)) {
					unset($userEmails[$key]);
				}
			}

			//adding project user if not in user emails
			if(($key = array_search($project['User']['username'], $userEmails)) === false) {
			    array_push($userEmails, $project['User']['username']);
			}

			//if excluded email comes then we will unset that from user emails
			if(isset($this->request->data['excludeEmail'])) {
				if(($key = array_search($this->request->data['excludeEmail'], $userEmails)) !== false) {
				    unset($userEmails[$key]);
				}
			}

			//finding emails of admin users
			$this->User->recursive = -1;
			$adminEmails = $this->User->find('list', array('conditions' => array('role_id' => 1, 'status_id' => 1), 'fields' => array('id', 'username')));

			//defining vars for email template
			$var = array('clientName' => $this->request->data['client'], 'projectName' => $project['ProjectFile']['title'], 'link' => 'http://designlab.espire.com/projectfiles/'.$project['ProjectFile']['project_folder'].'/'.$project['Category']['category_folder'].'/'.$project['ProjectFile']['date_folder']);

			/* Using sendemail2 for new email configrations, updated on 06/03/2017  */
			
			/*
			if($this->Email->sendemail($var, array_values($userEmails), array_values($adminEmails), 'comment-notification', 'New comments | '.$project['ProjectFile']['title'].' from '.$this->request->data['client'])) {
				echo '{"success":{"text":"Notification sent successfully"}}';
				exit();
			} else {
				echo '{"error":{"text":"Some error occurs please try again later"}}';
				exit();
			}
			*/

			if($this->Email->sendemail2($var, array_values($userEmails), array_values($adminEmails), 'comment-notification', 'New comments | '.$project['ProjectFile']['title'].' from '.$this->request->data['client'])) {
				echo '{"success":{"text":"Notification sent successfully"}}';
				exit();
			} else {
				echo '{"error":{"text":"Some error occurs please try again later"}}';
				exit();
			}

		}
	}
}