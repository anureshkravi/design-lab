<?php
App::uses('Controller', 'Controller');
App::uses('Folder', 'Utility');
class AppController extends Controller {
	public $components = array(
								'Session','Cookie','Email',
								'Auth' => array(
									'authError' => '',
									'authorize' => 'Controller',
									'authenticate' => array(
										'Form' => array(
											'scope' => array('User.status_id' => 1)
										)
									)
								)
						 );
						 
						 


	public function beforeFilter() {
		
		if( isset($this->params['admin']) && $this->params['admin'] ){
			$this->layout = 'default';
			$this->Auth->allow('admin_login');
		} else {
			if($this->params->controller == 'projectfiles'){
				$this->layout = 'frontend';
				$this->Auth->allow();
			} else if($this->name == 'CakeError') {
				$this->layout = 'error';
			} else {
				$this->layout = 'designlab';
				$this->Auth->allow();
			}
		}
	}

	public function isAuthorized( $user = null ) {
		if ( isset($user['role_id']) && $user['role_id'] == 1 ) {
			return true;
		} elseif( isset($user['role_id']) && $user['role_id'] != 1 ) {
			$currentPermission = $this->Session->read('permission');
			if($currentPermission['privilege']) {
				foreach($currentPermission['privilege'] as $permission) {
					if((($permission['Privilege']['controller'] == strtolower($this->params['controller'])) && (($permission['Privilege']['action'] == strtolower($this->params['action'])) || ($permission[ 'Privilege' ][ 'action'] == 'allactions')))) {
						if(isset($this->params->projectfolder)) {
							foreach($currentPermission['projects'] as $project) {
								if($project['UserProject']['project_name'] == $this->params->projectfolder || $project['UserProject']['project_name'] == 'all') {
									return true;
								}
							}
							return false;
						}
						return true;
					}
				}
			} else {
				if(isset($this->params->projectfolder)) {
					foreach($currentPermission['projects'] as $project) {
						if($project['UserProject']['project_name'] == $this->params->projectfolder || $project['UserProject']['project_name'] == 'all') {
							return true;
						}
					}
					return false;
				}
			}
		}
		return false;
    }

	public function beforeRender() {
		$this->response->disableCache();
	}
}
