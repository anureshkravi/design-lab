<?php
App::uses('AppController', 'Controller');
class UsersController extends AppController {

	public $components = array('Paginator');
	public $uses = array('User', 'ProjectFile', 'UserProject', 'Privilege', 'Thumbnail', 'Category');

	public function admin_login() {
		if( $this->Auth->loggedIn() ){
			$this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
		} else {
			if( $this->request->is('post') ) {
				if($this->Auth->login()) {
					//==setting sessions for permissions of controller and actions==//
					$permission['privilege'] = $this->Privilege->find('all', array('fields' => array('controller', 'action'),'conditions' => array('Privilege.role_id' => $this->Auth->user('role_id'), 'Privilege.status_id' => 1 )));

					//==setting sessions for permissions of projects==//
					$permission['projects'] = $this->UserProject->find('all', array('conditions' => array('user_id' => $this->Auth->user('id')), 'fields' => 'project_name'));

            		if($permission) {
						$this->Session->write('permission' , $permission);
					}
					$this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
				} else {
					$this->Session->setFlash(__('Email or Password is incorrect'), 'default', array(), 'auth');
					$this->set('title_for_layout', 'Login');
					$this->layout = 'login';
				}
			} else {
				$this->set('title_for_layout', 'Login');
				$this->layout = 'login';
			}
		}
	}
	
	public function admin_logout() {
		$this->redirect($this->Auth->logout());
	}

	public function admin_setstatus($id = null, $status = null) {
		$this->layout = 'ajax';
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->User->save(array('status_id' => $status) );
		echo ($status);
        exit;
	}

	public function admin_index() {
		$this->User->recursive = 0;
		$users = $this->User->find('all');
		$this->set('users', $users);
	}

	public function admin_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->request->data['User']['status_id'] = 1; //active
			$this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				//saving user projects to user_projects table
				foreach($this->request->data['projects'] as $project) {
					$this->request->data['UserProject']['project_name'] = $project;
					$this->request->data['UserProject']['user_id'] = $this->User->id;
					$data[] = $this->request->data['UserProject'];
				}
				if ($this->UserProject->saveAll($data)) {
					$this->Session->setFlash(__('The user has been saved.'), 'default', array('class' => 'alert alert-success'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->request->data['User']['password'] = '';
					$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
				}
			} else {
				$this->request->data['User']['password'] = '';
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		
		$projects = $this->ProjectFile->find('all', array('conditions' => array('ProjectFile.status_id' => array(1,2)), 'fields' => 'DISTINCT title, project_folder'));

		// --------------- For thumnail data retrevial ------------

		//$projectFiles = $this->ProjectFile->query("SELECT * FROM ( SELECT id,title,modified,project_folder,status_id,created,recordListingID FROM `project_files` WHERE status_id IN(1,2) ORDER BY modified DESC ) AS ProjectFile GROUP BY title ORDER BY modified DESC LIMIT 20;");

		$stack = array();

		foreach($projects as $key => $value){

			//$value['ProjectFile']['title']

			$thumbnail = $this->Thumbnail->findByProjectName($value['ProjectFile']['title']);
			if($thumbnail){
				//$projects[$key]['ProjectFile']['thumbnail'] = $thumbnail['Thumbnail']['filename'];
				$tobj = array('title' => $value['ProjectFile']['title'], 'filename' => $thumbnail['Thumbnail']['filename'], 'projfolder' => $value['ProjectFile']['project_folder']);
				array_push($stack, $tobj);
			} else {
				//$projects[$key]['ProjectFile']['thumbnail'] = '';
				$tobj = array('title' => $value['ProjectFile']['title'], 'filename' => '', 'projfolder' => $value['ProjectFile']['project_folder']);
				array_push($stack, $tobj);
			}
		}
		// --------------------------------------------------------

		$roles = $this->User->Role->find('list', array('conditions' => array('status_id' => 1)));
		$this->set(compact('statuses', 'roles', 'projects', 'stack'));
	}

	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				//deleting all the previous saved projects for this user
				$this->UserProject->deleteAll(array('user_id' => $id));
				//saving user projects to user_projects table
				foreach($this->request->data['projects'] as $project) {
					$this->request->data['UserProject']['project_name'] = $project;
					$this->request->data['UserProject']['user_id'] = $this->User->id;
					$data[] = $this->request->data['UserProject'];
				}
				if ($this->UserProject->saveAll($data)) {
					$this->Session->setFlash(__('The user has been saved.'), 'default', array('class' => 'alert alert-success'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->request->data['User']['password'] = '';
					$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
				}
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		$projects = $this->ProjectFile->find('all', array('conditions' => array('ProjectFile.status_id' => array(1,2)), 'fields' => 'DISTINCT title, project_folder'));

		// --------------------------------------------------
		// For Retreving thumbnail data 
		// --------------------------------------------------

		$stacklist = array();

		foreach($projects as $key => $value) {
			$thumbnail = $this->Thumbnail->findByProjectName($value['ProjectFile']['title']);
			if($thumbnail){				
				$tobj = array('title' => $value['ProjectFile']['title'], 'filename' => $thumbnail['Thumbnail']['filename'], 'projfolder' => $value['ProjectFile']['project_folder']);
				array_push($stacklist, $tobj);
			} else {				
				$tobj = array('title' => $value['ProjectFile']['title'], 'filename' => '', 'projfolder' => $value['ProjectFile']['project_folder']);
				array_push($stacklist, $tobj);
			}
		}

		$customUserProjects = $this->request->data['UserProject'];
		// --------------------------------------------------------

		$roles = $this->User->Role->find('list', array('conditions' => array('status_id' => 1)));
		$this->set(compact('statuses', 'roles', 'projects', 'stacklist', 'customUserProjects'));
	}

	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		//$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function admin_change_password() {
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $this->Auth->user('id')));
		$user = $this->User->find('first', $options);

		if($this->request->is('post') || $this->request->is('put')) { 	   
			$this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);								
			if($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Password Change Successfully'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'change_password'));
			} else {
				$this->Session->setFlash(__('Password could not be saved. Please try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$user['User']['password'] = '';
			$this->request->data = $user;
		}
	}
}
