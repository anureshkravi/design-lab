<?php
	//App::uses('CakeEmail', 'Network/Email');
	require_once 'PHPMailer/PHPMailerAutoload.php';

	class EmailComponent extends Component
	{
		public function sendemail($vars=array(), $emails=array(), $bcc=array(), $template, $subject) {
			//loading cake email and sending email
			
			$Email  = new CakeEmail();
			$Email->config(array('transport' => 'debug', 'host' => 'smtp.office365.com', 'port' => 25, 'username' => 'noreply@espire.com', 'password' => '1nd1a#3241'));
			//$Email->config('smtp');
			$Email->viewVars($vars);				
			$Email->template($template, 'default');
			$Email->emailFormat('html');
			$Email->from(array('noreply@espire.com' => SITE_NAME));
			$Email->to($emails);
			$Email->bcc($bcc);
			$Email->subject($subject);
			//var_dump($template);die;
			//$res = $Email->send();
			//echo true;
			if($Email->send()) {
				echo true;
			}
		}

		public function sendemail2($vars=array(), $emails=array(), $bcc=array(), $template, $subject) 
		{	
			$mail = new PHPMailer;
			$host = 'smtp.office365.com';
			$username = 'noreply@espire.com';
			$fromPswd = '1nd1a#3241';
			$port = 25;
			
			$clientname = $vars['clientName'];
			$projectname = $vars['projectName'];
			$projectlink = $vars['link'];

	        $mail->isSMTP();
	        $mail->Host = $host;               		// Specify main and backup SMTP servers
	        //$mail->SMTPDebug = 2;
	        $mail->Port = $port;               		// TCP port to connect to
	        $mail->SMTPSecure = false;         		// Enable TLS encryption, `ssl` also accepted
	        $mail->SMTPAuth = true;            		// Enable SMTP authentication
	        $mail->Username = $username;       		// SMTP username
	        $mail->Password = $fromPswd;       		// SMTP password
	        $mail->IsHTML(true);

	        $emailslength = count($emails);
	        $sendStatusArr = array();

	        for($i=0; $i<$emailslength; $i++)
	        {
	        	$mail->ClearAddresses();				
	            $address_to = $emails[$i];
		        $mail->setFrom($username, 'Espire Design Lab');		        
		        $mail->addAddress($address_to);   		// Add a recipient
		     
		        $bodyContent = "<p>Dear user,</p>
		        <p>You have received new comments from $clientname on $projectname.</p>
				<p>Follow this <a href='$projectlink'>link</a> to view comments.</p>
				<p>Regards,<br>
				Espire Design Lab Team</p>
				<p>Note: This is an auto generated mail, please do not reply to this email.</p>";

		        //Set the subject line
		        $mail->Subject = $subject;		        
		        $mail->Body    = $bodyContent;
		        $mail->IsHTML(true);

		        //var_dump($mail);die;

		        //send the message, check for errors
		        if (!$mail->send()) {
		        	array_push($sendStatusArr, "false");
		        	//return false;  			// Return will work only with PHP Mailer	                
	                //echo "Mailer Error: " . $mail->ErrorInfo;	                	                
		        } else {
	                //echo true;
	                array_push($sendStatusArr, "true");
	                //return true;  			// Return will work only with PHP Mailer	                
		        }
	        
		        $sendStatusArrLen = count($sendStatusArr);		        
		        $send_result = true;

		        if($sendStatusArrLen == $emailslength)
		        {
		        	for($k=0; $k<$sendStatusArrLen; $k++)
		        	{
						$ss_val = $sendStatusArrLen[$k];	        			
						if($ss_val == "false"){
							$send_result = false;
							break;	
						}
		        	}

		        	if(!$send_result){
		        		return false;
		        	} else {
		        		return true;
		        	}
		        }		        
	    	}
		}


	}	
?>