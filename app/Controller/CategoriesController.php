<?php
App::uses('AppController', 'Controller');

class CategoriesController extends AppController {

	public $components = array('Paginator');

	public function admin_index() {
		$this->Category->recursive = 0;
		$categories = $this->Category->find('all', array('conditions' => array('status_id' => array(1,2))));
		$this->set('categories', $categories);
	}

	/*public function admin_view($id = null) {
		if (!$this->Category->exists($id)) {
			throw new NotFoundException(__('Invalid category'));
		}
		$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
		$this->set('category', $this->Category->find('first', $options));
	}*/

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->request->data['Category']['status_id'] = 1; //active
			$this->request->data['Category']['category_folder'] = str_replace(' ', '-', strtolower($this->request->data['Category']['title']));
			$this->Category->create();
			if ($this->Category->save($this->request->data)) {
				$this->Session->setFlash(__('The category has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The category could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$statuses = $this->Category->Status->find('list', array('conditions' => array('id' => array(1,2))));
		$this->set(compact('statuses'));
	}

	/*public function admin_edit($id = null) {
		if (!$this->Category->exists($id)) {
			throw new NotFoundException(__('Invalid category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Category']['category_folder'] = str_replace(' ', '-', strtolower($this->request->data['Category']['title']));
			if ($this->Category->save($this->request->data)) {
				$this->Session->setFlash(__('The category has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The category could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
			$this->request->data = $this->Category->find('first', $options);
		}
		$statuses = $this->Category->Status->find('list', array('conditions' => array('id' => array(1,2))));
		$this->set(compact('statuses'));
	}

	public function admin_delete($id = null) {
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			throw new NotFoundException(__('Invalid category'));
		}
		if ($this->Category->delete()) {
			$this->Session->setFlash(__('The category has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The category could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function admin_setstatus($id = null, $status = null) {
		$this->layout = 'ajax';
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			throw new NotFoundException(__('Invalid Category'));
		}
		$this->Category->save(array('status_id' => $status));
		echo ($status);
        exit;
	}*/
}
