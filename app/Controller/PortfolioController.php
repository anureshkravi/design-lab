<?php
App::uses('AppController', 'Controller');

class PortfolioController extends AppController {
	
	public $uses = array('ProjectFile', 'Category', 'Tag');
	public $components = array('Auth');
	public function index(){
	
		  if (!$this->Auth->login()) {
         return  $this->redirect(array('controller'=>'users','action'=>'login','admin'=>true)); exit;
        }
		$projects = $this->ProjectFile->find('all', array('recursive' => -1, 'conditions' => array('ProjectFile.status_id' => 1), 'order' => array('recordListingID' => 'ASC', 'modified' => 'DESC')));
		$tags = $this->Tag->find('all', array('fields' => array('DISTINCT title')));
		$this->set(compact('projects', 'tags'));
	}

	public function view($tag=null) {
        $projectIds = $this->Tag->find('list', array('conditions' => array('title' => $tag), 'fields' => array('id', 'project_id')));
        foreach($projectIds as $value){
        	$project = $this->ProjectFile->find('first', array('recursive' => -1, 'conditions' => array('ProjectFile.id' => $value, 'ProjectFile.status_id' => 1)));
        	if(!empty($project)){
        		$projects[$project['ProjectFile']['recordListingID']] = $project;
        	}
        }
        ksort($projects);
        $tags = $this->Tag->find('all', array('fields' => array('DISTINCT title')));
        $this->set(compact('projects', 'tags'));
    }

    public function viewdetail(){
        $project = $this->ProjectFile->find('first', array('conditions' => array('ProjectFile.id' => $this->params->pass[1], 'ProjectFile.status_id' => 1)));
        $tags = $this->Tag->find('all', array('fields' => array('DISTINCT title')));
        $this->set(compact('project', 'tags'));
    }

    public function preview(){
        $this->Category->recursive = -1;
        $category = $this->Category->find('first', array('conditions' => array('category_folder' => $this->params->categoryfolder)));
        
        $this->ProjectFile->recursive = 0;
        $projectfiles = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $this->params->projectfolder, 'category_id' => $category['Category']['id'], 'date_folder' => $this->params->datefolder, 'ProjectFile.status_id' => 1)));

        if(!empty($projectfiles)){
            $path = 'files/'.$this->params->projectfolder.'/'.$this->params->categoryfolder.'/'.$this->params->datefolder;
            $dir = new Folder($path);
            $files = $dir->read(true, array('files', ''));

            $html = '';
            $gallery = '';
            foreach($files[1] as $value){
                if(substr(strrchr($value,'.'),1) == 'html'){
                    $html = 'html';
                } else {
                    $gallery = 'gallery';
                }
            }
            if($html != ''){
                $this->set('projectfiles', $projectfiles);
                $this->layout = 'html';
                $this->render('/projectfiles/admin_htmlfiles');
            } else {
                $this->set(compact('files', 'path', 'projectfiles'));
                $this->layout = 'gallery';
                $this->render('/projectfiles/admin_gallery');
            }
        }        
    }
}
