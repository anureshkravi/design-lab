<?php
App::uses('AppController', 'Controller');

class LinkedImagesController extends AppController {

	public $uses = array('LinkedImage');

	//Getting linked image data
	public function view($projectId, $thumbId, $cookieId, $linkedProjectFolder) {
		$allLinkedImageData = array();
		$allLinkedImageData = $this->LinkedImage->find('all', array('conditions'=>array('project_id' => $projectId, 'thumb_id' => $thumbId, 'status_id' => 1)));
		foreach($allLinkedImageData as $key=>$data){
			$allLinkedImageData[$key]['LinkedImage']['time'] = date('h:i A', strtotime($data['LinkedImage']['created']));
			$allLinkedImageData[$key]['LinkedImage']['date'] = date('M d, Y', strtotime($data['LinkedImage']['created']));
		}		
		echo json_encode($allLinkedImageData);		
		exit;
	}


	//for adding link to the images
	public function addLink() {
		
		if($this->Auth->user('role_id') == 1 || $this->Auth->user('role_id') == 2) {
			if ($this->request->is('post')) {
				$this->request->data = $this->request->input('json_decode');
				//___ Push the Name and Username from backend because thi is not comming from front end.____
				$this->request->data->name = $this->Auth->user('name');
				$this->request->data->email = $this->Auth->user('username');
				//______________________________________________________
				
				if(isset($this->request->data->update_key) && !empty($this->request->data->update_key) && $this->request->data->update_key>0){ // Update
						$this->LinkedImage->id = $this->request->data->update_key;
						if ($this->LinkedImage->save($this->request->data)) {
							$lastSavedRecord = $this->LinkedImage->findById($this->LinkedImage->id);
							$lastSavedRecord['LinkedImage']['time'] = date('h:i A', strtotime($lastSavedRecord['LinkedImage']['created']));
							$lastSavedRecord['LinkedImage']['date'] = date('M d, Y', strtotime($lastSavedRecord['LinkedImage']['created']));
							echo json_encode($lastSavedRecord);
							exit();
						}
				} else{ // save
					$this->LinkedImage->create();
					if ($this->LinkedImage->save($this->request->data)) {
						$lastSavedRecord = $this->LinkedImage->findById($this->LinkedImage->id);
						$lastSavedRecord['LinkedImage']['time'] = date('h:i A', strtotime($lastSavedRecord['LinkedImage']['created']));
						$lastSavedRecord['LinkedImage']['date'] = date('M d, Y', strtotime($lastSavedRecord['LinkedImage']['created']));
						echo json_encode($lastSavedRecord);
						exit();
					}
				}
			}
		} else{
			echo 'You dont have permission to add image link';
			exit();
		}
	}

	//deleting Hotspot only by admin
	public function admin_delete($hotspot_id = NULL) {
		if($hotspot_id > 0){
			if($this->Auth->user('role_id') == 1) {
				$this->request->onlyAllow('post', 'delete');
				$this->Comment->recursive = -1;
				if( $this->LinkedImage->delete($hotspot_id)) {
					echo "success: Hotspot deleted";
					exit();
				} else {
					echo 'Fail: Some error occurs please try again later';
					exit();
				}
			} else {
				echo 'Fail: You dont have permission to delete description';
				exit();
			}
		} else{
			echo 'Fail: Hotspot is empty.';
			exit();
		}
	}
}