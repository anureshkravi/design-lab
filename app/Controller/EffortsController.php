<?php
App::uses('AppController', 'Controller');

class EffortsController extends AppController {

	public $components = array('Paginator');

	public function admin_index() {
		$this->Effort->recursive = 0;
		$this->set('efforts', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->Effort->exists($id)) {
			throw new NotFoundException(__('Invalid effort'));
		}
		$options = array('conditions' => array('Effort.' . $this->Effort->primaryKey => $id));
		$this->set('effort', $this->Effort->find('first', $options));
	}

	public function admin_add() {
		$this->autoRender = false;
		if ($this->request->is('post')) {
			$ticket = $this->Effort->Ticket->findById($this->request->data['Effort']['ticket_id']);
			if($ticket['Ticket']['status_id'] == 10) {
				$this->Effort->create();
				if ($this->Effort->save($this->request->data)) {				
					//updating time spent on the ticket
					$ticket['Ticket']['time_spent'] += $this->request->data['Effort']['hours'];
					$this->Effort->Ticket->save($ticket);

					//returning saved effort
					$this->Effort->recursive = 0;
					$effort = $this->Effort->find('first', array('conditions' => array('Effort.id' => $this->Effort->id)));
					$effort['Effort']['created'] = date( 'M d, Y', strtotime( $effort['Effort']['created'] ) );
					return json_encode($effort);
				} else {
					return json_encode(array('error' => 'The effort could not be saved. Please, try again.'));
				}
			} else {
				return json_encode(array('error' => 'Please start the task first and then update the efforts'));
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$this->Effort->exists($id)) {
			throw new NotFoundException(__('Invalid effort'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Effort->save($this->request->data)) {
				$this->Session->setFlash(__('The effort has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The effort could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Effort.' . $this->Effort->primaryKey => $id));
			$this->request->data = $this->Effort->find('first', $options);
		}
		$tickets = $this->Effort->Ticket->find('list');
		$users = $this->Effort->User->find('list');
		$this->set(compact('tickets', 'users'));
	}

	public function admin_delete($id = null) {
		$this->autoRender = false;
		$this->request->onlyAllow('post', 'delete');
		$effort = $this->Effort->findById($id);
		if($effort && ($this->Auth->user('id') == 1 || $effort['Effort']['user_id'] == $this->Auth->user('id'))) {
			if ($this->Effort->delete($effort['Effort']['id'])) {
				$ticket = $this->Effort->Ticket->findById($effort['Effort']['ticket_id']);
				$ticket['Ticket']['time_spent'] -= $effort['Effort']['hours'];
				if($this->Effort->Ticket->save($ticket)) {
					return json_encode($effort);
				} else {
					return json_encode(array('error' => 'Effort deleted but ticket timespent not updated.'));
				}
			} else {
				return json_encode(array('error' => 'Effort could not be deleted. Please, try again.'));
			}
		} else {
			return json_encode(array('error' => 'You dont have the privelege to delete the efforts of other team member.'));
		}
	}
}
