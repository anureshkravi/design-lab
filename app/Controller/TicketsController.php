<?php
App::uses('AppController', 'Controller');

class TicketsController extends AppController {

	public $components = array('Paginator');
	public $uses = array('Ticket', 'User', 'Tag');

	public function add() {
		if ($this->request->is('post')) {
			$this->request->data['Ticket']['requested_time'] = $this->request->data['Ticket']['requested_time'] * $this->request->data['Ticket']['time'];
			$this->request->data['Ticket']['project_folder'] = str_replace(' ', '-', strtolower($this->request->data['Ticket']['project']));
			$this->request->data['Ticket']['project_description'] = nl2br($this->request->data['Ticket']['project_description']);
			$this->request->data['Ticket']['status_id'] = 6; //not approved
			$this->Ticket->create();
			if ($this->Ticket->save($this->request->data)) {
				$this->Ticket->User->recursive = -1;
				$adminNames = $this->Ticket->User->find('list', array('conditions' => array('User.role_id' => 1, 'User.status_id' => 1), 'fields' => array('id', 'name')));
				$adminEmails = $this->Ticket->User->find('list', array('conditions' => array('User.role_id' => 1, 'User.status_id' => 1)));

				$var = array('admin' => reset($adminNames), 'user' => $this->request->data['Ticket']['name'], 'link' => 'http://designlab.espire.com/admin/tickets');

				if($this->Email->sendemail($var, array_values($adminEmails), array(), 'new-ticket', 'New Request | '.$this->request->data['Ticket']['project'].' from '.$this->request->data['Ticket']['name'])) {

					$this->Session->setFlash(__('Thank you your information has been sent successfully.'), 'default', array('class' => 'alert alert-success'));
					return $this->redirect(array('action' => 'add'));
				}
			} else {
				$this->Session->setFlash(__('Some error occurs please try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$tags = $this->Tag->find('all', array('fields' => array('DISTINCT title')));
		$users = $this->Ticket->User->find('list');
		$statuses = $this->Ticket->Status->find('list');
		$this->set(compact('users', 'statuses', 'tags'));
	}

	public function admin_index() {
		if($this->Auth->user('role_id') == 1) {
			$this->Ticket->User->recursive = -1;
			$this->Ticket->recursive = 1;

			$users = $this->User->find('list', array('conditions' => array('role_id' => 2, 'status_id' => 1)));

			$data = $this->Ticket->find('all', array('conditions' => array('Ticket.status_id' => array(5,6)), 'fields' => array('id', 'name', 'email', 'project', 'project_folder', 'project_description', 'requested_time', 'billable', 'filename', 'status_id'), 'order' => array('Ticket.modified' => 'DESC')));
			$newtickets = array();
			foreach($data as $value) {
				$newtickets[$value['Ticket']['name']][] = $value;
			}
			$this->set(compact('newtickets', 'users'));
		}
	}

	public function admin_tickets() {
		$this->autoRender = false;
		$this->Ticket->User->recursive = -1;
		$this->Ticket->recursive = 1;
		$per_page = isset($this->request->query['per_page']) ? $this->request->query['per_page'] : 10;

		$users = $this->User->find('list', array('conditions' => array('role_id' => 2, 'status_id' => 1)));
		if($this->Auth->user('role_id') == 1) {
			$options = array('Ticket.status_id' => array(7,8,9,10,11));

		} else {
			$options = array('Ticket.status_id' => array(7,8,9,10,11), 'assigned_id' => $this->Auth->user('id'));
		}
		$tickets = $this->Ticket->find('all', array('conditions' => $options, 'order' => array('Ticket.modified' => 'DESC'), 'limit' => $per_page, 'offset' => 0));
		return json_encode(array('tickets' => $tickets, 'users' => $users, 'totalCount' => $this->Ticket->find('count', array('conditions' => $options)), 'role' => $this->Auth->user('role_id')));
	}

	public function admin_scroll() {
		$this->autoRender = false;
		$group_number = isset($this->request->query['group']) ? $this->request->query['group'] : 0;
		$per_page = isset($this->request->query['per_page']) ? $this->request->query['per_page'] : 10;
		$offset = ($group_number * $per_page);

		$this->Ticket->recursive = 1;
		if($this->Auth->user('role_id') == 1) {
			$tickets = $this->Ticket->find('all', array('conditions' => array('Ticket.status_id' => array(7,8,9,10,11)), 'order' => array('Ticket.modified' => 'DESC'), 'limit' => $per_page, 'offset' => $offset));
		} else {
			$tickets = $this->Ticket->find('all', array('conditions' => array('Ticket.status_id' => array(7,8,9,10,11), 'assigned_id' => $this->Auth->user('id')), 'order' => array('Ticket.modified' => 'DESC'), 'limit' => $per_page, 'offset' => $offset));
		}
		return json_encode($tickets);
	}

	public function admin_view($id = null) {
		if (!$this->Ticket->exists($id)) {
			throw new NotFoundException(__('Invalid ticket'));
		}
		$options = array('conditions' => array('Ticket.' . $this->Ticket->primaryKey => $id));
		$this->set('ticket', $this->Ticket->find('first', $options));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Ticket->create();
			if ($this->Ticket->save($this->request->data)) {
				$this->Session->setFlash(__('The ticket has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ticket could not be saved. Please, try again.'));
			}
		}
		$assigned = $this->Ticket->Assigned->find('list');
		$users = $this->Ticket->User->find('list');
		$statuses = $this->Ticket->Status->find('list');
		$this->set(compact('users', 'statuses', 'assigned'));
	}

	public function admin_edit($id = null) {
		if (!$this->Ticket->exists($id)) {
			throw new NotFoundException(__('Invalid ticket'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Ticket->save($this->request->data)) {
				$this->Session->setFlash(__('The ticket has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ticket could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Ticket.' . $this->Ticket->primaryKey => $id));
			$this->request->data = $this->Ticket->find('first', $options);
		}
		$assigned = $this->Ticket->Assigned->find('list');
		$users = $this->Ticket->User->find('list');
		$statuses = $this->Ticket->Status->find('list');
		$this->set(compact('users', 'statuses', 'assigned'));
	}

	public function admin_delete($id = null) {
		$this->autoRender = false;
		if($this->Auth->user('role_id') == 1) {
			$this->Ticket->id = $id;
			if (!$this->Ticket->exists()) {
				return json_encode(array('error' => 'Invalid Ticket'));
			}
			$this->request->onlyAllow('post', 'delete');
			if (!$this->Ticket->delete()) {
				return json_encode(array('error' => 'Ticket could not be deleted. Please, try again.'));
			}
			return true;
		} else {
			return json_encode(array('error' => 'You dont have permission to delete any ticket.'));
		}
	}

	public function admin_approve() {
		$this->autoRender = false;
		$this->Ticket->recursive = -1;
		$ticket = $this->Ticket->findById($this->request->data['Ticket']['id']);
		$ticket['Ticket']['status_id'] = $this->request->data['Ticket']['status_id'];
		if ($this->request->is('post')) {
			if($this->Ticket->save($ticket)) {
				$this->User->recursive = -1;
				$users = $this->User->find('list', array('conditions' => array('role_id' => 2, 'status_id' => 1)));				
				return json_encode(array('users' => $users, 'ticket' => $ticket));
			} else {
				return json_encode(array('error' => 'Some error occurs!!'));
			}
		}
	}

	public function admin_assign() {
		$this->autoRender = false;
		if ($this->request->is('post')) {
			//finding and saving ticket
			$ticket = $this->Ticket->find('first', array('conditions' => array('Ticket.id' => $this->request->data['Ticket']['id'])));

			//checking whether ticket has already been assigned to any team member or not
			$reassign = ($ticket['Ticket']['assigned_id'] == 0) ? false : true;

			$ticket['Ticket']['user_id'] = $this->Auth->user('id');
			$ticket['Ticket']['assigned_id'] = $this->request->data['Ticket']['assigned_id'];
			$ticket['Ticket']['status_id'] = $this->request->data['Ticket']['status_id'];
			$ticket['Ticket']['start_date'] = isset($this->request->data['Ticket']['start_date']) ? $this->request->data['Ticket']['start_date'] : $ticket['Ticket']['start_date'];

			if($this->Ticket->save($ticket)) {
				$assignedticket = $this->Ticket->find('first', array('conditions' => array('Ticket.id' => $this->Ticket->id)));

				//sending email to the assigned team member
				$assignedVar = array('user' => $assignedticket['Assigned']['name']);
				$assignedMail = $this->Email->sendemail($assignedVar, array(0 => $assignedticket['Assigned']['username']), array(), 'assign-ticket', 'New Ticket | '.$assignedticket['Ticket']['project']);

				//sending email to the manager if the ticket is not already assigned
				//will not send incase of re-assigning
				if($reassign === false) {
					$approvedVar = array('manager' => $assignedticket['Ticket']['name'], 'project' => $assignedticket['Ticket']['project'], 'userEmail' => $assignedticket['Assigned']['username'], 'userName' => $assignedticket['Assigned']['name'], 'startDate' => $assignedticket['Ticket']['start_date']);
					$approvedMail = $this->Email->sendemail($approvedVar, array(0 => $assignedticket['Ticket']['email']), array(), 'approve-ticket', 'Request Approved | '.$assignedticket['Ticket']['project']);
					if($assignedMail && $approvedMail) {
						return json_encode($assignedticket);
					}
				} else {
					if($assignedMail) {
						return json_encode($assignedticket);
					}
				}
			} else {
				return json_encode(array('error' => 'Some error occurs!!'));
			}
		}
	}

	public function admin_reassign() {
		$this->autoRender = false;
		$this->Ticket->recursive = -1;
		$this->User->recursive = -1;
		$ticket = $this->Ticket->findById($this->request->data['Ticket']['id']);		
		$users = $this->User->find('list', array('conditions' => array('role_id' => 2, 'status_id' => 1)));
		return json_encode(array('users' => $users, 'ticket' => $ticket));
	}

	public function admin_decline() {
		$this->autoRender = false;
		if ($this->request->is('post')) {
			//for saving mail body in mysql
			$mail = preg_replace('/\n(\s*\n)+/', '</p><p>', $this->request->data['Ticket']['mail']);
			$mail = preg_replace('/\n/', '<br>', $mail);
			$this->request->data['Ticket']['mail'] = '<p>'.$mail.'</p>';

			$this->request->data['Ticket']['user_id'] = $this->Auth->user('id');
			$this->request->data['Ticket']['assigned_id'] = 0;
			if($this->Ticket->save($this->request->data)) {
				$ticket = $this->Ticket->findById($this->Ticket->id);
				$vars = array('message' => $ticket['Ticket']['mail'], 'manager' => $ticket['Ticket']['name']);
				if($this->Email->sendemail($vars, array(0 => $ticket['Ticket']['email']), array(), 'decline-ticket', 'Re: New Request | '.$ticket['Ticket']['project'])) {
					return json_encode($ticket);
				}
			} else {
				return json_encode(array('error' => 'Some error occurs!!'));
			}
		}
	}

	public function admin_completed() {
		$this->autoRender = false;
		if ($this->request->is('post')) {
			if($this->Ticket->save($this->request->data)) {
				return json_encode($this->Ticket->find('first', array('conditions' => array('Ticket.id' => $this->Ticket->id))));
			} else {
				return json_encode(array('error' => 'Some error occurs!!'));
			}
		}
	}

	public function admin_start() {
		$this->autoRender = false;
		if ($this->request->is('post')) {
			if($this->Ticket->save($this->request->data)) {
				return json_encode($this->Ticket->find('first', array('conditions' => array('Ticket.id' => $this->Ticket->id))));
			} else {
				return json_encode(array('error' => 'Some error occurs!!'));
			}
		}
	}

	public function admin_search($keyword=null) {
		$this->autoRender = false;
		if($this->Auth->user('role_id') == 1) {
			$this->Ticket->User->recursive = -1;
			$findUsers = $this->Ticket->User->find('list', array('conditions' => array('OR' => array('User.username LIKE' => '%'.$keyword.'%'), 'User.role_id' => 2, 'User.status_id' => 1)));

			if($findUsers) {
				$users = implode(',', array_keys($findUsers));
				$tickets = $this->Ticket->find('all', array('conditions' => array('OR' => array('Ticket.project LIKE' => '%'.$keyword.'%', 'Ticket.name LIKE' => '%'.$keyword.'%', 'Ticket.assigned_id' => array($users)), 'Ticket.status_id' => array(7,8,9,10,11)), 'order' => array('Ticket.modified' => 'DESC')));
			} else {
				$tickets = $this->Ticket->find('all', array('conditions' => array('OR' => array('Ticket.project LIKE' => '%'.$keyword.'%', 'Ticket.name LIKE' => '%'.$keyword.'%'), 'Ticket.status_id' => array(7,8,9,10,11)), 'order' => array('Ticket.modified' => 'DESC')));
			}
		} else {
			$tickets = $this->Ticket->find('all', array('conditions' => array('OR' => array('Ticket.project LIKE' => '%'.$keyword.'%', 'Ticket.name LIKE' => '%'.$keyword.'%'), 'Ticket.status_id' => array(7,8,9,10,11), 'Ticket.assigned_id' => $this->Auth->user('id')), 'order' => array('Ticket.modified' => 'DESC')));
		}
		return json_encode($tickets);
	}

	public function admin_usersList() {
		$this->autoRender = false;
		$this->Ticket->User->recursive = -1;
		$users = $this->Ticket->User->find('list', array('conditions' => array('status_id' => 1, 'role_id' => 2)));
		return json_encode($users);
	}
}
