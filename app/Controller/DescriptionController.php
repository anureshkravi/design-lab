<?php
App::uses('AppController', 'Controller');

class DescriptionController extends AppController {

	public $uses = array('Description');

	//for plotting comments count on different locations
	public function view($projectId, $thumbId, $cookieId) {
		$allDescriptionData = array();
		$allDescriptionData = $this->Description->find('all', array('conditions'=>array('project_id' => $projectId, 'thumb_id' => $thumbId, 'status_id' => 1)));
		foreach($allDescriptionData as $key=>$data){
			$allDescriptionData[$key]['Description']['time'] = date('h:i A', strtotime($data['Description']['created']));
			$allDescriptionData[$key]['Description']['date'] = date('M d, Y', strtotime($data['Description']['created']));
		}
		
		
		/*foreach($comments as $key => $comment) {
			$comments[$key]['Description']['classname'] = 'unread';
			if($this->CommentView->find('first', array('conditions' => array('comment_id' => $comment['Comment']['id'], 'cookie_id' => $cookieId)))) {
				$comments[$key]['Comment']['classname'] = '';
			}
			if($comment['Comment']['cookie_id'] == $cookieId) {
				$comments[$key]['Comment']['classname'] = '';
			}
		}

		$finalComment = [];
		if($comments) {
			foreach($comments as $comment){
				$finalComment[$comment['Comment']['x_axis']][$comment['Comment']['y_axis']][] = $comment['Comment'];
			}
		}*/
		echo json_encode($allDescriptionData);
		exit();
	}


	//for adding Description
	public function addDescription() {
		if ($this->request->is('post')) {
			$response = array();
			$this->request->data = $this->request->input('json_decode');
			//___ Push the Name and Username from backend because thi is not comming from front end.____
			$this->request->data->name = $this->Auth->user('name');
			$this->request->data->email = $this->Auth->user('username');
			//______________________________________________________
				
			if(isset($this->request->data->update_key) && !empty($this->request->data->update_key) && $this->request->data->update_key>0){ // Update
					$this->Description->id = $this->request->data->update_key;
					if ($this->Description->save($this->request->data)) {
						$lastSavedRecord = $this->Description->findById($this->Description->id);
						$lastSavedRecord['Description']['time'] = date('h:i A', strtotime($lastSavedRecord['Description']['created']));
						$lastSavedRecord['Description']['date'] = date('M d, Y', strtotime($lastSavedRecord['Description']['created']));
						echo json_encode($lastSavedRecord);
						exit();
					}
			} else{ // save
				$this->Description->create();
				if ($this->Description->save($this->request->data)) {
					$lastSavedRecord = $this->Description->findById($this->Description->id);
					$lastSavedRecord['Description']['time'] = date('h:i A', strtotime($lastSavedRecord['Description']['created']));
					$lastSavedRecord['Description']['date'] = date('M d, Y', strtotime($lastSavedRecord['Description']['created']));
					
					/*_________ GET coUNT ______*/
					$projectId = $this->request->data->project_id;
					$thumbId = $this->request->data->thumb_id;
					$allDescriptionDataCount = $this->Description->find('count', array('conditions'=>array('project_id' => $projectId, 'thumb_id' => $thumbId, 'status_id' => 1)));
					
					$response["currentRecord"] = $lastSavedRecord;
					$response["descriptionCount"] = $allDescriptionDataCount;
					//echo json_encode($lastSavedRecord);
					echo json_encode($response);
					exit();
				}
			}
		}
	}

	//deleting comment only by admin
	public function delete($description_id = NULL) {
		if($description_id > 0){
			if($this->Auth->user('role_id') == 1 || $this->Auth->user('role_id') == 2) {
				$this->request->onlyAllow('post', 'delete');
				//counting total no of comments
				//$this->Comment->recursive = -1;
				//$count = $this->Description->find('count', array('conditions' => array('project_id' => $projectId, 'thumb_id' => $thumbId, 'x_axis' => $x_axis, 'y_axis' => $y_axis)));
				//deleting all the comments
				if( $this->Description->delete($description_id)) {
					echo "success: Description deleted";
					exit();
				} else {
					echo 'Some error occurs please try again later';
					exit();
				}
			} else {
				echo 'You dont have permission to delete description';
				exit();
			}
		} else{
			echo 'Description id is empty.';
			exit();
		}
	}
	
	// Change Title
	public function admin_changeTitle() {

		if($this->Auth->user('role_id') == 1 || $this->Auth->user('role_id') == 2) {
			if ($this->request->is(array('post', 'put'))) {
				$DescriptioTitleOld = trim($this->request->data['Description']['oldtitle']);
				$DescriptioTitleNew = trim($this->request->data['Description']['title']);
				$projectId = trim($this->request->data['Description']['project_id']);
				$thumbId = trim($this->request->data['Description']['thumb_id']);
				//finding all the projects which needs to be updated with new title
				$this->Description->recursive = -1;
				$descriptionData = array();
				$descriptionData = $this->Description->find('all', array('conditions'=>array('project_id' => $projectId, 'thumb_id' => $thumbId, 'title' => $DescriptioTitleOld, 'status_id' => 1)));
				if(count($descriptionData)>0){
					foreach($descriptionData as $key => $descriptionRow) {
						$descriptionData[$key]['Description']['title'] = $DescriptioTitleNew;
					}
					if ($this->Description->saveAll($descriptionData)) {
						/*$this->Session->setFlash(__('Description title updated successfully'), 'default', array('class' => 'alert alert-success'));
						return $this->redirect($this->referer());
						*/
						return $this->view($projectId, $thumbId);
					} else {
						/*$this->Session->setFlash(__('The project could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
						return $this->redirect($this->referer());
						*/
						$response = array('status' => 'Fail','msg' => 'The project could not be saved. Please, try again.');
						echo json_encode($response,true);die;
					}
				} else{
					/*$this->Session->setFlash(__('Please add the description then change the title'), 'default', array('class' => 'alert alert-danger'));
					return $this->redirect($this->referer());
					*/
					$response = array('status' => 'Fail','msg' => 'Please add the description then change the title');
					echo json_encode($response,true);die;
				}
			}
		} else{
			$response = array('status' => 'Fail','msg' => 'You are not authorised to change the title');
			echo json_encode($response,true);die;
		}
	}
	

	//for sending email
	public function sendNotification() {
		
			//finding project
			$this->request->data['projectId'] = 984;
			$this->request->data['client'] = 'abc';
			$project = $this->ProjectFile->findById($this->request->data['projectId']);

			//finding email ids from comment table
			$userEmails = $this->Comment->find('list', array('conditions' => array('project_id' => $this->request->data['projectId'], 'status_id' => 1), 'fields' => array('id', 'email'), 'group' => array('email')));

			//unsetting if any key is empty
			foreach($userEmails as $key => $email) {
				if(empty($email)) {
					unset($userEmails[$key]);
				}
			}

			//adding project user if not in user emails
			if(($key = array_search($project['User']['username'], $userEmails)) === false) {
			    array_push($userEmails, $project['User']['username']);
			}

			//if excluded email comes then we will unset that from user emails
			if(isset($this->request->data['excludeEmail'])) {
				if(($key = array_search($this->request->data['excludeEmail'], $userEmails)) !== false) {
				    unset($userEmails[$key]);
				}
			}

			//finding emails of admin users
			$this->User->recursive = -1;
			$adminEmails = $this->User->find('list', array('conditions' => array('role_id' => 1, 'status_id' => 1), 'fields' => array('id', 'username')));

			//defining vars for email template
			$var = array('clientName' => $this->request->data['client'], 'projectName' => $project['ProjectFile']['title'], 'link' => 'http://designlab.espire.com/projectfiles/'.$project['ProjectFile']['project_folder'].'/'.$project['Category']['category_folder'].'/'.$project['ProjectFile']['date_folder']);

			//var_dump($this->request->data['projectId']); die;

			//var_dump($var);die;
			// if($this->Email->sendemail($var, array_values($userEmails), array_values($adminEmails), 'comment-notification', 'New comments | '.$project['ProjectFile']['title'].' from '.$this->request->data['client'])) {
			// 	echo '{"success":{"text":"Notification sent successfully"}}';
			// 	exit();
			// } else {
			// 	echo '{"error":{"text":"Some error occurs please try again later"}}';
			// 	exit();
			// }


			if($this->Email->sendemail2($var, array_values($userEmails), array_values($adminEmails), 'comment-notification', 'New comments | '.$project['ProjectFile']['title'].' from '.$this->request->data['client']))
			{
				echo '{"success":{"text":"Notification sent successfully"}}';
				exit();
			} else {
				echo '{"error":{"text":"Some error occurs please try again later"}}';
				exit();
			}
		
	}



}