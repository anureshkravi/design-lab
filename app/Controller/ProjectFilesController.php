<?php
App::uses('AppController', 'Controller');

class ProjectFilesController extends AppController {
	
	public $uses = array('ProjectFile', 'Category', 'Tag', 'Thumbnail', 'Tagdetail');
	public $components = array('Paginator');

	/*Frontend function starts from here*/

	public function view($title = null) {
		//site/aztec
		//for thumbnail and title
		if (!$this->ProjectFile->findByProjectFolder($this->params->pass[0])) {
			throw new NotFoundException(__('Invalid Project'));
		}
		$projectFile = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $this->params->pass[0]), 'fields' => 'title'));
		foreach($projectFile as $key => $value){
			$thumbnail = $this->Thumbnail->findByProjectName($value['title']);
			if($thumbnail){
				$projectFile[$key]['thumbnail'] = $thumbnail['Thumbnail']['filename'];
			} else {
				$projectFile[$key]['thumbnail'] = '';
			}
		}
		//for projects
		$categories = $this->ProjectFile->Category->find('list', array('fields' => array('id', 'title'), 'order' => array('modified' => 'ASC')));
		$projects = array();		
		foreach($categories as $key => $value){
			$projects[$value] = $this->ProjectFile->find('all', array('conditions' => array('category_id' => $key, 'project_folder' => $this->params->pass[0], 'ProjectFile.status_id' => array(1,2)), 'order' => array('ProjectFile.created' => 'DESC')));
		}
		$this->set(compact('categories', 'projects', 'projectFile'));
	}

	public function viewCategoryListing(){
		//site/aztec/html
		//for thumbnail and title
		if (!$this->ProjectFile->Category->findByCategoryFolder($this->params->pass[1])) {
			throw new NotFoundException(__('Invalid Project'));
		}
		$projectFile = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $this->params->pass[0]), 'fields' => 'title'));
		foreach($projectFile as $key => $value){
			$thumbnail = $this->Thumbnail->findByProjectName($value['title']);
			if($thumbnail){
				$projectFile[$key]['thumbnail'] = $thumbnail['Thumbnail']['filename'];
			} else {
				$projectFile[$key]['thumbnail'] = '';
			}
		}
		//for projects
		$categories = $this->ProjectFile->Category->find('list', array('fields' => array('id', 'title'), 'conditions' => array('category_folder' => $this->params->pass[1])));
		$projects = array();		
		foreach($categories as $key => $value){
			$projects[$value] = $this->ProjectFile->find('all', array('conditions' => array('category_id' => $key, 'project_folder' => $this->params->pass[0], 'ProjectFile.status_id' => array(1,2)), 'order' => array('ProjectFile.created' => 'DESC')));
		}
		$this->set(compact('categories', 'projects', 'projectFile'));
	}
	public function projectLogin()
	{
		$psw = $_POST['psw'];
		//$host = $_POST['host'];
		//$project = $_POST['prjfile'];		
		$category = $this->Category->find('first', array('conditions' => array('category_folder' => $this->params->categoryfolder)));
		
		$this->ProjectFile->recursive = 0;
		$projectfiles = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $this->params->projectfolder, 'category_id' => $category['Category']['id'], 'date_folder' => $this->params->datefolder, 'ProjectFile.status_id' => array(1,2), 'ProjectFile.password' => md5($psw))));
		if(!empty($projectfiles)){
			$path = 'files/'.$this->params->projectfolder.'/'.$this->params->categoryfolder.'/'.$this->params->datefolder;
			$dir = new Folder($path);
			$files = $dir->read(true, array('files', ''));

			$html = '';
			$gallery = '';
			foreach($files[1] as $value){
				if(substr(strrchr($value,'.'),1) == 'html'){
					$html = 'html';
				} else {
					$gallery = 'gallery';
				}
			}
			if($html != ''){
				$this->set('projectfiles', $projectfiles);
				$this->layout = 'html';
				$this->render('/projectfiles/admin_htmlfiles');
			} else {
				$this->set(compact('files', 'path', 'projectfiles'));
				$this->layout = 'gallery';
				$this->render('/projectfiles/admin_gallery');
			}
		}
		
		
	}
	public function viewdetails(){
		//site/aztec/html/2014-06-30
		$this->Category->recursive = -1;
		$category = $this->Category->find('first', array('conditions' => array('category_folder' => $this->params->categoryfolder)));
		
		$this->ProjectFile->recursive = 0;
		$projectfiles = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $this->params->projectfolder, 'category_id' => $category['Category']['id'], 'date_folder' => $this->params->datefolder, 'ProjectFile.status_id' => array(1,2))));
		//Debugger::dump($projectfiles['ProjectFile']['is_password']);
		
		if(!empty($projectfiles)){
			if($projectfiles['ProjectFile']['is_password'] == 1)
			{
				$this->set('projectfiles', $projectfiles);
				$this->layout = 'default';
				$this->render('/projectfiles/projLogin');
			}
			else if($projectfiles['ProjectFile']['is_password'] == 0 && $projectfiles['ProjectFile']['id'] > 1201)
			{
				//$this->set('projectfiles', $projectfiles);
				$this->layout = 'default';
				$this->render('/projectfiles/notfound');
			}
			else{
			$path = 'files/'.$this->params->projectfolder.'/'.$this->params->categoryfolder.'/'.$this->params->datefolder;
			$dir = new Folder($path);
			$files = $dir->read(true, array('files', ''));

			$html = '';
			$gallery = '';
			foreach($files[1] as $value){
				if(substr(strrchr($value,'.'),1) == 'html'){
					$html = 'html';
				} else {
					$gallery = 'gallery';
				}
			}
			if($html != ''){
				$this->set('projectfiles', $projectfiles);
				$this->layout = 'html';
				$this->render('/projectfiles/admin_htmlfiles');
			} else {
				$this->set(compact('files', 'path', 'projectfiles'));
				$this->layout = 'gallery';
				$this->render('/projectfiles/admin_gallery');
			}
			}
		}
		
	}

	/*Admin function starts from here*/
	
	public function admin_publish(){
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['ProjectFile']['status_id'] = 1; //publish(active)
			if ($this->ProjectFile->save($this->request->data['ProjectFile'])) {
				//deleting tags for the same ID
				$projectId = $this->ProjectFile->id;
				$this->Tag->deleteAll(array('project_id' => $projectId));
				//saving tags
				$tags = explode(',', preg_replace('/\s+/', '', $this->request->data['Tag']['title']));
				foreach($tags as $value){
					if($value != '') {
						$tag['Tag'] = array('project_id' => $this->request->data['ProjectFile']['id'], 'title' => $value);
						$this->Tag->create();
						$this->Tag->save($tag);
					}
				}
				$this->Session->setFlash(__('Project Published Successfully'), 'default', array('class' => 'alert alert-success'));
				if(!empty($this->request->data['ProjectFile']['categoryname'])){
					return $this->redirect('/admin/projectfiles/'.$this->request->data['ProjectFile']['projectname'].'/'.$this->request->data['ProjectFile']['categoryname']);
				} else {
					return $this->redirect(array('controller' => 'projectfiles', 'action' => 'view', 'projectfolder' => $this->request->data['ProjectFile']['projectname']));
				}
			} else {
				$this->Session->setFlash(__('Some error occurs please try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
	}
	
	public function admin_thumb(){
		if ($this->request->is(array('post', 'put'))) {	
			if($this->ProjectFile->save($this->request->data)){
				$this->Session->setFlash(__('Thumbnail Added Successfully.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Some error occurs. Please try again later!!'), 'default', array('class' => 'alert alert-danger'));
				return $this->redirect(array('action' => 'index'));
			}
		}
	}

	public function admin_setstatus($id = null, $status = null) {
		$this->layout = 'ajax';
		$this->ProjectFile->id = $id;
		if (!$this->ProjectFile->exists()) {
			throw new NotFoundException(__('Invalid Project'));
		}
		
		/* _____ Added new design */
		$getProjectFolder = $this->ProjectFile->find('first', array('conditions' => array('ProjectFile.id' => $id)));
		if(!empty($getProjectFolder["ProjectFile"])){
			$projectFolder = $getProjectFolder["ProjectFile"]["project_folder"];
		}

		$projectIdWithTagsTableData = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $projectFolder,'ProjectFile.status_id' =>array(1,2)), 'fields' => array('id', 'title', 'project_folder'),'order' => array('id ASC')));
		$projectIdWithTagsTableData = $projectIdWithTagsTableData["ProjectFile"]["id"];
		if($status == 4){
			$tagMappedId = $projectIdWithTagsTableData;
		}

		/*_____END________________*/
		
		$res = $this->ProjectFile->save(array('status_id' => $status) );
		
		/* _____________________________________________________
			Added new design |
			we are setting the project id in backend using the query (status in 1,2 and id asc) with first row.
			this project id is tagged with tags table when updating or inserting tags.
			but if we are making protected this record then status will update to 4 in this case
			we have to update tags table mapping which is mapped with this project_id.
			which is below.
		*/
		
		
		$newProjectFileId = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $projectFolder,'ProjectFile.status_id' =>array(1,2)), 'fields' => array('id', 'title', 'project_folder'),'order' => array('id ASC')));
		$newProjectFileId = $newProjectFileId["ProjectFile"]["id"];
		if($status == 2){
			$tagMappedId = $newProjectFileId;
		}

		if($res){
			
			if(!empty($newProjectFileId) && $newProjectFileId > 0 && $id == $tagMappedId){
				$returnStatus = $this->ProjectFile->query("update tags set project_id = ".$newProjectFileId." where project_id = ".$projectIdWithTagsTableData);
			}
		}
		/*____________________________________________________*/
		
		echo ($status);
        exit;
	}
	
	public function admin_setpassword($id = null, $status = null, $pwd = null) {
		$this->layout = 'ajax';
		$this->ProjectFile->id = $id;
		if (!$this->ProjectFile->exists()) {
			throw new NotFoundException(__('Invalid Project'));
		}
		$this->ProjectFile->query("UPDATE project_files set is_password = $status, password = '$pwd' where id = $id");
		echo 'SUCCESS';
		exit;	
        
	}


	public function admin_index() {
		//checking records assigned to the loggedin user
		$userProjects = $this->Session->read('permission');
		foreach($userProjects['projects'] as $projects) {
			$assignedProjects[] = $projects['UserProject']['project_name'];
		}

		//fetching records assigned to the loggedin user
		if(in_array('all', $assignedProjects)) {
			
			// YSI_08_05_2017 : Modified to make data filter according to latest date.
			/* Added for new design || check both the query | below original query has been commented */
			
			//$projectFiles = $this->ProjectFile->query("SELECT * FROM ( SELECT id,title,modified,project_folder,status_id,created,recordListingID FROM `project_files` WHERE status_id IN(1,2) ORDER BY modified DESC ) AS ProjectFile GROUP BY title ORDER BY modified DESC LIMIT 20;");
			$projectFiles = $this->ProjectFile->query("SELECT * FROM ( SELECT id,title,modified,project_folder,status_id,created,recordListingID FROM `project_files` WHERE status_id IN(1,2) ORDER BY modified DESC ) AS ProjectFile GROUP BY title ORDER BY recordListingID ASC LIMIT 20;");
			
			//for javascript total_groups variable in admin index page
			$totalGroups = $this->ProjectFile->query("SELECT COUNT(*) FROM ( SELECT id,title,modified,project_folder,status_id,recordListingID FROM `project_files` WHERE status_id IN(1,2) ) AS ProjectFile GROUP BY title;");
		} else {
			$projects = '"'.implode('", "', $assignedProjects).'"';
			$projectFiles = $this->ProjectFile->query("SELECT * FROM ( SELECT id,title,modified,project_folder,status_id,recordListingID FROM `project_files` WHERE status_id IN(1,2) AND project_folder IN($projects) ORDER BY modified DESC ) AS ProjectFile GROUP BY title ORDER BY recordListingID ASC LIMIT 20;");
			//echo "<pre>";
			//echo "SELECT * FROM ( SELECT id,title,modified,project_folder,status_id,recordListingID FROM `project_files` WHERE status_id IN(1,2) AND project_folder IN($projects) ORDER BY modified DESC ) AS ProjectFile GROUP BY title ORDER BY recordListingID ASC LIMIT 20;";die;
			//for javascript total_groups variable in admin index page
			$totalGroups = $this->ProjectFile->query("SELECT COUNT(*) FROM ( SELECT id,title,modified,project_folder,status_id,recordListingID FROM `project_files` WHERE status_id IN(1,2) AND project_folder IN($projects) ) AS ProjectFile GROUP BY title;");
		}		
		$tags = $this->ProjectFile->query("select distinct title from tags");
		//getting thumbnails from thumbnail table
		//Debugger::dump($projectFiles);
		foreach($projectFiles as $key => $value){
			$thumbnail = $this->Thumbnail->findByProjectName($value['ProjectFile']['title']);
			if($thumbnail){
				$projectFiles[$key]['ProjectFile']['thumbnail'] = $thumbnail['Thumbnail']['filename'];
			} else {
				$projectFiles[$key]['ProjectFile']['thumbnail'] = '';
			}
		}
		$this->set(compact('projectFiles', 'totalGroups', 'tags'));
	}

	public function admin_archives() {
		$projects = $this->ProjectFile->find('all', array('conditions' => array('ProjectFile.status_id' => 3), 'order' => array('ProjectFile.modified' => 'DESC', 'ProjectFile.title' => 'DESC')));
		$this->set(compact('projects'));
	}

	public function admin_recover() {
		if($this->Auth->user('role_id') == 1) {
			$project = $this->ProjectFile->find('first', array('conditions' => array('ProjectFile.id' => $this->request->params['pass'][0]), 'fields' => array('id', 'status_id')));
			$project['ProjectFile']['status_id'] = 2;		
			if ($this->ProjectFile->save($project)) {
				$this->Session->setFlash(__('Project recovered successfully.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'projectfiles', 'action' => 'archives'));
			} else {
				$this->Session->setFlash(__('Project could not be recovered. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			return $this->redirect(array('action' => 'index'));
		}
	}

	public function admin_deletefiles() {
		if($this->Auth->user('role_id') == 1) {
			if($this->ProjectFile->delete($this->request->params['pass'][0])) {
				$this->Session->setFlash(__('Project Deleted successfully.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'projectfiles', 'action' => 'archives'));
			} else {
				$this->Session->setFlash(__('Some error occurred. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			return $this->redirect(array('action' => 'index'));
		}
	}

	public function admin_deleteAllfiles() {
		$deletedProjects = $this->ProjectFile->find('all', array('conditions' => array('ProjectFile.status_id' => 3)));

		if($this->Auth->user('role_id') == 1) {
			foreach($deletedProjects as $project) {			
				$this->ProjectFile->delete($project['ProjectFile']['id']);
			}
			$this->Session->setFlash(__('Projects Deleted successfully.'), 'default', array('class' => 'alert alert-success'));
			return $this->redirect(array('action' => 'projectfiles', 'action' => 'archives'));
		} else {
			$this->Session->setFlash(__('Some error occurred. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
	}

	public function admin_scroll($group_number) {
		$this->layout = 'ajax';
		$position = ($group_number * 20);

		//checking records assigned to the loggedin user
		$userProjects = $this->Session->read('permission');
		foreach($userProjects['projects'] as $projects) {
			$assignedProjects[] = $projects['UserProject']['project_name'];
		}
		//fetching records assigned to the loggedin user
		if(in_array('all', $assignedProjects)) {
			$projectFiles = $this->ProjectFile->query("SELECT * FROM ( SELECT id,title,modified,project_folder,status_id,recordListingID FROM `project_files` WHERE status_id IN(1,2) ORDER BY modified DESC ) AS ProjectFile GROUP BY title ORDER BY recordListingID ASC LIMIT $position,20;");
		} else {
			$projects = '"'.implode('", "', $assignedProjects).'"';
			$projectFiles = $this->ProjectFile->query("SELECT * FROM ( SELECT id,title,modified,project_folder,status_id,recordListingID FROM `project_files` WHERE status_id IN(1,2) AND project_folder IN($projects) ORDER BY modified DESC ) AS ProjectFile GROUP BY title ORDER BY recordListingID ASC LIMIT $position,20;");
		}

		//getting thumbnails from thumbnail table and beautify date
		foreach($projectFiles as $key => $value){
			$thumbnail = $this->Thumbnail->findByProjectName($value['ProjectFile']['title']);
			if($thumbnail){
				$projectFiles[$key]['ProjectFile']['thumbnail'] = $thumbnail['Thumbnail']['filename'];
			} else {
				$projectFiles[$key]['ProjectFile']['thumbnail'] = '';
			}
			//beautify date
			$projectFiles[$key]['ProjectFile']['modified'] = date( 'M d, Y', strtotime( $projectFiles[$key]['ProjectFile']['modified'] ) );
		}
		echo json_encode($projectFiles);
		exit;
	}
	public function admin_tagupdate($tags = null, $id=null)
	{		
	$this->layout = 'ajax';
		//Debugger::dump("Hello");
	if($tags == null)
	{
		echo json_encode("NoTAG");
		exit;
	}
	else if($id == null)
	{
		echo json_encode("PROJECT UNDEFINED");
		exit;
	}
	else
		{
				$arrTags = explode(",",$tags);
				$pid = $id;
				//$projectFiles = $this->ProjectFile->query("delete from tags where project_id = ".$pid);
				
				/* NOTE::--  If you will comment below section code then  open above commented line.*/
				
				/*_____Added for  new design || bug | Delete all data from tags table with selected project of all project id 
					to solve the problem of garbage data, it will delete all data tag with any id of selected project
				*/
				$allProjectsData = $this->ProjectFile->find('first', array('conditions' => array('ProjectFile.id' => $pid)));
				if(!empty($allProjectsData["ProjectFile"])){
					$projectPath = $allProjectsData["ProjectFile"]["project_folder"];
					$allProjectsData = $this->ProjectFile->find('all', array('conditions' => array('ProjectFile.project_folder' => $projectPath)));
					foreach($allProjectsData as $key=>$projectDataRow){
						$projectRowId = $projectDataRow["ProjectFile"]["id"];
						$projectFiles = $this->ProjectFile->query("delete from tags where project_id = ".$projectRowId);
					}
				}
				
				/*Added for  new design || bug | Update all record of selected project all record must be tag with same data _______	
				*/
				$allProjectsData = $this->ProjectFile->find('first', array('conditions' => array('ProjectFile.id' => $pid)));
				if(!empty($allProjectsData["ProjectFile"])){
					$projectPath = $allProjectsData["ProjectFile"]["project_folder"];
					$allProjectsData = $this->ProjectFile->find('all', array('conditions' => array('ProjectFile.project_folder' => $projectPath)));
					foreach($allProjectsData as $key=>$projectDataRow){
						$projectRowId = $projectDataRow["ProjectFile"]["id"];
						//echo $projectRowId."<br>";
						//$this->ProjectFile->updateAll(array('ProjectFile.recordListingID' => $listingCounter), array('ProjectFile.id' => $projectRowId));	
						$modifiedDate = date("Y:m:d H:i:s");
						$projectFiles = $this->ProjectFile->query("update project_files set tags = '".$tags."', modified='".$modifiedDate."' where id = ".$projectRowId);
					}
				}
				/*______________________________________________________*/
				
				/* NOTE::--  If you will comment above code then open open bellow commented line.*/
				
				//$projectFiles = $this->ProjectFile->query("update project_files set tags = '".$tags."' where id = ".$pid);
				
				foreach($arrTags as $tag)
				{
					$tagtitle = trim($tag);
					if($tagtitle!='')
						{//$projectID = $this->ProjectFile->id;
													
							$projectFiles = $this->ProjectFile->query("Insert into tags(project_id,title) values($pid,'$tagtitle')");
						}
				}
				//echo json_encode($tags);
				echo json_encode('SUCCESS');
				exit;
		}	//$this->Session->setFlash(__('Projects Deleted successfully.'), 'default', array('class' => 'alert alert-success'));
	}
	
	public function admin_tags($tags = null, $page = null) {
		$this->layout = 'ajax';
		$cond = '';
		$limit = '';
		//checking records assigned to the loggedin user
		if($page != null)
		{	
			$recordCount = $page * 20;
			
			$limit = "LIMIT $recordCount, 20";
			//echo $limit;
		}
		else
		{
			$limit = "LIMIT 20";
		}
		$userProjects = $this->Session->read('permission');
		foreach($userProjects['projects'] as $projects) {
			$assignedProjects[] = $projects['UserProject']['project_name'];
		}
			
			if($tags == null)
			{
				$cond = 'where';
			}
			else
			{
				$tagArr = explode(',',urldecode($tags));			
				$tagArr = implode(',', $tagArr);
				$tagArr = str_replace(",","','",$tagArr);
				if(count($tagArr)>0)
					{						
						$cond = "inner join `tags` t on p.id = t.project_id and t.title in ('$tagArr') and";
					}
					else
					{
						$cond = 'where';
					}
			}			
			if(in_array('all', $assignedProjects)) {
				$projectFiles = $this->ProjectFile->query("SELECT * FROM (SELECT p.id,p.title,modified,project_folder,status_id,recordListingID FROM `project_files` p $cond p.status_id IN(1,2) ORDER BY modified DESC ) AS ProjectFile GROUP BY title ORDER BY recordListingID ASC $limit;");
				
			} else 
			{
				$projects = '"'.implode('", "', $assignedProjects).'"';
				$projectFiles = $this->ProjectFile->query("SELECT * FROM ( SELECT p.id,p.title,modified,project_folder,status_id,recordListingID FROM `project_files` p $cond p.status_id IN(1,2) AND project_folder IN($projects) ORDER BY modified DESC ) AS ProjectFile GROUP BY title ORDER BY recordListingID ASC $limit;");				
				
			}
			//echo "SELECT * FROM ( SELECT p.id,p.title,modified,project_folder,status_id,recordListingID FROM `project_files` p $cond p.status_id IN(1,2) AND project_folder IN($projects) ORDER BY modified DESC ) AS ProjectFile GROUP BY title ORDER BY recordListingID ASC $limit";;die;
			//getting thumbnails from thumbnail table and beautify date
			foreach($projectFiles as $key => $value){
				$thumbnail = $this->Thumbnail->findByProjectName($value['ProjectFile']['title']);
				if($thumbnail){
					$projectFiles[$key]['ProjectFile']['thumbnail'] = $thumbnail['Thumbnail']['filename'];
				} else {
					$projectFiles[$key]['ProjectFile']['thumbnail'] = '';
				}
				//beautify date
				$projectFiles[$key]['ProjectFile']['modified'] = date( 'M d, Y', strtotime( $projectFiles[$key]['ProjectFile']['modified'] ) );
			}	
		if(count($projectFiles)>0)
			echo json_encode($projectFiles);
		else
			echo json_encode('No Data');
		//print_r(count($projectFiles));
		exit;
	}
	public function admin_search($keyword) {
		$this->layout = 'ajax';
		
		//checking records assigned to the loggedin user
		$userProjects = $this->Session->read('permission');	
		
			foreach($userProjects['projects'] as $projects) {
				$assignedProjects[] = $projects['UserProject']['project_name'];
			}
			//fetching records assigned to the loggedin user
			if(in_array('all', $assignedProjects)) {
				$projectFiles = $this->ProjectFile->query("SELECT * FROM ( SELECT id,title,modified,project_folder,status_id,recordListingID FROM `project_files` WHERE (title LIKE '%$keyword%' or tags LIKE '%$keyword%') AND status_id IN(1,2) ORDER BY modified DESC ) AS ProjectFile GROUP BY title ORDER BY recordListingID ASC;");
			} else {
				$projects = '"'.implode('", "', $assignedProjects).'"';
				$projectFiles = $this->ProjectFile->query("SELECT * FROM ( SELECT id,title,modified,project_folder,status_id,recordListingID FROM `project_files` WHERE (title LIKE '%$keyword%' or tags LIKE '%$keyword%') AND status_id IN(1,2) AND project_folder IN($projects) ORDER BY modified DESC ) AS ProjectFile GROUP BY title ORDER BY recordListingID ASC;");
			}
			//getting thumbnails from thumbnail table and beautify date
			foreach($projectFiles as $key => $value){
				$thumbnail = $this->Thumbnail->findByProjectName($value['ProjectFile']['title']);
				if($thumbnail){
					$projectFiles[$key]['ProjectFile']['thumbnail'] = $thumbnail['Thumbnail']['filename'];
				} else {
					$projectFiles[$key]['ProjectFile']['thumbnail'] = '';
				}
				//beautify date
				$projectFiles[$key]['ProjectFile']['modified'] = date( 'M d, Y', strtotime( $projectFiles[$key]['ProjectFile']['modified'] ) );
			}
		
		echo json_encode($projectFiles);
		exit;
	}

	public function admin_view($title = null) {
		//site/aztec
		//for thumbnail and title
		if (!$this->ProjectFile->findByProjectFolder($this->params->pass[0])) {
			throw new NotFoundException(__('Invalid Project'));
		}
		$projectFile = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $this->params->pass[0]), 'fields' => array('id', 'title', 'project_folder')));
		
		
		// NOTE::- Added for new design || to make the order right || Query has been changed || order from DESC to ASC change and status_id condition added		
		//$projectFileId = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $this->params->pass[0]), 'fields' => array('id', 'title', 'project_folder'),'order' => array('id DESC')));
		$projectFileId = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $this->params->pass[0],'ProjectFile.status_id' =>array(1,2)), 'fields' => array('id', 'title', 'project_folder'),'order' => array('id ASC')));
		
		//Debugger::dump($projectFileId['ProjectFile']['id']);
		
		foreach($projectFile as $key => $value){
			$thumbnail = $this->Thumbnail->findByProjectName($value['title']);
			if($thumbnail){
				$projectFile[$key]['thumbnail'] = $thumbnail['Thumbnail']['filename'];
			} else {
				$projectFile[$key]['thumbnail'] = '';
			}
		}
		//for projects
		$categories = $this->ProjectFile->Category->find('list', array('fields' => array('id', 'title'), 'order' => array('modified' => 'ASC')));
		$projects = array();		
		foreach($categories as $key => $value){
			if($this->Auth->user('role_id') == 1 || $this->Auth->user('role_id') == 2) {
				$projects[$value] = $this->ProjectFile->find('all', array('conditions' => array('category_id' => $key, 'project_folder' => $this->params->pass[0], 'ProjectFile.status_id' => array(1,2,4)), 'order' => array('ProjectFile.created' => 'DESC')));
			} else {
				$projects[$value] = $this->ProjectFile->find('all', array('conditions' => array('category_id' => $key, 'project_folder' => $this->params->pass[0], 'ProjectFile.status_id' => array(1,2)), 'order' => array('ProjectFile.created' => 'DESC')));
			}
		}
		//for tags
		
		foreach($projects as $key => $project){
			foreach($project as $keyy => $value){
				$tags = $this->Tag->find('list', array('conditions' => array('project_id' => $value['ProjectFile']['id']), 'fields' => array('id', 'title')));
				$tag = implode(',',$tags);
				$projects[$key][$keyy]['ProjectFile']['tags'] = $tag;
			}
		}

		//finding all the unique tags for auto suggest in publish options
		$findTags = $this->Tag->find('all', array('fields'=>'DISTINCT title'));
		foreach($findTags as $tag) {
			$uniqueTags[] = '"'.$tag['Tag']['title'].'"';
		}
		$finalTags = implode(",",$uniqueTags);
		$tags = $this->ProjectFile->query("select distinct title from tags where project_id =".$projectFileId['ProjectFile']['id']);
		$allTags = $this->ProjectFile->query("select distinct title from tags");
		$pid = $projectFileId['ProjectFile']['id'];
		$this->set(compact('categories', 'projects', 'projectFile', 'finalTags' , 'tags', 'allTags', 'pid'));
	}

	public function admin_viewCategoryListing(){
		//site/aztec/html
		//for thumbnail and title
		if (!$this->ProjectFile->Category->findByCategoryFolder($this->params->pass[1])) {
			throw new NotFoundException(__('Invalid Project'));
		}
		$projectFile = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $this->params->pass[0]), 'fields' => 'title'));

		foreach($projectFile as $key => $value){
			$thumbnail = $this->Thumbnail->findByProjectName($value['title']);
			if($thumbnail){
				$projectFile[$key]['thumbnail'] = $thumbnail['Thumbnail']['filename'];
			} else {
				$projectFile[$key]['thumbnail'] = '';
			}
		}
		//for projects
		$categories = $this->ProjectFile->Category->find('list', array('fields' => array('id', 'title'), 'conditions' => array('category_folder' => $this->params->pass[1])));
		$projects = array();		
		foreach($categories as $key => $value) {
			if($this->Auth->user('role_id') == 1 || $this->Auth->user('role_id') == 2) {
				$projects[$value] = $this->ProjectFile->find('all', array('conditions' => array('category_id' => $key, 'project_folder' => $this->params->pass[0], 'ProjectFile.status_id' => array(1,2,4)), 'order' => array('ProjectFile.created' => 'DESC')));
			} else {
				$projects[$value] = $this->ProjectFile->find('all', array('conditions' => array('category_id' => $key, 'project_folder' => $this->params->pass[0], 'ProjectFile.status_id' => array(1,2)), 'order' => array('ProjectFile.created' => 'DESC')));
			}
		}
		//for tags
		foreach($projects as $key => $project){
			foreach($project as $keyy => $value){
				$tags = $this->Tag->find('list', array('conditions' => array('project_id' => $value['ProjectFile']['id']), 'fields' => array('id', 'title')));
				$tag = implode(',',$tags);
				$projects[$key][$keyy]['ProjectFile']['tags'] = $tag;
			}
		}

		//finding all the unique tags for auto suggest in publish options
		$findTags = $this->Tag->find('all', array('fields'=>'DISTINCT title'));
		foreach($findTags as $tag) {
			$uniqueTags[] = '"'.$tag['Tag']['title'].'"';
		}
		$finalTags = implode(",",$uniqueTags);
		$this->set(compact('categories', 'projects', 'projectFile', 'finalTags'));
	}
	
	public function admin_viewdetails() {
		//site/aztec/html/2014-06-30
		$this->Category->recursive = -1;
		$category = $this->Category->find('first', array('conditions' => array('category_folder' => $this->params->categoryfolder)));
		
		$this->ProjectFile->recursive = 0;
		if($this->Auth->user('role_id') == 1 || $this->Auth->user('role_id') == 2) {
			$projectfiles = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $this->params->projectfolder, 'category_id' => $category['Category']['id'], 'date_folder' => $this->params->datefolder, 'ProjectFile.status_id' => array(1,2,3,4))));
		} else {
			$projectfiles = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $this->params->projectfolder, 'category_id' => $category['Category']['id'], 'date_folder' => $this->params->datefolder, 'ProjectFile.status_id' => array(1,2))));
		}

		if(!empty($projectfiles)){
			$path = 'files/'.$this->params->projectfolder.'/'.$this->params->categoryfolder.'/'.$this->params->datefolder;
			$dir = new Folder($path);
			$files = $dir->read(true, array('files', ''));

			$html = '';
			$gallery = '';
			foreach($files[1] as $value){
				if(substr(strrchr($value,'.'),1) == 'html'){
					$html = 'html';
				} else {
					$gallery = 'gallery';
				}
			}
			if($html != '') {
				$this->set('projectfiles', $projectfiles);
				$this->layout = 'html';
				$this->render('/projectfiles/admin_htmlfiles');
			} else {
				$this->set(compact('files', 'path', 'projectfiles'));
				$this->layout = 'gallery';
				$this->render('/projectfiles/admin_gallery');
			}
		}
	}

	public function admin_addnewelement() {

		// echo ( $this->request->data['ProjectFile']['category_id'] );
		// echo ( $this->request->is('post') );
  //       exit;
		// Debugger::dump( $this->request->data['ProjectFile']['category_id'] );
		// Debugger::dump( $this->request->is('post') );

		if ($this->request->is('post')) 
		{
			if(!isset($this->request->data['ProjectFile']['category_id'])){
				$this->Session->setFlash(__('Please select category for your project'), 'default', array('class' => 'alert alert-danger'));
				return $this->redirect(array('action' => 'add'));						
			}		
			
			$this->request->data['ProjectFile']['user_id'] = $this->Auth->user('id');
			$this->request->data['ProjectFile']['status_id'] = 2; //Not Active
			$this->request->data['ProjectFile']['project_folder'] = str_replace(' ', '-', strtolower($this->request->data['ProjectFile']['title']));			
			$tgs = $this->request->data['ProjectFile']['tags'];

			//Debugger::dump($tgs);

			if(trim($tgs)=='')
			{
				$this->Session->setFlash(__('Please select tags for your project'), 'default', array('class' => 'alert alert-danger'));
				return $this->redirect(array('action' => 'add'));			
			}

			$tags  = explode(',',$tgs);							
			$this->ProjectFile->create();

			if ($this->ProjectFile->save($this->request->data))
			{
				foreach($tags as $tag)
				{	
					$tagtitle = trim($tag);
					if($tagtitle!='')
					{	
						$projectID = $this->ProjectFile->id;
						$projectFiles = $this->ProjectFile->query("Insert into tags(project_id,title) values($projectID,'$tagtitle')");
					}							
				}
				
				$projTitle = strtolower($this->request->data['ProjectFile']['title']);
				//echo "test123";
				//return $this->redirect(array('action' => 'index'));

				header('Location: http://designlab.espire.com/admin/projectfiles/'.$projTitle);
				exit();		

			} else {
				$this->Session->setFlash(__('The project could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}			
		}

		$tags ='';		
		if(isset($this->params->projectname))
		{
			$projectFileId = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $this->params->projectname), 'fields' => array('id', 'title', 'project_folder'),'order' => array('id DESC')));
			$tags = $this->ProjectFile->query("select distinct title from tags where project_id =".$projectFileId['ProjectFile']['id']);
		}

		$categories = $this->ProjectFile->Category->find('all', array('recursive' => 0, 'fields' => array('id', 'title'), 'conditions' => array('status_id' => 1)));		
		$users = $this->ProjectFile->User->find('list');
		$statuses = $this->ProjectFile->Status->find('list', array('conditions' => array('id' => array(1,2))));
		
		//finding unique tag names for auto suggest on add page		
		$allTags = $this->ProjectFile->query("select distinct title from tags where title<>''");		
				
		$projects = $this->ProjectFile->find('all', array('fields' => 'DISTINCT title'));
		
		foreach($projects as $project) {
			$uniqueProjects[] = '"'.$project['ProjectFile']['title'].'"';
		}

		$finalProjects = implode(",",$uniqueProjects);

		if(isset($this->params->projectname)){
			$title = $this->ProjectFile->find('list', array('conditions' => array('project_folder' => $this->params->projectname), 'limit' => 1));
			$this->set('title', $title);
		}

		$this->set(compact('categories', 'users', 'statuses', 'finalProjects','tags','allTags'));				
	}

	public function admin_add() {
		/*___ Added for new design | if condition is added to populate tags on the basis of project selection__ 
			When use is on create new project window.
			Check If condition and compare the code else condition is used to create or add new project
			This ajax call is added in admin_add.ctp view file

		*/
		if($this->request->data["action"] == "gettags"){
			$this->layout = 'ajax';
			$resTag = array();
			$selectedProject = trim(urldecode($this->request->data["projectName"]));
			if(!empty($selectedProject)){
				$projectFolder = str_replace(' ', '-', trim(strtolower($selectedProject)));
				$this->ProjectFile->recursive = -1;				
				$projectExistOrNot = $this->ProjectFile->find('first', array('conditions' => array('ProjectFile.project_folder' => $projectFolder,'ProjectFile.status_id' =>array(1,2)), 'fields' => array('id', 'title', 'status_id', 'tags', 'recordListingID', 'project_folder'),'order' => array('id ASC')));
				//echo "<pre>";
				//print_r($projectExistOrNot);die;
				if(!empty($projectExistOrNot)){
					
					$existProjectTags = $projectExistOrNot["ProjectFile"]["tags"];
					$resTag["tags"] = explode(",",$existProjectTags);
				}
			}
			echo json_encode($resTag);
			exit;
		} else{
			
			$prequesttype = "";
			if ($this->request->is('post')) {
				if(isset($this->request->data['ProjectFile']['prequesttype'])){ // new design check if condition
					$prequesttype = $this->request->data['ProjectFile']['prequesttype'];
				}
			}

			$projinnertagstr = "";
			if ($this->request->is('post')) {
				if(isset($this->request->data['ProjectFile']['projinnertag'])){  // new design check if condition
					$projinnertagstr = $this->request->data['ProjectFile']['projinnertag'];
				}
			}

			if ($this->request->is('post')) {
				if(!isset($this->request->data['ProjectFile']['category_id'])){
					$this->Session->setFlash(__('Please select category for your project'), 'default', array('class' => 'alert alert-danger'));
					// Added new design || redirection has been changed
					//return $this->redirect(array('action' => 'add'));
					return $this->redirect($this->referer());
				}
				
				$this->request->data['ProjectFile']['user_id'] = $this->Auth->user('id');
				$this->request->data['ProjectFile']['status_id'] = 2; //Not Active
				$this->request->data['ProjectFile']['project_folder'] = str_replace(' ', '-', strtolower($this->request->data['ProjectFile']['title']));
				
				
				/* New design ________START ____________
				 When we adding the new project from inner project and then error comes its redirected to another page 
				 which is a project addition page , we can also add project in existing project from here.
				 but in this case $prequesttype is not set although it is from inner page
				 so to hnadle this case we are using below step.
				*/
				//if(!isset($prequesttype) || $prequesttype == "" || empty($prequesttype)){
					$projectFolder = str_replace(' ', '-', strtolower($this->request->data['ProjectFile']['title']));
					$this->ProjectFile->recursive = -1;
					//$projectExistOrNot = $this->ProjectFile->find('first', array('conditions' => array('ProjectFile.project_folder' => $projectFolder)));
					$projectExistOrNot = $this->ProjectFile->find('first', array('conditions' => array('ProjectFile.project_folder' => $projectFolder,'ProjectFile.status_id' =>array(1,2)), 'fields' => array('id', 'title', 'status_id', 'tags', 'recordListingID', 'project_folder'),'order' => array('id ASC')));
					
					//echo "<pre>";
					//print_r($projectExistOrNot);die;
				
					if(!empty($projectExistOrNot)){
						$prequesttype = 'projectinnerpage';
						$existProjectTags = $projectExistOrNot["ProjectFile"]["tags"];
					}
				//}

				//________ END ___________________________
				
				
				
				if ($prequesttype != 'projectinnerpage') {
					$tgs = $this->request->data['ProjectFile']['tags'];

					if(trim($tgs)==''){
						$this->Session->setFlash(__('Please select tags for your project'), 'default', array('class' => 'alert alert-danger'));
						return $this->redirect(array('action' => 'add'));
					}
				}
				
				

				if ($prequesttype == 'projectinnerpage') {	
					//$tags  = explode(',',$projinnertagstr);
					// Added new design || pls match the code if condition
					$tags  = explode(',',$existProjectTags);
				} else {
					$tags  = explode(',',$tgs);
				}

				// ____ STRAT ___ added for new design || Below line is added to update tags in project tables. ___
				if ($prequesttype == 'projectinnerpage') {
					$this->request->data['ProjectFile']['tags'] = trim($existProjectTags);
					$this->request->data['ProjectFile']['recordListingID'] = $projectExistOrNot["ProjectFile"]["recordListingID"];
				} else{
					$this->request->data['ProjectFile']['tags'] = trim($this->request->data['ProjectFile']['tags']);
				}
				//__________________END______________________________________________________

				$this->ProjectFile->create();
				
				if ($this->ProjectFile->save($this->request->data)) {
					
					/*___ START _ added for new design || If the project added from project inner page then dont insert tags.
						I have added only if condition.pls check below..
					*/
					if ($prequesttype != 'projectinnerpage') {
						foreach($tags as $tag)
						{	
							$tagtitle = trim($tag);
							if($tagtitle!='')
							{
								$projectID = $this->ProjectFile->id;
								$projectFiles = $this->ProjectFile->query("Insert into tags(project_id,title) values($projectID,'$tagtitle')");
							}
							
						}
					}

					// Check if form is submitted
					if ($prequesttype == 'projectinnerpage') 
					{
						// New design check below line code. Set the message and redirect to same page
						
						// when project adding from project home page
						/*$projTitle = $this->request->data['ProjectFile']['titleproj'];
						header('Location: http://designlab.espire.com/admin/projectfiles/'.$projTitle);
						exit();	
						*/

						$this->Session->setFlash(__('The project has been saved.'), 'default', array('class' => 'alert alert-success'));
						return $this->redirect(array('action' => 'index'));	
								
					} else {					
						// when project add from project listing page
						$this->Session->setFlash(__('The project has been saved.'), 'default', array('class' => 'alert alert-success'));
						return $this->redirect(array('action' => 'index'));				  	
					}

				} else {
					$this->Session->setFlash(__('The project could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
				}
				
				
			}
			$tags ='';
			if(isset($this->params->projectname))
			{
				/*_____ STRAT _____ Added new design , Query has been changed check below query _______
				*/
				//$projectFileId = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $this->params->projectname), 'fields' => array('id', 'title', 'project_folder'),'order' => array('id DESC')));				
				$projectFileId = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $this->params->projectname,'ProjectFile.status_id' =>array(1,2)), 'fields' => array('id', 'title', 'project_folder'),'order' => array('id ASC')));
				$tags = $this->ProjectFile->query("select distinct title from tags where project_id =".$projectFileId['ProjectFile']['id']);
			}
			$categories = $this->ProjectFile->Category->find('all', array('recursive' => 0, 'fields' => array('id', 'title'), 'conditions' => array('status_id' => 1)));
			$users = $this->ProjectFile->User->find('list');
			$statuses = $this->ProjectFile->Status->find('list', array('conditions' => array('id' => array(1,2))));
			
			//finding unique tag names for auto suggest on add page
			
			$allTags = $this->ProjectFile->query("select distinct title from tags where title<>''");
			
			
			//Debugger::dump($allTags);
			$projects = $this->ProjectFile->find('all', array('fields' => 'DISTINCT title'));
			foreach($projects as $project) {
				$uniqueProjects[] = '"'.$project['ProjectFile']['title'].'"';
			}
			$finalProjects = implode(",",$uniqueProjects);

			if(isset($this->params->projectname)){
				$title = $this->ProjectFile->find('list', array('conditions' => array('project_folder' => $this->params->projectname), 'limit' => 1));
				$this->set('title', $title);
			}
			$this->set(compact('categories', 'users', 'statuses', 'finalProjects','tags','allTags'));
		}
	}

	public function admin_edit() {
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProjectFile->save($this->request->data)) {
				$this->Session->setFlash(__('The project has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
	}

	public function admin_delete($id=null, $projectUrl=null, $categoryUrl=null) {
		if($categoryUrl != null) {
			$redirectUrl = $projectUrl.'/'.$categoryUrl;
		} else {
			$redirectUrl = $projectUrl;
		}
		
		/* _____ Added new design */
		$projectIdWithTagsTableData = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $projectUrl,'ProjectFile.status_id' =>array(1,2)), 'fields' => array('id', 'title', 'project_folder'),'order' => array('id ASC')));
		$projectIdWithTagsTableData = $projectIdWithTagsTableData["ProjectFile"]["id"];
		/*_____END________________*/
		
		
		$this->ProjectFile->recursive = -1;
		$project = $this->ProjectFile->find('first', array('conditions' => array('id' => $id), 'fields' => array('id', 'status_id')));
		if($project){
			$project['ProjectFile']['status_id'] = 3; //deleted
			if ($this->ProjectFile->save($project)) {
				
				/* _____________________________________________________
					Added new design |
					we are setting the project id in backend using the query (status in 1,2 and id asc) with first row.
					this project id is tagged with tags table when updating or insering tags.
					but if we are deleting this record then we have to update tags table mapping.
					which is below.
				*/
				
				$newProjectFileId = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $projectUrl,'ProjectFile.status_id' =>array(1,2)), 'fields' => array('id', 'title', 'project_folder'),'order' => array('id ASC')));
				$newProjectFileId = $newProjectFileId["ProjectFile"]["id"];
				//echo $id .'=='. $projectIdWithTagsTableData;die;
				if(!empty($newProjectFileId) && $newProjectFileId > 0 && $id == $projectIdWithTagsTableData){
					$returnStatus = $this->ProjectFile->query("update tags set project_id = ".$newProjectFileId." where project_id = ".$id);
					
				}
				/*____________________________________________________*/
				
				
				$this->Session->setFlash(__('The project has been Deleted.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => $redirectUrl));
			} else {
				$this->Session->setFlash(__('Some Error occurs please try again later.'), 'default', array('class' => 'alert alert-danger'));
				return $this->redirect(array('action' => $redirectUrl));
			}
		} else {
			throw new NotFoundException(__('Invalid project'));
		}
	}
	
	public function admin_deleteall($project_folder = null){
		$this->ProjectFile->recursive = -1;
		$projects = $this->ProjectFile->find('all', array('conditions' => array('project_folder' => $project_folder), 'fields' => array('id', 'status_id')));
		if($projects){
			foreach($projects as $project){
				$project['ProjectFile']['status_id'] = 3; //deleted
				$this->ProjectFile->save($project);		
			}
			$this->Session->setFlash(__('The project has been Deleted.'), 'default', array('class' => 'alert alert-success'));
			return $this->redirect(array('action' => 'index'));
		} else {
			throw new NotFoundException(__('Invalid project'));
		}
	}

	public function admin_recordListing() {
		$this->layout = 'ajax';
		$action = $this->request->data['action'];
		$updateRecordsArray = $this->request->data['recordsArray'];
		if ($action == "updateRecordsListings"){	
			$listingCounter = 1;
			foreach ($updateRecordsArray as $recordIDValue) {
				
				/*_____ added new design || bug | update recordListingID of all row of each project with same value _______*/
				$allProjectsData = $this->ProjectFile->find('first', array('conditions' => array('ProjectFile.id' => $recordIDValue)));
				if(!empty($allProjectsData["ProjectFile"])){
					$projectPath = $allProjectsData["ProjectFile"]["project_folder"];
					$allProjectsData = $this->ProjectFile->find('all', array('conditions' => array('ProjectFile.project_folder' => $projectPath)));
					foreach($allProjectsData as $key=>$projectDataRow){
						//$this->ProjectFile->recursive = -1;
						$projectRowId = $projectDataRow["ProjectFile"]["id"];
						$modifiedDate = date('Y:m:d H:i:s');
						//$this->ProjectFile->updateAll(array('ProjectFile.recordListingID' => $listingCounter,'ProjectFile.modified' => "'$modifiedDate'"), array('ProjectFile.id' => $projectRowId));	
						$this->ProjectFile->updateAll(array('ProjectFile.recordListingID' => $listingCounter), array('ProjectFile.id' => $projectRowId));	
					}
				}
				/*______________________END _____________________________*/
				
				//NOTE::-  if above code block will comment then open below line comment
				
				//$this->ProjectFile->updateAll(array('ProjectFile.recordListingID' => $listingCounter), array('ProjectFile.id' => $recordIDValue));	
				$listingCounter = $listingCounter + 1;
			}
		}
		pr($this->request->data);
		exit();
	}

	public function admin_sorting(){
		$projectFiles = $this->ProjectFile->find('all', array('conditions' => array('ProjectFile.status_id' => 1), 'order' => array('recordListingID' => 'ASC', 'ProjectFile.modified' => 'DESC')));
		$this->set('projectFiles', $projectFiles);
	}

	public function admin_changeTitle() {
		if ($this->request->is(array('post', 'put'))) {
			$projectFolder = str_replace(' ', '-', strtolower($this->request->data['ProjectFile']['title']));

			//finding all the projects which needs to be updated
			$this->ProjectFile->recursive = -1;
			$projects = $this->ProjectFile->find('all', array('conditions' => array('ProjectFile.project_folder' => $this->request->data['oldfolder']), 'fields' => array('id', 'title', 'project_folder')));

			foreach($projects as $key => $project) {
				$projects[$key]['ProjectFile']['title'] = $this->request->data['ProjectFile']['title'];
				$projects[$key]['ProjectFile']['project_folder'] = $projectFolder;
			}

			if ($this->ProjectFile->saveAll($projects)) {
				//rename project folder on server with the new name
				rename(WWW_ROOT.'files/'.$this->request->data['oldfolder'], WWW_ROOT.'files/'.$projectFolder);

				//updating thumbnail table
				$thumbnail = $this->Thumbnail->findByProjectName($this->request->data['oldfolder']);
				if($thumbnail) {
					$thumbnail['Thumbnail']['project_name'] = $this->request->data['ProjectFile']['title'];
					if($this->Thumbnail->save($thumbnail)) {
						$this->Session->setFlash(__('Project updated successfully'), 'default', array('class' => 'alert alert-success'));
						return $this->redirect(array('controller' => 'projectfiles', 'action' => 'view', 'projectfolder' => $projectFolder));
					}
				} else {
					$this->Session->setFlash(__('Project updated successfully'), 'default', array('class' => 'alert alert-success'));
					return $this->redirect(array('controller' => 'projectfiles', 'action' => 'view', 'projectfolder' => $projectFolder));
				}
			} else {
				$this->Session->setFlash(__('The project could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
	}
	/* download PDF created by Dharna 30th May 2016 */
	public function download_pdf($projectfolder,$categoryfolder,$datefolder){
      
	   $this->layout = 'pdf';
	 echo 
	 $this->Category->recursive = -1;
		$category = $this->Category->find('first', array('conditions' => array('category_folder' => $this->params->categoryfolder)));
		
		$this->ProjectFile->recursive = 0;
		if($this->Auth->user('role_id') == 1 || $this->Auth->user('role_id') == 2) {
			$projectfiles = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $this->params->projectfolder, 'category_id' => $category['Category']['id'], 'date_folder' => $this->params->datefolder, 'ProjectFile.status_id' => array(1,2,3,4))));
		} else {
			$projectfiles = $this->ProjectFile->find('first', array('conditions' => array('project_folder' => $this->params->projectfolder, 'category_id' => $category['Category']['id'], 'date_folder' => $this->params->datefolder, 'ProjectFile.status_id' => array(1,2))));
		
		}
		$thumbnail = $this->Thumbnail->findByProjectName($projectfiles['ProjectFile']['title']);
		//echo "<pre>";
		//print_r($thumbnail);
		
		 if(!empty($projectfiles)){
			$path = 'files/'.$projectfolder.'/'.$categoryfolder.'/'.$datefolder;
			
			$dir = new Folder($path);
			$files = $dir->read(true, array('files', ''));

			$html = '';
			$gallery = '';
			
		}
		$this->set(compact('files', 'path', 'projectfiles','thumbnail'));
		
			
		$this->render()->type('application/pdf');
		// $this->render('viewdetails1');
	 
    }
	
	
	
	//for multiple projects to be changed at once
	/*public function admin_setstatus($project_folder = null, $status = null) {
		$this->layout = 'ajax';
		$this->ProjectFile->recursive = -1;
		$projects = $this->ProjectFile->find('all', array('conditions' => array('project_folder' => $project_folder), 'fields' => array('id', 'status_id')));
		if($projects){
			foreach($projects as $project){				
				$project['ProjectFile']['status_id'] = $status;
				$this->ProjectFile->save($project);
			}
		}
		echo $status;
        exit;
	}*/

	/*public function admin_edit($id = null) {
		if (!$this->ProjectFile->exists($id)) {
			throw new NotFoundException(__('Invalid project file'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['ProjectFile']['user_id'] = $this->Auth->user('id');
			$this->request->data['ProjectFile']['status_id'] = 2; //Not Active
			$this->request->data['ProjectFile']['project_folder'] = str_replace(' ', '-', strtolower($this->request->data['ProjectFile']['title']));
			if ($this->ProjectFile->save($this->request->data)) {
				$this->Session->setFlash(__('The project has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('ProjectFile.' . $this->ProjectFile->primaryKey => $id));
			$this->request->data = $this->ProjectFile->find('first', $options);
		}
		$categories = $this->ProjectFile->Category->find('all', array('recursive' => 0, 'fields' => array('id', 'title'), 'conditions' => array('status_id' => 1)));
		$users = $this->ProjectFile->User->find('list');
		$statuses = $this->ProjectFile->Status->find('list', array('conditions' => array('id' => array(1,2))));
		$this->set(compact('categories', 'users', 'statuses'));
	}*/
}
