<?php
App::uses('AppController', 'Controller');

class ThumbnailsController extends AppController {

	public $components = array('Paginator');

	public function admin_add() {
		if ($this->request->is('post')) {
			$project = $this->Thumbnail->findByProjectName($this->request->data['Thumbnail']['project_name']);
			if($project){
				$this->Thumbnail->id = $project['Thumbnail']['id'];
				if ($this->Thumbnail->save($this->request->data)) {
					$this->Session->setFlash(__('The thumbnail has been saved.'), 'default', array('class' => 'alert alert-success'));
					return $this->redirect(array('controller' => 'projectfiles', 'action' => 'index'));
				} else {
					$this->Session->setFlash(__('The thumbnail could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
					return $this->redirect(array('controller' => 'projectfiles', 'action' => 'index'));
				}
			} else {
				$this->Thumbnail->create();
				if ($this->Thumbnail->save($this->request->data)) {
					$this->Session->setFlash(__('The thumbnail has been saved.'), 'default', array('class' => 'alert alert-success'));
					return $this->redirect(array('controller' => 'projectfiles', 'action' => 'index'));
				} else {
					$this->Session->setFlash(__('The thumbnail could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
					return $this->redirect(array('controller' => 'projectfiles', 'action' => 'index'));
				}
			}
		}
	}

	/*public function admin_index() {
		$this->Thumbnail->recursive = 0;
		$this->set('thumbnails', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->Thumbnail->exists($id)) {
			throw new NotFoundException(__('Invalid thumbnail'));
		}
		$options = array('conditions' => array('Thumbnail.' . $this->Thumbnail->primaryKey => $id));
		$this->set('thumbnail', $this->Thumbnail->find('first', $options));
	}*/

	/*public function admin_edit($id = null) {
		if (!$this->Thumbnail->exists($id)) {
			throw new NotFoundException(__('Invalid thumbnail'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Thumbnail->save($this->request->data)) {
				$this->Session->setFlash(__('The thumbnail has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The thumbnail could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Thumbnail.' . $this->Thumbnail->primaryKey => $id));
			$this->request->data = $this->Thumbnail->find('first', $options);
		}
	}

	public function admin_delete($id = null) {
		$this->Thumbnail->id = $id;
		if (!$this->Thumbnail->exists()) {
			throw new NotFoundException(__('Invalid thumbnail'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Thumbnail->delete()) {
			$this->Session->setFlash(__('The thumbnail has been deleted.'));
		} else {
			$this->Session->setFlash(__('The thumbnail could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}*/
}
