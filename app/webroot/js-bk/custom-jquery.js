jQuery(document).ready(function($) {
    //for changing status(projects and user status)//
	$('.jquery_action_status').click( function(){
        var buttonId = $(this).attr('id'),
            d = buttonId.split('_'),
            action = d[0],
            id = d[1],
            controller= d[2],
            status = $(this).attr('rel'),
            setUrl = '/admin/'+controller+'/setstatus/'+ id + '/'+ status;

        $.ajax({
            type: "GET",
            url: setUrl,
            success: function(data) {
                if( data == 2 ) {
                    if(action == 'publish'){ 
                        projectStatus('#'+buttonId, 'unpublish.png', 1, 'Publish again', 'active');
                    } else if(action == 'protected'){ 
                        projectStatus('#'+buttonId, 'unpublish.png', 4, 'Protected', 'error');
                    } else {
                        userStatus('#'+buttonId, 'unpublish.png', 1);
                    }
                } else if( data == 1) {
                    if(action == 'publish'){
                        projectStatus('#'+buttonId, 'publish.png', 2, 'Unpublish', 'active');
                    } else {
                        userStatus('#'+buttonId, 'publish.png', 2);
                    }
                } else if ( data == 4 ) {
                    projectStatus('#'+buttonId, 'publish.png', 2, 'Unprotected', 'error');
                }
            }
        });
        return false;
    });

    //for project file add page (add project name)
    var siteName = '/';
    jQuery('.jquery_project_add').click(function(){
        $('.modal-body .panel-body .alert').remove();
        var projectTitle = $('#ProjectTitle').val();
        var projectDescription = $('#ProjectDescription').val();
        var setUrl = siteName+'admin/projects/add/'+projectTitle+'/'+projectDescription;
        jQuery.ajax({
            type: "GET",
            url: setUrl,
            success: function(data) {
                if( data == 'success' ){
                    jQuery('.modal-body .panel-body').prepend('<div class="alert alert-success">Project has been added successfully.</div>');
                } else if( data == 'validation-error' ){
                    jQuery('.modal-body .panel-body').prepend('<div class="alert alert-danger">Please enter the Project Name</div>');
                } else {
                    jQuery('.modal-body .panel-body').prepend('<div class="alert alert-danger">Some error occurs please try again later.</div>');
                }
            }
        });
        return false;
    });

    jQuery('#sample-modal-dialog-1 .modal-header .close').click(function(){
        window.location = siteName+"admin/projectfiles/add";
    });

    //for project file index page (add thumbnail)
    $('.ui-sortable').on('click', '.addThumBtn', function(){
        var title = $(this).attr('id');
        $('#ThumbnailProjectName').remove();
        $('#projectThumb fieldset').prepend('<input type="hidden" id="ThumbnailProjectName" maxlength="250" name="data[Thumbnail][project_name]" value="'+title+'" >');
    });

    //for publish project
    jQuery('.publishProject').click(function(){
        var id = $(this).attr('id');
        var projectName = $(this).attr('projectname');
        var categoryName = $(this).attr('categoryname');

        $('#projectFileId, #projectFileName, #projectFileCategoryName').remove();
        $("#publishModal .modal-body #projectPublishTitle").val ('');
        $("#publishModal .modal-body #projectDescription").val ('');
        $("#publishModal .modal-body #projectTags").val ('');
        jQuery('#publishModal .modal-body').prepend('<input type="hidden" id="projectFileCategoryName" name="data[ProjectFile][categoryname]" value="'+categoryName+'" >');
        jQuery('#publishModal .modal-body').prepend('<input type="hidden" id="projectFileName" name="data[ProjectFile][projectname]" value="'+projectName+'" >');
        jQuery('#publishModal .modal-body').prepend('<input type="hidden" id="projectFileId" name="data[ProjectFile][id]" value="'+id+'" >');

        //for radio buttons
        $('#inlineRadio1').prop('checked', true);
    });

    //for edit/publish project
    jQuery('.editProject').click(function(){
        var title = $(this).attr('title');
        var desc = $(this).attr('desc');
        var id = $(this).attr('id');
        var tags = $(this).attr('tags');
        var projectName = $(this).attr('projectname');
        var categoryName = $(this).attr('categoryname');
        var url = $(this).attr('url');

        $('#projectFileId, #projectFileName, #projectFileCategoryName').remove();
        $("#publishModal .modal-body #projectPublishTitle").val (title);
        $("#publishModal .modal-body #projectDescription").val (desc);
        $("#publishModal .modal-body #projectUrl").val (url);
        $("#publishModal .modal-body #projectTags").val (tags);
        jQuery('#publishModal .modal-body').prepend('<input type="hidden" id="projectFileCategoryName" name="data[ProjectFile][categoryname]" value="'+categoryName+'" >');
        jQuery('#publishModal .modal-body').prepend('<input type="hidden" id="projectFileName" name="data[ProjectFile][projectname]" value="'+projectName+'" >');
        jQuery('#publishModal .modal-body').prepend('<input type="hidden" id="projectFileId" name="data[ProjectFile][id]" value="'+id+'" >');

        //for radio buttons
        var radio = $(this).attr('radio');
        if(radio == 'filter_portfolio filter_uncategorized width-and-small isotope-item  brick-with-img all_cat'){
            $('#inlineRadio1').prop('checked', true);
        } else if(radio == 'filter_blog filter_branding filter_portfolio filter_uncategorized width-and-height isotope-item  brick-with-img all_cat1'){
            $('#inlineRadio2').prop('checked', true);
        } else if(radio == 'filter_blog filter_branding filter_portfolio filter_uncategorized width-and-long isotope-item  brick-with-img all_cat2'){
            $('#inlineRadio3').prop('checked', true);
        } else if(radio == 'filter_blog filter_photography-2 filter_portfolio filter_slider filter_uncategorized width-and-big isotope-item  brick-with-img all_cat3'){
            $('#inlineRadio4').prop('checked', true);
        }
    });
    
    //for edit project file (uploading files again)
    $('.editProjectFiles').click(function() {
        $('#editModal .modal-body #projectFileId').remove();
        var id = $(this).attr('id');
        var brief = $(this).attr('brief');
        $('#editModal .modal-body').prepend('<input type="hidden" id="projectFileId" name="data[ProjectFile][id]" value="'+id+'" >');
        $('#editModal .modal-body #ProjectFileProjectBrief').val(brief);
    });

    //$('.publishProject, .deleteProject').hide();
    $('.projectTable table tr td').hover(
        function(){
           $(this).find('.pop').show(); 
           //$(this).find('.publishProject, .deleteProject').show();
        }, function(){
            $(this).find('.pop').hide();
            //$(this).find('.publishProject, .deleteProject').hide();
        }
    );

    //for dynamic action in privileges controller//
    $('#PrivilegeController').change( function(){
        var controller = $(this).val();
        var setUrl = '/privileges/listactions/'+controller;
        $.ajax({
            type: "GET",
            url: setUrl,
            success: function(data) {
                if( data ){
                    $('#PrivilegeAction').html(data);
                }
            }
        });
        return false;
    });

    //for edit projects on project view page
    $('h1').dblclick(function(){
        $(this).hide();
        $(this).next('form').show();
        $(this).next('form').find('input[type="text"]').focus();
    });

    $('#ProjectFileEditTitle').blur(function() {
        var title = $('h1').text();
        if(title !== $('#ProjectFileEditTitle').val()) {
            $("#ProjectFileAdminViewForm").submit();
        } else {
            $('#ProjectFileAdminViewForm').hide();
            $('h1').show();
        }
    });

    //for projects tags on different pages
    if($('#projectTags').html() !== undefined) {
        $('#projectTags').tagsInput({width:'auto'});
    }
});

function projectStatus(id, img, rel, text, className) {
    $(id).attr('rel', rel);
    $(id).html('<img src="/img/'+img+'" alt="" width="17" height="17"> ' + text);

    if(rel === 1) {
        $(id).parent('.pop').siblings('a').removeClass(className);
        $(id).siblings('.editProject').hide();
        $(id).siblings('.deleteProject, .jquery_action_status').removeClass('hide');
        $(id).siblings('.jquery_action_status').attr('rel', 4);
    } else if(rel === 2) {
        $(id).parent('.pop').siblings('a').removeClass('error').addClass(className);
        $(id).siblings('.editProject').show();
        if(className === 'active') {
            $(id).siblings('.deleteProject, .jquery_action_status').addClass('hide');
            $(id).siblings('.jquery_action_status').html('<img src="/img/unpublish.png" alt="" width="17" height="17"> Protected');
        }
    } else if(rel === 4) {
        $(id).parent('.pop').siblings('a').removeClass(className);
    }
}

function userStatus(id, img, rel) {
    $(id).attr('rel', rel);
    $(id).html('<img src="/img/'+img+'" alt="">');
}