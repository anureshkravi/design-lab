'use strict';

var Ticket = (function() {
	var DEFAULTS = {
		url: '/admin/tickets'
	}

	return {
		initialize: function() {
			this.bindUIActions();
		},
		bindUIActions: function() {
		    $('h2').click(function() {
		    	alert();
		    });
		},
		ajaxCall: function(data, method, url, callback) {
			return $.ajax({
	            url: url,
	            type: method,
	            data: {Ticket: data},
	            success: function (data) {
	            	callback(JSON.parse(data));
	            },
	            error: function() {
	            	console.log('Ajax Error');
	            }
	        });
		},
		approve: function(id) {
			this.ajaxCall({id: id, status_id: 5}, 'POST', DEFAULTS.url+'/approve/', this.assignForm);
			return this;
		},
		assignForm: function(response) {
			var users = response.users;
			var options='';
			var key;
			for(key in users) {
				options += '<option value="'+key+'">'+users[key]+'</option>';
			}
			var assignForm = '<div class="form-inline">'+
								'<div class="form-group">'+
									'<select id="users" class="form-control" required>'+
										'<option value="">Select User</option>'+options+
									'</select>'+
								'</div>'+
								'<button type="button" class="btn btn-success assign" data-id="'+response.ticketId+'">Assign</button>'+
							'</div>';
			$('#action_'+response.ticketId).html(assignForm);
		},
		assign: function(assignedId, ticketId) {
			this.ajaxCall({id: ticketId, assigned_id: assignedId, status_id: 11}, 'POST', DEFAULTS.url+'/assign/', this.updateTickets);
			return this;
		},
		decline: function(ticketId) {
			this.ajaxCall({id: ticketId, status_id: 7}, 'POST', DEFAULTS.url+'/decline/', this.updateTickets);
			return this;
		},
		updateTickets: function(ticket) {
			Ticket.deleteRequestedTicket(ticket);
			//adding assigned ticket to assigned list
			var assignedTicket = '<tr id="assignTicket_'+ticket.Ticket.id+'">'+
									'<td>'+ticket.Ticket.project+'</td>'+
									'<td>'+ticket.Assigned.username+'</td>'+
									'<td>'+ticket.Ticket.requested_time+' hrs</td>'+
									'<td>'+ticket.Ticket.time_spent+' hrs</td>'+
									'<td class="status">'+ticket.Status.title+'</td>'+
									'<td class="ticket-actions">';
									if(ticket.Status.id != 7) {
										assignedTicket += '<a class="btn btn-primary start" href="#" role="button" data-id="'+ticket.Ticket.id+'">Start</a>';
									}										
				assignedTicket += '<a class="btn btn-warning reassign" href="#" role="button" data-id="'+ticket.Ticket.id+'">Re-assign</a>'+
					              '</td>'+
								'</tr>';
			$('.assigned-projects tbody').prepend(assignedTicket);
		},
		deleteRequestedTicket: function(data) {
			var ticket = $('#ticket_'+data.Ticket.id),
				ticketPanel = ticket.parents('.panel'),
				requestCount = ticketPanel.find('.ticketCount');

			if(parseInt(requestCount.html()) == 1) {
				ticketPanel.remove();
			} else {
				requestCount.html(parseInt(requestCount.html()) -1);
				ticket.remove();
			}
		},
		completed: function(ticketId) {
			this.ajaxCall({id: ticketId, status_id: 8}, 'POST', DEFAULTS.url+'/completed/', this.updateStatus);
			return this;
		},
		hold: function(ticketId) {
			this.ajaxCall({id: ticketId, status_id: 9}, 'POST', DEFAULTS.url+'/completed/', this.updateStatus);
			return this;
		},
		start: function(ticketId) {
			this.ajaxCall({id: ticketId, status_id: 10}, 'POST', DEFAULTS.url+'/start/', this.updateStatus);
			return this;			
		},
		remove: function(ticketId) {
			return this;
		},
		reassign: function(ticketId) {
			return this;
		},
		updateStatus: function(ticket) {
			$('tr#assignTicket_'+ticket.Ticket.id+' .status').html(ticket.Status.title);
			//updating buttons as per the ticket status
			/*7=decline, 8=completed, 9=hold, 10=process, 11=assign*/
			if(ticket.Status.id != 7 && ticket.Status.id != 8 && ticket.Status.id != 9 && ticket.Status.id != 11) {
				var actions = '<a class="btn btn-success completed" href="#" role="button" data-id="'+ticket.Ticket.id+'">Completed</a>'+
        					'<a class="btn btn-danger hold" href="#" role="button" data-id="'+ticket.Ticket.id+'">Hold</a>';
				$('tr#assignTicket_'+ticket.Ticket.id+' .ticket-actions').html(actions);
			} 
			if(ticket.Status.id != 7 && ticket.Status.id != 10) {
				console.log(1);
				var actions = '<a class="btn btn-primary start" href="#" role="button" data-id="'+ticket.Ticket.id+'">Start</a>';
				$('tr#assignTicket_'+ticket.Ticket.id+' .ticket-actions').html(actions);
			}
			var ressignBtn = '<a class="btn btn-warning reassign" href="#" role="button" data-id="'+ticket.Ticket.id+'">Re-assign</a>';
			$('tr#assignTicket_'+ticket.Ticket.id+' .ticket-actions').append(ressignBtn);
		}
	};

})();

Ticket.initialize();