﻿function showPopUp(url,w,h)
{
    window.open(url,'_blank','width=' + w + ',height=' + h + ',scrollbars=yes,menubars=no,toolbars=no');
}

function showMessage(show)
{
    if(show == "True")
    {
        alert('You must accept the Conditions of Use in order to proceed.');
    }    
}

 function showChangePasswordMessage(url,message)
{                     
    alert(message);
    
    window.open(url,'_self');
}


function CallPrint(strid)
{
    var winPage = window.open ('','','left=1px,top=1px,toolbar=0,scrollbars=0,status=0');            
    winPage.document.write(document.getElementById(strid).innerHTML);
    
    var browser=navigator.appName;          
    var fileref=winPage.document.createElement("link");
  
     fileref.rel = "stylesheet";
     fileref.type = "text/css";       
     fileref.href = "/App_Themes/print.css";  

    if (browser=="Microsoft Internet Explorer")
    {
   
        if (typeof fileref!="undefined")                                                           
        winPage.document.getElementsByTagName("head").item(0).appendChild(fileref);
                                 
    }
    else
    {
        if (typeof fileref!="undefined")
        winPage.document.getElementsByTagName("head")[0].appendChild(fileref);
    }                                                                               
                             
    winPage.document.close();
    winPage.focus();
    //call print
    winPage.print();
    winPage.close();
   }

function bannerClickThrough()
{
    try {
        var titleTag = titleTagName();
        if (titleTag.indexOf("Toyota") >= 0) {
            showPopUp('http://www.toyota.com.au/finance/insurance/motor-vehicle', '1000px', '750px');
            ga('send', 'event', 'Customer Portal', 'Toyota Banner Image', 'Banner Click', 1);
        }
        else if (titleTag.indexOf("Lexus") >= 0)
            showPopUp('http://www.lexus.com.au/services/finance/', '1000px', '750px');
        }
        catch (e)
        {
	        showPopUp('http://www.toyotafinance.com.au','1000px','750px');
        }
}
function showFAQPage() {
    try {
        var toyotaFAQURL = $(".toyotaFAQ").val();
        var lexusFAQURL = $(".lexusFAQ").val();
        var titleTag = titleTagName();
        if (titleTag.indexOf("Toyota") >= 0) {
            showPopUp(toyotaFAQURL, '1000px', '750px');
            
        }
        else if (titleTag.indexOf("Lexus") >= 0)
            showPopUp(lexusFAQURL, '1000px', '750px');
    }
    catch (e) {
        throw e;
    }
}
function showLoginFAQPage(faqURL) {
    try {
        var titleTag = titleTagName();
        if (titleTag.indexOf("Toyota") >= 0) {
            showPopUp(faqURL, '1000px', '750px');

        }
        else if (titleTag.indexOf("Lexus") >= 0)
            showPopUp(faqURL, '1000px', '750px');
    }
    catch (e) {
        throw e;
    }

}
function showFAQIndividualSection(questName) {
    try {
        var toyotaFAQURL = $(".toyotaFAQ").val();
        var lexusFAQURL = $(".lexusFAQ").val() + questName;
        var titleTag = titleTagName();
        if (titleTag.indexOf("Toyota") >= 0) {
            showPopUp(toyotaFAQURL, '1000px', '750px');
        }
        else if (titleTag.indexOf("Lexus") >= 0)
            showPopUp(lexusFAQURL, '1000px', '750px');
    }
    catch (e) {
        throw e;
    }
}


function titleTagName() { 
    var browser = navigator.appName;
    var titleTag;
    if (browser == "Microsoft Internet Explorer") {
        titleTag = document.getElementsByTagName("title").item(0).innerText;
    }
    else if (browser == "netscape" || browser == "Netscape") {
        if (document.title.indexOf("Lexus") >= 0) {
            titleTag = "Lexus";
        }
        else {
            titleTag = "Toyota";
        }
    }
    else {
        titleTag = document.getElementsByTagName("title")[0].innerText;
    }
    return titleTag;
}

