//frontend UI Events only
$(document).ready(function() {
    // submit button click
    $('#sub_Req_Btn').click(function() {
        $(this).animate({
            left: '-1000px',
            right: '1000px',
            opacity: '0.9'
        }, 400, function() {
            $('#form_Ticket').fadeIn(1000);
        });
    });

    //cancel button
    $('#cancel_new_ticket').click(function() {
        $('#form_Ticket').fadeOut('500', function() {

        if (screen.width < 480) {
             $('#sub_Req_Btn').css("left", "6%");
        }
        else {
            $('#sub_Req_Btn').animate({
                left: '17%',
                opacity: '1'
            });
        }

        });
    });


    // Trace button click
    $('#Trace_Req_Btn').click(function() {
        $(this).animate({
            right: '-1000px',
            left: '1000px',
            opacity: '0.9'
        }, 400, function() {
            timeoutfunc();
        });
    });

    // coming soon container
    function timeoutfunc() {
        $('#right_login').fadeIn(1000);
        setTimeout(function() {
            $('#right_login').fadeOut(500);
            reverseTrace();
        }, 3000);
    }

    //Flash message disappear after 3 seconds
    setTimeout(function() {
            $('#flashMessage').fadeOut(500);
            }, 3000);
    

    // return trace request button
    function reverseTrace() {

        if (screen.width < 480) {
             $('#Trace_Req_Btn').css("left", "6%");
        }
        else {
            $('#Trace_Req_Btn').animate({
                right: '15%',
                left: '15%',
                opacity: '1'
            }, 400);
        }


    }

    //radio ui style
    function customRadio(radioName){
        var radioButton = $('input[name="'+ radioName +'"]');
        $(radioButton).each(function(){
            $(this).wrap( "<span class='custom-radio'></span>" );
            if($(this).is(':checked')){
                $(this).parent().addClass("selected");
            }
        });
        $(radioButton).click(function(){
            if($(this).is(':checked')){
                $(this).parent().addClass("selected");
            }
            $(radioButton).not(this).each(function(){
                $(this).parent().removeClass("selected");
            });
        });
    }
    $(document).ready(function (){
        customRadio("data[Ticket][billable]");
    })


});