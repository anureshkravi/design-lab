var count = 1,
    wireframLatest,
    creativesLatest,
    htmlFrame,
    asseFrame,
    oldHistory;

$(document).ready(function() {
    initialize();

    //skip Button
    $('#skip').click(function(){
        slidego();
    });

    //gotit Button
    $('#gotit').click(function(){
        $.cookie('project-overlay', 1, { expires: 365, path: '/' });
        slidego();
    });

    //replay Button
    $('#replay').click(function(){
        //hiding current slide and showing slide 1
        $('#slide'+count).hide();
        $('#slide1').show();

        //activate next button again
        $('#next').show();
        $('#replay').hide();
        count = 1;
    });

    //Project Preview
    $('#next').click(function() {
        count++;
        checkSlide(count);
        $('#slide'+(count-1)).hide();
        $('#slide'+count).show();
        $('#Prev').show();
        if(count > 4){
            $(this).hide();                        
        }
    });

    $('#Prev').click(function() {
        count--;
        checkSlide(count);
        $('#slide'+(count+1)).hide();
        $('#slide'+count).show();
        if(count < 2){
           $(this).hide();
        }
        if(count < 5){
            $('#replay').hide();
            $('#next').show();
        }                    
    });

    //Help button click coming mockup sliding
    $('.needmeCreative a').click(function(){
    $('body').addClass('stop-scrolling'); 
     $('.overLay').fadeIn(1000);
     count=1;
     $('.slidecontainer').hide();
     $('#slide'+count).show();
     checkSlide(count);
    });
});

function initialize() {
    wireframLatest = $( ".wireFramLatestPro td" ).first().html();
    creativesLatest = $( ".creativeLatestPro td" ).first().html(),
    htmlFrame = $(".htmlLatestPro td" ).first().html(),
    asseFrame = $(".assetsLatestPro td" ).first().html(),
    oldHistory = $('.oldHistoryPro').html(),
    oldHistorycode = $('.oldHistoryPro tr:eq(2)').html();
}


//Set Cookies
setTimeout(function(){
    if($.cookie('project-overlay') === undefined) {
        if ($(window).width() >= 1024){
            $('body').addClass('stop-scrolling'); 
            $('.overLay').fadeIn(2000);
            $('.mainContainer').css({'top': $(".project-detail").position().top-13, 'position': 'relative'}); 
            if(wireframLatest.length > 0){
                $('.overLay .wireframLatestproApp td').append(wireframLatest);
                $('.wireframLatestproApp, #slide1 .topwireLeft').show();
            }
        }
    }                              
}, 1000);

$(window).resize(function(){
    if ($(window).width() < 1024){  
        $('.overLay').hide();
    }
});

//Slide out width oval effect
function slidego(){
    $('.overLay').fadeOut('fast', function(){
        $('#Prev, #replay').hide();
        $('#next').show();
        $('body').removeClass('stop-scrolling'); 
        $('.needmeCreative').show();
        $('.circleAni').animate({
            opacity: '0.3',
            height: '100px',
            right: '-50px',
            top: '-50px',
            width: '100px'
        },300, function(){
            $('.circleAniText').slideDown();
            setTimeout(function() {
          $('.circleAniText').fadeOut('slow');
            }, 3000);
        });

    });           
}

function checkSlide(count) {
    //Creatives Slide

    if(count === 2 && creativesLatest.length > 0) {
        $('.overLay .creativeLatestProApp td').html(creativesLatest);
        $('.creativeLatestProApp, #slide2 .topwireLeft').show();
    }
    //Html Slide
    if(count === 3 && htmlFrame.length > 0){
        $('.htmlLatestProApp td').html(htmlFrame);
        $('.overLay .htmlLatestProApp, #slide3 .htmlMockupChild').show();
    }
    // Assets Slide
    if(count === 4 && asseFrame.length > 0 ){

        $('.assetsLatestProApp td').html(asseFrame);
        $('.overLay .assetsLatestProApp, #slide4 .assestMockupChild').show();
    }
    // Old History Slide
    if(count === 5 && oldHistorycode !== undefined && oldHistorycode.length > 0 ){
        $('.oldHistoryProApp').append(oldHistory).show();
        $('#slide5 .oldMockupChild').show();
    }
    if(count === 5) {
        $(this).hide();
        $('#replay').show();
    }
}

//Escape Command
$(document).on('keyup',function(evt) {
    if (evt.keyCode == 27){
        slidego();
    }
});