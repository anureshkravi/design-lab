var windowWidth = $(window).width(),
    windowHeight = $(window).height(),
    trackNotification = 0,
    rgImage,
    imageResizeWidth,
    imageOriginalWidth,
    imageResizeHeight,
    imageOriginalHeight,
    timer;

$(document).ready(function() {
    //fetching comments from the server for thumb counts
    var project_id = $('.es-carousel li a').attr('data-project');
    $.ajax({
        type: "GET",
        url: '/comments/viewAll/'+project_id+'/'+$.cookie('cookie_id'),
        success: function(data) {
            var data = JSON.parse(data);
            for(comment in data) {
                var classname = 'read';
                for(value in data[comment]) {
                    //console.log(data[comment][value]['Comment']['classname']);
                    if(data[comment][value]['Comment']['classname'] === 'unread') {
                        classname = 'unread';
                    }
                }

                var count = Object.keys(data[comment]).length;
                $('.es-carousel li a#'+comment).append('<span class="commentsCount '+classname+'">'+count+'</span>');
            }
        }
    });

    //opening comment box if someone click anywhere on the image
    $(document.body).on('click', 'div.rg-image img' ,function(e){
        if($('body').hasClass('comments-active') && $('.comments-container').css('display') === 'none' && !$('.notification-container').hasClass('active')) {
            
            var projectId = $(this).attr('data-project'),
                thumbId = $(this).attr('data-thumb'),
                x_axis = e.pageX, //page x axis will use for css
                y_axis = e.pageY, //page y_axis will use for css
                css_x = Math.floor(x_axis - $(this).offset().left),
                css_y = Math.floor(y_axis - $(this).offset().top),
                data_x = (imageResizeWidth/imageOriginalWidth) * css_x, //will store in DB
                data_y = (imageResizeHeight/imageOriginalHeight) * css_y; //will store in DB

            //storing actual point as per the orignal width
            if(imageOriginalWidth > imageResizeWidth) {
                data_x = (imageOriginalWidth/imageResizeWidth) * css_x;
            }

            //storing actual point as per the orignal height
            if(imageOriginalHeight > imageResizeHeight) {
                data_y = (imageOriginalHeight/imageResizeHeight) * css_y;
            }

            //moving comment window as per the click event
            if(windowWidth - x_axis < 405) {
                css_x = css_x - 400;
            }
            if(windowHeight - (windowHeight - y_axis) < 205) {
                css_y = css_y + 40;
            }

            $('.name-container').hide();
            $('.comments-container .user-comments').html('').hide();
            $('.comments-container').css({top: css_y, left: css_x}).attr({'data-x': data_x, 'data-y': data_y, 'data-project': projectId, 'data-thumb': thumbId}).show();
            $('.comments-container #comment').focus();
        } else {
            hideCommentBox();
            hideNotification();
        }
    });

    //comments points/circle with count
    $(document.body).on('click', '.comments' ,function(){
        var self = this,
            projectId = $(this).attr('data-project'),
            cookie_id = $.cookie('cookie_id'),
            thumbId = $(this).attr('data-thumb'),
            x_axis = parseInt($(this).attr('data-x')), //for data-x (storing in database)
            css_x_axis = parseInt($(this).css('left'))+25, //for css position
            y_axis = parseInt($(this).attr('data-y')), //for data-y (storing in database)
            css_y_axis = parseInt($(this).css('top'))+40; //for css position

        if(windowWidth - css_x_axis < 405) {
            css_x_axis = css_x_axis - 425;
        }
        if(windowHeight - (windowHeight - css_y_axis) < 205) {
            css_y_axis = css_y_axis + 5;
        }

        //fetching records from the server
        $.ajax({
            type: "GET",
            url: '/comments/viewByAxis/'+x_axis+'/'+y_axis+'/'+projectId+'/'+thumbId+'/'+cookie_id,
            success: function(data) {
                var comments = JSON.parse(data);
                var userComments = '';
                $.each( comments, function( key, value ) {
                    userComments += '<div class="client-comments-container"><p class="date">'+key+'</p>';
                    $.each(value, function( index, value ) {
                        userComments += '<div class="client-comments clearfix">'+
                                        '<strong>'+value['Comment']['name']+'</strong>'+
                                        '<p class="time">'+value['Comment']['time']+'</p>';
                                        if(value['Comment']['cookie_id'] === cookie_id) {
                                            userComments += '<p class="removeComment" data-id="'+value['Comment']['id']+'">Remove</p>';
                                        }
                        userComments += '<p class="client-comments-content">'+value['Comment']['comment']+'</p></div>';
                    });
                    userComments += '</div>';
                });

                //empty comment textarea
                $('.comments-container #comment').val('');

                //inserting comments into the comment box
                $('.comments-container .user-comments').html(userComments).show();
                $('.comments-container').css({top: css_y_axis, left: css_x_axis}).attr({'data-x': x_axis, 'data-y': y_axis, 'data-project': projectId, 'data-thumb': thumbId}).show();
                $('.comments-container #comment').focus();

                //updating read/unread classes
                checkClass(x_axis, y_axis, thumbId);
            }
        });
    });
    
    //focus/change event for comment box
    $('#comment').on('keydown', function() {
        if($.cookie('user') === undefined || $.cookie('email') === undefined) {
            //opening name modal for user
            var x_axis = $(this).parents('.comments-container').css('left').split('px');
            var y_axis = $(this).parents('.comments-container').css('top').split('px');
            $('.name-container').css({left:parseInt(x_axis[0])+60, top:parseInt(y_axis[0])+10}).show();
            $('.name-container #name').focus();
            //disabling submit button of comments container
            $('#addComment').attr('disabled', 'disabled');
        }
    });

    //user name form/creating cookie for user name
    $('#name-form').submit(function(e) {
        e.preventDefault();
        var name = $('#name').val();
        var email = $('#email').val();

        if(name !== '' && email !== '') {
            $.cookie('user', name, {expires: 365, path: '/'});
            $.cookie('email', email, {expires: 365, path: '/'});
            $('.name-container').remove();
            //comments container
            $('.comments-container #comment').focus();
            $('#addComment').removeAttr('disabled');
        } else {
            return false;
        }
    });

    $('#cancelCommenting').click(function() {
        hideCommentBox();
        return false;
    });

    //adding comments
    $('#comment-form').submit(function(e) {
        e.preventDefault();
        var name = $.cookie('user'),
            email = $.cookie('email'),
            cookie_id = $.cookie('cookie_id'),
            comment = $('#comment').val(),
            x_axis = $(this).parents('.comments-container').attr('data-x'),
            y_axis = $(this).parents('.comments-container').attr('data-y'),
            css_x = x_axis,
            css_y = y_axis,
            projectId = $(this).parents('.comments-container').attr('data-project'),
            thumbId = $(this).parents('.comments-container').attr('data-thumb'),
            formData = {name: name, email: email, cookie_id: cookie_id, comment: comment.replace(/\r?\n/g, '<br />'), x_axis: x_axis, y_axis: y_axis, project_id: projectId, thumb_id: thumbId, status_id: 1};

        if(imageOriginalWidth > imageResizeWidth) {
            css_x = (imageResizeWidth/imageOriginalWidth) * css_x;
        }

        if(imageOriginalHeight > imageResizeHeight) {
            css_y = (imageResizeHeight/imageOriginalHeight) * css_y;
        }

        if(name !== undefined) {
            $.ajax({
                type: "POST",
                url: '/comments/addComment',
                dataType : "json",
                data: JSON.stringify(formData),
                success: function(data) {
                    if($(".user-comments p.date:contains('"+data['Comment']['date']+"')").length > 0) {
                        var userComments = '<div class="client-comments clearfix">'+
                                           '<strong>'+data['Comment']['name']+'</strong>'+
                                           '<p class="time">'+data['Comment']['time']+'</p>'+
                                           '<p class="client-comments-content">'+data['Comment']['comment']+'</p>'+
                                           '</div>';
                        $( "p.date:contains('"+data['Comment']['date']+"')" ).parent('.client-comments-container').append(userComments);
                    } else {
                        var userComments = '<div class="client-comments-container">'+
                                                '<p class="date">'+data['Comment']['date']+'</p>'+
                                                '<div class="client-comments clearfix">'+
                                                    '<strong>'+data['Comment']['name']+'</strong>'+
                                                    '<p class="time">'+data['Comment']['time']+'</p>'+
                                                    '<p class="client-comments-content">'+data['Comment']['comment']+'</p>'+
                                                '</div>'+
                                            '</div>';
                        //$('.user-comments').show().append(userComments);
                    }
                    $('#comment-form').trigger("reset");

                    //incrementing the value in the circle or inserting the comments count
                    var commentCircle = $('.comments[data-x="'+x_axis+'"][data-y="'+y_axis+'"]'),
                        prevHtml = commentCircle.html();

                    if(prevHtml !== undefined) {
                        var prevCount = parseInt(prevHtml.split('<span')[0]);
                        commentCircle.html(prevCount + 1);
                        if(userRoleId == 1) {
                            commentCircle.append('<span class="deleteComment">Delete</span>');
                        }
                    } else {
                        var count = '<div class="comments read" data-project="'+projectId+'" data-thumb="'+thumbId+'" data-x="'+x_axis+'" data-y="'+y_axis+'" style="position:absolute; left:'+css_x+'px; top:'+css_y+'px;">'+1;

                            if(userRoleId == 1) {
                                count += '<span class="deleteComment">Delete</span>';
                            }
                            count += '</div>' ;

                        $('.rg-image').append(count);
                    }

                    //incrementing the value of total comments in thumb list
                    var thumbValue = $('#'+thumbId+' .commentsCount').html() || 0;
                    if(thumbValue === 0) {
                        $('.es-carousel li #'+thumbId).append('<span class="commentsCount read">'+1+'</span>');
                    } else {
                        $('#'+thumbId+' .commentsCount').html(parseInt(thumbValue) + 1);
                    }

                    //closing comment container after successful submission
                    $('.comments-container').hide();

                    //opening notification popup
                    if(trackNotification === 0) {
                        $('#notification').show().addClass('active');
                        showNotification();
                        //hiding after 10 sec for the first time only
                        timer = setTimeout(function() {
                            hideNotification();
                        }, 10000);

                        //updating track notification global var
                        trackNotification += 1;
                    }
                },
                error: function() {
                    console.log('Some error occurs while posting comment!! Please try again later.');
                }
            });
        }
    });
    
    //closing comment box
    $('#closeCommentBox').click(function() {
        hideCommentBox();
    });

    //hiding comments from the page
    $('#hideComments').click(function() {
        $('body').removeClass('comments-active');
        $(this).hide();
        $('#showComments').show();
    });

    //showing comments
    $('#showComments').click(function() {
        $('body').addClass('comments-active');
        $(this).hide();
        $('#hideComments').show();
    });

    //for deleting comments
    if(userRoleId === 1) {
        $(document.body).on('mouseover', '.comments' ,function(){
            $(this).find('.deleteComment').show();
        });

        $(document.body).on('mouseleave', '.comments' ,function(){
            $(this).find('.deleteComment').hide();
        });
    }

    $(document.body).on('click', '.deleteComment' ,function(){
        var projectId = $(this).parent('.comments').attr('data-project'),
            thumbId = $(this).parent('.comments').attr('data-thumb'),
            x_axis = $(this).parent('.comments').attr('data-x'),
            y_axis = $(this).parent('.comments').attr('data-y'),
            self = $(this);

        $.ajax({
            type: "DELETE",
            url: '/admin/comments/delete/'+x_axis+'/'+y_axis+'/'+projectId+'/'+thumbId,
            success: function(data) {
                self.parent('.comments').remove();
                //minus the no of comments we are deleting from thumb icon
                var totalCount = parseInt($('#'+thumbId).find('.commentsCount').html());
                if((totalCount - parseInt(data)) > 0) {
                    $('#'+thumbId).find('.commentsCount').html(totalCount - parseInt(data));
                } else {
                    $('#'+thumbId).find('.commentsCount').remove();
                }
                //closing comments container if open
                hideCommentBox();
            }
        });
        return false;
    });

    $(document.body).on('click', '.removeComment', function() {
        var self = $(this),
            commentId = $(this).attr('data-id'),
            thumbId = $(this).parents('.comments-container').attr('data-thumb'),
            x_axis = $(this).parents('.comments-container').attr('data-x'),
            y_axis = $(this).parents('.comments-container').attr('data-y'),
            cookie_id = $.cookie('cookie_id');

        $.ajax({
            type: "POST",
            url: '/comments/setstatus/'+commentId+'/2/'+cookie_id,
            success: function() {
                self.parent('.client-comments').remove();

                //minus the value in the circle or comments count
                var commentCircle = $('.comments[data-x="'+x_axis+'"][data-y="'+y_axis+'"]'),
                    prevHtml = commentCircle.html(),
                    prevCount = parseInt(prevHtml.split('<span')[0]);

                if(prevCount > 1) {
                    commentCircle.html(prevCount - 1);
                    if(userRoleId == 1) {
                        commentCircle.append('<span class="deleteComment">Delete</span>');
                    }
                } else {
                    commentCircle.remove();
                    hideCommentBox();
                }

                //minus the no of comments we are deleting from thumb icon
                var totalCount = parseInt($('#'+thumbId).find('.commentsCount').html());
                if((totalCount - 1) > 0) {
                    $('#'+thumbId).find('.commentsCount').html(totalCount - 1);
                } else {
                    $('#'+thumbId).find('.commentsCount').remove();
                }
            }
        });
    });

    //sending notification email to the admin
    $('#notify').click(function() {
        //sending notification
        if($.cookie('user') !== undefined) {
            //check user notification status
            var excludeEmail;
            if(parseInt($.cookie('notify')) === 2) excludeEmail = $.cookie('email');

            //disabling notify button
            var self = $(this);
            self.attr('disabled', 'disabled');
            self.text('Sending...');

            //sending request to the server
            var projectId = self.val();
            $.ajax({
                type: "POST",
                url: '/comments/sendNotification/',
                data: {projectId: projectId, client: $.cookie('user'), excludeEmail: excludeEmail},
                success: function(data) {
                    var data = $.parseJSON(data);
                    if(data.error) {
                        console.log(data.error);
                    } else {
                        //passing success message
                        self.siblings('h3').text(data.success.text).show();
                        self.siblings('h4').hide();
                        self.siblings('.notify-text').hide();
                        self.hide();
                    }
                },
                error: function() {
                    console.log('Some error occured while sending email!! Please try again later');
                    self.siblings('h3').text('Some error occured!! Please try again later.').show();
                    self.siblings('h4').hide();
                    self.hide();
                }
            });
        } else {
            console.log('Notification can only be send once you put some comments');
        }
    });

    //opening notification container when user clicks on send notification
    $('#notification').click(function() {
        showNotification();
    });

    //closing notification box
    $('#closeNotificationBox').click(function() {
        hideNotification();
        clearTimeout(timer);
    });

    //for user notification
    if($.cookie('notify') === undefined || parseInt($.cookie('notify')) === 1) {
        $.cookie('notify', 1, {expires: 365});
        $('#notifyStatus').attr('checked', 'checked');
    }

    $('#notifyStatus').click(function() {
        if(parseInt($.cookie('notify')) === 1) {
            $.cookie('notify', 2, {expires: 365});
            $('#notifyStatus').removeAttr('checked');
        } else {
            $.cookie('notify', 1, {expires: 365});
            $('#notifyStatus').attr('checked', 'checked');
        }
    });
});

//closing modals if user press esc key
$(document).keyup(function(e) {
    if(e.keyCode == 27) {
        hideCommentBox();
    }
});

//custom function
function showNotification() {
    $('.notification-container h3').empty().hide();
    $('.notification-container h4').show();
    $('.notify-text').show();
    $('.notification-container button').removeAttr('disabled').text('Notify').show();
    $('.notification-container').addClass('active').show();
}

function hideNotification() {
    $('.notification-container').removeClass('active').fadeOut();
}

function hideCommentBox() {
    //name container
    $('#name-form').trigger("reset");
    $('.name-container').hide();
    //comments container
    $('#comment-form').trigger("reset");
    $('.comments-container').hide();
}

function checkClass(x, y, thumbId) {
    $('.comments[data-x="'+x+'"][data-y="'+y+'"][data-thumb="'+thumbId+'"]').removeClass('unread').addClass('read');
    if(!$('.comments').hasClass('unread')) {
        $('a#'+thumbId+' .commentsCount').removeClass('unread').addClass('read');
    }
}

function loadComments() {
    //defining max-width for rg image container so that comments dont get move when someone zoomin and out
    $('.rg-image').css({'max-width': $(window).width()});

    //gloabl variables
    rgImage = $('.rg-image img'),
    imageResizeWidth = parseInt(rgImage.width()),
    imageOriginalWidth = parseInt(rgImage.get(0).naturalWidth),
    imageResizeHeight = parseInt(rgImage.height()),
    imageOriginalHeight = parseInt(rgImage.get(0).naturalHeight);

    //defining variables for ajax call
    var projectId = rgImage.attr('data-project'),
        thumbId = rgImage.attr('data-thumb');

    //fetching comments
    $('.comments').remove();
    $.ajax({
        type: "GET",
        url: '/comments/view/'+projectId+'/'+thumbId+'/'+$.cookie('cookie_id'),
        success: function(data) {
            var comments = JSON.parse(data);
            if(Object.keys(comments).length > 0) {
                for(y_axis in comments) {
                    for(value in comments[y_axis]) {
                        var data_x = comments[y_axis][value][0]['x_axis'],
                            data_y = comments[y_axis][value][0]['y_axis'],
                            css_x = data_x,
                            css_y = data_y,
                            length = Object.keys(comments[y_axis][value]).length;

                        if(imageOriginalWidth > imageResizeWidth) {
                            css_x = (imageResizeWidth/imageOriginalWidth) * data_x;
                        }

                        if(imageOriginalHeight > imageResizeHeight) {
                            css_y = (imageResizeHeight/imageOriginalHeight) * data_y;
                        }

                        //defining classname as per the fetched records
                        //comment read/unread classes
                        var classname = 'read';
                        for(comment in comments[y_axis][value]) {
                            if(comments[y_axis][value][comment]['classname'] === 'unread') {
                                classname = 'unread';
                            }
                        }

                        var count = '<div class="comments '+classname+'" data-project="'+projectId+'" data-thumb="'+thumbId+'" data-x="'+data_x+'" data-y="'+data_y+'" style="position:absolute; left:'+css_x+'px; top:'+css_y+'px;">'+length;

                        //userRoleId has been defined in gallery.ctp layout file
                        if(userRoleId === 1) {
                            count += '<span class="deleteComment">Delete</span></div>';
                        }
                        $('.rg-image').append(count);
                    }
                }
            }
        }
    });
}