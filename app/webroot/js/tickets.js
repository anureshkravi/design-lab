$(document).ready(function() {
	$('.hours').on('click', function() {
        $(this).parent().siblings('.hours-container').slideToggle();
        return false;
    });

    $(document.body).on('click', '.approve' ,function(){
    	var ticketId = $(this).attr('data-id');
    	Ticket.approve(ticketId);
    	return false;
    });

    $(document.body).on('click', '.decline' ,function(){
        var ticketId = $(this).attr('data-id');
        Ticket.decline(ticketId);
        return false;
    });

    $(document.body).on('click', '.assign' ,function(){
    	var assignedId = $('#users').val();
    	var ticketId = $(this).attr('data-id');
    	Ticket.assign(assignedId, ticketId);
        return false;
    });

    $(document.body).on('click', '.completed' ,function(){
        var ticketId = $(this).attr('data-id');
        Ticket.completed(ticketId);
        return false;
    });

    $(document.body).on('click', '.hold' ,function(){
        var ticketId = $(this).attr('data-id');
        Ticket.hold(ticketId);
        return false;
    });

    $(document.body).on('click', '.start' ,function(){
        var ticketId = $(this).attr('data-id');
        Ticket.start(ticketId);
        return false;
    });

});