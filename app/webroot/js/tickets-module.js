'use strict';

var Ticket = (function() {
	var currentdate = new Date(),
		year = (currentdate.getYear()-100+2000),
		month = (currentdate.getMonth()+1) < 10 ? '0'+(currentdate.getMonth()+1) : currentdate.getMonth()+1,
		date = (currentdate.getDate()) < 10 ? '0'+(currentdate.getDate()) : currentdate.getDate(),
		hours = currentdate.getHours(),
		minutes = currentdate.getMinutes(),
		DEFAULTS = {
			url: '/admin/tickets',
			effortsUrl: '/admin/efforts',
			users: {},
			tickets: {},
			track_load: 1,
			loading: false,
			per_page: 20,
			total_groups: 1,	
			role_id: 1
		}

	return {
		initialize: function() {
			this.bindUIActions();
			this.ajaxCall('', 'GET', DEFAULTS.url+'/tickets?per_page='+DEFAULTS.per_page, function(response) {
				DEFAULTS.users = response.users;
				DEFAULTS.role_id = parseInt(response.role);
				DEFAULTS.tickets = response.tickets;
				DEFAULTS.total_groups = response.totalCount/DEFAULTS.per_page;
				Ticket.createAssignedTickets(DEFAULTS.tickets);
			});
		},
		ajaxCall: function(data, method, url, callback) {
			$('#loader').show();
			return $.ajax({
	            url: url,
	            type: method,
	            data: data,
	            success: function (response) {
	            	var data = JSON.parse(response);
	            	if(data.error !== undefined) {
	            		$('#content').prepend('<div id="flashMessage" class="alert alert-danger">'+data.error+'</div>');
	            		setTimeout(function(){
							$('#flashMessage').fadeOut();
						}, 3000);
	            	} else {
	            		callback(data);
	            	}
	            	$('#loader').hide();
	            },
	            error: function() {
	            	console.log('Ajax Error');
	            }
	        });
		},
		approve: function(id) {
			this.ajaxCall({Ticket:{id: id, status_id: 5}}, 'POST', DEFAULTS.url+'/approve/', this.assignForm);
			return this;
		},
		assignForm: function(response) {
			var users = response.users,
				ticket = response.ticket.Ticket,
				options='';

			for(var key in users) {
				options += '<option value="'+key+'">'+users[key]+'</option>';
			}
			var assignForm = '<div class="form-inline">'+
								'<div class="form-group">'+
									'<select class="users form-control" required>'+
										'<option value="">Select User</option>'+options+
									'</select>'+
								'</div>';

								//if approved then only not in case of re-assign
								if(parseInt(ticket.status_id) === 5) {
									assignForm += '<div class="form-group date-container">'+
									                '<div class="input-group date datetimepicker">'+
									                    '<input type="text" class="form-control" />'+
									                    '<span class="input-group-addon">'+
									                        '<span class="glyphicon glyphicon-calendar"></span>'+
									                    '</span>'+
									                '</div>'+
									            '</div>';
								}
				assignForm += '<button type="button" class="btn btn-success assign" data-id="'+ticket.id+'">Assign</button>'+
							'</div>';

			//if approved
			if(parseInt(ticket.status_id) === 5) {
				$('#action_'+ticket.id).html(assignForm);
				$('.datetimepicker').datetimepicker({
					defaultDate: year+'-'+month+'-'+date+' '+hours+':'+minutes,
	                format: 'YYYY-MM-DD HH:mm'
				});
			} else {
				//if re-assign
				$('#assignTicket_'+ticket.id+' .ticket-actions').html(assignForm);
			}
		},
		assign: function(assignedId, ticketId, startDate) {
			this.ajaxCall( {Ticket:{id: ticketId, assigned_id: assignedId, start_date: startDate, status_id: 11 }}, 'POST', DEFAULTS.url+'/assign/', this.updateTickets );
			return this;
		},
		decline: function(ticketId) {
			var declineForm = '<div class="form">'+
								'<div class="form-group">'+
									'<textarea placeholder="Message" class="mailMessage"></textarea>'+
								'</div>'+
								'<button type="button" id="sendMail" class="btn btn-success" data-id="'+ticketId+'">Send Mail</button>'+
							'</div>';
			$('#action_'+ticketId).html(declineForm);
		},
		sendDeclineMail: function(ticketId, mail) {
			this.ajaxCall({Ticket:{id: ticketId, status_id: 7, mail: mail.replace(/<br>/g,"\n")}}, 'POST', DEFAULTS.url+'/decline/', this.updateTickets);
			return this;
		},
		updateTickets: function(ticket) {
			if($('#ticket_'+ticket.Ticket.id)) {
				Ticket.deleteRequestedTicket(ticket);
			}
			if($('#assignTicket_'+ticket.Ticket.id).html() === undefined) {
				//adding assigned ticket to assign list
				var assignedTicket = Ticket.createAssignedTicket(ticket);
				$('.assigned-projects').prepend(assignedTicket);
			} else {
				//incase of re-assigning
				for(var findTicket in DEFAULTS.tickets) {
					var currentTicket = DEFAULTS.tickets[findTicket];
					if(currentTicket.Ticket.id === ticket.Ticket.id) {
						var index = DEFAULTS.tickets.indexOf(currentTicket);
						DEFAULTS.tickets[index] = ticket;
					}
				}
				$('#assignTicket_'+ticket.Ticket.id+' input[type="hidden"]').val(ticket.Assigned.id);
				Ticket.updateStatus(ticket);
			}
		},
		createAssignedTickets: function(tickets) {
			if(tickets.length > 0) {
				for(var ticket in tickets) {
					$('.assigned-projects').append(this.createAssignedTicket(tickets[ticket]));
				}
			} else {
				$('.assigned-projects').append('<div class="text-center" style="font-size: 17px;"><strong>No Project / Ticket assigned to you</strong></div>');
			}
		},
		createAssignedTicket: function(ticket) {
			var assignedTicket = '<div class="clearfix tickets" id="assignTicket_'+ticket.Ticket.id+'">'+
									'<div class="col-xs-2">'+
										'<strong>'+ticket.Ticket.project+'</strong><br>'+
										'<span>From: '+ticket.Ticket.name+'</span><br>';
										if(this.dateFormat(ticket.Ticket.start_date) !== undefined) {
											assignedTicket += '<span>Start Date: '+this.dateFormat(ticket.Ticket.start_date)+'</span>';
										} 
				assignedTicket +=   '</div>'+
									'<div class="col-xs-2">';									
										if(ticket.Ticket.filename.length > 0) {
											var files = ticket.Ticket.filename.split(',');			
											for(var file in files) {
												assignedTicket += '<a href="/documents/'+ticket.Ticket.project_folder+'/'+files[file]+'" target="_blank">'+files[file]+'</a><br>';
											}
										}
				assignedTicket +=	'</div><div class="col-xs-2 user">'+(ticket.Assigned.username !== null ? ticket.Assigned.username : '-')+'</div>'+

									'<div class="col-xs-1">'+ticket.Ticket.requested_time+' hrs'+
									(ticket.Ticket.billable ? ' (Billable)' : ' (Non Billable)')+

									'</div><div class="col-xs-1 hours">'+ticket.Ticket.time_spent+' hrs</div>'+

									'<div class="col-xs-1 status">'+
										ticket.Status.title+
									 	(parseInt(ticket.Status.id) === 7 && DEFAULTS.role_id === 1 ? ' <span class="check-email">(Message)</span>' : '')+
									 '</div>'+

									'<div class="col-xs-3 ticket-actions">'+this.generateButtons(ticket)+'</div>';
				assignedTicket += this.createTicketEfforts(ticket);
				assignedTicket += (ticket.Ticket.mail.length > 0 && DEFAULTS.role_id === 1) ? this.createDeclineMail(ticket) : '';
				assignedTicket += '</div>';
			return assignedTicket;
		},
		createTicketEfforts: function(ticket) {
			var ticketEfforts = '<div class="table-responsive col-xs-8 col-xs-push-1 hours-container">'+
									'<table class="table table-bordered">'+
										'<tbody>'+
											'<tr>'+
												'<td colspan="5">'+
													'<form class="form-inline">'+
														'<input type="hidden" value="'+ticket.Assigned.id+'">'+
														'<div class="form-group">'+
															'<label>Add Hours</label>'+
															'<input type="number" class="form-control">'+
														'</div>'+
														'<div class="form-group">'+
															'<label>Description</label>'+
															'<input type="text" class="form-control">'+
														'</div>'+
														'<button type="submit" class="btn btn-success update-efforts" data-id="'+ticket.Ticket.id+'">Update</button>'+
													'</form>'+
												'</td>'+
											'</tr>'+
											'<tr>'+
												'<td width="20%"><strong>Date</strong></td>'+
												'<td width="20%"><strong>Hours</strong></td>'+
												'<td width="30%"><strong>Description</strong></td>'+
												'<td width="30%"><strong>Worked by</strong></td>'+
												'<td><strong>Actions</strong></td>'+
											'</tr>';
									if(ticket.Effort.length > 0) {
										var effort;
										for(effort in ticket.Effort) {
											ticketEfforts += '<tr id="effort_'+ticket.Effort[effort]['id']+'">'+
																'<td width="30%">'+this.dateFormat(ticket.Effort[effort]['created'])+'</td>'+
																'<td width="20%">'+ticket.Effort[effort]['hours']+' hrs</td>'+
																'<td>'+ticket.Effort[effort]['description']+'</td>'+
																'<td>'+DEFAULTS.users[ticket.Effort[effort]['user_id']]+'</td>'+
																'<td><button type="button" class="btn btn-danger delete-efforts" data-id="'+ticket.Effort[effort]['id']+'">Delete</button></td>'+
															'</tr>';
										}
									}
				ticketEfforts += '</tbody>'+
									'</table>'+
								'</div>';
			return ticketEfforts;
		},
		createDeclineMail: function(ticket) {
			var mail = '<div class="mail-container col-xs-5 col-xs-push-6">'+
						'<h2>Message</h2>'+
						'<div class="mail-body">'+ticket.Ticket.mail+'</div>'+
					'</div>';
			return mail;
		},
		deleteRequestedTicket: function(data) {
			var ticket = $('#ticket_'+data.Ticket.id),
				ticketPanel = ticket.parents('.panel'),
				requestCount = ticketPanel.find('.ticketCount');

			if(parseInt(requestCount.html()) == 1) {
				ticketPanel.remove();
			} else {
				requestCount.html(parseInt(requestCount.html()) -1);
				ticket.remove();
			}
		},
		completed: function(ticketId) {
			this.ajaxCall({Ticket:{id: ticketId, status_id: 8}}, 'POST', DEFAULTS.url+'/completed/', this.updateStatus);
			return this;
		},
		hold: function(ticketId) {
			this.ajaxCall({Ticket:{id: ticketId, status_id: 9}}, 'POST', DEFAULTS.url+'/completed/', this.updateStatus);
			return this;
		},
		resume: function(ticketId) {
			this.ajaxCall({Ticket:{id: ticketId, status_id: 10}}, 'POST', DEFAULTS.url+'/start/', this.updateStatus);
			return this;			
		},
		deleteTicket: function(ticketId) {
			this.ajaxCall('', 'DELETE', DEFAULTS.url+'/delete/'+ticketId, function() {
				$('#assignTicket_'+ticketId).remove();
			});
			return this;
		},
		reassign: function(ticketId) {
			this.ajaxCall({Ticket:{id: ticketId}}, 'POST', DEFAULTS.url+'/reassign/', this.assignForm);
			return this;
		},
		updateStatus: function(ticket) {
			$('#assignTicket_'+ticket.Ticket.id+' .user').html(ticket.Assigned.username);
			$('#assignTicket_'+ticket.Ticket.id+' .status').html(ticket.Status.title);
			$('#assignTicket_'+ticket.Ticket.id+' .ticket-actions').html(Ticket.generateButtons(ticket));
		},
		saveEfforts: function(data) {
			this.ajaxCall({Effort: data}, 'POST', DEFAULTS.effortsUrl+'/add/', function(data) {
				var effort = '<tr id="effort_'+data.Effort.id+'">'+
								'<td>'+data.Effort.created+'</td>'+
								'<td>'+data.Effort.hours+' hrs</td>'+
								'<td>'+data.Effort.description+'</td>'+
								'<td>'+data.User.username+'</td>'+
								'<td><button type="button" class="btn btn-danger delete-efforts" data-id="'+data.Effort.id+'">Delete</button></td>'+
							'</tr>';
				$('#assignTicket_'+data.Effort.ticket_id+' tbody').append(effort);
				$('#assignTicket_'+data.Effort.ticket_id+' input[type="number"]').val('');
				$('#assignTicket_'+data.Effort.ticket_id+' input[type="text"]').val('');
				$('#assignTicket_'+data.Effort.ticket_id+' input').parent().removeClass('has-error');

				//updating total time spent
				var timeSpent = parseInt($('#assignTicket_'+data.Effort.ticket_id+' .hours').html().split(' hrs')[0]) + parseInt(data.Effort.hours);
				$('#assignTicket_'+data.Effort.ticket_id+' .hours').html(timeSpent + ' hrs');
			});
			return this;
		},
		deleteEfforts: function(effortId) {
			this.ajaxCall('', 'POST', DEFAULTS.effortsUrl+'/delete/'+effortId, function(data) {
				$('tr#effort_'+data.Effort.id).remove();
				var timeSpent = parseInt($('#assignTicket_'+data.Effort.ticket_id+' .hours').html().split(' hrs')[0]) - parseInt(data.Effort.hours);
				$('#assignTicket_'+data.Effort.ticket_id+' .hours').html(timeSpent + ' hrs');
			});
			return this;
		},
		searchTicket: function(keyword) {
			this.ajaxCall('', 'POST', DEFAULTS.url+'/search/'+keyword, function(tickets) {
				if(tickets.length > 0) {
					$('.assigned-projects').html('');
					var ticket;
					for(ticket in tickets) {
						$('.assigned-projects').append(Ticket.createAssignedTicket(tickets[ticket]));
					}
				}
			});
			return this;
		},
		generateButtons: function(ticket) {
			var buttons = '';
			/*7=decline, 8=completed, 9=hold, 10=process, 11=assign*/
			if(ticket.Status.id != 7 && ticket.Status.id != 8 && ticket.Status.id != 9 && ticket.Status.id != 11) {
				buttons += '<a class="btn btn-success completed" href="#" role="button" data-id="'+ticket.Ticket.id+'">Completed</a>'+
        					'<a class="btn btn-danger hold" href="#" role="button" data-id="'+ticket.Ticket.id+'">Hold</a>';
			} 
			if(ticket.Status.id != 7 && ticket.Status.id != 10) {
				buttons += '<a class="btn btn-primary start" href="#" role="button" data-id="'+ticket.Ticket.id+'">Start</a>';
			}
			buttons += '<a class="btn btn-warning reassign" href="#" role="button" data-id="'+ticket.Ticket.id+'">Re-assign</a>';

			if(DEFAULTS.role_id === 1) {
				buttons += '<a class="deleteTicket" href="#" data-id="'+ticket.Ticket.id+'"><span class="glyphicon glyphicon-trash"></span></a>';
			}
			return buttons;
		},
		dateFormat: function(date) {
			var d = new Date(date),
				year = (d.getYear() - 100) + 2000,
				month = d.getMonth(),
				date = d.getDate(),
				months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
			if(isNaN(date)) {
				return undefined;
			} else {
				return months[month]+' '+date+', '+year;
			}
		},
		scroll: function() {
	    	if(DEFAULTS.track_load < DEFAULTS.total_groups && DEFAULTS.loading==false) {
        		DEFAULTS.loading = true;
        		this.ajaxCall('', 'GET', DEFAULTS.url+'/scroll?group='+DEFAULTS.track_load+'&per_page='+DEFAULTS.per_page, function(response) {        			
        			var ticket;
        			for(ticket in response) {
        				$('.assigned-projects').append(Ticket.createAssignedTicket(response[ticket]));
        			}
					DEFAULTS.track_load++;
					DEFAULTS.loading = false;
				});
				return this;
        	}
		},
		bindUIActions: function() {
			var self = this;

			$(window).scroll(function() {
            	if($(window).scrollTop() + $(window).height() === $(document).height() && $('#searchTicket').val() == '') {
            		self.scroll();
                }
            });

			$('#searchTicket').keyup(function(){
				var value = $(this).val();
				if(value.length > 2) {
					self.searchTicket(value);
				} else {
					$('.assigned-projects').html('');
					Ticket.createAssignedTickets(DEFAULTS.tickets);
				}
			});

			$(document.body).on('click', '.deleteTicket' ,function(){
				if(window.confirm("Are you sure, you want to delete the ticket permanently?")) {
					var id = $(this).attr('data-id');
					self.deleteTicket(id);
				}
				return false;
		    });

			$(document.body).on('click', '.hours' ,function(){
		        $(this).siblings('.hours-container').slideToggle();
		        return false;
		    });

		    $(document.body).on('click', '.check-email' ,function(){
		        $(this).parent().siblings('.mail-container').slideToggle();
		        return false;
		    });

		    $(document.body).on('click', '.reassign' ,function(){
		    	$(this).hide();
		    	$(this).siblings('.btn').hide();		    	
		    	var ticketId = $(this).attr('data-id');
		    	self.reassign(ticketId);
		        return false;
		    });

		    $(document.body).on('click', '.delete-efforts' ,function(){
		    	if(window.confirm("Are you sure, you want to delete the efforts?")) {
		    		var effortId = $(this).attr('data-id');
		        	self.deleteEfforts(effortId);
		    	}
		    	return false;
		    });

		    $(document.body).on('click', '.update-efforts' ,function(){
		        var ticketId = $(this).attr('data-id'),
		        	hours = $(this).siblings('.form-group').find('input[type="number"]').val(),
		        	description = $(this).siblings('.form-group').find('input[type="text"]').val(),
		        	userId = $(this).siblings('input[type="hidden"]').val();

		        if(hours !== '' && hours !== 0) {
		        	self.saveEfforts({ticket_id: ticketId, user_id: userId, hours: hours, description: description});
		        } else {
		        	$(this).siblings('.form-group').find('input[type="number"]').parent().addClass('has-error');
		        }
		    	return false;
		    });

		    $(document.body).on('click', '.approve' ,function(){
		    	var ticketId = $(this).attr('data-id');
		    	self.approve(ticketId);
		    	return false;
		    });

		    $(document.body).on('click', '.decline' ,function(){
		        var ticketId = $(this).attr('data-id');
		        self.decline(ticketId);
		        return false;
		    });

		    $(document.body).on('click', '#sendMail' ,function(){
		        var ticketId = $(this).attr('data-id'),
		        	mail = $(this).siblings('.form-group').find('.mailMessage').val();
		        self.sendDeclineMail(ticketId, mail);
		        return false;
		    });

		    $(document.body).on('click', '.assign' ,function(){
		    	var assignedId = $(this).siblings('.form-group').find('.users').val(),
		    		startDate = $(this).siblings('.date-container').find('input').val(),
		    		ticketId = $(this).attr('data-id');
		    	self.assign(assignedId, ticketId, startDate);
		        return false;
		    });

		    $(document.body).on('click', '.completed' ,function(){
		        var ticketId = $(this).attr('data-id');
		        self.completed(ticketId);
		        return false;
		    });

		    $(document.body).on('click', '.hold' ,function(){
		        var ticketId = $(this).attr('data-id');
		        self.hold(ticketId);
		        return false;
		    });

		    $(document.body).on('click', '.start' ,function(){
		        var ticketId = $(this).attr('data-id');
		        self.resume(ticketId);
		        return false;
		    });

		    $('.datetimepicker').datetimepicker({
				defaultDate: year+'-'+month+'-'+date+' '+hours+':'+minutes,
                format: 'YYYY-MM-DD HH:mm'
			});
		}
	};

})();

Ticket.initialize();