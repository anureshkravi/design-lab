var slidCount = 1;

$(document).ready(function() {
	//Escape Command
	$(document).on('keyup',function(evt) {
		if (evt.keyCode == 27){
			slidegoCreative();
		}
	});

	//skip Button
	$('#skip').click(function(){
		slidegoCreative();
	});

	//gotit Button
	$('#gotit').click(function(){
		$.cookie('gallery-overlay', 1, { expires: 365, path: '/' });
		slidegoCreative();
	});

	//Creative Preview
	$('#NextButton').click(function() {
		nextSlide(slidCount);
	});

	//Hiding NextButton & Showing ReplayButton
	$('#ReplayButton').click(function(){
		slidCount = 1;
		replay(slidCount);
	});

	//Creative Preview
	$('#PrevButton').click(function() {
		prevSlide(slidCount);
	});


	//Help button click coming mockup sliding
	$('.needmeCreative a').click(function(){
		//$('body').addClass('stop-scrolling'); 
		$('.overLay').fadeIn(2000);
		$('.creativeContainer').hide();
		slidCount = 0;
		nextSlide(slidCount);
	});
});


//Set Cookies
setTimeout(function(){
	if($.cookie('gallery-overlay') === undefined) {
		if ($(window).width() >= 1024){
			//$('body').addClass('stop-scrolling');   
			$('.overLay').fadeIn(2000);
		}
	}    
}, 1000);

$(window).resize(function(){
    if ($(window).width() < 1024){  
        $('.overLay').hide();
    }
});

//Replay all sliding mockup
function replay (id) {
	$('.creativeContainer').hide();
	$('#slid'+id+' .creativeContainer').show();
	$('#ReplayButton').hide();
	$('#NextButton').show();

	if(id === 1) {
		$('#PrevButton').hide();
	}
}

//Slide out width oval effect
function slidegoCreative(){
	$('.circleAni').show();
	$('.overLay').fadeOut('fast', function(){
		$('body').removeClass('stop-scrolling');
		$('#PrevButton, #ReplayButton').hide();
		$('#NextButton').show();
		$('.needmeCreative').show();
		$('.circleAni').animate({
			opacity: '0.3',
			height: '100px',
		    right: '-50px',
		    top: '-50px',
		    width: '100px'
		},300, function(){
			$('.circleAniText').slideDown();
			setTimeout(function() {
				$('.circleAniText').fadeOut('slow');
			}, 3000);
		});
	});	
}

function nextSlide(id) {
	if(id <= 8) {
		$('#slid'+(id+1)+' .creativeContainer').show();
		$('#slid'+id+' .creativeContainer').hide();
		if(id === 8) {
			$('#ReplayButton').show();
			$('#NextButton').hide();
		}
		if(id >= 1) {
			$('#PrevButton').show();
		}
		slidCount++;
	}
}

function prevSlide(id) {
	if(id !== 1) {
		$('#slid'+(id-1)+' .creativeContainer').show();
		$('#slid'+(id)+' .creativeContainer').hide();
	}
	if(id === 2) {
		$('#PrevButton').hide();
	}
	if(id !== 8) {
			$('#ReplayButton').hide();
			$('#NextButton').show();
		}
	slidCount--;
}