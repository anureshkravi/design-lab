var windowWidth = $(window).width(),
        windowHeight = $(window).height(),
        trackNotification = 0,
        rgImage,
        imageResizeWidth,
        imageOriginalWidth,
        imageResizeHeight,
        imageOriginalHeight,
        timer;

$(document).ready(function () {

    //fetching comments from the server for thumb counts
    var project_id = $('.es-carousel li a').attr('data-project');
    $.ajax({
        type: "GET",
        url: '/comments/viewAll/' + project_id + '/' + $.cookie('cookie_id'),
        success: function (data) {
            var data = JSON.parse(data);
            for (comment in data) {
                var classname = 'read';
                for (value in data[comment]) {
                    //console.log(data[comment][value]['Comment']['classname']);
                    if (data[comment][value]['Comment']['classname'] === 'unread') {
                        classname = 'unread';
                    }
                }

                var count = Object.keys(data[comment]).length;
                $('.es-carousel li a#' + comment).append('<span class="commentsCount ' + classname + '">' + count + '</span>');
            }
        }
    });

    //opening comment box if someone click anywhere on the image
    $(document.body).on('click', 'div.rg-image img', function (e) {

        if ($('body').hasClass('comments-active') &&
                $('.comments-container').css('display') === 'none' &&
                !$('.notification-container').hasClass('active')) {

            var projectId = $(this).attr('data-project'),
                    thumbId = $(this).attr('data-thumb'),
                    x_axis = e.pageX, //page x axis will use for css
                    y_axis = e.pageY, //page y_axis will use for css
                    //css_x = Math.floor(x_axis),
                    //css_y = Math.floor(y_axis),
                    data_x = Math.floor((imageResizeWidth / imageOriginalWidth) * (x_axis - $(this).offset().left)), //will store in DB
                    data_y = Math.floor((imageResizeHeight / imageOriginalHeight) * (y_axis - $(this).offset().top)); //will store in DB

            //storing actual point as per the orignal width
            if (imageOriginalWidth > imageResizeWidth) {
                data_x = Math.floor((imageOriginalWidth / imageResizeWidth) * (x_axis - $(this).offset().left));
            }

            //storing actual point as per the orignal height
            if (imageOriginalHeight > imageResizeHeight) {
                data_y = Math.floor((imageOriginalHeight / imageResizeHeight) * (y_axis - $(this).offset().top));
            }

            //moving comment window as per the click event
            if (windowWidth - x_axis < 405) {
                x_axis = x_axis - 400;
            }
            if (windowHeight - (windowHeight - y_axis) < 205) {
                y_axis = y_axis + 40;
            }

            $('.name-container').hide();
            $('.comments-container .user-comments').html('').hide();

            $('.comments-container').css({
                top: y_axis,
                left: x_axis
            }).attr({
                'data-x': data_x,
                'data-y': data_y,
                'data-project': projectId,
                'data-thumb': thumbId
            }).show();

            // var name = $.cookie('user')
            if (($.cookie('user') != undefined) && ($.cookie('user') != "")) {
                $("#commentusername").html($.cookie('user') + ":");
                $('.comments-container #comment').focus();
            }

        } else {
            hideCommentBox();
            hideNotification();
        }
    });

    //comments points/circle with count
    $(document.body).on('click', '.comments', function () {
        var self = this,
                projectId = $(this).attr('data-project'),
                cookie_id = $.cookie('cookie_id'),
                thumbId = $(this).attr('data-thumb'),
                x_axis = parseInt($(this).attr('data-x')), //for data-x (storing in database)
                y_axis = parseInt($(this).attr('data-y')), //for data-y (storing in database)
                css_x_axis = parseInt($(this).offset().left), //for css position
                css_y_axis = parseInt($(this).offset().top); //for css position

        if (windowWidth - css_x_axis < 405) {
            css_x_axis = css_x_axis - 400;
        }
        if (windowHeight - (windowHeight - css_y_axis) < 205) {
            css_y_axis = css_y_axis + 40;
        }

        //fetching records from the server
        $.ajax({
            type: "GET",
            url: '/comments/viewByAxis/' + x_axis + '/' + y_axis + '/' + projectId + '/' + thumbId + '/' + cookie_id,
            success: function (data) {
                var comments = JSON.parse(data);
                var userComments = '';
                $.each(comments, function (key, value) {
                    userComments += '<div class="client-comments-container"><p class="date">' + key + '</p>';
                    $.each(value, function (index, value) {
                        userComments += '<div class="client-comments clearfix">' +
                                '<span class="cmt_username" style="cursor:pointer;"><strong>' + value['Comment']['name'] + '</strong></span>' +
                                '<p class="time">' + value['Comment']['time'] + '</p>';
                        if (value['Comment']['cookie_id'] === cookie_id) {
                            userComments += '<p class="removeComment" data-id="' + value['Comment']['id'] + '">Remove</p>';
                        }
                        userComments += '<p class="client-comments-content">' + value['Comment']['comment'] + '</p></div>';
                    });
                    userComments += '</div>';
                });

                //empty comment textarea
                $('.comments-container #comment').val('');

                //inserting comments into the comment box
                $('.comments-container .user-comments').html(userComments).show();
                $('.comments-container').css({
                    top: css_y_axis,
                    left: css_x_axis
                }).attr({
                    'data-x': x_axis,
                    'data-y': y_axis,
                    'data-project': projectId,
                    'data-thumb': thumbId
                }).show();
                $('.comments-container #comment').focus();

                //updating read/unread classes
                checkClass(x_axis, y_axis, thumbId);
            }
        });

    });


    //opening comment box if someone click anywhere on the image
    $(document.body).on('click', '.cmt_username', function (e) {

        //console.log(e.target, " : ", e.currentTarget);

        var x_axis = $('.comments-container').css('left').split('px');
        var y_axis = $('.comments-container').css('top').split('px');
        $('.name-container').css({
            left: parseInt(x_axis[0]) + 60,
            top: parseInt(y_axis[0]) + 10
        }).show();

        $('.name-container #name').focus();
        //disabling submit button of comments container
        $('#addComment').attr('disabled', 'disabled');

        $(".name-container h3").text('Update your details');
        $("#name-form").find('input[type="submit"]').attr('value', 'Update');
    });


    $('#comment').on('focus', function () {
        if ($.cookie('user') === undefined || $.cookie('email') === undefined) {
            //opening name modal for user
            var x_axis = $(this).parents('.comments-container').css('left').split('px');
            var y_axis = $(this).parents('.comments-container').css('top').split('px');
            $('.name-container').css({
                left: parseInt(x_axis[0]) + 60,
                top: parseInt(y_axis[0]) + 10
            }).show();
            $('.name-container #name').focus();
            //disabling submit button of comments container
            $('#addComment').attr('disabled', 'disabled');
            $(".name-container h3").text('Enter your details');
            $("#name-form").find('input[type="submit"]').attr('value', 'Submit');
        }
    });


    //user name form/creating cookie for user name
    $('#name-form').submit(function (e) {
        e.preventDefault();
        var name = $('#name').val();
        var email = $('#email').val();

        if (name !== '' && email !== '') {
            $.cookie('user', name, {
                expires: 365,
                path: '/'
            });
            $.cookie('email', email, {
                expires: 365,
                path: '/'
            });
            $('.name-container').remove();
            //comments container
            $('.comments-container #comment').focus();
            $('#addComment').removeAttr('disabled');
            $('#addDescription').removeAttr('disabled'); // Added for new design
        } else {
            return false;
        }

        if (($.cookie('user') != undefined) && ($.cookie('user') != "")) {

            // Added for new design || to add username first time || check this if else condition
            if ($(".description-container").is(":visible")) {
                $("#descriptionusername").html($.cookie('user') + ":");
                $('.description-container #description').focus();
            } else {
                $("#commentusername").html($.cookie('user') + ":");
                $('.comments-container #comment').focus();
            }
        }

        // Showing the tool tip
        $(".hover-tooltip-msg").show();
        $(".hover-tooltip-msg a").tooltip('show');

        setTimeout(function () {
            $(".hover-tooltip-msg").hide();
        }, 112000);

    });



    $('#cancelCommenting').click(function () {
        hideCommentBox();
        return false;
    });

    $('#cancelEmailCred').click(function () {
        hideEmailCredentialBox();
        return false;
    });

    //adding comments
    $('#comment-form').submit(function (e) {
        e.preventDefault();
        var name = $.cookie('user'),
                email = $.cookie('email'),
                cookie_id = $.cookie('cookie_id'),
                comment = $('#comment').val(),
                x_axis = $(this).parents('.comments-container').attr('data-x'),
                y_axis = $(this).parents('.comments-container').attr('data-y'),
                css_x = (x_axis > 15) ? x_axis - 15 : x_axis,
                css_y = (y_axis > 40) ? y_axis - 40 : y_axis,
                projectId = $(this).parents('.comments-container').attr('data-project'),
                thumbId = $(this).parents('.comments-container').attr('data-thumb'),
                formData = {
                    name: name,
                    email: email,
                    cookie_id: cookie_id,
                    comment: comment.replace(/\r?\n/g, '<br />'),
                    x_axis: x_axis,
                    y_axis: y_axis,
                    project_id: projectId,
                    thumb_id: thumbId,
                    status_id: 1
                };

        if (imageOriginalWidth > imageResizeWidth) {
            var x = Math.floor((imageResizeWidth / imageOriginalWidth) * x_axis);
            css_x = (x > 15) ? x - 15 : x;
        }

        if (imageOriginalHeight > imageResizeHeight) {
            var y = Math.floor((imageResizeHeight / imageOriginalHeight) * y_axis);
            css_y = (y > 40) ? y - 40 : y;
        }

        if (name !== undefined) {
            $.ajax({
                type: "POST",
                url: '/comments/addComment',
                dataType: "json",
                data: JSON.stringify(formData),
                success: function (data) {
                    if ($(".user-comments p.date:contains('" + data['Comment']['date'] + "')").length > 0) {
                        var userComments = '<div class="client-comments clearfix">' +
                                '<strong>' + data['Comment']['name'] + '</strong>' +
                                '<p class="time">' + data['Comment']['time'] + '</p>' +
                                '<p class="client-comments-content">' + data['Comment']['comment'] + '</p>' +
                                '</div>';
                        $("p.date:contains('" + data['Comment']['date'] + "')").parent('.client-comments-container').append(userComments);
                    } else {
                        var userComments = '<div class="client-comments-container">' +
                                '<p class="date">' + data['Comment']['date'] + '</p>' +
                                '<div class="client-comments clearfix">' +
                                '<strong>' + data['Comment']['name'] + '</strong>' +
                                '<p class="time">' + data['Comment']['time'] + '</p>' +
                                '<p class="client-comments-content">' + data['Comment']['comment'] + '</p>' +
                                '</div>' +
                                '</div>';
                        //$('.user-comments').show().append(userComments);
                    }
                    $('#comment-form').trigger("reset");

                    //incrementing the value in the circle or inserting the comments count
                    var commentCircle = $('.comments[data-x="' + x_axis + '"][data-y="' + y_axis + '"]'),
                            prevHtml = commentCircle.html();

                    if (prevHtml !== undefined) {
                        var prevCount = parseInt(prevHtml.split('<span')[0]);
                        commentCircle.html(prevCount + 1);
                        if (userRoleId == 1) {
                            commentCircle.append('<span class="deleteComment">Delete</span>');
                        }
                    } else {
                        var count = '<div class="comments read" data-project="' + projectId + '" data-thumb="' + thumbId + '" data-x="' + x_axis + '" data-y="' + y_axis + '" style="position:absolute; left:' + css_x + 'px; top:' + css_y + 'px;">' + 1;

                        if (userRoleId == 1) {
                            count += '<span class="deleteComment">Delete</span>';
                        }
                        count += '</div>';

                        $('.rg-image').append(count);
                    }

                    //incrementing the value of total comments in thumb list
                    var thumbValue = $('#' + thumbId + ' .commentsCount').html() || 0;
                    if (thumbValue === 0) {
                        $('.es-carousel li #' + thumbId).append('<span class="commentsCount read">' + 1 + '</span>');
                    } else {
                        $('#' + thumbId + ' .commentsCount').html(parseInt(thumbValue) + 1);
                    }

                    //closing comment container after successful submission
                    $('.comments-container').hide();

                    //opening notification popup
                    if (trackNotification === 0) {
                        $('#notification').show().addClass('active');
                        showNotification();
                        //hiding after 10 sec for the first time only
                        timer = setTimeout(function () {
                            hideNotification();
                        }, 10000);

                        //updating track notification global var
                        trackNotification += 1;
                    }
                },
                error: function () {
                    console.log('Some error occurs while posting comment!! Please try again later.');
                }
            });
        }
    });

    //closing comment box
    $('#closeCommentBox').click(function () {
        hideCommentBox();
    });

    //hiding comments from the page
    $('#hideComments').click(function () {
        $('body').addClass("image-linking-active").addClass("preview-mode");
        $('body').removeClass('comments-active');
        $('.commentsView').addClass('active');
        $(this).hide();
        $('#showComments').show();
    });

    //showing comments
    $('#showComments').click(function () {
        $('body').removeClass("image-linking-active").removeClass("preview-mode");
        $('body').addClass('comments-active');
        $('.commentsView').removeClass('active');
        $(this).hide();
        $('#hideComments').show();
    });

    //for deleting comments
    if (userRoleId === 1) {
        $(document.body).on('mouseover', '.comments', function () {
            $(this).find('.deleteComment').show();
        });

        $(document.body).on('mouseleave', '.comments', function () {
            $(this).find('.deleteComment').hide();
        });
    }

    $(document.body).on('click', '.deleteComment', function () {
        var projectId = $(this).parent('.comments').attr('data-project'),
                thumbId = $(this).parent('.comments').attr('data-thumb'),
                x_axis = $(this).parent('.comments').attr('data-x'),
                y_axis = $(this).parent('.comments').attr('data-y'),
                self = $(this);

        $.ajax({
            type: "DELETE",
            url: '/admin/comments/delete/' + x_axis + '/' + y_axis + '/' + projectId + '/' + thumbId,
            success: function (data) {
                self.parent('.comments').remove();
                //minus the no of comments we are deleting from thumb icon
                var totalCount = parseInt($('#' + thumbId).find('.commentsCount').html());
                if ((totalCount - parseInt(data)) > 0) {
                    $('#' + thumbId).find('.commentsCount').html(totalCount - parseInt(data));
                } else {
                    $('#' + thumbId).find('.commentsCount').remove();
                }
                //closing comments container if open
                hideCommentBox();
            }
        });
        return false;
    });

    $(document.body).on('click', '.removeComment', function () {
        var self = $(this),
                commentId = $(this).attr('data-id'),
                thumbId = $(this).parents('.comments-container').attr('data-thumb'),
                x_axis = $(this).parents('.comments-container').attr('data-x'),
                y_axis = $(this).parents('.comments-container').attr('data-y'),
                cookie_id = $.cookie('cookie_id');

        $.ajax({
            type: "POST",
            url: '/comments/setstatus/' + commentId + '/2/' + cookie_id,
            success: function () {
                self.parent('.client-comments').remove();

                //minus the value in the circle or comments count
                var commentCircle = $('.comments[data-x="' + x_axis + '"][data-y="' + y_axis + '"]'),
                        prevHtml = commentCircle.html(),
                        prevCount = parseInt(prevHtml.split('<span')[0]);

                if (prevCount > 1) {
                    commentCircle.html(prevCount - 1);
                    if (userRoleId == 1) {
                        commentCircle.append('<span class="deleteComment">Delete</span>');
                    }
                } else {
                    commentCircle.remove();
                    hideCommentBox();
                }

                //minus the no of comments we are deleting from thumb icon
                var totalCount = parseInt($('#' + thumbId).find('.commentsCount').html());
                if ((totalCount - 1) > 0) {
                    $('#' + thumbId).find('.commentsCount').html(totalCount - 1);
                } else {
                    $('#' + thumbId).find('.commentsCount').remove();
                }
            }
        });
    });

    //sending notification email to the admin
    $('#notify').click(function () {
        //sending notification
        if ($.cookie('user') !== undefined) {
            //check user notification status
            var excludeEmail;
            if (parseInt($.cookie('notify')) === 2)
                excludeEmail = $.cookie('email');

            //disabling notify button
            var self = $(this);
            self.attr('disabled', 'disabled');
            self.text('Sending...');

            //sending request to the server
            var projectId = self.val();
            $.ajax({
                type: "POST",
                url: '/comments/sendNotification/',
                data: {
                    projectId: projectId,
                    client: $.cookie('user'),
                    excludeEmail: excludeEmail
                },
                success: function (data) {
                    var data = $.parseJSON(data);
                    if (data.error) {
                        console.log(data.error);
                    } else {
                        //passing success message
                        self.siblings('h3').text(data.success.text).show();
                        self.siblings('h4').hide();
                        self.siblings('.notify-text').hide();
                        self.hide();
                    }
                },
                error: function (e) {
                    console.log('Some error occured while sending email!! Please try again later');
                    self.siblings('h3').text('Some error occured!! Please try again later.').show();
                    self.siblings('h4').hide();
                    self.hide();
                }
            });
        } else {
            console.log('Notification can only be send once you put some comments');
        }
    });

    //opening notification container when user clicks on send notification
    $('#notification').click(function () {
        showNotification();
    });

    //closing notification box
    $('#closeNotificationBox').click(function () {
        hideNotification();
        clearTimeout(timer);
    });

    //for user notification
    if ($.cookie('notify') === undefined || parseInt($.cookie('notify')) === 1) {
        $.cookie('notify', 1, {
            expires: 365
        });
        $('#notifyStatus').attr('checked', 'checked');
    }

    $('#notifyStatus').click(function () {
        if (parseInt($.cookie('notify')) === 1) {
            $.cookie('notify', 2, {
                expires: 365
            });
            $('#notifyStatus').removeAttr('checked');
        } else {
            $.cookie('notify', 1, {
                expires: 365
            });
            $('#notifyStatus').attr('checked', 'checked');
        }
    });


});

//closing modals if user press esc key
$(document).keyup(function (e) {
    if (e.keyCode == 27) {
        hideCommentBox();
    }
});

//custom function
function showNotification() {
    $('.notification-container h3').empty().hide();
    $('.notification-container h4').show();
    $('.notify-text').show();
    $('.notification-container button').removeAttr('disabled').text('Notify').show();
    $('.notification-container').addClass('active').show();
}

function hideNotification() {
    $('.notification-container').removeClass('active').fadeOut();
}

function hideCommentBox() {
    //name container
    $('#name-form').trigger("reset");
    $('.name-container').hide();
    //comments container
    $('#comment-form').trigger("reset");
    $('.comments-container').hide();
}

function hideEmailCredentialBox() {
    //name container
    $('#comment-form').trigger("reset");
    $('.comments-container').hide();
//comments container
//$('#comment-form').trigger("reset");
//$('.comments-container').hide();
}

function checkClass(x, y, thumbId) {
    $('.comments[data-x="' + x + '"][data-y="' + y + '"][data-thumb="' + thumbId + '"]').removeClass('unread').addClass('read');
    if (!$('.comments').hasClass('unread')) {
        $('a#' + thumbId + ' .commentsCount').removeClass('unread').addClass('read');
    }
}

function loadComments() {
    //resetting max width ofr the container
    $('.rg-image').css({
        'max-width': '100%'
    });

    //gloabl variables
    rgImage = $('.rg-image img'),
            imageResizeWidth = parseInt(rgImage.width()),
            imageOriginalWidth = parseInt(rgImage.get(0).naturalWidth),
            imageResizeHeight = parseInt(rgImage.height()),
            imageOriginalHeight = parseInt(rgImage.get(0).naturalHeight);

    //defining max-width for rg image container so that comments dont get move when someone zoomin and out
    if (imageOriginalWidth < $(window).width()) {
        $('.rg-image').css({
            'max-width': imageOriginalWidth
        });
    } else {
        $('.rg-image').css({
            'max-width': $(window).width()
        });
    }

    //defining variables for ajax call
    var projectId = rgImage.attr('data-project'),
            thumbId = rgImage.attr('data-thumb');

    //fetching comments
    $('.comments').remove();
    $.ajax({
        type: "GET",
        url: '/comments/view/' + projectId + '/' + thumbId + '/' + $.cookie('cookie_id'),
        success: function (data) {
            var comments = JSON.parse(data);
            if (Object.keys(comments).length > 0) {
                for (y_axis in comments) {
                    for (value in comments[y_axis]) {
                        var data_x = comments[y_axis][value][0]['x_axis'],
                                data_y = comments[y_axis][value][0]['y_axis'],
                                css_x = (data_x > 15) ? data_x - 15 : data_x,
                                css_y = (data_y > 40) ? data_y - 40 : data_y,
                                length = Object.keys(comments[y_axis][value]).length;

                        if (imageOriginalWidth > imageResizeWidth) {
                            var x = Math.floor((imageResizeWidth / imageOriginalWidth) * data_x);
                            css_x = (x > 15) ? x - 15 : x;
                        }

                        if (imageOriginalHeight > imageResizeHeight) {
                            var y = Math.floor((imageResizeHeight / imageOriginalHeight) * data_y);
                            css_y = (y > 40) ? y - 40 : y;
                        }

                        //defining classname as per the fetched records
                        //comment read/unread classes
                        var classname = 'read';
                        for (comment in comments[y_axis][value]) {
                            if (comments[y_axis][value][comment]['classname'] === 'unread') {
                                classname = 'unread';
                            }
                        }

                        var count = '<div class="comments ' + classname + '" data-project="' + projectId + '" data-thumb="' + thumbId + '" data-x="' + data_x + '" data-y="' + data_y + '" style="position:absolute; left:' + css_x + 'px; top:' + css_y + 'px;">' + length;

                        //userRoleId has been defined in gallery.ctp layout file
                        if (userRoleId === 1) {
                            count += '<span class="deleteComment">Delete</span></div>';
                        }
                        $('.rg-image').append(count);
                    }
                }
            }
            // New design || Change
            loadDescription();

            // Added for Image linking || hotspot
            loadLinkedImages();
        }
    });
}

/*
 Added code for new design || Change
 Below code is required to add description panel
 
 */

$(document).ready(function () {

    $(document.body).on('click', 'div #description-button,#hideDescription', function (e) {
        if ($('.description-container').css('display') === 'none') {
            var projectId = $("div.rg-image img").attr('data-project'),
                    thumbId = $("div.rg-image img").attr('data-thumb'),
                    x_axis = e.pageX, //page x axis will use for css
                    y_axis = e.pageY, //page y_axis will use for css
                    //css_x = Math.floor(x_axis),
                    //css_y = Math.floor(y_axis),
                    data_x = Math.floor((imageResizeWidth / imageOriginalWidth) * (x_axis - $(this).offset().left)), //will store in DB
                    data_y = Math.floor((imageResizeHeight / imageOriginalHeight) * (y_axis - $(this).offset().top)); //will store in DB
            //$('.name-container').hide();
            //$('.description-container .user-description').html('').hide();

            $('.description-container').css({
            }).attr({
                'data-x': 700,
                'data-y': 800,
                'data-project': projectId,
                'data-thumb': thumbId
            }).fadeIn();
            $('.description-container').removeClass('fadeOutRight');
            $('.description-container').addClass('fadeInRight');
            $('.rg-gallery').css("margin-right", "400" + "px");

            // var name = $.cookie('user')
            if (($.cookie('user') != undefined) && ($.cookie('user') != "")) {
                $("#descriptionusername").html($.cookie('user') + ":");
                $('.description-container #description').focus();
            }
            if ($('.client-description-content').length == 0) {
                //$('.description-area').show();
                //$('#action_button_div').hide();
                //$(".user-description").hide();
            } else {
                //$('.description-area').hide();
                //$('#action_button_div').show();
                //$(".user-description").show();
            }
            $("#description-button").addClass("active");
			//$("#description-button").find("span").attr("title", "Hide Notes")
        } else {
            //hideCommentBox();
            //hideNotification();
            hideDescription();
            $("#description-button").removeClass("active");
			//$("#description-button").find("span").attr("title", "Add Notes")
            $('.description-container').removeClass('fadeInRight');
            $('.description-container').addClass('fadeOutRight');
            $('.rg-gallery').css("margin-right", "0" + "px");
        }
    });



    $('#description-form').submit(function (e) {
        e.preventDefault();
        $("#d_flashMessage").hide();
        var name = $.cookie('user'),
                email = $.cookie('email'),
                cookie_id = $.cookie('cookie_id'),
                description = $('#description').val(),
                x_axis = $(this).parents('.description-container').attr('data-x'),
                y_axis = $(this).parents('.description-container').attr('data-y'),
                css_x = (x_axis > 15) ? x_axis - 15 : x_axis,
                css_y = (y_axis > 40) ? y_axis - 40 : y_axis,
                projectId = $(this).parents('.description-container').attr('data-project'),
                thumbId = $(this).parents('.description-container').attr('data-thumb'),
                updateKey = $("#update-key").val(),
                title = $.trim($("#description_title_heading").text()),
                formData = {
                    name: name,
                    email: email,
                    cookie_id: cookie_id,
                    description: description.replace(/\r?\n/g, '<br />'),
                    x_axis: x_axis,
                    y_axis: y_axis,
                    project_id: projectId,
                    thumb_id: thumbId,
                    title: title,
                    status_id: 1,
                    update_key: updateKey
                };

        //console.log(formData);return false;
        if (imageOriginalWidth > imageResizeWidth) {
            var x = Math.floor((imageResizeWidth / imageOriginalWidth) * x_axis);
            css_x = (x > 15) ? x - 15 : x;
        }

        if (imageOriginalHeight > imageResizeHeight) {
            var y = Math.floor((imageResizeHeight / imageOriginalHeight) * y_axis);
            css_y = (y > 40) ? y - 40 : y;
        }
        if (name !== undefined) {
            $.ajax({
                type: "POST",
                url: '/description/addDescription',
                dataType: "json",
                data: JSON.stringify(formData),
                success: function (data) {
                    /*var data = JSON.parse(data); 
                     $(".user-description").empty();
                     var userDescription = '<div class="client-description-container">' +
                     '<input type="hidden" id="update-key" value="'+ data['Description']['id'] +'">' +
                     '<p class="date">' + data['Description']['date'] + '</p>' +
                     '<div class="client-description clearfix">' +
                     '<strong>' + data['Description']['name'] + '</strong>' +
                     '<p class="time">' + data['Description']['time'] + '</p>' +
                     '<p class="client-description-content">' + data['Description']['description'] + '</p>' +
                     '</div>' +
                     '<div id="action_button_div">' +
                     '<input type="button" id="update_btn" value="Update" onclick = "javascript:clickOnUpdateButton();">' +
                     '<input type="button" id="delete_btn" value="Delete" onclick = "javascript:clickOnDeleteButton();">' +
                     '</div>' +
                     '</div>';
                     $('.user-description').show().append(userDescription);
                     $('.description-area').hide();
                     $('#description-form').trigger("reset");					
                     hideDescription();
                     */
                    var descriptionId = data['Description']['id'];
                    var descriptionTitle = data['Description']['title'];
                    if ($(".user-description p.date:contains('" + data['Description']['date'] + "')").length > 0) {
                        var userDescription = '<div class="client-description clearfix">' +
                                '<strong>' + data['Description']['name'] + '</strong>' +
                                '<p class="time">' + data['Description']['time'] + '</p>';
                        if (userRoleId == 1 || userRoleId == 2) {
                            userDescription += '<p class="removeDescription" data-id="' + descriptionId + '" onclick = "javascript:clickOnDeleteButton(' + descriptionId + ');">Remove</p>';
                        }
                        userDescription += '<p class="client-description-content">' + data['Description']['description'] + '</p>' +
                                '</div>';
                        $("p.date:contains('" + data['Description']['date'] + "')").parent('.client-description-container').append(userDescription);
                    } else {
                        var userDescription = '<div class="client-description-container">' +
                                '<p class="date">' + data['Description']['date'] + '</p>' +
                                '<div class="client-description clearfix">' +
                                '<strong>' + data['Description']['name'] + '</strong>' +
                                '<p class="time">' + data['Description']['time'] + '</p>';
                        if (userRoleId == 1 || userRoleId == 2) {
                            userDescription += '<p class="removeDescription" data-id="' + descriptionId + '" onclick = "javascript:clickOnDeleteButton(' + descriptionId + ');">Remove</p>';
                        }
                        userDescription += '<p class="client-description-content">' + data['Description']['description'] + '</p>' +
                                '</div>' +
                                '</div>';
                        $('.user-description').show().append(userDescription);
                    }

                    $("#description_title_heading").html(descriptionTitle);
                    $("#d_oldtitle").val(descriptionTitle);
                    $("#DescriptionEditTitle").val(descriptionTitle);

                    $('#description-form').trigger("reset");
                    //hideDescription();
                    $('.name-container').hide();
                    $('#description').html("");
                    $('#description').focus();
                    //comments container
                    $("#d_flashMessage").text("").hide();

                },
                error: function () {
                    console.log('Some error occurs while posting comment!! Please try again later.');
                }
            });
        }
    });

    /* submit  a form to change Description title */

    var titleForm = document.getElementById("DescriptionChangeTitle");
    titleForm.addEventListener("keydown", function (event) {
        if (event.keyCode === 13) {  //checks whether the pressed key is "Enter"
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: '/admin/description/changeTitle',
                dataType: "json",
                data: $(titleForm).serialize(),
                success: function (response) {
                    if (response.status != "Fail" || response.length > 0) {
                        var descriptionTitle = response[0]['Description']['title'];
                        $("#description_title_heading").html(descriptionTitle);
                        $("#d_oldtitle").val(descriptionTitle);
                        $("#DescriptionEditTitle").val(descriptionTitle);
                        $("#description-form").trigger("reset");
                        $("#d_flashMessage").hide();
                        $("#d_flashMessage").removeClass("alert alert-danger").addClass("alert alert-success").text("Description title updated successfully").show();
                        $("#DescriptionEditTitle").blur(); // Remove focus
                        //hideDescription();
                        $('.name-container').hide();
                        $('#description').html("");
                        $('#description').focus();
                        $("#d_flashMessage").text("").hide();
                    } else {
                        $("#d_flashMessage").hide();
                        $("#d_flashMessage").removeClass("alert alert-success").addClass("alert alert-danger").text(response.msg).hide().show();
                    }
                },
                error: function () {
                    $("#d_flashMessage").hide();
                    console.log('Some error occurs while posting comment!! Please try again later.');
                }
            });
        }
    });


    //Open Username and email box 
    $('#description').on('focus', function () {
        if ($.cookie('user') === undefined || $.cookie('email') === undefined) {
            //opening name modal for user
            var x_axis = $(this).parents('.description-container').css('left').split('px');
            var y_axis = $(this).parents('.description-container').css('top').split('px');
            $('.name-container').css({
                //left: parseInt(x_axis[0]) + 60,
                //top: parseInt(y_axis[0]) + 10
                left: 840,
                top: 390
            }).show();
            $('.name-container #name').focus();
            //disabling submit button of comments container
            $('#addDescription').attr('disabled', 'disabled');
            $(".name-container h3").text('Enter your details');
            $("#name-form").find('input[type="submit"]').attr('value', 'Submit');
        }
    });

    //closing description box
    /*$('.hideDescription').click(function () {
     $('.description-container').removeClass('fadeInRight');
     $('.description-container').addClass('fadeOutRight');
     hideDescription();
     return false;
     });*/

    $('#closeDescriptionBox').click(function () {
        hideDescription();
        return false;
    });

    $('#cancelDescription').click(function () {
        $("#description").val("");
    })

    $(document.body).on("dblclick", "#description_title_heading", function () {
        $(this).hide();
        $(this).next('form').show();
        $(this).next('form').find('input[type="text"]').focus();
    }).on("blur", "#DescriptionEditTitle", function () {
        $("#description_title_heading").show();
        $(this).closest('form').hide();
    });
})

function hideDescription() {
    //$('#name-form').trigger("reset");
    $('.name-container').hide();
    $('#description').html("");
    //comments container
    $('#description-form').trigger("reset");
    $('.description-container').fadeOut();
    $("#d_flashMessage").text("").hide();
}

function loadDescription() {

    //gloabl variables
    rgImage = $('.rg-image img'),
            imageResizeWidth = parseInt(rgImage.width()),
            imageOriginalWidth = parseInt(rgImage.get(0).naturalWidth),
            imageResizeHeight = parseInt(rgImage.height()),
            imageOriginalHeight = parseInt(rgImage.get(0).naturalHeight);

    //defining variables for ajax call
    var projectId = rgImage.attr('data-project'),
            thumbId = rgImage.attr('data-thumb');

    //fetching comments
    // $('.comments').remove();
    $.ajax({
        type: "GET",
        url: '/description/view/' + projectId + '/' + thumbId + '/' + $.cookie('cookie_id'),
        success: function (data) {
            var description = JSON.parse(data);

            //alert(description);
            $(".user-description").empty();
            if (description.length > 0) {
                $.each(description, function (key, data) {

                    /*var userComments = '<div class="client-description-container">' +
                     '<input type="hidden" id="update-key" value="'+ data['Description']['id'] +'">' +
                     '<p class="date">' + data['Description']['date'] + '</p>' +
                     '<div class="client-description clearfix">' +
                     '<strong>' + data['Description']['name'] + '</strong>' +
                     '<p class="time">' + data['Description']['time'] + '</p>' +
                     '<p class="client-description-content">' + data['Description']['description'] + '</p>' +
                     '</div>' +
                     '<div id="action_button_div">' +
                     '<input type="button" id="update_btn" value="Update" onclick = "javascript:clickOnUpdateButton();">' +
                     '<input type="button" id="delete_btn" value="Delete" onclick = "javascript:clickOnDeleteButton();">' +
                     '</div>' +
                     '</div>';
                     $('.user-description').show().append(userComments);
                     $('.description-area').hide();
                     */
                    var descriptionId = data['Description']['id'];
                    var descriptionTitle = data['Description']['title'];
                    if ($(".user-description p.date:contains('" + data['Description']['date'] + "')").length > 0) {
                        var userDescription = '<div class="client-description clearfix">' +
                                '<strong>' + data['Description']['name'] + '</strong>' +
                                '<p class="time">' + data['Description']['time'] + '</p>';
                        if (userRoleId == 1 || userRoleId == 2) {
                            userDescription += '<p class="removeDescription" data-id="' + descriptionId + '" onclick = "javascript:clickOnDeleteButton(' + descriptionId + ');">Remove</p>';
                        }
                        userDescription += '<p class="client-description-content">' + data['Description']['description'] + '</p>' +
                                '</div>';
                        $("p.date:contains('" + data['Description']['date'] + "')").parent('.client-description-container').append(userDescription);
                    } else {
                        var userDescription = '<div class="client-description-container">' +
                                '<p class="date">' + data['Description']['date'] + '</p>' +
                                '<div class="client-description clearfix">' +
                                '<strong>' + data['Description']['name'] + '</strong>' +
                                '<p class="time">' + data['Description']['time'] + '</p>';

                        if (userRoleId == 1 || userRoleId == 2) {
                            userDescription += '<p class="removeDescription" data-id="' + descriptionId + '" onclick = "javascript:clickOnDeleteButton(' + descriptionId + ');">Remove</p>';
                        }
                        userDescription += '<p class="client-description-content">' + data['Description']['description'] + '</p>' +
                                '</div>' +
                                '</div>';
                        $('.user-description').show().append(userDescription);
                    }
                    $("#description_title_heading").html(descriptionTitle);
                    $("#d_oldtitle").val(descriptionTitle);
                    $("#DescriptionEditTitle").val(descriptionTitle);

                    $('#description-form').trigger("reset");
                    //hideDescription();
                    $('.name-container').hide();
                    $('#description').html("");
                    $('#description').focus();
                    //comments container
                    $("#d_flashMessage").text("").hide();
                });
            } else {
                //$('#action_button_div').fadeOut();
                //$('.description-area').fadeIn();
            }
            $("#d_project_id").val(projectId);
            $("#d_thumb_id").val(thumbId);
            $('#description-form').trigger("reset");
            //hideDescription();
            $('.name-container').hide();
            $('#description').html("");
            $('#description').focus();
            //comments container
            $("#d_flashMessage").text("").hide();
        }
    });
}

function clickOnUpdateButton() {
    $(".user-description").hide();
    $('#action_button_div').fadeOut();
    $('.description-area').fadeIn();

    var descriptionData = $('.client-description-content').html().replace(/<br\s*\/?>/mg, "\n");
    ;
    $("#description").html("");
    $('#description').html(descriptionData);
}

function clickOnDeleteButton(descriptionId) {
    //var updateKey = $("#update-key").val();
    if (descriptionId > 0 && (userRoleId == 1 || userRoleId == 2)) {
        $.ajax({
            type: "DELETE",
            url: '/description/delete/' + descriptionId,
            success: function (data) {
                if (data.search("success:") === 0) {
                    /*$(".user-description").empty();
                     $(".description-area").show();
                     $('#description-form').trigger("reset");
                     hideDescription();
                     */
                    loadDescription();
                    //$(".description-container").fadeIn();
                }
            }
        });
    }
}
/*____________________ END OF DESCRIPTION FEATURE _____________________________*/


/* __________________________________________
 
 START IMAGE LINKING || HOTSPOT FEATURE
 
 */
var canvas;
var ctx;
var rect = {};
var drag = false;
var imageLinkingPopupIsOpen = false;
$(document).ready(function () {

    //Initiate to draw rectangle
    $("#resizeDiv").draggable({
        containment: '.rg-image-wrapper .rg-image',
        start: function (e, ui) {
            $(".image-linking-container").fadeOut();
        },
        stop: function (e, ui) {
            //elementPos.html(JSON.stringify(ui.position));

            var left = ui.position.left;
            var top = ui.position.top;
            var width = $("#resizeDiv").css("width").split("px")[0];
            var height = $("#resizeDiv").css("height").split("px")[0];
            var attributes = {left: left, top: top, pageX: e.pageX, pageY: e.pageY, width: width, height: height};
            $("#resizeDiv").removeData("attributes");
            $("#resizeDiv").data("attributes", attributes);

            // To open image linking popup
            if ($("#resizeDiv").is(":visible")) {
                var attributes = $("#resizeDiv").data('attributes');
                var left = attributes.left;
                var top = attributes.top;
                var pageX = attributes.pageX;
                var pageY = attributes.pageY;
                var width = attributes.width;
                var height = attributes.height;
                openImageLinkingPopup(left, top, pageX, pageY, width, height);
            }
        }
    }).resizable({
        containment: '.rg-image-wrapper .rg-image',
        start: function (e, ui) {
            $(".image-linking-container").fadeOut();
        },
        stop: function (e, ui) {
            var left = ui.position.left;
            var top = ui.position.top;

            if (ui.size.height < 40) {
                var height = 40;
            } else {
                var height = ui.size.height;
            }
            var attributes = {left: left, top: top, pageX: e.pageX, pageY: e.pageY, width: ui.size.width, height: height};
            $("#resizeDiv").removeData("attributes");
            $("#resizeDiv").data("attributes", attributes);
            //openImageLinkingPopup(left, top, e.pageX, e.pageY, ui.size.width, ui.size.height);

            // To open image linking popup
            if ($("#resizeDiv").is(":visible")) {
                var attributes = $("#resizeDiv").data('attributes');
                var left = attributes.left;
                var top = attributes.top;
                var pageX = attributes.pageX;
                var pageY = attributes.pageY;
                var width = attributes.width;
                var height = attributes.height;
                openImageLinkingPopup(left, top, pageX, pageY, width, height);
            }
        }
    });

    $("#resizeDiv").on("click", function () {
        if (imageLinkingPopupIsOpen) {
            if ($(".image-linking-container").is(":visible")) {
                $(".image-linking-container").hide();
            } else {
                $(".image-linking-container").show();
            }
        }
    })

    /* 
     When finally draw rectangle done. when click on ok button a popup will open to link images.
     NOTE:-- This functionality has been moved inside drag and drop	
     Initially image linking popup opens when click on "OK" button
     
     $("#hotspot_creation_done").on("click", function(){
     if($("#resizeDiv").is(":visible")){
     var attributes = $("#resizeDiv").data('attributes');
     var left = attributes.left;
     var top = attributes.top;
     var pageX = attributes.pageX;
     var pageY = attributes.pageY;
     var width = attributes.width;
     var height = attributes.height;
     openImageLinkingPopup(left, top, pageX, pageY, width, height);
     }
     })
     */


    //Push the all images into select dropdown
    var totalImageCount = $(".es-carousel-wrapper .es-carousel ul li").length - 1; // Front end showing 1 li extra
    var optionHtml = "<option value=''> Select </option>";
    for (var i = 1; i <= totalImageCount; i++) {
        optionHtml += '<option value=' + i + '> Image-' + i + '</option>';
    }
    $("#image-linking-select").append(optionHtml);

    // Will show all comments
    $("#comment_mode").click(function () {
        $("#interactive_mode").removeClass("active");
        $(this).addClass("active");
        $('#hideComments').show();
        $('#showComments').hide();
        $('#mode_button').hide();
        $("input[name='mode']:checked").attr("checked", false)
        $('body').removeClass('image-linking-active preview-mode');
        $('body').addClass('comments-active');
        //$("#hotspot_creation_done").hide();
    });

    // This mode show the create hotspot and link images
    $("#interactive_mode").click(function () {
        $("#comment_mode").removeClass("active");
        $(this).addClass("active");
        $('body').removeClass("image-linking-active").removeClass("comments-active").removeClass("preview-mode");
        $('.video-view').removeClass("active");
        $('#hideComments').hide();
        $('#showComments').show();
        $('#mode_button').show();
        //$("#hotspot_creation_done").hide();
        hideDrawRectangleBox();
    });


    //hiding comments from the page

    /*$('#hideImageLinking').click(function() {
     $('body').removeClass('image-linking-active');
     //$('body').addClass('comments-active');
     $(this).hide();
     $('#create_hotspot').show();
     hideDrawRectangleBox();
     //$('#hideComments').show();
     //$('#showComments').hide();
     });
     */

    //make active to draw rectangle
    $('#create_hotspot').click(function () {
        $('body').removeClass();
        $('body').addClass("page-body");
        $('body').addClass('image-linking-active');
        $('body').removeClass('preview-mode');
        $('.page-view-mode').addClass("active");
        $('#showComments').show();
        $('#hideComments').hide();
        setLinkedImageIconOpacity();
        hideDrawRectangleBox();
    });

    // Onclick dont create rectangle
    $('#preview_mode').click(function () {
        $('body').removeClass();
        $('body').addClass("page-body");
        $('body').addClass('image-linking-active').addClass('preview-mode');
        $('.video-view').removeClass('active');
        $('#showComments').show();
        $('#hideComments').hide();
        hideDrawRectangleBox();
    })

    // When click on body document
    $(document.body).on('click', 'div.rg-image img', function (e) {
        //console.log(e);
        if ($('body').hasClass('image-linking-active') && !$('body').hasClass('preview-mode') && $('.image-linking-container').css('display') === 'none' && !$("#resizeDiv").is(":visible")) {
            var projectId = $(this).attr('data-project'),
                    thumbId = $(this).attr('data-thumb'),
                    x_axis = e.pageX, //page x axis will use for css
                    y_axis = e.pageY; //page y_axis will use for css

            if (imageOriginalWidth == imageResizeWidth) {
                var data_x = Math.floor((imageResizeWidth / imageOriginalWidth) * (x_axis - $(this).offset().left)); //will store in DB
                var data_y = Math.floor((imageResizeHeight / imageOriginalHeight) * (y_axis - $(this).offset().top)); //will store in DB
            } else {
                var data_x = x_axis;
                var data_y = y_axis;
            }

            //storing actual point as per the orignal width
            /*if (imageOriginalWidth > imageResizeWidth) {
             data_x = Math.floor((imageOriginalWidth / imageResizeWidth) * (x_axis - $(this).offset().left));
             }
             
             //storing actual point as per the orignal height
             if (imageOriginalHeight > imageResizeHeight) {
             data_y = Math.floor((imageOriginalHeight / imageResizeHeight) * (y_axis - $(this).offset().top));
             }*/


            //moving comment window as per the click event
            /*if (windowWidth - x_axis < 405) {
             x_axis = x_axis - 400;
             }
             if (windowHeight - (windowHeight - y_axis) < 205) {
             y_axis = y_axis + 40;
             }*/

            var width = $("#resizeDiv").css("width", "0px");
            var height = $("#resizeDiv").css("height", "0px");
            $('.dragBox').css({
                top: y_axis,
                left: x_axis,
                width: width,
                height: height
            }).attr({
                'data-x': data_x,
                'data-y': data_y,
                'data-project': projectId,
                'data-thumb': thumbId
            }).show();

        } else {
            hideDrawRectangleBox();
        }
    });


    // Submiting Image Linking to the database

    $('#image-linking-form').submit(function (e) {
        e.preventDefault();
        var name = $.cookie('user'),
                email = $.cookie('email'),
                cookie_id = $.cookie('cookie_id'),
                linkedImageVal = $('#image-linking-select').val(),
                x_axis = $(this).parents('.image-linking-container').attr('data-x'),
                y_axis = $(this).parents('.image-linking-container').attr('data-y'),
                css_x = (x_axis > 15 && 0) ? x_axis - 15 : x_axis,
                css_y = (y_axis > 40 && 0) ? y_axis - 40 : y_axis,
                projectId = $(this).parents('.image-linking-container').attr('data-project'),
                thumbId = $(this).parents('.image-linking-container').attr('data-thumb');

        var linkedImageActualPathRel = $(".es-carousel-wrapper .es-carousel ul li").find('a#image' + linkedImageVal + '').find("img").attr("data-large");
        var linkedImageActualValArr = linkedImageActualPathRel.split("/");
        var linkedImageActualName = $.trim(linkedImageActualValArr[linkedImageActualValArr.length - 1]);
        var linkedProjectFolder = $.trim(linkedImageActualValArr[linkedImageActualValArr.length - 2]);

        var existingImageActualPathRel = $(".es-carousel-wrapper .es-carousel ul li").find('a#' + thumbId + '').find("img").attr("data-large");
        var existingImageActualValArr = existingImageActualPathRel.split("/");
        var existingImageActualName = $.trim(existingImageActualValArr[existingImageActualValArr.length - 1]);
        var existingImageVal = thumbId.split("image")[1];

        //Property of rectangle box
        var rectangleWidth = $(this).parents('.image-linking-container').attr('data-width');
        var rectangleHeight = $(this).parents('.image-linking-container').attr('data-height');
        var rectangleLeft = $(this).parents('.image-linking-container').attr('data-left');
        var rectangleTop = $(this).parents('.image-linking-container').attr('data-top');


        var formData = {
            name: name,
            email: email,
            cookie_id: cookie_id,
            linked_image_name: linkedImageActualName,
            linked_image_value: linkedImageVal,
            asset_project_linked_folder: linkedProjectFolder,
            existing_image_name: existingImageActualName,
            existing_image_value: existingImageVal,
            asset_category: 3, // Creatives
            x_axis: x_axis,
            y_axis: y_axis,
            rectangle_width: rectangleWidth,
            rectangle_height: rectangleHeight,
            rectangle_left: rectangleLeft,
            rectangle_top: rectangleTop,
            project_id: projectId,
            thumb_id: thumbId,
            status_id: 1
        };

        /*if (imageOriginalWidth > imageResizeWidth) {
         var x = Math.floor((imageResizeWidth / imageOriginalWidth) * x_axis);
         css_x = (x > 15) ? x - 15 : x;
         }
         
         if (imageOriginalHeight > imageResizeHeight) {
         var y = Math.floor((imageResizeHeight / imageOriginalHeight) * y_axis);
         css_y = (y > 40) ? y - 40 : y;
         }
         */
        /*__ User window will not popup here (After discussion with Manmath) 
         does not matter name and email are set in cookie are not.
         pusch the username and email from backend only in this case
         */
        if (name !== undefined || true) {
            $.ajax({
                type: "POST",
                url: '/linkedimages/addLink',
                dataType: "json",
                data: JSON.stringify(formData),
                success: function (data) {
                    /*var userComments = '<div class="client-comments-container">' +
                     '<p class="date">' + data['LinkedImages']['date'] + '</p>' +
                     '<div class="client-comments clearfix">' +
                     '<strong>' + data['LinkedImages']['name'] + '</strong>' +
                     '<p class="time">' + data['LinkedImages']['time'] + '</p>' +
                     '<p class="client-comments-content">' + data['LinkedImages']['LinkedImages'] + '</p>' +
                     '</div>' +
                     '</div>';
                     */
                    $('#image-linking-form').trigger("reset");

                    //incrementing the value in the circle or inserting the comments count
                    var count = '<div class="linked-image read" data-project="' + projectId + '" data-thumb="' + thumbId + '" data-linked-image-val="' + linkedImageVal + '" data-x="' + x_axis + '" data-y="' + y_axis + '" style="position:absolute; left:' + css_x + 'px; top:' + css_y + 'px; width:' + rectangleWidth + 'px; height:' + rectangleHeight + 'px;">' + linkedImageVal;

                    if (userRoleId == 1) {
                        //count += '<span class="deleteimageLinking">Delete</span>';
                    }
                    count += '</div>';

                    $('.rg-image').append(count);

                    //closing comment container after successful submission
                    $('.image-linking-container').hide();
                    $('#resizeDiv').hide();

                },
                error: function () {
                    console.log('Some error occurs while posting comment!! Please try again later.');
                }
            });
        }
    });

    // When click on linked icon to goto the linked image
    $(document.body).on("click", ".linked-image", function () {
        var linkedImgVal = $(this).attr("data-linked-image-val");
        var imageAnchorId = 'image' + linkedImgVal;
        //if($(".rg-image").find("img").attr("data-thumb") == "image1"){
        $(".es-carousel-wrapper .es-carousel ul li").find('a#' + imageAnchorId + '').find('img').click();
        //}
    })

    $('.cancelImageLinking').click(function () {
        hideImageLinkingBox();
        return false;
    });

    $('#closeImageLinkingBox').click(function () {
        hideImageLinkingBox();
        return false;
    });
})

// Load image linking box when resize stop

function openImageLinkingPopup(leftX, topY, pageX, pageY, width, height) {
    //$(document.body).on('click', 'div.rg-image img', function(e) {

    var obj = $('div.rg-image img');
    //var obj = $('div#resizeDiv');

    if ($('body').hasClass('image-linking-active') && $('.image-linking-container').css('display') === 'none' /*&& !$('.notification-container').hasClass('active')*/) {
        var projectId = obj.attr('data-project'),
                thumbId = obj.attr('data-thumb'),
                x_axis = leftX, //page x axis will use for css
                y_axis = topY; //page y_axis will use for css
        if (imageOriginalHeight == imageResizeHeight) {
            var data_x = Math.floor((imageResizeWidth / imageOriginalWidth) * (x_axis - obj.offset().left)); //will store in DB
            var data_y = Math.floor((imageResizeHeight / imageOriginalHeight) * (y_axis - obj.offset().top)); //will store in DB
        } else {
            var data_x = x_axis;
            var data_y = y_axis;
        }
        //storing actual point as per the orignal width
        /*if (imageOriginalWidth > imageResizeWidth) {
         data_x = Math.floor((imageOriginalWidth / imageResizeWidth) * (x_axis - obj.offset().left));
         }
         
         //storing actual point as per the orignal height
         if (imageOriginalHeight > imageResizeHeight) {
         data_y = Math.floor((imageOriginalHeight / imageResizeHeight) * (y_axis - obj.offset().top));
         }*/
        //moving comment window as per the click event
        /*if (windowWidth - x_axis < 405) {
         x_axis = x_axis - 400;
         }
         if (windowHeight - (windowHeight - y_axis) < 205) {
         y_axis = y_axis + 40;
         }*/

        $('.name-container').hide();
        $('.image-linking-container .user-comments').html('').hide();
        $('.image-linking-container').css({
            top: y_axis,
            left: x_axis,
        }).attr({
            'data-x': data_x,
            'data-y': data_y,
            'data-project': projectId,
            'data-thumb': thumbId,
            'data-width': width,
            'data-height': height,
            'data-left': x_axis,
            'data-top': y_axis,

        });

        if (!imageLinkingPopupIsOpen) {
            $('.image-linking-container').show();
            imageLinkingPopupIsOpen = true;
        }

        // var name = $.cookie('user')
        if (($.cookie('user') != undefined) && ($.cookie('user') != "")) {
            $("#imagelinkingusername").html($.cookie('user') + ":");
            $('.image-linking-container #addImageLinking').focus();
        }
        //hideDrawRectangleBox();
        adjustHeightImageLinkingBox();
    } else {
        hideImageLinkingBox();
        hideDrawRectangleBox();
    }
    //});
}

function loadLinkedImages() {
    //resetting max width ofr the container
    $('.rg-image').css({
        'max-width': '100%'
    });
    //gloabl variables
    rgImage = $('.rg-image img'),
            imageResizeWidth = parseInt(rgImage.width()),
            imageOriginalWidth = parseInt(rgImage.get(0).naturalWidth),
            imageResizeHeight = parseInt(rgImage.height()),
            imageOriginalHeight = parseInt(rgImage.get(0).naturalHeight);

    //defining max-width for rg image container so that comments dont get move when someone zoomin and out
    if (imageOriginalWidth < $(window).width()) {
        $('.rg-image').css({
            'max-width': imageOriginalWidth
        });
    } else {
        $('.rg-image').css({
            'max-width': $(window).width()
        });
    }

    //defining variables for ajax call
    var projectId = rgImage.attr('data-project'),
            thumbId = rgImage.attr('data-thumb');

    var existingImageActualPathRel = rgImage.attr('src');
    var linkedImageActualValArr = existingImageActualPathRel.split("/");
    var linkedProjectFolder = $.trim(linkedImageActualValArr[linkedImageActualValArr.length - 2]);

    //fetching Linked images
    $('.linked-image').remove();
    $.ajax({
        type: "GET",
        url: '/linkedimages/view/' + projectId + '/' + thumbId + '/' + $.cookie('cookie_id') + '/' + linkedProjectFolder,
        success: function (data) {
            var linkedImages = JSON.parse(data);
            if (Object.keys(linkedImages).length > 0) {
                for (var i = 0; i < Object.keys(linkedImages).length; i++) {
                    var data_x = linkedImages[i]['LinkedImage']['x_axis'],
                            data_y = linkedImages[i]['LinkedImage']['y_axis'],
                            css_x = (data_x > 15 && 0) ? data_x - 15 : data_x,
                            css_y = (data_y > 40 && 0) ? data_y - 40 : data_y,
                            length = 1,
                            linkedImageVal = linkedImages[i]['LinkedImage']['linked_image_value'];
                    rectangleWidth = linkedImages[i]['LinkedImage']['rectangle_width'];
                    rectangleHeight = linkedImages[i]['LinkedImage']['rectangle_height'];

                    /*if (imageOriginalWidth > imageResizeWidth) {
                     var x = Math.floor((imageResizeWidth / imageOriginalWidth) * data_x);
                     css_x = (x > 15) ? x - 15 : x;
                     }
                     
                     if (imageOriginalHeight > imageResizeHeight) {
                     var y = Math.floor((imageResizeHeight / imageOriginalHeight) * data_y);
                     css_y = (y > 40) ? y - 40 : y;
                     }*/

                    //defining classname as per the fetched records
                    //comment read/unread classes | No need of this class name
                    var classname = 'read';

                    var count = '<div class="linked-image ' + classname + '" data-project="' + projectId + '" data-thumb="' + thumbId + '" data-linked-image-val="' + linkedImageVal + '" data-x="' + data_x + '" data-y="' + data_y + '" style="position:absolute; left:' + css_x + 'px; top:' + css_y + 'px; width:' + rectangleWidth + 'px; height:' + rectangleHeight + 'px;">' + linkedImageVal;

                    //userRoleId has been defined in gallery.ctp layout file
                    if (userRoleId === 1) {
                        //count += '<span class="deleteAnnotation">Delete</span></div>';
                    }
                    $('.rg-image').append(count);
                }
            }
        }
    });
    hideImageLinkingBox();
    hideDrawRectangleBox();
    setTimeout(function () {
        setLinkedImageIconOpacity();
    }, 500)
}

function hideImageLinkingBox() {
    //name container
    $('#name-form').trigger("reset");
    $('.name-container').hide();
    //linking container
    $('#image-linking-form').trigger("reset");
    $('.image-linking-container').hide();
}

function hideDrawRectangleBox() {
    $('#resizeDiv').hide();
}

/*__ Adjust the height of image linked popup ____*/
var constant = 12;
function adjustHeightImageLinkingBox() {
    var height = $("#resizeDiv").css("height").split("px")[0];
    var finalPos = parseInt(height) + parseInt(constant);
    $('.image-linking-container-inner').css("margin-top", finalPos + "px");
}

/* _____ To change the opacity of linked image icon____ */
function setLinkedImageIconOpacity() {

    if ($("body").hasClass("image-linking-active") && !$("body").hasClass("preview-mode")) {
        $(".linked-image").each(function () {
            $(this).css("opacity", ".8");
        });
    } else {
        setTimeout(function () {
            $(".linked-image").each(function () {
                $(this).css("opacity", ".01");
            })
        }, 500);
    }

    $(".rg-image").on("click", function () {
        if ($("body").hasClass("preview-mode")) {
            $(".linked-image").each(function () {
                $(this).css("opacity", ".8");
            });
            setTimeout(function () {
                $(".linked-image").each(function () {
                    $(this).css("opacity", ".01");
                });
            }, 500)
        } else {

        }
    })
}


// To make the icon section only one active at a time when click
$(document).ready(function () {
    $("#hideComments").hide();
    $("#showComments").show();
    $(".page-view-mode, .video-view, .commentsView, .description-view").on("click", function () {
        var iconDiv = $(".control-panel").children();
        iconDiv.each(function () {
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
            }
        });
        var clickedElement = $(this);
        clickedElement.addClass("active");
    })
})

$(document).ready(function () {

    /*____ For Thumbnail and Section _____*/
    $(".rg-view-full").click(function () {
        $('.tumbnails-gallery-view').removeClass('fadeInUp');
        $('.tumbnails-gallery-view').addClass('fadeOutDown');
        $('.thumb-hide-show').show().addClass('fadeInUp').addClass('active');
        $('.thumb-view-separator').show();
    });
    $(".hide-show-frame-btn").click(function () {
        $('.tumbnails-gallery-view').removeClass('fadeOutDown');
        $('.tumbnails-gallery-view').addClass('fadeInUp');
        $('.thumb-hide-show').hide().removeClass('fadeInUp').removeClass('active');
        $('.thumb-view-separator').hide();
    });

    /* ____For Tooltip _______*/
    $('[data-toggle="tooltip"]').tooltip({
        //animation: true,
        //delay: {show: 500, hide: 100000}
    });

    setLinkedImageIconOpacity();
});

$(document).ready(function(){
	var iconArea = $("#create_hotspot,#preview_mode");
})


