<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');  
if(isset($_POST) && !empty($_POST['video_data']))
{	

	//print_r($_POST); die;
	$video_data=$_POST['video_data'];
	
	/* connect to database */
	
	$db=  new PDO('mysql:host=localhost;dbname=video_demo','root','');
	$query=$db->prepare("UPDATE  `videos` SET video_data = :json where video_id=1");
	$query->bindValue(':json', $video_data, PDO::PARAM_STR);
	$res=$query->execute();
	if($res)
	{
		echo "success"; die;
	}
	$video_data='{
    "videos": [{
        "videoName": "Introduction",
        "videPath": "video/Why-is-good-UIUX-important.mp4",
        "overLay": [{
            "startPoint": 0,
            "EndPoint": 9,
            "imageUrl": "https://www.tecsolsoftware.com/wp-content/uploads/2016/03/ui-ux-design.png",
            "menuText": ""

        }, {
            "startPoint": 10,
            "EndPoint": 20,
            "imageUrl": "",
            "menuText": ["<a href=\'https://www.google.co.in/\' target=\'_blank\'>User Experience</a>", "UX Designer", "Confusing UX Design", "Interface Design"]

        }, {
            "startPoint": 21,
            "EndPoint": 30,
            "imageUrl": "http://anythingcartoon.com/images/me-front-page-u16215.png",
            "menuText": ""
        }, {
            "startPoint": 31,
            "EndPoint": 40,
            "imageUrl": "",
            "menuText": ["<a href=\'https://www.google.co.in/\' target=\'_blank\'>User Experience</a>", "UX Designer", "Confusing UX Design", "Interface Design"]

        }, {
            "startPoint": 41,
            "EndPoint": 50,
            "imageUrl": "https://www.tecsolsoftware.com/wp-content/uploads/2016/03/ui-ux-design.png",
            "menuText": ""

        }]

    }, {
        "videoName": "Research",
        "videPath": "video/Get-Started-in-UX.mp4",
        "overLay": [{
            "startPoint": 0,
            "EndPoint": 15,
            "imageUrl": "https://s-media-cache-ak0.pinimg.com/originals/b6/1b/96/b61b965a3071e22584d2b4c4e2616794.gif",
            "menuText": ""

        }, {
            "startPoint": 16,
            "EndPoint": 27,
            "imageUrl": "",
            "menuText": ["<a href=\'https://www.google.co.in/\' target=\'_blank\'>User Experience</a>", "UX Designer", "Confusing UX Design", "Interface Design"]

        }, {
            "startPoint": 28,
            "EndPoint": 40,
            "imageUrl": "http://www.isabellethye.com/wp-content/uploads/2014/08/ui_ux_experience.png",
            "menuText": ""
        }, {
            "startPoint": 41,
            "EndPoint": 45,
            "imageUrl": "",
            "menuText": ["<a href=\'https://www.google.co.in/\' target=\'_blank\'>User Experience</a>", "UX Designer", "Confusing UX Design", "Interface Design"]

        }, {
            "startPoint": 46,
            "EndPoint": 55,
            "imageUrl": "https://www.atomix.com.au/media/2013/12/atomix_ux.png",
            "menuText": ""

        }]

    }, {
        "videoName": "Analysis",
        "startPoint": 107,
        "EndPoint": 201,
        "videPath": "video/Skills-Youll-To-Land-a-UX-Design-Job.mp4",
        "overLay": [{
            "startPoint": 0,
            "EndPoint": 15,
            "imageUrl": "http://static1.squarespace.com/static/52cff4eae4b031d07dd9fbf5/t/52e69ee1e4b0d1eb5c79fc2f/1390845665703/ux-kits.png",
            "menuText": ""

        }, {
            "startPoint": 16,
            "EndPoint": 27,
            "imageUrl": "",
            "menuText": ["<a href=\'https://www.google.co.in/\' target=\'_blank\'>User Experience</a>", "UX Designer", "Confusing UX Design", "Interface Design"]

        }, {
            "startPoint": 28,
            "EndPoint": 40,
            "imageUrl": "http://marychadwick.com/img/badges/moving-forward-w-ux.png",
            "menuText": ""
        }, {
            "startPoint": 41,
            "EndPoint": 45,
            "imageUrl": "",
            "menuText": ["<a href=\'https://www.google.co.in/\' target=\'_blank\'>User Experience</a>", "UX Designer", "Confusing UX Design", "Interface Design"]

        }, {
            "startPoint": 46,
            "EndPoint": 55,
            "imageUrl": "https://www.atomix.com.au/media/2013/12/atomix_ux.png",
            "menuText": ""

        }]
    }, {
        "videoName": "Design",
        "startPoint": 107,
        "EndPoint": 201,
        "videPath": "video/What is UX_.mp4",
        "overLay": [{
            "startPoint": 0,
            "EndPoint": 15,
            "imageUrl": "http://www.ivchenko-design.com/assets/images/gif/Preloader.gif",
            "menuText": ""

        }, {
            "startPoint": 16,
            "EndPoint": 27,
            "imageUrl": "",
            "menuText": ["<a href=\'https://www.google.co.in/\' target=\'_blank\'>User Experience</a>", "UX Designer", "Confusing UX Design", "Interface Design"]

        }, {
            "startPoint": 28,
            "EndPoint": 40,
            "imageUrl": "http://marychadwick.com/img/badges/moving-forward-w-ux.png",
            "menuText": ""
        }, {
            "startPoint": 41,
            "EndPoint": 45,
            "imageUrl": "",
            "menuText": ["<a href=href=\'https://www.google.co.in/\' target=\'_blank\'>User Experience</a>", "UX Designer", "Confusing UX Design", "Interface Design"]

        }, {
            "startPoint": 46,
            "EndPoint": 55,
            "imageUrl": "https://www.atomix.com.au/media/2013/12/atomix_ux.png",
            "menuText": ""

        }]
    }, {
        "videoName": "Production",
        "startPoint": 107,
        "EndPoint": 201,
        "videPath": "video/What-the-UX-Design_.mp4",
        "overLay": [{
            "startPoint": 0,
            "EndPoint": 15,
            "imageUrl": "http://www.ivchenko-design.com/assets/images/gif/Preloader.gif",
            "menuText": ""

        }, {
            "startPoint": 16,
            "EndPoint": 27,
            "imageUrl": "",
            "menuText": ["<a href=href=\'https://www.google.co.in/\' target=\'_blank\'>User Experience</a>", "UX Designer", "Confusing UX Design", "Interface Design"]

        }, {
            "startPoint": 28,
            "EndPoint": 40,
            "imageUrl": "http://marychadwick.com/img/badges/moving-forward-w-ux.png",
            "menuText": ""
        }, {
            "startPoint": 41,
            "EndPoint": 45,
            "imageUrl": "",
            "menuText": ["<a href=href=\'https://www.google.co.in/\' target=\'_blank\'>User Experience</a>", "UX Designer", "Confusing UX Design", "Interface Design"]

        }, {
            "startPoint": 46,
            "EndPoint": 55,
            "imageUrl": "https://www.atomix.com.au/media/2013/12/atomix_ux.png",
            "menuText": ""

        }]
    }, {
        "videoName": "Faq",
        "startPoint": 107,
        "EndPoint": 201,
        "videPath": "video/Why-is-good-UIUX-important.mp4",
        "overLay": [{
            "startPoint": 0,
            "EndPoint": 15,
            "imageUrl": "http://www.ivchenko-design.com/assets/images/gif/Preloader.gif",
            "menuText": ""

        }, {
            "startPoint": 16,
            "EndPoint": 27,
            "imageUrl": "",
            "menuText": ["<a href=href=\'https://www.google.co.in/\' target=\'_blank\'>User Experience</a>", "UX Designer", "Confusing UX Design", "Interface Design"]

        }, {
            "startPoint": 28,
            "EndPoint": 40,
            "imageUrl": "http://marychadwick.com/img/badges/moving-forward-w-ux.png",
            "menuText": ""
        }, {
            "startPoint": 41,
            "EndPoint": 45,
            "imageUrl": "",
            "menuText": ["<a href=href=\'https://www.google.co.in/\' target=\'_blank\'>User Experience</a>", "UX Designer", "Confusing UX Design", "Interface Design"]

        }, {
            "startPoint": 46,
            "EndPoint": 55,
            "imageUrl": "https://www.atomix.com.au/media/2013/12/atomix_ux.png",
            "menuText": ""

        }]
    }]
}
';
	
}else{

	echo "Invalid Access.";

}



?>