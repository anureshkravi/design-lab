<?php
App::uses('AppController', 'Controller');

class ProjectFilesController extends AppController {

	public $components = array('Paginator');
	
	public function admin_thumb(){
		if ($this->request->is(array('post', 'put'))) {			
			if ($this->ProjectFile->save($this->request->data)) {				
				$this->Session->setFlash(__('The project file has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project file could not be saved. Please, try again.'));
				return $this->redirect(array('action' => 'index'));
			}
		}
	}
	
	public function admin_setstatus($id = null, $status = null) {
		$this->layout = 'ajax';
		$this->ProjectFile->id = $id;
		if (!$this->ProjectFile->exists()) {
			throw new NotFoundException(__('Invalid Project'));
		}
		$this->ProjectFile->save(array('status_id' => $status) );
		echo ($status);
        exit;
	}

	public function admin_index() {
		$this->ProjectFile->recursive = 0;
		$projectFiles = $this->ProjectFile->find('all', array('conditions' => array('ProjectFile.status_id' => array(1,2))));
		$this->set('projectFiles', $projectFiles);
	}

	public function admin_view($id = null) {
		if (!$this->ProjectFile->exists($id)) {
			throw new NotFoundException(__('Invalid project file'));
		}
		$options = array('conditions' => array('ProjectFile.' . $this->ProjectFile->primaryKey => $id));
		$this->set('projectFile', $this->ProjectFile->find('first', $options));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->request->data['ProjectFile']['user_id'] = $this->Auth->user('id');

			$project = $this->ProjectFile->Project->find('first', array('conditions' => array('Project.id' => $this->request->data['ProjectFile']['project_id']), 'recursive' => -1));
			$this->request->data['ProjectFile']['project'] = $project['Project']['project_folder'];

			$category = $this->ProjectFile->Category->find('first', array('conditions' => array('id' => $this->request->data['ProjectFile']['category_id']), 'recursive' => -1));
			$this->request->data['ProjectFile']['category'] = $category['Category']['category_folder'];

			$this->ProjectFile->create();
			if ($this->ProjectFile->save($this->request->data)) {
				$this->Session->setFlash(__('The project file has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project file could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$projects = $this->ProjectFile->Project->find('list', array('conditions' => array('status_id' => 1)));
		$categories = $this->ProjectFile->Category->find('list', array('conditions' => array('status_id' => 1)));
		$users = $this->ProjectFile->User->find('list');
		$statuses = $this->ProjectFile->Status->find('list', array('conditions' => array('id' => array(1,2))));
		$this->set(compact('projects', 'categories', 'users', 'statuses'));
	}

	public function admin_edit($id = null) {
		if (!$this->ProjectFile->exists($id)) {
			throw new NotFoundException(__('Invalid project file'));
		}
		if ($this->request->is(array('post', 'put'))) {
			//finding project by id
			$project = $this->ProjectFile->find('first', array('conditions' => array('ProjectFile.id' => $this->request->data['ProjectFile']['id'])));
			$this->request->data['ProjectFile']['user_id'] = $this->Auth->user('id');
			$this->request->data['ProjectFile']['project'] = $project['Project']['project_folder'];
			$this->request->data['ProjectFile']['category'] = $project['Category']['category_folder'];		
			if ($this->ProjectFile->save($this->request->data)) {
				$this->Session->setFlash(__('The project file has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project file could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('ProjectFile.' . $this->ProjectFile->primaryKey => $id));
			$this->request->data = $this->ProjectFile->find('first', $options);
		}
		$projects = $this->ProjectFile->Project->find('list', array('conditions' => array('status_id' => 1)));
		$categories = $this->ProjectFile->Category->find('list', array('conditions' => array('status_id' => 1)));
		$users = $this->ProjectFile->User->find('list');
		$statuses = $this->ProjectFile->Status->find('list', array('conditions' => array('id' => array(1,2))));
		$this->set(compact('projects', 'categories', 'users', 'statuses'));
	}

	public function admin_delete($id = null) {
		$this->ProjectFile->recursive = -1;
		$project = $this->ProjectFile->find('first', array('conditions' => array('id' => $id), 'fields' => array('id', 'status_id')));
		if($project){
			$project['ProjectFile']['status_id'] = 3; //deleted
			if ($this->ProjectFile->save($project)) {
				$this->Session->setFlash(__('The project has been Deleted.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Some Error occurs please try again later.'), 'default', array('class' => 'alert alert-danger'));
				return $this->redirect(array('action' => 'index'));
			}
		} else {
			throw new NotFoundException(__('Invalid project'));
		}
	}
}
