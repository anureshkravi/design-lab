<?php
App::uses('AppController', 'Controller');
class ProjectsController extends AppController {

	public $components = array('Paginator');
	public $uses = array('Project', 'Category', 'ProjectFile');

	public function index(){
		$this->Project->recursive = 0;
		$projects = $this->Project->find('all', array('conditions' => array('Project.status_id' => 1)));
		$this->set('projects', $projects);
	}

	public function categories(){
		$this->Category->recursive = 0;
		$categories = $this->Category->find('all', array('conditions' => array('Category.status_id' => 1)));
		$this->set('categories', $categories);
	}

	public function view(){
		$this->Project->recursive = -1;
		$project = $this->Project->find('first', array('conditions' => array('project_folder' => $this->params->folder)));
		
		$this->Category->recursive = -1;
		$category = $this->Category->find('first', array('conditions' => array('category_folder' => $this->params->subfolder)));
		
		$this->ProjectFile->recursive = 0;
		$projectfiles = $this->ProjectFile->find('all', array('fields' => 'date_folder', 'conditions' => array('project_id' => $project['Project']['id'], 'category_id' => $category['Category']['id'], 'ProjectFile.status_id' => 1)));
		$this->set('projectfiles', $projectfiles);
	}

	public function files(){
		$path = 'projects/'.$this->params->folder.'/'.$this->params->subfolder.'/'.$this->params->datefolder;
		$dir = new Folder($path);
		$files = $dir->read(true, array('files', ''));
		$this->set('files', $files);
		pr($files);
		exit();
	}

	public function admin_index() {
		$this->Project->recursive = 0;
		$this->set('projects', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->Project->exists($id)) {
			throw new NotFoundException(__('Invalid project'));
		}
		$options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
		$this->set('project', $this->Project->find('first', $options));
	}

	public function admin_add($title='', $description='') {
		$this->layout = 'ajax';
		if(empty($title)){
			echo 'validation-error';
			exit;
		}
		$categories = $this->Category->find('list');
		$data = array('Project' => array('title' => $title, 'description' => $description, 'status_id' => 1, 'user_id' => $this->Auth->user('id'), 'project_folder' => strtolower(Inflector::slug($title, '-')), 'categories' => $categories));
		$this->Project->create();
		if ($this->Project->save($data)) {
			echo 'success';
		} else {
			echo 'error';
		}
        exit;
	}

	/*public function admin_add() {
		if ($this->request->is('post')) {
			if(!isset($this->request->data['Project']['status_id'])){
				$this->request->data['Project']['status_id'] = 1; //active
			}
			$this->request->data['Project']['user_id'] = $this->Auth->user('id');
			$this->request->data['Project']['project_folder'] = strtolower(Inflector::slug($this->request->data['Project']['title'], '-'));
			$this->Project->create();
			if ($this->Project->save($this->request->data)) {
				$this->Session->setFlash(__('The project has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project could not be saved. Please, try again.'));
			}
		}
		$users = $this->Project->User->find('list');
		$statuses = $this->Project->Status->find('list', array('conditions' => array('id' => array(1,2))));
		$this->set(compact('categories', 'users', 'statuses'));
	}*/

	public function admin_edit($id = null) {
		if (!$this->Project->exists($id)) {
			throw new NotFoundException(__('Invalid project'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if(!isset($this->request->data['Project']['status_id'])){
				$this->request->data['Project']['status_id'] = 1; //active
			}
			$this->request->data['Project']['user_id'] = $this->Auth->user('id');
			$this->request->data['Project']['project_folder'] = strtolower(Inflector::slug($this->request->data['Project']['title'], '-'));
			if ($this->Project->save($this->request->data)) {
				$this->Session->setFlash(__('The project has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
			$this->request->data = $this->Project->find('first', $options);
		}
		$users = $this->Project->User->find('list');
		$statuses = $this->Project->Status->find('list', array('conditions' => array('id' => array(1,2))));
		$this->set(compact('categories', 'users', 'statuses'));
	}

	public function admin_delete($id = null) {
		$this->Project->id = $id;
		if (!$this->Project->exists()) {
			throw new NotFoundException(__('Invalid project'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Project->delete()) {
			$this->Session->setFlash(__('The project has been deleted.'));
		} else {
			$this->Session->setFlash(__('The project could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
