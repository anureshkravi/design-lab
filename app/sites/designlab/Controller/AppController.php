<?php
App::uses('Controller', 'Controller');
App::uses('Folder', 'Utility');
class AppController extends Controller {
	public $components = array(
								'Session','Cookie',
								'Auth' => array(
									'authenticate' => array(
										'Form' => array(
											'scope' => array('User.status_id' => 1)
										)
									)
								)
						 );

	public function beforeFilter() {
		if( isset($this->params['admin']) && $this->params['admin'] ){
			$this->layout = 'default';
			$this->Auth->allow('admin_login');
		} else {
			$this->layout = 'frontend';
			$this->Auth->allow();
		}
	}

	public function beforeRender() {
		$this->response->disableCache();
	}
}
