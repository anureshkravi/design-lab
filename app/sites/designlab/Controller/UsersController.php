<?php
App::uses('AppController', 'Controller');
class UsersController extends AppController {

	public $components = array('Paginator');

	public function admin_login() {
		if( $this->Auth->loggedIn() ){
			$this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
		} else {
			if( $this->request->is('post') ) {
				if($this->Auth->login()) {
					$this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
				} else {
					$this->Session->setFlash(__('Email or Password is incorrect'), 'default', array(), 'auth');
					$this->set('title_for_layout', 'Login');
					$this->layout = 'login';
				}
			} else {
				$this->set('title_for_layout', 'Login');
				$this->layout = 'login';
			}
		}
	}
	
	public function admin_logout() {
		$this->redirect($this->Auth->logout());
	}

	public function admin_setstatus($id = null, $status = null) {
		$this->layout = 'ajax';
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->User->save(array('status_id' => $status) );
		echo ($status);
        exit;
	}

	public function admin_index() {
		$this->User->recursive = 0;
		$users = $this->User->find('all');
		$this->set('users', $users);
	}

	public function admin_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->request->data['User']['password'] = '';
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$roles = $this->User->Role->find('list');
		$statuses = $this->User->Status->find('list', array('conditions' => array('id' => array(1,2))));
		$this->set(compact('statuses', 'roles'));
	}

	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		$roles = $this->User->Role->find('list');
		$statuses = $this->User->Status->find('list', array('conditions' => array('id' => array(1,2))));
		$this->set(compact('statuses', 'roles'));
	}

	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
