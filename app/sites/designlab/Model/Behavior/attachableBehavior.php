<?php
class AttachableBehavior extends ModelBehavior {
	public $_defaults = array(
								'label' => 'filename',
								'errorMessages' => array(
									'isUploaded' => 'Malicious file upload.',
									'moveUploaded' => 'Error moving file upload.',
									'isValidType' => 'Invalid file type. Please check your file type and try again.',
									'isValidSize' => 'Invalid file size. Please check your file size and try again.'
								) ,
								'thumbQuality' => 80,
								'maxSize' => '1048576',

								'fields' => array(
									'filename' => 'filename'
								) ,
								'types' => array(
									'application/zip' => 'zip'
							),
	);

	public $_validThumbTypes = array('application/zip');
	public $_attachmentRoot = null;
	public $_errorMsg = "Error uploading file";
	public $settings = array();
	public $model = null;

	public function setup(Model $model, $config = array()) {
		$this->settings[$model->name] = array_merge($this->_defaults, $this->settings);
		$this->model = $model;
	}

	public function beforeSave(Model $model) {
		if(isset($model->data[$model->name]['project'])){
			$this->_attachmentRoot = WWW_ROOT . str_replace(array('/','\\') , array(DS,DS) , 'projects' . DS . $model->data[$model->name]['project'] . DS . $model->data[$model->name]['category'] . DS . $model->data[$model->name]['date_folder'] . DS);

			extract($this->settings[$model->name]);
			$attachment = $model->data[$model->name][$label];

			if (!empty($attachment['name'])) {
				$model->data[$model->name][$fields['filename']] = ($this->_fileExists($this->_attachmentRoot . $attachment['name'])) ? time() . '_' . $attachment['name'] : $attachment['name'];
				
				if (!$this->_isUploaded($attachment) || 
					!$this->_isValidSize($attachment['size']) || 
					!$this->_isValidType($attachment) || 
					!$this->_dirExists($this->_attachmentRoot) || 
					!$this->_moveUploaded($attachment['tmp_name'], $model->data[$model->name][$fields['filename']])
					) {
						$model->validationErrors[$label] = $this->_errorMsg;
						return false;
				}
			} else {
				$record = $model->findById($model->id);
				$model->data[$model->name][$fields['filename']] = $record[$model->name][$fields['filename']];
				return true;
			}
			return true;
		}
		return true;
	}

	protected function _moveUploaded($tmpName, $attachmentName) {
		//deleting all the files which is already there in the attachment root
		$dir = new Folder($this->_attachmentRoot);
		$files = $dir->read(true, array('files', ''));
		foreach ($files[0] as $file) {
			$folder = new Folder($this->_attachmentRoot.$file);
			$folder->delete();
		}		
		foreach ($files[1] as $file) {
			$value = new File($dir->pwd() . DS . $file);
			$value->delete();
		}
		
		//extracting zip folder
		$zip = new ZipArchive;
		if ($zip->open($tmpName) === TRUE) {
			$zip->extractTo($this->_attachmentRoot . $attachmentName);
			$zip->close();
			
			// copying files from zip folder and pasting into root folder
			$filename = explode('.zip', $attachmentName);
			$folder1 = new Folder($this->_attachmentRoot . $attachmentName . '\\' . $filename['0'] . '\\');
			$folder1->copy($this->_attachmentRoot);

			// deleting .zip folder inside the root folder
			$folder = new Folder($this->_attachmentRoot . $attachmentName);
			$folder->delete();
			return true;
		} else {
			$this->_errorMsg = $this->settings[$this->model->name]['errorMessages']['moveUploaded'];
			return false;
		}
	}

	protected function _isUploaded($attachment) {
		if (isset($attachment['error']) && $attachment['error'] == 0 || !empty($attachment['tmp_name']) && $attachment['tmp_name'] != 'none') {
			return is_uploaded_file($attachment['tmp_name']);
		}
		$this->_errorMsg = $this->settings[$this->model->name]['errorMessages']['isUploaded'];
		return false;
	}

	protected function _isValidType($attachment) {
		$ext = substr($attachment['name'], strrpos($attachment['name'], '.') + 1);
		if (isset($this->settings[$this->model->name]['types'][$attachment['type']]) && $ext == $this->settings[$this->model->name]['types'][$attachment['type']]) {
			return true;
		}
		$this->_errorMsg = $this->settings[$this->model->name]['errorMessages']['isValidType'];
		return false;
	}

	protected function _isValidSize($size) {
		if ($size == 0) return false;
		if ($size <= $this->settings[$this->model->name]['maxSize']) {
			return true;
		}
		$this->_errorMsg = $this->settings[$this->model->name]['errorMessages']['isValidSize'];
		return false;
	}

	protected function _dirExists($dir) {
		if (!file_exists($dir)) {
			trigger_error(__("AttachableBehavior Error - Please create '{$dir}' directory and set permissions for uploading.", true) , E_USER_WARNING);
			return false;
		}
		return true;
	}

	protected function _fileExists($file) {
		if (file_exists($file)) {
			return true;
		}
		return false;
	}
}
?>
