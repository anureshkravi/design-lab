<?php
	class ProjectfileBehavior extends ModelBehavior {
		public function beforeSave(Model $model) {
			if(isset($model->data[$model->name]['project']) && !isset($model->data[$model->name]['id'])){
				$path = 'projects'.DS.$model->data[$model->name]['project'].DS.$model->data[$model->name]['category'].DS.$model->data[$model->name]['date_folder'];
				if(!file_exists($path)) {
					$folder = new Folder();
					$folder->create($path);
					return true;
				} else {
					$model->validationErrors['date_folder'] = 'Folder with this date already exists!!';
					return false;
				}
			}
			if(isset($model->data[$model->name]['id']) && $model->data[$model->name]['status_id'] == 3){
				$model->recursive = 0;
				$project = $model->findById($model->data[$model->name]['id']);
				$path = 'projects'.DS.$project['Project']['project_folder'].DS.$project['Category']['category_folder'].DS.$project['ProjectFile']['date_folder'];
				$folder = new Folder($path);
				$folder->delete();
			}
			return true;
		}
	}
?>