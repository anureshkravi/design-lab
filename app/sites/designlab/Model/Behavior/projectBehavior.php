<?php
	class ProjectBehavior extends ModelBehavior{

		public function afterSave(Model $model) {
			$path = 'projects'.DS.strtolower(Inflector::slug($model->data[$model->name]['title'], '-'));
			$folder = new Folder();
			if( !file_exists($path) ){
				$folder->create($path);
			}
			foreach($model->data[$model->name]['categories'] as $value){
				$categoryPath = 'projects'.DS.strtolower(Inflector::slug($model->data[$model->name]['title'], '-')).DS.strtolower(Inflector::slug($value, '-'));
				if( !file_exists($categoryPath) ){
					$folder->create($categoryPath);
				}
			}
		}
	}
?>