<?php
	class UserBehavior extends ModelBehavior{
		public function beforeSave(Model $model) {
			if(isset($model->data[$model->name]['username'])){
				if(!isset($model->data[$model->name]['id'])) {
					if($model->findAllByUsername($model->data[$model->name]['username']))	{
						$model->validationErrors['username'] = 'Username already exists!!';
						return false;
					}
					return true;
				} else {
					$user = $model->findById($model->data[$model->name]['id']);
					if($user['User']['username'] != $model->data[$model->name]['username']) {
						if($model->findAllByUsername($model->data[$model->name]['username'])) {
							$model->validationErrors['username'] = 'Username already exists!!';
							return false;
						}
					}
				}
			}
		}
	}
?>