$(document).on('swipeleft swiperight', '.fluidHeight', function(event) {
	event.stopPropagation();
	event.preventDefault();
});

/*$(document).on('mobileinit', function () {
   $.mobile.ignoreContentEnabled = true;
   $.mobile.defaultPageTransition = "slide";
});
$('.ui-content').on('swipeleft', function(e) {
   e.stopImmediatePropagation();
});*/

$( document ).on( "pagecreate", "#home-page, #activity-page, #activity-search, #activity-detail, #contact-us, #direction, #events-page, #events-search, #events-detail, #facility-page, #facility-search, #facility-detail, #promotion, #promotion-detail", function() {
	$( document ).on( "swipeleft swiperight", "#home-page, #activity-page, #activity-search, #activity-detail, #contact-us, #direction, #events-page, #events-search, #events-detail, #facility-page, #facility-search, #facility-detail, #promotion, #promotion-detail", function( e ) {
		if ( $( ".ui-page-active" ).jqmData( "panel" ) !== "open" ) {
			if ( e.type === "swipeleft" ) {
				$( "#right-panel" ).panel( "open" );
			} else if ( e.type === "swiperight" ) {
				$( "#left-panel" ).panel( "open" );
			}
		}
	});
});

$(document).on("pagecreate","#home-page",function(){
//$(document).ready(function() {
	$('.iosSlider').iosSlider({
		scrollbar: true,
		snapToChildren: true,
		desktopClickDrag: true,
		scrollbarLocation: 'top',
		scrollbarMargin: '10px 10px 0 10px',
		scrollbarBorderRadius: '0',
		responsiveSlideWidth: true,
		navSlideSelector: $('.iosSliderButtons .button'),
		infiniteSlider: true,
		startAtSlide: '1',
		onSlideChange: slideContentChange,
		onSlideComplete: slideContentComplete,
		onSliderLoaded: slideContentLoaded
	});
	
	function slideContentChange(args) {		
		$(args.sliderObject).parent().find('.iosSliderButtons .button').removeClass('selected');
		$(args.sliderObject).parent().find('.iosSliderButtons .button:eq(' + (args.currentSlideNumber - 1) + ')').addClass('selected');		
	}
	
	function slideContentComplete(args) {		
		if(!args.slideChanged) return false;
		
		$(args.sliderObject).find('.text1, .text2').attr('style', '');
		
		$(args.currentSlideObject).children('.text1').animate({
			right: '100px',
			opacity: '1'
		}, 400, 'easeOutQuint');
		
		$(args.currentSlideObject).children('.text2').delay(200).animate({
			right: '50px',
			opacity: '1'
		}, 400, 'easeOutQuint');		
	}
	
	function slideContentLoaded(args) {		
		$(args.sliderObject).find('.text1, .text2').attr('style', '');
		
		$(args.currentSlideObject).children('.text1').animate({
			right: '100px',
			opacity: '1'
		}, 400, 'easeOutQuint');
		
		$(args.currentSlideObject).children('.text2').delay(200).animate({
			right: '50px',
			opacity: '1'
		}, 400, 'easeOutQuint');
		
		$(args.sliderObject).parent().find('.iosSliderButtons .button').removeClass('selected');
		$(args.sliderObject).parent().find('.iosSliderButtons .button:eq(' + (args.currentSlideNumber - 1) + ')').addClass('selected');		
	}
	
	var imageCount = $(".slider img").length;
	var width = imageCount * 768;
	
	$('.iosSlider').css({'width':'100%', 'height':'100%'});
	$('.slider').css({'width':width, 'position':'relative', 'left':'-2304px'});	
	var left = 768;	
	$(".slider img").each(function(){
		$(this).css({'position':'absolute', 'left':left, 'width':'768px'});
		left += 768;
	});
});