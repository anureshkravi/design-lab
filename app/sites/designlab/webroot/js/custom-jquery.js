jQuery(document).ready(function($) {
    //for changing status//
	jQuery('.jquery_action_status').click( function(){
        var buttonId = jQuery(this).attr('id');
        var d = buttonId.split('_');
        var id = d[1];
        var controller= d[2];
        var status = jQuery(this).attr('rel');
        var setUrl = controller+'/setstatus/'+ id + '/'+ status;
        jQuery.ajax({
            type: "GET",
            url: setUrl,
            success: function(data) {
                if( data == 2 ) {
                    jQuery('#'+buttonId).attr('rel', 1);
                    jQuery('#'+buttonId).html('<i class="entypo-check"></i> Activate');
                    jQuery('#change_'+id+'_status').html('Not Active');
                    jQuery('#'+buttonId).removeClass('btn-primary');
                    jQuery('#'+buttonId).addClass('btn-success');
                } else if ( data == 1) {
                	jQuery('#'+buttonId).attr('rel', 2);
                    jQuery('#'+buttonId).html('<i class="entypo-cancel"></i> Deactivate');
                    jQuery('#change_'+id+'_status').html('Active');
                    jQuery('#'+buttonId).removeClass('btn-success');
                    jQuery('#'+buttonId).addClass('btn-primary');
                }
            }
        });
        return false;
    });

    //for pagination in tables//
    jQuery("#table-1").dataTable({
        //"sPaginationType": "bootstrap",
        //"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        //"bStateSave": true,

        "fnDrawCallback": function ( oSettings ) {
            /* Need to redo the counters if filtered or sorted */
            if ( oSettings.bSorted || oSettings.bFiltered )
            {
                for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
                {
                    $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
                }
            }
        },
        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [ 0 ] }
        ],
        "aaSorting": [[ 1, 'asc' ]],
        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
    });

    jQuery(".dataTables_wrapper select").select2({
        minimumResultsForSearch: -1
    });

    //for project file add page (add project name)
    var siteName = '/';
    jQuery('.jquery_project_add').click(function(){
        $('.modal-body .panel-body .alert').remove();
        var projectTitle = $('#ProjectTitle').val();
        var projectDescription = $('#ProjectDescription').val();
        var setUrl = siteName+'admin/projects/add/'+projectTitle+'/'+projectDescription;
        jQuery.ajax({
            type: "GET",
            url: setUrl,
            success: function(data) {
                if( data == 'success' ){
                    jQuery('.modal-body .panel-body').prepend('<div class="alert alert-success">Project has been added successfully.</div>');
                } else if( data == 'validation-error' ){
                    jQuery('.modal-body .panel-body').prepend('<div class="alert alert-danger">Please enter the Project Name</div>');
                } else {
                    jQuery('.modal-body .panel-body').prepend('<div class="alert alert-danger">Some error occurs please try again later.</div>');
                }
            }
        });
        return false;
    });

    jQuery('#sample-modal-dialog-1 .modal-header .close').click(function(){
        window.location = siteName+"admin/projectfiles/add";
    });

    //for project file index page (add thumbnail)
    jQuery('.addThumBtn').click(function(){
        var id = $(this).attr('id');
        $('#projectFileId').remove();
        jQuery('#sample-modal-dialog-2 fieldset').prepend('<input type="hidden" id="projectFileId" name="data[ProjectFile][id]" value="'+id+'" >');
    });
});