<h2>Users</h2>
<br />
<table class="table table-bordered datatable" id="table-1">
	<thead>
		<tr>
			<th>S.no</th>
			<th>Username</th>
			<th>Role</th>
			<th>Status</th>
			<th>Created</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($users as $user): ?>
			<tr>
				<td></td>
				<td><?php echo h($user['User']['username']); ?>&nbsp;</td>
				<td><?php echo h($user['Role']['title']); ?>&nbsp;</td>
				<td id="change_<?php echo $user['User']['id']; ?>_status"><?php echo $user['Status']['title']; ?></td>
				<td><?php echo $this->Html->beautifulDate($user['User']['created']); ?>&nbsp;</td>
				<td>
					<a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'edit', $user['User']['id'])); ?>" class="btn btn-default btn-sm btn-icon icon-left">
						<i class="entypo-pencil"></i>
						Edit
					</a>
					
					<?php
						if($user['Status']['id'] == 1){
					?> 
						<a href="#" class="btn btn-danger btn-sm btn-icon icon-left jquery_action_status" id = 'action_<?php echo $user['User']['id']; ?>_users' rel="2">
							<i class="entypo-cancel"></i>
							Deactivate
						</a>
					<?php } else {
					?> 
						<a href="#" class="btn btn-success btn-sm btn-icon icon-left jquery_action_status" id = 'action_<?php echo $user['User']['id']; ?>_users' rel="1">
							<i class="entypo-check"></i>
							Activate
						</a>
					<?php }	?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<th>S.no</th>
			<th>Username</th>
			<th>Role</th>
			<th>Status</th>
			<th>Created</th>
			<th>Action</th>
		</tr>
	</tfoot>
</table>

<a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'add')); ?>" class="btn btn-primary">
	<i class="entypo-plus"></i>
	Add User
</a>