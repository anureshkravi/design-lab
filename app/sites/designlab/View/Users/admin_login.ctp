<div class="login-header login-caret">
    <div class="login-content">
      <a href="#" class="logo"><?php echo $this->Html->image('/assets/images/logo.png'); ?></a>
      <p class="description">
      	<?php
			if($this->Session->check('Message.auth')){
				echo $this->Session->flash('auth');
			}
		?>
      </p>
      <!-- progress bar indicator -->
      <div class="login-progressbar-indicator">
        <h3>43%</h3>
        <span>logging in...</span>
      </div>
    </div>
</div>
  
<div class="login-progressbar">
  <div></div>
</div>
  
<div class="login-form">
  <div class="login-content">
    <?php echo $this->Form->create('User'); ?>        
      <div class="form-group">
        <div class="input-group">
          <div class="input-group-addon">
            <i class="entypo-mail"></i>
          </div>
          <?php echo $this->Form->input('username', array('class' => 'form-control', 'placeholder' => 'Email', 'label' => false, 'div' => false)); ?>
        </div>
      </div>
      
      <div class="form-group">
        <div class="input-group">
          <div class="input-group-addon">
            <i class="entypo-key"></i>
          </div>
          <?php echo $this->Form->input('password', array('class' => 'form-control', 'placeholder' => 'Password', 'label' => false, 'div' => false)); ?>
        </div>
      </div>
      
      <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block btn-login">
          Login In
          <i class="entypo-login"></i>
        </button>
      </div>
    <?php echo $this->Form->end(); ?>
    
    <div class="login-bottom-links">
      <a href="#" class="link">Forgot your password?</a><br />
      <a href="#"><?php echo Configure::read('siteName'); ?></a>
    </div>
  </div>
</div>