<div class="modal fade" id="sample-modal-dialog-1">
	<div class="modal-dialog">
		<div class="modal-content">			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Project</h4>
			</div>
			
			<form action="" method="post">
			<div class="modal-body">
				<div class="users form">
					<div class="panel panel-primary" style="border:none;">
						<div class="panel-body">
							<fieldset>
								<?php
									$allprojects = implode(',', $projects);
								?>
								<div class="form-group">
									<label class="control-label" for="title">Project Name</label>				
									<input type="text" id="ProjectTitle" name="data[title]" class="form-control typeahead" data-local="<?php echo $allprojects; ?>" />
								</div>

								<div class="form-group">
									<label class="control-label" for="description">Description</label>
									<textarea id="ProjectDescription" rows="6" cols="30" class="form-control autogrow" name="data[description]" value="" style="height:100px;"></textarea>
								</div>
							</fieldset>

							<div class="form-group submit">
								<input type="submit" value="Submit" class="btn btn-success jquery_project_add">
							</div>
						</div>
					</div>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="sample-modal-dialog-2">
	<div class="modal-dialog">
		<div class="modal-content">			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Project Thumbnail</h4>
			</div>
			
			<?php echo $this->Form->create(null, array('url' => array('controller' => 'projectfiles', 'action' => 'thumb'), 'type' => 'file')); ?>
			<div class="modal-body">
				<div class="users form">
					<div class="panel panel-primary" style="border:none;">
						<div class="panel-body">
							<fieldset>
								<div class="form-group">
									<label class="control-label" for="title">Select Thumbnail</label>
									<input id="ProjectFileThumbnail" type="file" required="required" class="form-control" name="data[ProjectFile][thumbnail]">
								</div>
							</fieldset>

							<div class="form-group submit">
								<input type="submit" value="Submit" class="btn btn-success submitThumbnail">
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>