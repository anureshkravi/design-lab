<header class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="navbar-brand">
            <a href="<?php echo $this->Html->url(array('controller' => 'dashboard', 'action' => 'index')); ?>" class="logo"><?php echo $this->Html->image('/assets/images/logo-horizontal.png'); ?></a>
        </div>

        <ul class="navbar-nav">
            <li><a href="<?php echo $this->Html->url(array('controller' => 'dashboard', 'action' => 'index')); ?>"><i class="entypo-gauge"></i><span>Dashboard</span></a></li>
            <li><a href="<?php echo $this->Html->url(array('controller' => 'projects', 'action' => 'index')); ?>"><i class="entypo-layout"></i> Categories</a>
                <ul>
                    <li><a href="<?php echo $this->Html->url(array('controller' => 'categories', 'action' => 'add')); ?>">New Category</a></li>
                    <li><a href="<?php echo $this->Html->url(array('controller' => 'categories', 'action' => 'index')); ?>">List Categories</a></li>
                </ul>
            </li>
            <li><a href="<?php echo $this->Html->url(array('controller' => 'projects', 'action' => 'index')); ?>"><i class="entypo-layout"></i> Projects</a>
                <ul>
                    <li><a href="<?php echo $this->Html->url(array('controller' => 'projectfiles', 'action' => 'add')); ?>">New Project</a></li>
                    <li><a href="<?php echo $this->Html->url(array('controller' => 'projectfiles', 'action' => 'index')); ?>">List Projects</a></li>
                </ul>
            </li>
            <?php if($this->Html->user('role_id') == 1) { ?>
            <li><a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'index')); ?>"><i class="entypo-layout"></i> Users</a>
                <ul>
                    <li><a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'add')); ?>">New User</a></li>
                    <li><a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'index')); ?>">List Users</a></li>
                </ul>
            </li>
            <?php } ?>
            <li><a href="#"><i class="entypo-layout"></i> Live Site</a></li>
        </ul>

        <ul class="nav navbar-right pull-right">
            <!-- mobile only -->
            <li class="visible-xs">
                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="horizontal-mobile-menu visible-xs">
                    <a href="#" class="with-animation">
                        <!-- add class "with-animation" to support animation -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>
            </li>

            <li style="display:block;"><a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'logout')); ?>">Log Out <i class="entypo-logout right"></i></a></li>
        </ul>
    </div>
</header>