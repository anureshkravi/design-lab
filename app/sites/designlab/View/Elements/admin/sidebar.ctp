<div class="sidebar-menu">
    <header class="logo-env">
        <div class="logo">
            <a href="#">
                <?php echo $this->Html->image('/assets/images/logo.png'); ?>
            </a>
        </div>

        <div class="sidebar-collapse">
            <a href="#" class="sidebar-collapse-icon with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>

        <div class="sidebar-mobile-menu visible-xs">
            <a href="#" class="with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>
    </header>

    <ul id="main-menu" class="">
        <li id="search">
            <form method="get" action="#">
                <input type="text" name="q" class="search-input" placeholder="Search something..." />
                <button type="submit"><i class="entypo-search"></i></button>
            </form>
        </li>

        <li class="active opened active">
            <a href="<?php echo $this->Html->url(array('controller' => 'dashboard', 'action' => 'index')); ?>"><i class="entypo-gauge"></i><span>Dashboard</span></a>
        </li>

        <li>
            <a href="#"><i class="entypo-layout"></i><span>Category</span></a>
            <ul>
                <li><a href="#">New Category</a></li>
                <li><a href="#">List Categories</a></li>
            </ul>
        </li>

        <li>
            <a href="#"><i class="entypo-layout"></i><span>Projects</span></a>
            <ul>
                <li><a href="#">New Project</a></li>
                <li><a href="#">List Projects</a></li>
            </ul>
        </li>
    </ul>
</div>