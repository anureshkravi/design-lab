<div class="categories form">
	<h2>Add Category</h2>
	<br />
	<div class="panel panel-primary">
		<div class="panel-heading">
			<div class="panel-title">All fields have validation rules</div>
		</div>
		<div class="panel-body">
			<?php echo $this->Form->create('Category'); ?>
				<fieldset>
					<?php echo $this->Form->input('title', array('class' => 'form-control', 'label' => array('class' => 'control-label'), 'div' => array('class' => 'form-group'))); ?>

					<?php echo $this->Form->input('status_id', array('empty' => 'Select Status', 'class' => 'form-control', 'label' => array('class' => 'control-label'), 'div' => array('class' => 'form-group'))); ?>
				</fieldset>
			<?php
				$options = array(
					'class' => 'btn btn-success',
				    'label' => false,
				    'div' => array(
				        'class' => 'form-group submit',
				    )
				);
				echo $this->Form->end($options);
			?>
		</div>
	</div>
</div>


