<h2>Categories</h2>
<br />
<table class="table table-bordered datatable" id="table-1">
	<thead>
		<tr>
			<th>S.no</th>
			<th>Title</th>
			<th>Status</th>
			<th>Created</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($categories as $category): ?>
			<tr>
				<td></td>
				<td><?php echo h($category['Category']['title']); ?>&nbsp;</td>
				<td id="change_<?php echo $category['Category']['id']; ?>_status"><?php echo $category['Status']['title']; ?></td>
				<td><?php echo $this->Html->beautifulDate($category['Category']['created']); ?>&nbsp;</td>
				<td>
					<a href="<?php echo $this->Html->url(array('controller' => 'categories', 'action' => 'edit', $category['Category']['id'])); ?>" class="btn btn-default btn-sm btn-icon icon-left">
						<i class="entypo-pencil"></i>
						Edit
					</a>
					
					<?php
						if($category['Status']['id'] == 1){
					?> 
						<a href="#" class="btn btn-danger btn-sm btn-icon icon-left jquery_action_status" id = 'action_<?php echo $category['Category']['id']; ?>_categories' rel="2">
							<i class="entypo-cancel"></i>
							Deactivate
						</a>
					<?php } else {
					?> 
						<a href="#" class="btn btn-success btn-sm btn-icon icon-left jquery_action_status" id = 'action_<?php echo $category['Category']['id']; ?>_categories' rel="1">
							<i class="entypo-check"></i>
							Activate
						</a>
					<?php }	?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<th>S.no</th>
			<th>Title</th>
			<th>Status</th>
			<th>Created</th>
			<th>Action</th>
		</tr>
	</tfoot>
</table>

<a href="<?php echo $this->Html->url(array('controller' => 'categories', 'action' => 'add')); ?>" class="btn btn-primary">
	<i class="entypo-plus"></i>
	Add Category
</a>