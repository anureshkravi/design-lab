<div class="projectFiles view">
<h2><?php echo __('Project File'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($projectFile['ProjectFile']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectFile['Project']['title'], array('controller' => 'projects', 'action' => 'view', $projectFile['Project']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Category'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectFile['Category']['title'], array('controller' => 'categories', 'action' => 'view', $projectFile['Category']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectFile['User']['id'], array('controller' => 'users', 'action' => 'view', $projectFile['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Filename'); ?></dt>
		<dd>
			<?php echo h($projectFile['ProjectFile']['filename']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Folder'); ?></dt>
		<dd>
			<?php echo h($projectFile['ProjectFile']['date_folder']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectFile['Status']['title'], array('controller' => 'statuses', 'action' => 'view', $projectFile['Status']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($projectFile['ProjectFile']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($projectFile['ProjectFile']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Project File'), array('action' => 'edit', $projectFile['ProjectFile']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Project File'), array('action' => 'delete', $projectFile['ProjectFile']['id']), null, __('Are you sure you want to delete # %s?', $projectFile['ProjectFile']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Files'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project File'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects'), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project'), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Statuses'), array('controller' => 'statuses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Status'), array('controller' => 'statuses', 'action' => 'add')); ?> </li>
	</ul>
</div>
