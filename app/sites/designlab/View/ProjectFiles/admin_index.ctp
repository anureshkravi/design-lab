<h2>Latest Projects</h2>
<br />
<table class="table table-bordered datatable" id="table-1">
	<thead>
		<tr>
			<th>Id</th>
			<th>Project Name</th>
			<th>Category</th>
			<th>Thumbnail</th>
            <th>Description</th>
			<th>User</th>
			<th>Status</th>
			<th>Date Folder</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($projectFiles as $projectFile): ?>
			<tr>
				<td></td>
				<td><?php echo h($projectFile['Project']['title']); ?>&nbsp;</td>
				<td><?php echo h($projectFile['Category']['title']); ?>&nbsp;</td>
				<td>
					<?php 
						if(!empty($projectFile['ProjectFile']['thumbnail'])){
							echo $this->Html->image('/files/thumbnails/'.$projectFile['ProjectFile']['thumbnail'], array('width' => 75, 'height' => 75)); } else {
							echo $this->Html->image('/files/thumbnails/thumb.jpg', array('width' => 75, 'height' => 75));
						}
					?>
                </td>
                <td><?php echo h($projectFile['ProjectFile']['description']); ?>&nbsp;</td>
				<td><?php echo h($projectFile['User']['username']); ?>&nbsp;</td>
				<td id="change_<?php echo $projectFile['ProjectFile']['id']; ?>_status"><?php echo $projectFile['Status']['title']; ?></td>
				<td><?php echo $projectFile['ProjectFile']['date_folder']; ?>&nbsp;</td>
				<td>
                	<a href="projectfiles/edit/<?php echo $projectFile['ProjectFile']['id']; ?>" class="btn btn-default btn-sm btn-icon icon-left">
						<i class="entypo-pencil"></i>
						Edit
					</a>
					<a href="#" data-toggle="modal" data-target="#sample-modal-dialog-2" class="btn btn-info btn-sm btn-icon icon-left addThumBtn" id = '<?php echo $projectFile['ProjectFile']['id']; ?>'>
						<i class="entypo-plus"></i>
						Thumbnail
					</a>
					<?php
						if($projectFile['Status']['id'] == 1){
					?> 
						<a href="#" class="btn btn-primary btn-sm btn-icon icon-left jquery_action_status" id = 'action_<?php echo $projectFile['ProjectFile']['id']; ?>_projectfiles' rel="2">
							<i class="entypo-cancel"></i>
							Not Active
						</a>
					<?php } else {
					?> 
						<a href="#" class="btn btn-success btn-sm btn-icon icon-left jquery_action_status" id = 'action_<?php echo $projectFile['ProjectFile']['id']; ?>_projectfiles' rel="1">
							<i class="entypo-check"></i>
							Active
						</a>
					<?php }	?>                    
                    <a onclick="if (confirm(&quot;Are you sure you wish to delete? All Files related to this project will also get deleted.&quot;)) { return true; } return false;" class="btn btn-danger btn-sm btn-icon icon-left addThumBtn" href="projectfiles/delete/<?php echo $projectFile['ProjectFile']['id']; ?>"><i class="entypo-cancel"></i> Delete</a>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<th>Id</th>
			<th>Project Name</th>
			<th>Category</th>
			<th>Thumbnail</th>
            <th>Description</th>
			<th>User</th>
			<th>Status</th>
			<th>Date Folder</th>
			<th>Action</th>
		</tr>
	</tfoot>
</table>

<a href="<?php echo $this->Html->url(array('controller' => 'projectfiles', 'action' => 'add')); ?>" class="btn btn-primary">
	<i class="entypo-plus"></i>
	Upload Files
</a>