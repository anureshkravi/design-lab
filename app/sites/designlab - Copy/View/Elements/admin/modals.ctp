<div class="modal fade" id="sample-modal-dialog-1">
	<div class="modal-dialog">
		<div class="modal-content">			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Project</h4>
			</div>
			
			<div class="modal-body">
				<div class="users form">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<div class="panel-title">All fields have validation rules</div>
						</div>
						<div class="panel-body">
							<?php echo $this->Form->create('Project'); ?>
								<fieldset>
									<?php echo $this->Form->input('title', array('class' => 'form-control', 'label' => array('class' => 'control-label', 'text' => 'Project Name'), 'div' => array('class' => 'form-group'))); ?>

									<?php echo $this->Form->input('description', array('type' => 'textarea', 'class' => 'form-control autogrow', 'label' => array('class' => 'control-label'), 'div' => array('class' => 'form-group'))); ?>
								</fieldset>
							<?php
								$options = array(
									'class' => 'btn btn-success',
								    'label' => false,
								    'div' => array(
								        'class' => 'form-group submit',
								    )
								);
								echo $this->Form->end($options);
							?>
						</div>
					</div>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>