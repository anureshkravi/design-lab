<div class="users form">
	<h2 style="float:left;">Upload Files</h2>
	<a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="btn btn-primary" style="float:right; margin-top:10px;">
		<i class="entypo-plus"></i>
		Add Project
	</a>
	<div class="clearfix"></div>
	<br />

	<div class="panel panel-primary">
		<div class="panel-heading">
			<div class="panel-title">All fields have validation rules</div>
		</div>
		<div class="panel-body">
			<?php echo $this->Form->create('Project', array('type' => 'file')); ?>
				<fieldset>
					<?php echo $this->Form->input('title', array('class' => 'form-control', 'label' => array('class' => 'control-label', 'text' => 'Project Name'), 'div' => array('class' => 'form-group'))); ?>

					<?php echo $this->Form->input('filename', array('type' => 'file', 'class' => 'form-control', 'label' => array('class' => 'control-label', 'text' => 'Files (Zip Folder Only)'), 'div' => array('class' => 'form-group'))); ?>

					<?php echo $this->Form->input('category_id', array('empty' => 'Select Category', 'class' => 'form-control', 'label' => array('class' => 'control-label'), 'div' => array('class' => 'form-group'))); ?>

					<?php echo $this->Form->input('status_id', array('empty' => 'Select Status', 'class' => 'form-control', 'label' => array('class' => 'control-label'), 'div' => array('class' => 'form-group'))); ?>
				</fieldset>
			<?php
				$options = array(
					'class' => 'btn btn-success',
				    'label' => false,
				    'div' => array(
				        'class' => 'form-group submit',
				    )
				);
				echo $this->Form->end($options);
			?>
		</div>
	</div>
</div>