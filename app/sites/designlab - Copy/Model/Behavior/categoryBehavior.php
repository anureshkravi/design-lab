<?php
	class CategoryBehavior extends ModelBehavior{
		public function beforeSave(Model $model) {
			if(isset($model->data[$model->name]['title'])){
				if(!isset($model->data[$model->name]['id'])) {
					if($model->findAllByTitle($model->data[$model->name]['title']))	{
						$model->validationErrors['title'] = 'Category with this name already exists.';
						return false;
					}
					return true;
				} else {
					$category = $model->findById($model->data[$model->name]['id']);
					if($category['Category']['title'] != $model->data[$model->name]['title']) {
						if($model->findAllByTitle($model->data[$model->name]['title'])) {
							$model->validationErrors['title'] = 'Category with this name already exists.';
							return false;
						}
					}
				}
			}
		}

		public function afterSave(Model $model) {
			if(isset($model->data[$model->name]['title'])){
				$path = 'projects'.DS.strtolower(Inflector::slug($model->data[$model->name]['title'], '-'));
				$folder = new Folder();
				if( !file_exists($path) ){
					$folder->create($path);
				}
			}
		}
	}
?>