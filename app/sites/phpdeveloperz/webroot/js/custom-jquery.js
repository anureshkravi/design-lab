jQuery(document).ready(function($)
{
    //for changing status//
	jQuery('.jquery_action_status').click( function(){
        var buttonId = jQuery(this).attr('id');
        var d = buttonId.split('_');
        var id = d[1];
        var controller= d[2];
        var status = jQuery(this).attr('rel');
        var setUrl = '/'+controller+'/setstatus/'+ id + '/'+ status;
        jQuery.ajax({
            type: "GET",
            url: setUrl,
            success: function(data) {
                if( data == 2 ) {
                    jQuery('#'+buttonId).attr('rel', 1);
                    jQuery('#'+buttonId).html('<i class="entypo-check"></i> Activate');
                    jQuery('#change_'+id+'_status').html('Not Active');
                    jQuery('#'+buttonId).removeClass('btn-danger');
                    jQuery('#'+buttonId).addClass('btn-success');
                } else if ( data == 1) {
                	jQuery('#'+buttonId).attr('rel', 2);
                    jQuery('#'+buttonId).html('<i class="entypo-cancel"></i> Deactivate');
                    jQuery('#change_'+id+'_status').html('Active');
                    jQuery('#'+buttonId).removeClass('btn-success');
                    jQuery('#'+buttonId).addClass('btn-danger');
                }
            }
        });
        return false;
    });

    //for dynamic action in privileges controller//
    jQuery('#PrivilegeController').change( function(){
        var controller = $(this).val();
        var setUrl = '/privileges/listactions/'+controller;
        jQuery.ajax({
            type: "GET",
            url: setUrl,
            success: function(data) {
                if( data ){
                    jQuery('#PrivilegeAction').html(data);
                }
            }
        });
        return false;
    });
    
    //user registeration form validation for admin//
    jQuery("#UserAddButton").click(function() {
        var email  = jQuery('#email').val();
        var setUrl = "/phpdeveloperz/users/checkEmail/"+email;
        jQuery.ajax({
            type: "POST",
            url: setUrl,
            success: function(data){
                if( data == 'Email id Already Exists' ){
                    jQuery('#emailContainer').addClass('validate-has-error');
                    jQuery('#emailContainer').find('span').remove();
                    jQuery('#emailContainer').append('<span for="email" class="validate-has-error">'+data+'</span>');
                } else {
                    $('#UserAdminAddForm').submit();
                }
            }
        });
        return false;
    });

    //user registeration form validation for frontend//
    /*jQuery('#userRegisterButton').click(function(){
        if( jQuery('#password').val() != jQuery('#conf_password').val()){
            jQuery('#conf_passwordErr').html('Password and Confirm password should be same.');
            return false;
        }
        if(jQuery('#terms').prop('checked') == false){
            jQuery('#termsErr').html('Please check and accept terms and conditions.');
            return false;
        }
        var email  = jQuery('#email').val();
        var setUrl = "/users/checkEmail/"+email;
        jQuery.ajax({
            type: "POST",
            url: setUrl,
            success: function(data){
                if( data == 'Email id Already Exists' ){
                    jQuery('#emailErr').html('Email Id Already Exists.');
                } else  {
                    jQuery('#registrationForm').submit();
                }
            }
        });
        return false;
    });*/

    //for pagination in tables//
    jQuery("#table-1").dataTable({
        "sPaginationType": "bootstrap",
        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "bStateSave": true
    });

    jQuery(".dataTables_wrapper select").select2({
        minimumResultsForSearch: -1
    });

    //for password vaildation//
    jQuery.validator.addMethod("password", function(value, element) {
        return this.optional(element) || /^(?=(.*[a-z]){1})(?=(.*[A-Z]){1})(?=(.*[0-9]){1})(?=(.*[\$\@\#]){1}).{4,}$/.test(value);
    }, "One lowercase letter, one capital letter, one number and one of the characters (#@$).");
});