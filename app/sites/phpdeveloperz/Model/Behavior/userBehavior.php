<?php
	class UserBehavior extends ModelBehavior{
		public function beforeSave(Model $model) {			
			if(!isset($model->data[$model->name]['id'])) {
				if($model->findAllByEmail($model->data[$model->name]['email']))	{
					$model->validationErrors['email'] = 'Email id already exists!!';
					return false;
				}
				return true;
			} else {
				$user = $model->findById($model->data[$model->name]['id']);
				if($user['User']['email'] != $model->data[$model->name]['email']) {
					if($model->findAllByEmail($model->data[$model->name]['email'])) {
						$model->validationErrors['email'] = 'Email id already exists!!';
						return false;
					}
				}
			}
		}
	}
?>