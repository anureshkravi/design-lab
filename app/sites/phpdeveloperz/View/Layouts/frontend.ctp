<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
		<link href="http://www.phpdeveloperz.com/testmode/images/favicon.ico" />
		<title>Jobs for PHP Developers | Best Job Portal for PHP Professionals | PHP Jobs in India | PHPDEVELOPEZ.COM</title>
		<meta name="description" content="For the best Jobs for PHP Professionals apply on PHPDEVELOPERZ.COM. PHPDEVELOPERZ.COM is India's leading job website for php professionals." />
		<meta name="keywords" content="Job Portal Dedicated for PHP Professionals, Jobs in India, PHP Jobs, Career Opportunities for PHP Developers" />
		<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script>
		<?php 
			echo $this->Html->css('jobseeker');
			echo $this->Html->script('custom-jquery');
		?>
	</head>
	<body>
		<?php echo $this->element('header'); ?>
		<div class="wrapper">
			<?php 
				echo $this->Session->flash();
				echo $this->fetch('content');
				echo $this->element('footer');
			?>
		</div><!--wrapper-->
		<?php echo $this->element('sql_dump'); ?>
	</body>
</html>