<div class="users form">
	<h2>Add Privilege</h2>
	<br />
	<div class="panel panel-primary">
		<div class="panel-heading">
			<div class="panel-title">All fields have validation rules</div>
		</div>		
		<div class="panel-body">
			<form role="form" id="PrivilegeAdminAddForm" method="post" class="validate" action="/admin/privileges/add">			
				<div class="form-group">
					<label class="control-label">Role</label>					
					<select class="form-control" name="role_id" required="required">
						<option value="">Select Role</option>
						<?php
							foreach( $roles as $key => $value ){
								echo "<option value='".$key."'>".$value."</option>";
							}
						?>
					</select>
				</div>

				<div class="form-group">
					<label class="control-label">Controller</label>					
					<select class="form-control" name="controller" id="PrivilegeController" required="required">
						<option value="">Select Controller</option>
						<?php
							foreach( $controllers as $key => $value ){
								echo "<option value='".$key."'>".$value."</option>";
							}
						?>
					</select>
				</div>

				<div class="form-group">
					<label class="control-label">Action</label>					
					<select class="form-control" name="action" id="PrivilegeAction" required="required">
						<option value="">Select Action</option>
					</select>
				</div>

				<div class="form-group">
					<label class="control-label">Status</label>					
					<select class="form-control" name="status_id" required="required">
						<option value="">Select Status</option>
						<?php
							foreach( $statuses as $key => $value ){
								echo "<option value='".$key."'>".$value."</option>";
							}
						?>
					</select>
				</div>
								
				<div class="form-group">
					<input type="submit" class="btn btn-success" value="Submit">
					<input type="reset" class="btn" value="Reset">
				</div>			
			</form>		
		</div>
	</div>
</div>
