<div class="privileges view">
<h2><?php  echo __('Privilege'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($privilege['Privilege']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Role'); ?></dt>
		<dd>
			<?php echo $this->Html->link($privilege['Role']['title'], array('controller' => 'roles', 'action' => 'view', $privilege['Role']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Controller'); ?></dt>
		<dd>
			<?php echo h($privilege['Privilege']['controller']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Action'); ?></dt>
		<dd>
			<?php echo h($privilege['Privilege']['action']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo $this->Html->link($privilege['Status']['title'], array('controller' => 'statuses', 'action' => 'view', $privilege['Status']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($privilege['Privilege']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($privilege['Privilege']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
