<h2><?php echo __('Privileges'); ?></h2>
<br />
<table class="table table-bordered datatable" id="table-1">
	<thead>
		<tr>
			<th>Id</th>
			<th>Role</th>
			<th>Controller</th>
			<th>Action</th>
			<th>Status</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($privileges as $privilege): ?>
			<tr>
				<td><?php echo h($privilege['Privilege']['id']); ?>&nbsp;</td>
				<td><?php echo $privilege['Role']['title']; ?>&nbsp;</td>
				<td><?php echo h($privilege['Privilege']['controller']); ?>&nbsp;</td>
				<td><?php echo h($privilege['Privilege']['action']); ?>&nbsp;</td>
				<td id="change_<?php echo $privilege['Privilege']['id']; ?>_status"><?php echo $privilege['Status']['title']; ?>&nbsp;</td>
				<td>
					<a href="<?php echo $this->Html->u('privileges', 'view', $privilege['Privilege']['id']); ?>" class="btn btn-info btn-sm btn-icon icon-left">
						<i class="entypo-user"></i>
						View
					</a>

					<a href="<?php echo $this->Html->u('privileges', 'edit', $privilege['Privilege']['id']); ?>" class="btn btn-default btn-sm btn-icon icon-left">
						<i class="entypo-pencil"></i>
						Edit
					</a>
					
					<?php
						if($privilege['Status']['id'] == 1){
					?> 
						<a href="#" class="btn btn-danger btn-sm btn-icon icon-left jquery_action_status" id = 'action_<?php echo $privilege['Privilege']['id']; ?>_privileges' rel="2">
							<i class="entypo-cancel"></i>
							Deactivate
						</a>
					<?php } else {
					?> 
						<a href="#" class="btn btn-success btn-sm btn-icon icon-left jquery_action_status" id = 'action_<?php echo $privilege['Privilege']['id']; ?>_privileges' rel="1">
							<i class="entypo-check"></i>
							Activate
						</a>
					<?php }	?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<th>Id</th>
			<th>Role</th>
			<th>Controller</th>
			<th>Action</th>
			<th>Status</th>
			<th>Actions</th>
		</tr>
	</tfoot>
</table>

<a href="<?php echo $this->Html->u('privileges', 'add'); ?>" class="btn btn-primary">
	<i class="entypo-plus"></i>
	Add Privilege
</a>
