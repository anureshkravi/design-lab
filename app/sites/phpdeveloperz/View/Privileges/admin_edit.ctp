<div class="users form">
	<h2>Edit Privilege</h2>
	<br />
	<div class="panel panel-primary">
		<div class="panel-heading">
			<div class="panel-title">All fields have validation rules</div>
		</div>		
		<div class="panel-body">
			<form role="form" id="PrivilegeAdminEditForm" method="post" class="validate" action="/admin/privileges/edit/<?php echo $this->request->data['Privilege']['id']; ?>">
				<input type="hidden" name="id" value="<?php echo $this->request->data['Privilege']['id']; ?>">			
				<div class="form-group">
					<label class="control-label">Role</label>					
					<select class="form-control" name="role_id" required="required">
						<option value="">Select Role</option>
						<?php
							foreach( $roles as $key => $value ){
								echo "<option value='".$key."'";
								if( $this->request->data['Privilege']['role_id'] == $key ){
									echo "selected";
								}
								echo ">".$value."</option>";
							}
						?>
					</select>
				</div>

				<div class="form-group">
					<label class="control-label">Controller</label>					
					<select class="form-control" name="controller" id="PrivilegeController" required="required">
						<option value="">Select Controller</option>
						<?php
							foreach( $controllers as $key => $value ){
								echo "<option value='".$key."'";
								if( $this->request->data['Privilege']['controller'] == $key ){
									echo "selected";
								}
								echo ">".$value."</option>";
							}
						?>
					</select>
				</div>

				<div class="form-group">
					<label class="control-label">Action</label>					
					<select class="form-control" name="action" id="PrivilegeAction" required="required">
						<option value="">Select Action</option>
						<?php
							foreach( $dropdown_action as $key => $value ){
								echo "<option value='".$key."'";
								if( $this->request->data['Privilege']['action'] == $key ){
									echo "selected";
								}
								echo ">".$value."</option>";
							}
						?>
					</select>
				</div>

				<div class="form-group">
					<label class="control-label">Status</label>					
					<select class="form-control" name="status_id" required="required">
						<option value="">Select Status</option>
						<?php
							foreach( $statuses as $key => $value ){
								echo "<option value='".$key."'";
								if( $this->request->data['Privilege']['status_id'] == $key ){
									echo "selected";
								}
								echo ">".$value."</option>";
							}
						?>
					</select>
				</div>
				
				<div class="form-group">
					<input type="submit" class="btn btn-success" value="Submit">
					<input type="reset" class="btn" value="Reset">
				</div>			
			</form>		
		</div>
	</div>
</div>
