<div class="login_header">
	<div class="login_head_top" style="padding-top:25px; text-align:center;">
		<h1>LOGIN TO JOBSEEKER ACCOUNT</h1>
	</div><!--login_head_top-->

	<div class="login_header_inner">    	
		<div class="login_head_left">
			<h2 style="color:#E90D00; padding-bottom:12px;">Member Login</h2>
			<div class="facebook-margin">
				<a href="?loginFB=true">
					<?php echo $this->Html->image('faceb-login.jpg', array('border' => 0)); ?>
				</a>
			</div>
			Kindly use your Email ID &amp; Password to enjoy jobseeker benefits:
			<div class="login_form" style="margin-left:50px; margin-top:20px;">            	
				<form name="loginFrm" id="loginFrm" method="post" action="">
				<table style="margin-top:35px;">                
				<tr>
					<td valign="top" width="40%" ><label>Email <span class="red"> * </span></label></td>
					<td width="60%">
						<input name="email" id="email" value="" type="text" size="30" />
						<div class="error" id="emailErr"></div>
					</td>
				</tr>
				<tr>
				<td><label>Password <span class="red"> * </span></label></td>
				<td>
					<input type="password" name="password" id="password" value="" size="30" style="margin-top:10px;" />
					<div class="error" id="passwordErr"></div>
				</td>
				</tr>
				<tr>
				<td></td>
				<td>
                    <input type="hidden" name="return" id="return" value="">
					<input name="submitLogin" type="submit" value="Submit" class="resume_submit" />
				</td>
				</tr>
				<tr>
				<td></td>
				<td><a href="<?php echo $this->Html->url('/users/forgot_password'); ?>">Forgot Your Password?</a></td>
				</tr>    
				</table>           
				</form>                
			</div><!--login_form-->
		</div><!--login_head_left-->
		<div class="blue_line" style="margin-top:20px;"></div>
		
		<div class="login_head_right">
			<h2>
				<span style="color:#E90D00;">Not yet a member</span>
				<a href="#">Join Now</a>
			</h2>
			<h2 style="font-weight:normal; font-size:15px; color:#5a5a5a; margin-bottom:15px; margin-top:15px;">Why Register as JOBSEEKER?</h2>            
			<ul>
				<li>Receive job alerts in your email</li>
				<li>Post your resume online FREE.</li>
				<li>Attract featured employers and recruiters</li>
				<li>Apply to suitable jobs instantly</li>
				<li>Get Jobs in your inbox that match your profile</li>
			</ul>

			<p class="facebook-p">Or</p>

			<div class="facebook-signup">
				<a href="#">
					<?php echo $this->Html->image('faceb-signup.jpg', array('border' => 0)); ?>
				</a>
			</div>           
		</div><!--login_head_right-->
		<div class="clear"></div>
	</div><!--login_header_inner-->
</div><!--login_header-->