<form action="/index.php" method="post">
<p>
Name (4 characters minimum):
<input name="user" data-validation="length" data-validation-length="min4">
</p>
<p>
Year (yyyy-mm-dd):
<input name="birth" data-validation="date" data-validation-format="yyyy-mm-dd">
</p>
<p>
Website:
<input name="website" data-validation="url">
</p>

<p>
email:
<input name="website" data-validation="email">
</p>

<p>
password:
<input name="pass_confirmation" data-validation="strength" data-validation-strength="2" type="password">
</p>

<p>
Confirm password:
<input name="pass" data-validation="confirmation" type="password">
</p>

<p>
checkbox:
<input type="checkbox" data-validation-error-msg="You have to agree to our terms" data-validation="required" value="1" name="check">
</p>


<p>
<input type="submit">
</p>
</form>
 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script>
<script>
$.validate({
modules : 'security',
onModulesLoaded : function() {
$('input[name="pass_confirmation"]').displayPasswordStrength();
}
});
</script>