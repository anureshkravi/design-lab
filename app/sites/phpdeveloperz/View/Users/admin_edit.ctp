<div class="users form">
	<h2>Edit User</h2>
	<br />
	<div class="panel panel-primary">
		<div class="panel-heading">
			<div class="panel-title">All fields have validation rules</div>
		</div>		
		<div class="panel-body">
			<form role="form" id="UserAdminEditForm" method="post" class="validate" action="/admin/users/edit/<?php echo $this->request->data['User']['id']?>">
				<input type="hidden" class="form-control" name="id" value="<?php echo $this->request->data['User']['id']; ?>" />			
				<div class="form-group">
					<label class="control-label">Name *</label>				
					<input type="text" class="form-control" name="name" required="required" value="<?php echo $this->request->data['User']['name']; ?>" />
				</div>
				
				<div class="form-group">
					<label class="control-label">Email *</label>					
					<input type="email" class="form-control" name="email" required="required" value="<?php echo $this->request->data['User']['email']; ?>" readonly="readonly" />
				</div>

				<div class="form-group">
					<label class="control-label">Gender</label>					
					<select class="form-control" name="gender">
						<option value="M" <?php if($this->request->data['User']['gender'] == 'M') echo "selected"; ?>>M</option>
						<option value="F" <?php if($this->request->data['User']['gender'] == 'F') echo "selected"; ?>>F</option>
					</select>
				</div>

				<div class="form-group">
					<label class="control-label">Address</label>					
					<textarea placeholder="" name="address" id="field-ta" class="form-control"><?php echo $this->request->data['User']['address']; ?></textarea>
				</div>

				<div class="form-group">
					<label class="control-label">City</label>					
					<input type="text" class="form-control" name="city" placeholder="" value="<?php echo $this->request->data['User']['city']; ?>" />
				</div>

				<div class="form-group">
					<label class="control-label">State</label>					
					<input type="text" class="form-control" name="state" placeholder="" value="<?php echo $this->request->data['User']['state']; ?>" />
				</div>

				<div class="form-group">
					<label class="control-label">Country</label>					
					<input type="text" class="form-control" name="country" placeholder="" value="<?php echo $this->request->data['User']['country']; ?>" />
				</div>

				<div class="form-group">
					<label class="control-label">Role</label>					
					<select class="form-control" name="role_id" required="required">
						<option value="">Select Role</option>
						<?php
							foreach( $roles as $key => $value ){
								echo "<option value='".$key."'";
								if( $this->request->data['User']['role_id'] == $key ){
									echo "selected";
								}
								echo ">".$value."</option>";
							}
						?>
					</select>
				</div>

				<div class="form-group">
					<label class="control-label">Status</label>					
					<select class="form-control" name="status_id" required="required">
						<option value="">Select Status</option>
						<?php
							foreach( $statuses as $key => $value ){
								echo "<option value='".$key."'";
								if( $this->request->data['User']['status_id'] == $key ){
									echo "selected";
								}
								echo ">".$value."</option>";
							}
						?>
					</select>
				</div>

				<div class="form-group">
					<label class="control-label">Featured</label>					
					<select class="form-control" name="featured">
						<option value="0" <?php if( $this->request->data['User']['featured'] == 0 ) echo "selected"; ?>>Not Featured</option>
						<option value="1" <?php if( $this->request->data['User']['featured'] == 1 ) echo "selected"; ?>>Featured</option>
					</select>
				</div>

				<div class="form-group">
					<label class="control-label">Subscribe</label>					
					<select class="form-control" name="subscribe">
						<option value="0" <?php if( $this->request->data['User']['subscribe'] == 0 ) echo "selected"; ?>>Not Subscribe</option>
						<option value="1" <?php if( $this->request->data['User']['subscribe'] == 1 ) echo "selected"; ?>>Subscribe</option>
					</select>
				</div>
				
				<div class="form-group">
					<input type="submit" class="btn btn-success" value="Submit">
					<input type="reset" class="btn" value="Reset">
				</div>			
			</form>		
		</div>
	</div>
</div>
