<div class="users form">
	<h2>Add User</h2>
	<br />
	<div class="panel panel-primary">
		<div class="panel-heading">
			<div class="panel-title">All fields have validation rules</div>
		</div>
		<div class="panel-body">
			<form role="form" id="UserAdminAddForm" method="post" class="validate" action="/admin/users/add">
				<div class="form-group">
					<label class="control-label">Name *</label>			
					<input type="text" class="form-control" name="name" required="required" />
				</div>
				
				<div class="form-group" id="emailContainer">
					<label class="control-label">Email *</label>					
					<input type="email" class="form-control" id="email" name="email" required="required" />
				</div>

				<div class="form-group">
					<label class="control-label">Password *</label>					
					<input type="password" class="form-control" name="password" required="required" />
				</div>

				<div class="form-group">
					<label class="control-label">Gender</label>					
					<select class="form-control" name="gender">
						<option value="M">M</option>
						<option value="F">F</option>
					</select>
				</div>

				<div class="form-group">
					<label class="control-label">Address</label>					
					<textarea placeholder="" name="address" id="field-ta" class="form-control"></textarea>
				</div>

				<div class="form-group">
					<label class="control-label">City</label>					
					<input type="text" class="form-control" name="city" placeholder="" />
				</div>

				<div class="form-group">
					<label class="control-label">State</label>					
					<input type="text" class="form-control" name="state" placeholder="" />
				</div>

				<div class="form-group">
					<label class="control-label">Country</label>					
					<input type="text" class="form-control" name="country" placeholder="" />
				</div>

				<div class="form-group">
					<label class="control-label">Role</label>					
					<select class="form-control" name="role_id" required="required">
						<option value="">Select Role</option>
						<?php
							foreach( $roles as $key => $value ){
								echo "<option value='".$key."'>".$value."</option>";
							}
						?>
					</select>
				</div>

				<div class="form-group">
					<label class="control-label">Status</label>					
					<select class="form-control" name="status_id" required="required">
						<option value="">Select Status</option>
						<?php
							foreach( $statuses as $key => $value ){
								echo "<option value='".$key."'>".$value."</option>";
							}
						?>
					</select>
				</div>

				<div class="form-group">
					<label class="control-label">Featured</label>					
					<select class="form-control" name="featured">
						<option value="0">Not Featured</option>
						<option value="1">Featured</option>
					</select>
				</div>

				<div class="form-group">
					<label class="control-label">Subscribe</label>					
					<select class="form-control" name="subscribe">
						<option value="0">Not Subscribe</option>
						<option value="1">Subscribe</option>
					</select>
				</div>
								
				<div class="form-group">
					<input type="submit" id="UserAddButton" class="btn btn-success" value="Submit">
					<input type="reset" class="btn" value="Reset">
				</div>
			</form>
		</div>
	</div>
</div>
