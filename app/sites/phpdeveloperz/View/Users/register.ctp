<div class="reg_header">
    <div class="reg_head_left">
        <div class="reg_head_left_top">
            <h1>USER REGISTRATION - GET STARTED</h1>
        </div><!--reg_head_left_top-->

        <div class="reg_head_left_one">
            <h2>Jobseeker Registration - Apply now for a FREE account!</h2>
            <div class="clear"></div>
        </div><!--reg_head_left_one-->

        <div style="float:left;margin:10px 0 0 20px;">
            Please fill the fields below to start your registration process. <span class="red"> * Required Fields </span>
        </div>

        <div class="clear"></div>

        <form action="/users/register" method="post" id="registrationForm">
            <div class="register_form">
                <div style="float:left;" class="reg_form_left">
                    <table cellspacing="0" cellpadding="5" border="0" style="width:500px;">
                        <tbody>
                            <tr>
                                <td width="40%" valign="top">Name: <span class="red"> * </span></td>
                                <td width="60%">
                                    <input type="text" value="" id="name" name="name" required="required">
                                    <div id="nameErr" class="error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Gender:</td>
                                <td>
                                    <input type="radio" checked="checked" value="M" name="gender">Male
                                    &nbsp;<input type="radio" value="F" name="gender">Female
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Email: <span class="red"> * </span></td>
                                <td>
                                    <input data-validation="email" type="text" value="" name="email">
                                    <div id="emailErr" class="error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Password: <span class="red"> * </span></td>
                                <td>
                                    <input type="password" value="" id="password" name="password" required="required">
                                    <div id="passwordErr" class="error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Confirm Password: <span class="red"> * </span></td>
                                <td>
                                    <input type="password" value="" id="conf_password" name="conf_password" required="required">
                                    <div id="conf_passwordErr" class="error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>Newsletter:</td>
                                <td>
                                    <input type="radio" checked="checked" value="1" name="subscribe">Yes
                                    &nbsp;<input type="radio" checked="" value="0" name="subscribe">No
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="border-top:#333333 thin dotted; padding-top:10px; text-align:left; font-size:14px;" colspan="2">
                                    I accept the <a href="#">terms &amp; conditions</a><span class="red"> * </span>
                                    <input type="checkbox" checked="checked" id="terms" name="terms" style="margin-right:15px;">
                                    <div id="termsErr" class="error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <input type="submit" id="userRegisterButton" class="submit" value="Submit">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div><!--reg_form_left-->
                <div class="clear"></div>
            </div><!--register_form--> 
        </form>

        <div id="shadowing"></div>

        <div id="box1">
            <span id="boxtitle"> Terms of Use </span>
            <p class="cross"> <a onclick="closebox('box1')" href="javascript:void(0);"> Close Window </a> </p>
            <div class="popupWindow">
                <strong>Welcome to PhpDeveloperz.com</strong><br><br>
                PhpDeveloperz.com is a directory based listing tool that abides by certain Terms and Conditions (T&amp;C). PhpDeveloperz.com has the authority to review and update them occasionally without issuing any notice. Using the services featured, you agree to adhere the terms and conditions of PhpDeveloperz.com. In case of any other services given by PhpDeveloperz.com, the same may be governed by a different set of Terms and Conditions.<br>
                Unless precisely stated otherwise, any new feature that adds or improves the current service, are automatically made accountable to the T&amp;C.<br>
                <br>
                <u><strong> PhpDeveloperz.com Content</strong></u><br>
                PhpDeveloperz.com offers search services to users worldwide. The content shown in the portal is developed by people over whom PhpDeveloperz.com exercises no control. Thus, you hereby realize and agree that the information given is “as-is” and that PhpDeveloperz.com does not assure accuracy, completeness and timeliness of the data.<br>
                <br>
                You agree that using the service, you are responsible to be exposed to content that is indecent, offensive, and objectionable. In such case, PhpDeveloperz.com will not be accountable in any ways for content, including, but not limited to, for any omissions or mistakes in the content.<br>
                <br>
                Only you are liable for getting access to the service, which may include third party fees. PhpDeveloperz.com is not liable for those fees, including the ones associated with the display or delivery of advertisements. You must give and are liable for all equipments necessary to access the service.<br>
                <br>
                <u><strong> Pre-evaluation of Content</strong></u><br>
                PhpDeveloperz.com is not obliged to monitor the information it posts. In any case, PhpDeveloperz.com cannot assure that a specific directory will not include any other data other than the particular directory. PhpDeveloperz.com is not responsible for the content of any site included in any search results.<br>
                <br>
                PhpDeveloperz.com is liberal to pre-screen the content at any point of time. You agree to evaluate your message and suffer all risks concerning to the use of any message, including any dependence on the exactness, usefulness, or completeness of same. You admit that you may not depend on any content made by or submitted at PhpDeveloperz.com.<br>
                <br>
                Responsibilities concerning to the use of service<br>
                In consideration of your use of our service, you mean that you are of legal age to make a binding contract and are not a person banished from receiving services under any applicable jurisdiction.<br>
                <br>
                You hereby recognize that all data and content including text, music, software, sound, graphics, photos, videos, messages and other materials, whether privately transmitted or publicly posted, are the full responsibility of the provider. This means that the provider is solely responsible for all content posted or uploaded at PhpDeveloperz.com.<br>
                <br>
                You agree not to show any content that is harmful, illegal, abusive, threatening, defamatory, harassing, obscene, vulgar and invasive of another’s privacy, racially objectionable, hateful, or otherwise.<br>
                <br>
                You recognize not to post, upload, or make available any content that intervenes with or disrupts the service or refuse any requirements, policies, procedures, and regulations of networks connected to the service.<br>
                <br>
                <u><strong> Common Terms and Conditions</strong></u><br>
                You will adhere all local rules concerning to online conduct and acceptable content. You will follow all pertinent laws related with the transmission of technical information imported and/or exported to any nation. You agree to assure and hold Travelswlae.com risk-free from any demand or claim, made by any third party due to the content you submit, transmit, post or make available through<br>
                PhpDeveloperz.com.<br>
                <br>
                <u><strong> Alteration and Abortion of Service</strong></u><br>
                PhpDeveloperz.com reserves the authority to alter or abort, temporarily or permanently, the service with or without notice at any time. You agree that PhpDeveloperz.com bears no accountability to you or to any third party for any alteration or abortion of the service.<br>
                <div class="clear"></div>
            </div><!--popupWindow-->
        </div>
        <div class="content_box_bott"></div>
    </div><!--reg_head_left-->

    <div class="reg_head_right_outer">
        <div class="reg_head_right_top"></div>
        <div class="reg_head_right">
            <h2>Why Register as JOBSEEKER?</h2>
            * Receive job alerts in your email<br>
            <hr>
            * Post your resume online FREE<br>
            <hr>
            * Attract featured employers and recruiters<br>
            <hr>
            * Apply to suitable jobs instantly<br>
            <hr>
            * Get Jobs in your inbox that match your profile<br>
            <hr>
            * Join the fastest growing community of php professionals<br>
            <div class="clear"></div>
        </div><!--reg_head_right-->

        <div class="reg_head_right_bott"></div>

    </div><!--reg_head_right_outer-->		
    <div class="clear"></div>
</div>