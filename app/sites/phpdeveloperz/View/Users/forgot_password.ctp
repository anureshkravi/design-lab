<div class="login_header">    
    <div style="padding-top:25px; text-align:center;" class="login_head_top">
        <h1>FORGOT PASSWORD?</h1>
    </div><!--login_head_top-->

    <div class="login_header_inner">
        <div class="login_head_left">
            <h2 style="color:#E90D00; padding-bottom:12px;">Job Seeker: Forgot password?</h2>
            Enter your email address below and we will send you a link to reset your password.

            <div style="margin-left:50px; margin-top:10px;" class="login_form">
                <form action="#" method="post" id="forgotPasswordFrm" name="forgotPasswordFrm">
                <table style="margin-top:25px;">                
                <tbody><tr>
                    <td width="40%"><label>Email: <span class="red"> * </span></label></td>
                    <td width="60%">
                        <input type="text" class="profile_textfield" size="30" value="" id="email" name="email">
                        <div id="emailErr" class="error"></div>
                    </td>
                </tr>      
                <tr>
                    <td></td>
                    <td>
                        <img border="0" alt="Jobseeker Forget Password Captcha" src="captchaJsForgetPwdImage.php">
                        <input type="hidden" value="a6061d" id="pin" name="pin">
                    </td>
                </tr>
                <tr>
                    <td><label>Enter the code: <span class="red"> * </span></label></td>
                    <td>                            
                        <input type="text" class="profile_textfield" id="confirm_pin" name="confirm_pin" value="" size="30">
                        <div id="confirm_pinErr" class="error"></div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" style="margin-top:15px;" class="resume_submit" name="submitForgetPwd" value="Submit">
                    </td>
                </tr>               
                <tr>
                    <td></td>
                    <td><a href="<?php echo $this->Html->url('/users/login', true); ?>">Click Here to Login</a></td>
                </tr>    
                </tbody></table>          
                </form>                
            </div><!--login_form-->
        </div><!--login_head_left-->

        <div class="blue_line"></div>       
        
        <div class="login_head_right">
            <h2 style="padding-bottom:15px;">
                <span style="color:#5a5a5a;">Not yet a member</span>
                <a href="http://www.phpdeveloperz.com/testmode/registration.html">Join Now</a>
            </h2>

            <h2 style="font-weight:normal; font-size:15px; color:#5a5a5a; margin-bottom:15px;">
                Why Register as <span style="color:#E90D00; font-weight:bold;">JOBSEEKER?</span>
            </h2>           

            <ul>
                <li>Receive job alerts in your email</li>
                <li>Post your resume online FREE.</li>
                <li>Attract featured employers and recruiters</li>
                <li>Apply to suitable jobs instantly</li>
                <li>Get Jobs in your inbox that match your profile</li>
            </ul>            
            <a href="<?php echo $this->Html->url('/users/register'); ?>">
                <?php echo $this->Html->image('register-butt.jpg', array("style" => array('margin-top:10px; margin-left:50px;'))); ?>
            </a>           
        </div><!--login_head_right-->
        <div class="clear"></div>
    </div><!--login_header_inner-->
</div>