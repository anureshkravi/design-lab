<div class="users form">
	<h2>Add Role</h2>
	<br />
	<div class="panel panel-primary">
		<div class="panel-heading">
			<div class="panel-title">All fields have validation rules</div>
		</div>		
		<div class="panel-body">
			<form role="form" id="RoleAdminAddForm" method="post" class="validate" action="/admin/roles/add">			
				<div class="form-group">
					<label class="control-label">Name *</label>				
					<input type="text" class="form-control" name="title" required="required" />
				</div>

				<div class="form-group">
					<label class="control-label">Status</label>					
					<select class="form-control" name="status_id" required="required">
						<option value="">Select Status</option>
						<?php
							foreach( $statuses as $key => $value ){
								echo "<option value='".$key."'>".$value."</option>";
							}
						?>
					</select>
				</div>
				
				<div class="form-group">
					<input type="submit" class="btn btn-success" value="Submit">
					<input type="reset" class="btn" value="Reset">
				</div>			
			</form>		
		</div>
	</div>
</div>
