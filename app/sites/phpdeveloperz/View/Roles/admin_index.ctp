<h2><?php echo __('Roles'); ?></h2>
<br />
<table class="table table-bordered datatable" id="table-1">
	<thead>
		<tr>
			<th>Id</th>
			<th>Role</th>
			<th>Created By</th>
			<th>Status</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ( $roles as $role ): ?>
			<tr>
				<td><?php echo h($role['Role']['id']); ?>&nbsp;</td>
				<td><?php echo h($role['Role']['title']); ?>&nbsp;</td>
				<td><?php echo $role['User']['name']; ?></td>
				<td id="change_<?php echo $role['Role']['id']; ?>_status"><?php echo $role['Status']['title']; ?></td>
				<td>
					<a href="<?php echo $this->Html->u('roles', 'view', $role['Role']['id']); ?>" class="btn btn-info btn-sm btn-icon icon-left">
						<i class="entypo-user"></i>
						View
					</a>

					<a href="<?php echo $this->Html->u('roles', 'edit', $role['Role']['id']); ?>" class="btn btn-default btn-sm btn-icon icon-left">
						<i class="entypo-pencil"></i>
						Edit
					</a>
					
					<?php
						if($role['Status']['id'] == 1){
					?> 
						<a href="#" class="btn btn-danger btn-sm btn-icon icon-left jquery_action_status" id = 'action_<?php echo $role['Role']['id']; ?>_roles' rel="2">
							<i class="entypo-cancel"></i>
							Deactivate
						</a>
					<?php } else {
					?> 
						<a href="#" class="btn btn-success btn-sm btn-icon icon-left jquery_action_status" id = 'action_<?php echo $role['Role']['id']; ?>_roles' rel="1">
							<i class="entypo-check"></i>
							Activate
						</a>
					<?php }	?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<th>Id</th>
			<th>Role</th>
			<th>Created By</th>
			<th>Status</th>
			<th>Actions</th>
		</tr>
	</tfoot>
</table>

<a href="<?php echo $this->Html->u('roles', 'add'); ?>" class="btn btn-primary">
	<i class="entypo-plus"></i>
	Add Role
</a>
