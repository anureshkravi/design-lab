<div class="users form">
	<h2>Edit Role</h2>
	<br />
	<div class="panel panel-primary">
		<div class="panel-heading">
			<div class="panel-title">All fields have validation rules</div>
		</div>		
		<div class="panel-body">
			<form role="form" id="RoleAdminEditForm" method="post" class="validate" action="/admin/roles/edit/<?php echo $this->request->data['Role']['id']; ?>">
				<input type="hidden" class="form-control" name="id" value="<?php echo $this->request->data['Role']['id']?>" />			
				<div class="form-group">
					<label class="control-label">Name *</label>				
					<input type="text" class="form-control" name="title" required="required" value="<?php echo $this->request->data['Role']['title']?>" />
				</div>

				<div class="form-group">
					<label class="control-label">Status</label>					
					<select class="form-control" name="status_id" required="required">
						<option value="">Select Status</option>
						<?php
							foreach( $statuses as $key => $value ){
								echo "<option value='".$key."'";
								if( $this->request->data['Role']['status_id'] == $key ){
									echo "selected";
								}
								echo ">".$value."</option>";
							}
						?>
					</select>
				</div>
				
				<div class="form-group">
					<input type="submit" class="btn btn-success" value="Submit">
					<input type="reset" class="btn" value="Reset">
				</div>			
			</form>		
		</div>
	</div>
</div>
