<div class="footer" style="width:1000px; margin-top:20px;">
	<div class="footer_top">
		<div class="footer_top_menu_box">
			<h3 style="margin:0 0 0 25px;padding:0px;width:175px;">Job Seeker</h3>
			<ul>
				<li><a href="#">Search Jobs</a></li>
				<li><a href="#">Login</a></li>
				<li><a href="#">Find Jobs By Skills</a></li>
				<li><a href="#">Find Jobs By Location</a></li>
				<li><a href="#">Find Jobs By Code</a></li>
			</ul>
		</div><!--footer_top_menu_box-->

		<div class="footer_top_menu_box">
			<h3 style="margin:0 0 0 25px;padding:0px;width:175px;">&nbsp;</h3>
			<ul>
				<li><a href="#">My Profile</a></li>
				<li><a href="#">My Resumes</a></li>
				<li><a href="#">Tell a friend</a></li>
				<li><a href="#">Post Resume</a></li>
				<li><a href="#">Jobseeker's FAQ</a></li>
			</ul>
		</div><!--footer_top_menu_box-->

		<div class="white-line"></div>

		<div class="footer_top_menu_box">
			<h3 style="margin:0 0 0 25px;padding:0px;width:175px;">Know about Us</h3>
			<ul>
				<li><a href="#">About Us</a></li>
				<li><a href="#">Our Vision & Mission</a></li>
				<li><a href="#">Contact Us</a></li>
				<li><a href="#">Sitemap</a></li>
			</ul>
		</div><!--footer_top_menu_box-->

		<div class="footer_top_menu_box">
			<h3 style="margin:0 0 0 25px;padding:0px;width:175px;">&nbsp;</h3>
			<ul>
				<li><a href="#">Disclaimer</a></li>
				<li><a href="#">Privacy Policy</a></li>
				<li><a href="#">Terms &amp; Conditions</a></li>
				<li><a href="#">Jobseeker's Testimonials</a></li>
				<li><a href="#">Jobseeker's Guided Tour</a></li>
			</ul>
		</div><!--footer_top_menu_box-->

		<div class="white-line"></div>

		<div class="footer_top_menu_box">
			<h3 style="margin:0 0 0 25px;padding:0px;width:175px;">Give your feedback</h3>
			<ul>
				<li><a href="#">Report Website Problems</a></li>
				<li><a href="#">Make a Suggestion</a></li>
				<li><a href="#">Resources</a></li>
			</ul>
		</div><!--footer_top_menu_box-->            

		<div class="clear"></div>
	</div><!--footer_top-->

	<div class="footer_bott" style="margin-top:10px; margin-bottom:15px; font-size:12px;margin-left:7px;">
		<div class="footer_bott_left" style="float:left; line-height:24px">
			Copyright &copy; 2013 - 2014 - All rights reserved. Concept & Design By <br />
			<a href="http://www.xpertwebservices.com" title="Website Development Company Delhi" target="_blank">
				<img alt="Xpert Web Services" src="img/xpert-web-services.png" style="margin-bottom:3px;" />
			</a>
		</div><!--footer_bott_left-->

		<div class="footer_bott_right" style="float:left; margin-right:10px;margin-left:70px; line-height:20px;">
			<div class="footer_row1_b">
				<div class="top" align="center">
					<div class="txt"><strong>Who's Online</strong></div>
				</div>
				<div class="top" style="clear:both;padding-top:5px;float:right;">
					<strong>Guest:</strong> 1 | <strong>Jobseekers:</strong> 0 | <strong>Employers:</strong> 0
				</div>
			</div>
		</div>

		<div class="footer_bott_right" style="float:right; margin-right:10px;">
			<div class="footer_row1_b">
				<div class="top">
					<div class="txt" style="float:right;"><strong>Our other sites</strong></div>
				</div>
				<div class="top" style="clear:both;padding-top:5px;">
					<div class="networks">
						<a href="http://www.website-development-company.in" title="website-development-company">
							<img alt="best web resourcess" src="img/website-development-company.png"  />
						</a>
					</div>
				</div>
			</div>
		</div><!--footer_bott_left-->
		<div class="clear"></div>
	</div>
</div><!--footer-->