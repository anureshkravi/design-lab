<div class="outer_header">
	<div class="header">
		<div class="logo">
			<?php echo $this->Html->link($this->Html->image('logo.jpg'), '#', array('escape' => false)); ?>
		</div><!--logo-->
		<div class="header_right">
			<div style="background-color:#E70400;float:right;text-align:center;-moz-border-radius: 0 0 5px 5px;border-radius: 0 0 5px 5px;padding:5px;font-size:13px;margin-bottom:10px;color:#ffffff;min-width:150px;min-height:20px;">          
				Welcome <strong>Guest</strong>
			</div>
			<div class="clear"> </div>
			<div id="menu">
				<ul>
					<li><a href="<?php echo $this->Html->url('/', true); ?>" title="">Home</a></li>
					<li ><a href="#" title="">Search Jobs</a></li>
					<li ><a href="#" title="">Tell a Friend</a></li>
					<li ><a href="#" title="">FAQ's</a></li>
					<li ><a href="<?php echo $this->Html->url('/users/login', true); ?>" title="">Login</a></li>
					<li ><a href="<?php echo $this->Html->url('/users/register', true); ?>" title="">Signup</a></li>
				</ul>
			</div><!-- end of menu -->
			<div class="clear"> </div>
		</div><!--header_right-->
		<div class="clear"> </div>
	</div><!--header-->
</div><!--outer_header-->