<div class="row">
    <div class="col-md-6 col-sm-8 clearfix">
        <ul class="user-info pull-left pull-none-xsm">
            <li class="profile-info dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="/assets/images/thumb-1.png" alt="" class="img-circle" />Art Ramadani</a>
                <ul class="dropdown-menu">
                    <li class="caret"></li>                    
                    <li><a href="#"><i class="entypo-user"></i>Edit Profile</a></li>
                </ul>
            </li>
        </ul>

        <ul class="user-info pull-left pull-right-xs pull-none-xsm">
            <li class="notifications dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"><i class="entypo-attention"></i><span class="badge badge-info">6</span></a>
                <ul class="dropdown-menu">
                    <li class="top">
                        <p class="small">
                            <a href="#" class="pull-right">Mark all Read</a>
                            You have <strong>3</strong> new notifications.
                        </p>
                    </li>

                    <li>
                        <ul class="dropdown-menu-list scroller">
                            <li class="unread notification-success">
                                <a href="#">
                                    <i class="entypo-user-add pull-right"></i>                            
                                    <span class="line"><strong>New user registered</strong></span>         
                                    <span class="line small">30 seconds ago</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="notification-danger">
                        <a href="#">
                            <i class="entypo-cancel-circled pull-right"></i>                  
                            <span class="line">John cancelled the event</span>                     
                            <span class="line small">9 hours ago</span>
                        </a>
                    </li>

                    <li class="external">
                        <a href="#">View all notifications</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>

    <div class="col-md-6 col-sm-4 clearfix hidden-xs">
        <ul class="list-inline links-list pull-right">
            <li><a href="#">Live Site</a></li>
            <li class="sep"></li>              
            <li><a href="<?php echo $this->Html->u('users','logout'); ?>">Log Out <i class="entypo-logout right"></i></a></li>
        </ul>
    </div>
</div>
<hr />