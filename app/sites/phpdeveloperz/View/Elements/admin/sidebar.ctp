<div class="sidebar-menu">
    <header class="logo-env">
        <div class="logo">
            <a href="#">
                <?php echo $this->Html->image('/assets/images/logo.png'); ?>
            </a>
        </div>

        <div class="sidebar-collapse">
            <a href="#" class="sidebar-collapse-icon with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>

        <div class="sidebar-mobile-menu visible-xs">
            <a href="#" class="with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>
    </header>

    <ul id="main-menu" class="">
        <li id="search">
            <form method="get" action="#">
                <input type="text" name="q" class="search-input" placeholder="Search something..." />
                <button type="submit"><i class="entypo-search"></i></button>
            </form>
        </li>

        <li class="active opened active">
            <a href="<?php echo $this->Html->u('dashboard', 'index'); ?>"><i class="entypo-gauge"></i><span>Dashboard</span></a>
        </li>

        <li>
            <a href="#"><i class="entypo-layout"></i><span>Job Seekers</span></a>
            <ul>
                <li><a href="#">Manage Users</a>
                    <ul>
                        <li><a href="<?php echo $this->Html->u('users', 'index'); ?>"><span>List Users</span></a></li>
                        <li><a href="#"><span>List Resumes</span></a></li>
                        <li><a href="#"><span>List Cover Letters</span></a></li>
                        <li><a href="#"><span>List Saved Jobs</span></a></li>
                        <li><a href="#"><span>List Applied Jobs</span></a></li>
                    </ul>
                </li>

                <li>
                    <a href="#"><span>Jobseeker Testimonials</span></a>
                    <ul>
                        <li><a href="#"><span>View Testimonials</span></a></li>
                        <li><a href="#"><span>Add Testimonial</span></a></li>
                    </ul>
                </li>
            </ul>
        </li>

        <li>
            <a href="#"><i class="entypo-layout"></i><span>Employers</span></a>
        </li>

        <li><a href="#"><i class="entypo-layout"></i><span>Roles</span></a>
            <ul>
                <li><a href="<?php echo $this->Html->u('roles', 'index'); ?>">View Roles</a></li>
                <li><a href="<?php echo $this->Html->u('roles', 'add'); ?>">Add Role</a></li>
            </ul>
        </li>
        <li><a href="#"><i class="entypo-layout"></i><span>Priveleges</span></a>
            <ul>
                <li><a href="<?php echo $this->Html->u('privileges', 'index'); ?>">View Priveleges</a></li>
                <li><a href="<?php echo $this->Html->u('privileges', 'add'); ?>">Add Priveleges</a></li>
            </ul>
        </li>
    </ul>
</div>