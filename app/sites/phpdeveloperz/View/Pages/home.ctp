<div class="outer_flash">
	<div class="flash_container">
		<div class="flash">
			<div class="header-slider">
				<div id="wowslider-container1">
					<div class="ws_images">
						<span><?php echo $this->Html->image('/img/1.jpg'); ?></span>
					</div>
				</div>
			</div><!--header-slider-->

			<form name="searchFrm" id="searchFrm" method="post" action="#">
				<div class="search_box">
					<h2> Quick Job Search: </h2>
					<p> Enter Keyword(s): </p>
					<p style="font-size:12px;"> (i.e job title, company name, etc): </p>
					<input name="keywords" class="search_textfield" type="text" value="Keywords" />
					<select class="search_dropdown" name="Country" id="Country">
						<option value="0">--Select Country--</option>
						<option value="1">Afghanistan</option>
						<option value="2">Albania</option>
						<option value="3">Algeria</option>
						<option value="4">American Samoa</option>
						<option value="5">Andorra</option>
						<option value="6">Angola</option>
						<option value="7">Anguilla</option>
						<option value="8">Antarctica</option>
						<option value="9">Antigua and Barbuda</option>
						<option value="10">Argentina</option>
						<option value="11">Armenia</option>
						<option value="12">Aruba</option>
						<option value="13">Australia</option>
						<option value="14">Austria</option>
						<option value="15">Azerbaijan</option>
						<option value="16">Bahamas</option>
						<option value="17">Bahrain</option>
						<option value="18">Bangladesh</option>
						<option value="19">Barbados</option>
						<option value="20">Belarus</option>
						<option value="21">Belgium</option>
						<option value="22">Belize</option>
						<option value="23">Benin</option>
						<option value="24">Bermuda</option>
						<option value="25">Bhutan</option>
						<option value="26">Bolivia</option>
						<option value="27">Bosnia and Herzegowina</option>
						<option value="28">Botswana</option>
						<option value="29">Bouvet Island</option>
						<option value="30">Brazil</option>
						<option value="32">Brunei Darussalam</option>
						<option value="33">Bulgaria</option>
						<option value="34">Burkina Faso</option>
						<option value="35">Burundi</option>
						<option value="36">Cambodia</option>
						<option value="37">Cameroon</option>
						<option value="38">Canada</option>
						<option value="39">Cape Verde</option>
						<option value="40">Cayman Islands</option>
						<option value="41">Central African Republic</option>
						<option value="42">Chad</option>
						<option value="244">Chile</option>
						<option value="243">Chile</option>
						<option value="44">China</option>
						<option value="45">Christmas Island</option>
						<option value="46">Cocos (Keeling) Islands</option>
						<option value="47">Colombia</option>
						<option value="48">Comoros</option>
						<option value="49">Congo</option>
						<option value="50">Cook Islands</option>
						<option value="51">Costa Rica</option>
						<option value="52">Cote D'Ivoire</option>
						<option value="53">Croatia</option>
						<option value="54">Cuba</option>
						<option value="55">Cyprus</option>
						<option value="56">Czech Republic</option>
						<option value="57">Denmark</option>
						<option value="58">Djibouti</option>
						<option value="59">Dominica</option>
						<option value="60">Dominican Republic</option>
						<option value="61">East Timor</option>
						<option value="62">Ecuador</option>
						<option value="63">Egypt</option>
						<option value="64">El Salvador</option>
						<option value="65">Equatorial Guinea</option>
						<option value="66">Eritrea</option>
						<option value="67">Estonia</option>
						<option value="68">Ethiopia</option>
						<option value="69">Falkland Islands (Malvinas)</option>
						<option value="70">Faroe Islands</option>
						<option value="71">Fiji</option>
						<option value="72">Finland</option>
						<option value="73">France</option>
						<option value="74">France, Metropolitan</option>
						<option value="75">French Guiana</option>
						<option value="76">French Polynesia</option>
						<option value="77">French Southern Territories</option>
						<option value="78">Gabon</option>
						<option value="79">Gambia</option>
						<option value="80">Georgia</option>
						<option value="81">Germany</option>
						<option value="82">Ghana</option>
						<option value="83">Gibraltar</option>
						<option value="84">Greece</option>
						<option value="85">Greenland</option>
						<option value="86">Grenada</option>
						<option value="87">Guadeloupe</option>
						<option value="88">Guam</option>
						<option value="89">Guatemala</option>
						<option value="90">Guinea</option>
						<option value="91">Guinea-bissau</option>
						<option value="92">Guyana</option>
						<option value="93">Haiti</option>
						<option value="94">Heard and Mc Donald Islands</option>
						<option value="95">Honduras</option>
						<option value="96">Hong Kong</option>
						<option value="97">Hungary</option>
						<option value="98">Iceland</option>
						<option value="99">India</option>
						<option value="100">Indonesia</option>
						<option value="101">Iran (Islamic Republic of)</option>
						<option value="102">Iraq</option>
						<option value="103">Ireland</option>
						<option value="104">Israel</option>
						<option value="105">Italy</option>
						<option value="106">Jamaica</option>
						<option value="107">Japan</option>
						<option value="108">Jordan</option>
						<option value="109">Kazakhstan</option>
						<option value="110">Kenya</option>
						<option value="111">Kiribati</option>
						<option value="114">Kuwait</option>
						<option value="115">Kyrgyzstan</option>
						<option value="117">Latvia</option>
						<option value="118">Lebanon</option>
						<option value="119">Lesotho</option>
						<option value="120">Liberia</option>
						<option value="121">Libyan Arab Jamahiriya</option>
						<option value="122">Liechtenstein</option>
						<option value="123">Lithuania</option>
						<option value="124">Luxembourg</option>
						<option value="125">Macau</option>
						<option value="127">Madagascar</option>
						<option value="128">Malawi</option>
						<option value="129">Malaysia</option>
						<option value="130">Maldives</option>
						<option value="131">Mali</option>
						<option value="132">Malta</option>
						<option value="133">Marshall Islands</option>
						<option value="134">Martinique</option>
						<option value="135">Mauritania</option>
						<option value="136">Mauritius</option>
						<option value="137">Mayotte</option>
						<option value="138">Mexico</option>
						<option value="139">Micronesia</option>
						<option value="140">Moldova, Republic of</option>
						<option value="141">Monaco</option>
						<option value="142">Mongolia</option>
						<option value="143">Montserrat</option>
						<option value="144">Morocco</option>
						<option value="145">Mozambique</option>
						<option value="146">Myanmar</option>
						<option value="147">Namibia</option>
						<option value="148">Nauru</option>
						<option value="149">Nepal</option>
						<option value="150">Netherlands</option>
						<option value="151">Netherlands Antilles</option>
						<option value="152">New Caledonia</option>
						<option value="153">New Zealand</option>
						<option value="154">Nicaragua</option>
						<option value="155">Niger</option>
						<option value="156">Nigeria</option>
						<option value="157">Niue</option>
						<option value="158">Norfolk Island</option>
						<option value="159">Northern Mariana Islands</option>
						<option value="160">Norway</option>
						<option value="161">Oman</option>
						<option value="162">Pakistan</option>
						<option value="163">Palau</option>
						<option value="164">Panama</option>
						<option value="165">Papua New Guinea</option>
						<option value="166">Paraguay</option>
						<option value="167">Peru</option>
						<option value="168">Philippines</option>
						<option value="169">Pitcairn</option>
						<option value="170">Poland</option>
						<option value="171">Portugal</option>
						<option value="172">Puerto Rico</option>
						<option value="173">Qatar</option>
						<option value="174">Reunion</option>
						<option value="175">Romania</option>
						<option value="176">Russian Federation</option>
						<option value="177">Rwanda</option>
						<option value="178">Saint Kitts and Nevis</option>
						<option value="179">Saint Lucia</option>
						<option value="181">Samoa</option>
						<option value="182">San Marino</option>
						<option value="183">Sao Tome and Principe</option>
						<option value="184">Saudi Arabia</option>
						<option value="185">Senegal</option>
						<option value="186">Seychelles</option>
						<option value="187">Sierra Leone</option>
						<option value="188">Singapore</option>
						<option value="189">Slovakia (Slovak Republic)</option>
						<option value="190">Slovenia</option>
						<option value="191">Solomon Islands</option>
						<option value="192">Somalia</option>
						<option value="193">South Africa</option>
						<option value="195">Spain</option>
						<option value="196">Sri Lanka</option>
						<option value="197">St. Helena</option>
						<option value="198">St. Pierre and Miquelon</option>
						<option value="199">Sudan</option>
						<option value="200">Suriname</option>
						<option value="202">Swaziland</option>
						<option value="203">Sweden</option>
						<option value="204">Switzerland</option>
						<option value="205">Syrian Arab Republic</option>
						<option value="206">Taiwan</option>
						<option value="207">Tajikistan</option>
						<option value="209">Thailand</option>
						<option value="210">Togo</option>
						<option value="211">Tokelau</option>
						<option value="212">Tonga</option>
						<option value="213">Trinidad and Tobago</option>
						<option value="214">Tunisia</option>
						<option value="215">Turkey</option>
						<option value="216">Turkmenistan</option>
						<option value="217">Turks and Caicos Islands</option>
						<option value="218">Tuvalu</option>
						<option value="219">Uganda</option>
						<option value="220">Ukraine</option>
						<option value="221">United Arab Emirates</option>
						<option value="222">United Kingdom</option>
						<option value="223">United States</option>
						<option value="225">Uruguay</option>
						<option value="226">Uzbekistan</option>
						<option value="227">Vanuatu</option>
						<option value="228">Vatican City State (Holy See)</option>
						<option value="229">Venezuela</option>
						<option value="230">Viet Nam</option>
						<option value="231">Virgin Islands (British)</option>
						<option value="232">Virgin Islands (U.S.)</option>
						<option value="233">Wallis and Futuna Islands</option>
						<option value="234">Western Sahara</option>
						<option value="235">Yemen</option>
						<option value="236">Yugoslavia</option>
						<option value="237">Zaire</option>
						<option value="238">Zambia</option>
						<option value="239">Zimbabwe</option>
					</select>

					<select name="State" id="State" class="search_dropdown">
						<option selected="selected" value="0">--Select State--</option>
					</select>

					<select name="City" id="City" class="search_dropdown" >
						<option selected="selected" value="0">--Select City--</option>
					</select>

					<p style="float:left;"> <a href="#"> Advance Search </a> </p>

					<input type="hidden" name="searchType" value="quickSearch" />
					<input name="submitSearch" type="submit" value="Search" class="search_bttn" />
				</div>
			</form>
		</div><!--flash-->
		<div class="clear"> </div>
	</div><!--flash_container-->  
</div><!--outer_flash-->

<div class="home_cont_top">
	<div class="cont_top_left" style="float:left; padding-top:10px; padding-left:20px; width:250px;">
		<div class="scrollable1">
			<div class="items">
				<div class="ad">
					<h2>Job Seeker Benefits</h2>
					<ul>
						<li>Top Ranking Display</li>
						<li>See Contact Information</li>
						<li>Send Personalized Messages</li>
						<li>Perfect Matches</li>
						<li>Highlighted Display</li>
						<li>See Contact Information</li>
						<li>Perfect Matches</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div><!--cont_top_left-->

	<div class="blue_line"></div>

	<div class="cont_top_mid" style="float:left; width:500px;">
		<div class="cont_top_mid_top">
			<ul id="tabs">
				<li><a href="#skills" class="current">Jobs by Top Skills</a></li>
				<li><a href="#location">Jobs by Top States</a></li>
				<li><a href="#category">Jobs by Top Cities</a></li>
			</ul>
		</div><!--cont_top_mid_top-->

		<div class="tabContent" id="skills">
			<ul style="padding-left:30px; margin-top:10px;">
				<li><a href="#">AJAX (3)</a></li>
				<li><a href="#">CakePHP (2)</a></li>
			</ul>
			<ul style="padding-left:45px; margin-top:10px;">
				<li><a href="#">HTML (0)</a></li>
				<li><a href="#">Javascript (0)</a></li>
			</ul>
			<ul style="padding-left:45px; margin-top:10px;">
				<li><a href="#">Phpbb (0)</a></li>
				<li><a href="#">ROR (0)</a></li>
			</ul>
		</div>
		<div class="clear"></div>
	</div><!--cont_top_mid-->

	<div class="blue_line"></div>

	<div class="cont_top_right" style="float:left; margin-left:20px; margin-top:25px;">
		<div class="scrollable2">
			<div class="items">
				<div class="ad"><a href="#"><img src="employers/images/package_images/96478_ad-1.jpg" /></a></div>
				<div class="ad"><a href="#"><img src="employers/images/package_images/90417_ad-2.jpg" /></a></div>
			</div>
		</div>
	</div><!--cont_top_right-->
	<div class="clear"></div>
</div><!--home_cont_top-->

<div class="home_logo">
	<marquee onmouseover="stop();" onmouseout="start();" scrolldelay="40" WIDTH="96%">
		<div align="center" style="width:154px;float:left;margin-top:8px;margin-right:10px;border:1px solid #333333;">
			<a href="#">
				<img border="0" title="Xpert Web Services" alt="Xpert Web Services" src="http://www.phpdeveloperz.com/testmode/employers/images/profile_images/26924_dj_rocks_thumb.jpg" width="154" height="64" />
			</a>
		</div>
		<div align="center" style="width:154px;float:left;margin-top:8px;margin-right:10px;border:1px solid #333333;">
			<a href="#">
				<img border="0" title="user2" alt="user2" src="http://www.phpdeveloperz.com/testmode/employers/images/profile_images/userDefaultImage.jpg" width="154" height="64" />
			</a>
		</div>
	</marquee>
</div><!--home_logo-->

<div class="home_cont_bott" style="margin-top:10px;">
	<div class="cont_bott_left" style="float:left; width:365px;">
		<h2>Job Vacancies This Week</h2>
	</div><!--cont_bott_left-->        

	<div class="blue_line" style="height:250px;"></div>

	<div class="cont_bott_mid" style="float:left; width:365px; margin-left:10px;">
		<h2 style="padding-bottom:10px;">Featured Jobs</h2>
	</div><!--cont_bott_mid-->

	<div class="blue_line" style="height:250px;"></div>

	<div class="cont_bott_right" style="float:left; margin-left:15px;">
		<a href="#"><img src="img/home-bottom-ad.gif" width="235" height="233" /></a>
	</div><!--cont_bott_right-->

	<div class="clear"></div>
</div><!--home_cont_bott-->