<p>Dear <?php echo $name; ?>,</p>

<p>We welcome you to join our family at PHP Developerz DAV.</p>
<p>To be a part of our exclusive club, we request you to verify your email by clicking on the link below.</p>

<a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'verify'), true) . '/' . $hash; ?>"><?php echo $this->Html->url(array('controller' => 'users', 'action' => 'verify'), true) . '/' . $hash; ?></a>

<p>Best Regards,</p>

<p>PHP Developerz</p>
