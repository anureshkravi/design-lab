<?php
App::uses('AppController', 'Controller');
class RolesController extends AppController {
	public function setstatus($id = null, $status = null) {
		$this->layout = 'ajax';
		$this->Role->id = $id;
		if (!$this->Role->exists()) {
			throw new NotFoundException(__('Invalid Role'));
		}
		$this->Role->save(array('status_id' => $status) );
		echo ($status);
        exit;
	}

	public function admin_index() {
		$this->Role->recursive = 0;
		$this->set('roles', $this->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->Role->exists($id)) {
			throw new NotFoundException(__('Invalid role'));
		}
		$options = array('conditions' => array('Role.' . $this->Role->primaryKey => $id));
		$this->set('role', $this->Role->find('first', $options));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$data['Role'] = $this->request->data;
			$this->request->data = $data;
			$this->request->data['Role']['user_id'] = $this->Auth->user('id');
			$this->Role->create();
			if ($this->Role->save($this->request->data)) {
				$this->Session->setFlash(__('The role has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The role could not be saved. Please, try again.'));
			}
		}
		$users = $this->Role->User->find('list');
		$statuses = $this->Role->Status->find('list', array('conditions' => array('id' => array(1,2))));
		$this->set(compact('users', 'statuses'));
	}

	public function admin_edit($id = null) {
		if (!$this->Role->exists($id)) {
			throw new NotFoundException(__('Invalid role'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$data['Role'] = $this->request->data;
			$this->request->data = $data;
			$this->request->data['Role']['user_id'] = $this->Auth->user('id');
			if ($this->Role->save($this->request->data)) {
				$this->Session->setFlash(__('The role has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The role could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Role.' . $this->Role->primaryKey => $id));
			$this->request->data = $this->Role->find('first', $options);
		}
		$users = $this->Role->User->find('list');
		$statuses = $this->Role->Status->find('list', array('conditions' => array('id' => array(1,2))));
		$this->set(compact('users', 'statuses'));
	}

	public function admin_delete($id = null) {
		$this->Role->id = $id;
		if (!$this->Role->exists()) {
			throw new NotFoundException(__('Invalid role'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Role->delete()) {
			$this->Session->setFlash(__('Role deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Role was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
