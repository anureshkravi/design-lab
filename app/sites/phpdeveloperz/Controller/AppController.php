<?php
class AppController extends Controller {	
	public $components = array(
								'Session','Cookie',
								'Auth' => array(
									'authorize' => 'Controller',
									'authenticate' => array(
										'Form' => array(
											'fields' => array('username' => 'email'),
											'scope' => array('User.status_id' => 1, 'User.role_id' => array(1,2))
										)
									)
								)
						 );
	
	public function beforeFilter() {
		if( isset($this->params['admin']) && $this->params['admin'] ){
			$this->layout = 'default';
			$this->Auth->allow('admin_login');
		} else {
			$this->layout = 'frontend';
			$this->Auth->allow();
		}
	}

	public function isAuthorized( $user = null ) {
		if ( isset($user['role_id']) && $user['role_id'] == 1 ) {
			return true;
		} elseif( isset($user['role_id']) && $user['role_id'] != 1 ) {
			$currentPermission = $this->Session->read('permission');
			foreach($currentPermission as $permission) {
				if(((strtolower($permission['Privilege']['controller']) == $this->params['controller']) && (($permission[ 'Privilege']['action'] == $this->params['action']) || ($permission[ 'Privilege' ][ 'action'] == 'allactions')))) {
					return true;
				}
			}
		}
		return false;
    }
	
	public function beforeRender() {
		$this->response->disableCache();
	}

	public function sendMail( $vars = array(), $template, $to, $subject, $provider = 'smtp', $success = 'sent') {
		$semail = SITE_EMAIL;
		$email = new CakeEmail($provider);
		$email->viewVars($vars);
		$email->template($template, 'default')->emailFormat( 'html' )->to( $to )->from( $semail )->subject( $subject );
		if( !$email->send() ) $success = "Error sending mail";
		return $success;
	}
}
