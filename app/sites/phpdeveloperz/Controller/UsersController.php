<?php
App::uses('CakeEmail', 'Network/Email');
App::uses('AppController', 'Controller');
class UsersController extends AppController {

	public function login(){
	}

	public function form(){		
	}

	public function logout(){
	}

	public function forgot_password(){
	}

	public function change_password(){
	}

	public function register(){
		if ($this->request->is('post')){
			exit();
			$data['User'] = $this->request->data;
			$this->request->data = $data;
			$this->request->data['User']['status_id'] = 3; //not confirmed
			$this->request->data['User']['role_id'] = 3; //jobseeker
			$this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);
			$this->request->data['User']['unique_id'] = substr(number_format(time() * rand(),0,'',''),0,10);
			$hash = sha1( $this->request->data['User']['name'] . time() . rand(0, 885466348) );
			$this->request->data['User']['token_hash'] = $hash;
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$var = array('hash' => $hash, 'name' =>  $this->request->data['User']['name']);
				$this->sendMail( $var , 'verify_user', $this->request->data['User']['email'], 'Email verification For new Regestration' );
				$this->Session->setFlash(__('Thank you for registering with us. Please verify your email address.'));
				$this->redirect(array('action' => 'login'));
			} else {
				$this->Session->setFlash(__('The User could not be saved. Please, try again.'));
			}
		}
	}

	public function admin_login() {
		if( $this->Auth->loggedIn() ){
			$this->redirect(array('controller' => 'Dashboard', 'action' => 'index'));
		} else {
			if( $this->request->is('post') ) {
				if($this->Auth->login()) {
					//==setting last login time==//
        			$this->request->data['User'] = array('id' => $this->Auth->user('id'), 'last_login' => date("Y-m-d H:i:s"));
        			$this->User->save($this->request->data);
        			//==setting sessions for permissions==//
        			$this->loadModel('Privilege');
					$permission = $this->Privilege->find('all', array('fields' => array('controller', 'action'),'conditions' => array('Privilege.role_id' => $this->Auth->user('role_id'), 'Privilege.status_id' => 1 )));
            		if($permission) {
						$this->Session->write('permission' , $permission);
					}
					$this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
				} else {
					$this->Session->setFlash(__('email or password is incorrect'), 'default', array(), 'auth');
					$this->set('title_for_layout', 'Login');
					$this->layout = 'login';
				}
			} else {
				$this->set('title_for_layout', 'Login');
				$this->layout = 'login';
			}
		}
	}
	
	public function admin_logout() {
		$this->redirect($this->Auth->logout());
	}

	public function admin_index() {
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$data['User'] = $this->request->data;
			$this->request->data = $data;
			$this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);			
			$this->request->data['User']['created_by'] = $this->Auth->user('id');
			$this->request->data['User']['unique_id'] = substr(number_format(time() * rand(),0,'',''),0,10);
			$hash = sha1( $this->request->data['User']['name'] . time() . rand(0, 885466348) );
			$this->request->data['User']['token_hash'] = $hash;
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$var = array('hash' => $hash, 'name' =>  $this->request->data['User']['name']);
				$this->sendMail( $var , 'verify_user', $this->request->data['User']['email'], 'Email verification For new Regestration' );
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
		$roles = $this->User->Role->find('list', array('conditions' => array('status_id' => 1)));
		$statuses = $this->User->Status->find('list');
		$this->set(compact('roles', 'statuses'));
	}

	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$data['User'] = $this->request->data;
			$this->request->data = $data;
			$this->request->data['User']['created_by'] = $this->Auth->user('id');
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		$roles = $this->User->Role->find('list', array('conditions' => array('status_id' => 1)));
		$statuses = $this->User->Status->find('list');
		$this->set(compact('roles', 'statuses'));
	}

	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('User deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('User was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	public function setstatus($id = null, $status = null) {
		$this->layout = 'ajax';
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->User->save(array('status_id' => $status) );
		echo ($status);
        exit;
	}

	public function checkEmail($email=null){
		$this->layout = 'ajax';
		$user = $this->User->find('all', array('conditions' => array('email' => $email)));
		if( !empty($user) ){
			echo "Email id Already Exists";
			exit();
		} else {
			echo "Email id Available";
			exit();
		}
	}

	public function verify( $hash = null ) {
        $user = $this->User->find('first', array( 'fields' => array( 'id', 'status_id' ), 'conditions' => array( 'token_hash' => $hash ) ) );
        if( $user && $user['User']['status_id'] == 3) {
            $this->User->save( array( 'id' => $user['User']['id'], 'status_id' => 1 ) );
            $this->Session->setFlash(__('Thank you for confirming your email address!! Please login to continue.'));
			$this->redirect(array('controller' => 'users', 'action' => 'login'));
        } elseif( $user && $user['User']['status_id'] == 1 ){
        	$this->Session->setFlash(__('Your account is already active!!'));
			$this->redirect(array('controller' => 'users', 'action' => 'login'));
        } elseif ( $user && $user['User']['status_id'] == 2 ) {
        	$this->Session->setFlash(__('Your account is already active!! Please contact support staff for more information'));
			$this->redirect(array('controller' => 'users', 'action' => 'login'));
        } else {
        	$this->Session->setFlash(__('Invalid User'));
			$this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
    }
}
