<?php
App::uses('AppController', 'Controller');
class PrivilegesController extends AppController {

	public $restrictAction = array('add', 'edit', 'view', 'index', 'delete', 'listControllers', 'listactions', 'lists', 'setstatus');

	public function setstatus($id = null, $status = null) {
		$this->layout = 'ajax';
		$this->Privilege->id = $id;
		if (!$this->Privilege->exists()) {
			throw new NotFoundException(__('Invalid Privilege'));
		}
		$this->Privilege->save(array('status_id' => $status) );
		echo ($status);
        exit;
	}

	public function admin_index() {
		$this->Privilege->recursive = 0;
		$this->set('privileges', $this->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->Privilege->exists($id)) {
			throw new NotFoundException(__('Invalid privilege'));
		}
		$options = array('conditions' => array('Privilege.' . $this->Privilege->primaryKey => $id));
		$this->set('privilege', $this->Privilege->find('first', $options));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Privilege->create();
			if ($this->Privilege->save($this->request->data)) {
				$this->Session->setFlash(__('The privilege has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The privilege could not be saved. Please, try again.'));
			}
		}
		$this->lists();
	}

	public function admin_edit($id = null) {
		if (!$this->Privilege->exists($id)) {
			throw new NotFoundException(__('Invalid privilege'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Privilege->save($this->request->data)) {
				$this->Session->setFlash(__('The privilege has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The privilege could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Privilege.' . $this->Privilege->primaryKey => $id));
			$this->request->data = $this->Privilege->find('first', $options);
		}
		$this->lists();
	}

	public function admin_delete($id = null) {
		$this->Privilege->id = $id;
		if (!$this->Privilege->exists()) {
			throw new NotFoundException(__('Invalid privilege'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Privilege->delete()) {
			$this->Session->setFlash(__('Privilege deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Privilege was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	public function listControllers() {
		$controllerClasses = App::objects('controller');
		$controllerClasses = array_diff( $controllerClasses, array( 'AppController', 'PagesController') );
		$controllerClasses = array_combine( $controllerClasses, $controllerClasses );
		foreach($controllerClasses as $module) {
			$module = str_replace( 'Controller', '', $module );
		  	$controllers[strtolower($module)] = $module;
		}
		return $controllers;
	}

	public function listactions($controller=null) {
        $this->layout = 'ajax';
        $controller = ucfirst($controller);
		App::import('Controller', $controller);
		$methods = array_diff(get_class_methods($controller.'Controller'), get_class_methods('AppController'));
		foreach( $methods as $method ) {
            $dropdown_action[strtolower($method)] = $method;
        }
        $dropdown_action = array_diff($dropdown_action, $this->restrictAction); 
        $dropdown_action = array_merge( $dropdown_action, array('allactions' => 'allow All Actions' ));
        $this->set('methods', $dropdown_action);
    }
	
	public function lists()
	{
		$controllers = $this->listControllers();
		if($this->params->action == 'admin_edit') {
			$controller = $this->request->data['Privilege']['controller'];
			App::import('Controller', $controller);
			$methods = array_diff(get_class_methods($controller.'Controller'), get_class_methods('AppController'));				
			foreach($methods as $method ) {
				$dropdown_action[strtolower($method)] = $method;
			}
			$dropdown_action = array_diff($dropdown_action, $this->restrictAction);
			$dropdown_action = array_merge( $dropdown_action, array('allactions' => 'allow All Actions' ));
			$this->set('dropdown_action', $dropdown_action);
		}
		$roles = $this->Privilege->Role->find('list', array('conditions' => array('status_id' => 1)));
		$statuses = $this->Privilege->Status->find('list', array('conditions' => array('id' => array(1,2))));
		$this->set(compact('roles', 'statuses', 'controllers'));
	}
}
