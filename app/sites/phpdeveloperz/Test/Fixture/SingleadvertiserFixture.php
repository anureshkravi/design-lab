<?php
/**
 * SingleadvertiserFixture
 *
 */
class SingleadvertiserFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'singleadvertiser';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'adID' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'adName' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'adType' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 64, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'advertiserGroup' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 64, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'advertiserID' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'advertiserName' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 128, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'adStatus' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 64, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'campaignID' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'campaignName' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'campaignStartDate' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'campaignEndDate' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'contentCategory' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 128, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'creativeGroup1' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 64, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'creativeGroup2' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 64, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'creativeID' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'creativeName' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'videoLength' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 64, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'creativeVersion' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 4),
		'clickThroughURL' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 2048, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'commercialID' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'creativeSize' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 64, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'creativeType' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 64, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'timePeriod' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'keyword' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 64, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'placementGroup' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'placementGroupID' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10),
		'placementGroupStrategy' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 128, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'placementName' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'placementTotalBookedUnits' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 64, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'placementRate' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '10,3'),
		'placementSize' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 64, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'paymentSource' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 64, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'placement_ID' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10),
		'placementStrategy' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 64, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'rateType' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 64, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'renderingID' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10),
		'siteID' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'siteName' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'sitePlacementKeyname' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 64, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'siteDirectoryName' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 64, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'siteDirectoryID' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10),
		'clickRate' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '10,4'),
		'clicks' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10),
		'codeServes' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10),
		'effectiveCPM' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '10,3'),
		'impressions' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10),
		'indexes' => array(
			'PRIMARY' => array('column' => array('adID', 'advertiserID', 'campaignID', 'creativeID', 'timePeriod', 'siteID'), 'unique' => 1),
			'TimePeriod' => array('column' => 'timePeriod', 'unique' => 0),
			'CampaignIDandPlacementID' => array('column' => array('campaignID', 'placement_ID'), 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'adID' => 1,
			'adName' => 'Lorem ipsum dolor sit amet',
			'adType' => 'Lorem ipsum dolor sit amet',
			'advertiserGroup' => 'Lorem ipsum dolor sit amet',
			'advertiserID' => 1,
			'advertiserName' => 'Lorem ipsum dolor sit amet',
			'adStatus' => 'Lorem ipsum dolor sit amet',
			'campaignID' => 1,
			'campaignName' => 'Lorem ipsum dolor sit amet',
			'campaignStartDate' => 'Lorem ip',
			'campaignEndDate' => 'Lorem ip',
			'contentCategory' => 'Lorem ipsum dolor sit amet',
			'creativeGroup1' => 'Lorem ipsum dolor sit amet',
			'creativeGroup2' => 'Lorem ipsum dolor sit amet',
			'creativeID' => 1,
			'creativeName' => 'Lorem ipsum dolor sit amet',
			'videoLength' => 'Lorem ipsum dolor sit amet',
			'creativeVersion' => 1,
			'clickThroughURL' => 'Lorem ipsum dolor sit amet',
			'commercialID' => 'Lorem ipsum dolor sit amet',
			'creativeSize' => 'Lorem ipsum dolor sit amet',
			'creativeType' => 'Lorem ipsum dolor sit amet',
			'timePeriod' => 'Lorem ip',
			'keyword' => 'Lorem ipsum dolor sit amet',
			'placementGroup' => 'Lorem ipsum dolor sit amet',
			'placementGroupID' => 1,
			'placementGroupStrategy' => 'Lorem ipsum dolor sit amet',
			'placementName' => 'Lorem ipsum dolor sit amet',
			'placementTotalBookedUnits' => 'Lorem ipsum dolor sit amet',
			'placementRate' => 1,
			'placementSize' => 'Lorem ipsum dolor sit amet',
			'paymentSource' => 'Lorem ipsum dolor sit amet',
			'placement_ID' => 1,
			'placementStrategy' => 'Lorem ipsum dolor sit amet',
			'rateType' => 'Lorem ipsum dolor sit amet',
			'renderingID' => 1,
			'siteID' => 1,
			'siteName' => 'Lorem ipsum dolor sit amet',
			'sitePlacementKeyname' => 'Lorem ipsum dolor sit amet',
			'siteDirectoryName' => 'Lorem ipsum dolor sit amet',
			'siteDirectoryID' => 1,
			'clickRate' => 1,
			'clicks' => 1,
			'codeServes' => 1,
			'effectiveCPM' => 1,
			'impressions' => 1
		),
	);

}
