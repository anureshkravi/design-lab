<?php
App::uses('Singleadvertiser', 'Model');

/**
 * Singleadvertiser Test Case
 *
 */
class SingleadvertiserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.singleadvertiser'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Singleadvertiser = ClassRegistry::init('Singleadvertiser');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Singleadvertiser);

		parent::tearDown();
	}

}
