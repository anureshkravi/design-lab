<?php
App::uses('Penalty', 'Model');

/**
 * Penalty Test Case
 *
 */
class PenaltyTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.penalty',
		'app.user',
		'app.role',
		'app.status',
		'app.privilege'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Penalty = ClassRegistry::init('Penalty');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Penalty);

		parent::tearDown();
	}

}
