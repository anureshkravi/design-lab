<?php
App::uses('Makegoodsingleadvertiser', 'Model');

/**
 * Makegoodsingleadvertiser Test Case
 *
 */
class MakegoodsingleadvertiserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.makegoodsingleadvertiser'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Makegoodsingleadvertiser = ClassRegistry::init('Makegoodsingleadvertiser');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Makegoodsingleadvertiser);

		parent::tearDown();
	}

}
