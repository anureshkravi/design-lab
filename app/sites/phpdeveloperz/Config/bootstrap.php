<?php
Cache::config('default', array('engine' => 'File'));
Configure::write('Dispatcher.filters', array(
	'AssetDispatcher',
	'CacheDispatcher'
));
App::uses('CakeLog', 'Log');
CakeLog::config('debug', array(
	'engine' => 'FileLog',
	'types' => array('notice', 'info', 'debug'),
	'file' => 'debug',
));
CakeLog::config('error', array(
	'engine' => 'FileLog',
	'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
	'file' => 'error',
));

//load Plugins
CakePlugin::load('Mongodb');

//APP Variables
Configure::write('ignoreAudit', array('Audit'));
Configure::write('ignoreAuditControllers', array( 'AppController', 'PagesController', 'AuditsController' ));

//site Constants
define( 'SITE_NAME', 'PHP Developerz');
define( 'SITE_EMAIL', 'dav.phpdeveloperz@gmail.com');
