<?php

App::uses('CakeEmail', 'Network/Email');

class UserVerificationShell extends AppShell {
   public $uses = array( 'User' );

    public function main() {
        $communications = $this->Communication->find('all', array('fields' => array( 'id', 'object_type', 'object_id', 'subject', 'message', 'mode' ), 'conditions' => array( 'is_active' => 1, 'status' => 0 ) ) );
        $userId = array();
        $agentId = array();
        foreach( $communications as $communication ) {
            if( $communication['Communication']['object_type'] == 'agent' && $communication['Communication']['mode'] == 'email' ) $agentId[] = $communication['Communication']['object_id'];
            if( $communication['Communication']['object_type'] == 'user' && $communication['Communication']['mode'] == 'email' ) $userId[] = $communication['Communication']['object_id'];
        }
        $email = new CakeEmail('smtp');
        if( !empty( $userId ) ) {
            $users = $this->Communication->find( 'all', array('fields' => array( 'Communication.id', 'Communication.subject', 'Communication.message', 'User.email' ), 'conditions' => array( 'User.id' => $userId, 'Communication.is_active' => 1, 'Communication.status' => 0 ), 'joins' => array( array( 'table' => 'users', 'alias' => 'User', 'type' => 'LEFT', 'conditions' => array( 'Communication.object_id = User.id' ) ) ) ) );
            foreach( $users as $user ) {
                try {
                    $email->viewVars( array( 'message' => str_replace( '\n', ' ', $user['Communication']['message'] ) ) );
                    $email->template('communication')->emailFormat('html')->bcc( $user['User']['email'] )->from('dms@bestylish.com')->subject( $user['Communication']['subject'] );
                    if( $email->send() ) {
                        $this->Communication->create();
                        $this->Communication->save( array( 'id' => $user['Communication']['id'], 'status' => 1 ) );      
                    }
                } catch(Exception $e) {
                    $this->_throwError($e->getMessage());
				}
            }
        }
        if( !empty( $agentId ) ) {
            $dealers = $this->Communication->find( 'all', array('fields' => array( 'Communication.id', 'Communication.subject', 'Communication.message', 'Dealer.email' ), 'conditions' => array( 'Dealer.id' => $agentId, 'Communication.is_active' => 1, 'Communication.status' => 0 ), 'joins' => array( array( 'table' => 'dealers', 'alias' => 'Dealer', 'type' => 'LEFT', 'conditions' => array( 'Communication.object_id = Dealer.id' ) ) ) ) );
            foreach( $dealers as $dealer ) {
                try {
                    $email->viewVars( array( 'message' => str_replace( '\n', ' ', $dealer['Communication']['message'] ) ) );
                    $email->template('communication')->emailFormat('html')->bcc( $dealer['Dealer']['email'] )->from('dms@bestylish.com')->subject( $dealer['Communication']['subject'] );
                    if( $email->send() ) {
                        $this->Communication->create();
                        $this->Communication->save( array( 'id' => $dealer['Communication']['id'], 'status' => 1 ) );      
                    }
                } catch(Exception $e) {
                    $this->_throwError($e->getMessage());
                }
            }
        }
        $this->out( "Task Completed" );
    }
    
    protected function _throwError($msg) {
        echo json_encode(array(
                'error' => true,
                'message' => $msg
        ));
    }
}
