<?php echo $this->Html->script(array('jquery-3.1.1.min', 'jquery-ui.min', 'bootstrap-multiselect.js')); ?>

<link rel="stylesheet" href="/css/jquery-ui.css">
<link rel="stylesheet" href="/css/bootstrap.min.css">

<style>


</style>

<script type="text/javascript">

    var MULTI_SELECTBOX_POPULATE_TIMEOUT = 10;

    $(document).ready(function()
    {        
        //for li sortable
        <?php if($this->Html->user('role_id') != 3) { ?>
            $(function() {
                $("#contentLeft ul").sortable({ opacity: 1, cursor: 'move', update: function() {
                    var order = $(this).sortable("serialize") + '&action=updateRecordsListings';
                    $.post("<?php echo $this->Html->url(array('controller' => 'projectfiles', 'action' => 'recordListing')); ?>", order, function(theResponse){
                        $("#contentRight").html(theResponse);
                    });                                                         
                }                                 
                });
            });
        <?php } ?>
        //scroll functionality starts from here
        var track_load = 1; //total loaded record group(s)
        var loading  = false; //to prevents multipal ajax loads
        var total_groups = <?php echo ceil(count($totalGroups)/20); ?>; //total record group(s)
        $(window).scroll(function() { 
		//detect page scroll
		        var tags = [];                
                tags = ($('#tagsCont').val()).slice();
                //console.log(">> ysi win scroll   : "+tags);    
				// $("input[name='tagcb[]']:checked").each(function ()
				// {
				// 	tags.push($(this).val());                    
				// });
               

            if($(window).scrollTop() + $(window).height() == $(document).height() && $('#searchFilter').val() == '' && tags.length==0) { //user scrolled to bottom of the page?            
                if(track_load < total_groups && loading==false) {
                    loading = true; //prevent further ajax loading
                    $('.animation_image').show(); //show loading image                
                    //load data from the server using a HTTP POST request
                    jQuery.ajax({
                        type: "GET",
                        url: '/admin/projectfiles/scroll/'+track_load,
                        success: function(data) {
                            var records = JSON.parse(data);
                            var output = '';
                            for(i=0; i<records.length; i++) {
                                output += '<li id="recordsArray_'+records[i]['ProjectFile']['id']+'">' +
                                            '<div class="col-xs-6">' +
                                                '<a href="/admin/projectfiles/'+records[i]['ProjectFile']['project_folder']+'" class="ThumBtn" id="'+records[i]['ProjectFile']['title']+'">';
                                                    if(records[i]['ProjectFile']['thumbnail'] !== '') {
                                                        output += '<img src="/files/groupthumb/'+records[i]['ProjectFile']['thumbnail']+'" alt="">';
                                                    } else {
                                                        output += '<img src="/img/thumb.jpg" alt="">';
                                                    }
                                output  += '</a><div class="pull-left projectNameContainer">' +
                                                '<a href="/admin/projectfiles/'+records[i]['ProjectFile']['project_folder']+'">'+records[i]['ProjectFile']['title']+'</a>';

                                                <?php if($this->Html->user('role_id') == 1 || $this->Html->user('role_id') == 2) { ?>
                                                    output  += '<p><a href="#" class="addThumBtn" id="'+records[i]['ProjectFile']['title']+'" data-toggle="modal" data-target="#myModal">Add Thumbnail</a></p>';
                                                <?php } ?>
                                output  += '</div></div>' +
                                            '<div class="col-xs-4">' +
                                                '<p>'+records[i]['ProjectFile']['last_project_added_date']+'</p>' +
                                            '</div>';

                                            <?php if($this->Html->user('role_id') == 1 || $this->Html->user('role_id') == 2) { ?>
                                            output += '<div class="col-xs-2">' +
                                                '<a href="/admin/projectfiles/add/project:'+records[i]['ProjectFile']['project_folder']+'" data-toggle="tooltip" title="edit" class="lbl"><img src="/img/edit.png" alt=""> Edit</a>' +                       
                                            
                                                '<a onclick="if (confirm(&quot;Are you sure you want to delete?&quot;)) { return true; } return false;" href="projectfiles/deleteall/'+records[i]['ProjectFile']['project_folder']+'" data-toggle="tooltip" title="delete" class="lbl"><img src="/img/delete.png" alt=""> Delete</a>' +
                                            '</div>';
                                            <?php } ?>
                                        '</li>';
                            }
                            $(".ui-sortable").append(output); //append received data into the element
                            //hide loading image
                            $('.animation_image').hide(); //hide loading image once data is received                    
                            track_load++; //loaded group increment
                            loading = false;
                        }
                    });
                }
            }
			else if($(window).scrollTop() + $(window).height() == $(document).height() && $('#searchFilter').val() == '' && tags.length!=0)
			{
				
                    jQuery.ajax({
                        type: "GET",
                        url: '/admin/projectfiles/tags/'+tags+'/'+track_load,
                        success: function(data) {
							var records = JSON.parse(data);
							//console.log(records)
							if(records != 'No Data')
							{
								loading = true; //prevent further ajax loading
								$('.animation_image').show(); //show loading image 	
								
						
					loading = true; //prevent further ajax loading
                    $('.animation_image').show(); //show loading image 							
                            var output = '';
                            for(i=0; i<records.length; i++) {
                                output += '<li id="recordsArray_'+records[i]['ProjectFile']['id']+'">' +
                                            '<div class="col-xs-6">' +
                                                '<a href="/admin/projectfiles/'+records[i]['ProjectFile']['project_folder']+'" class="ThumBtn" id="'+records[i]['ProjectFile']['title']+'">';
                                                    if(records[i]['ProjectFile']['thumbnail'] !== '') {
                                                        output += '<img src="/files/groupthumb/'+records[i]['ProjectFile']['thumbnail']+'" alt="">';
                                                    } else {
                                                        output += '<img src="/img/thumb.jpg" alt="">';
                                                    }
                                output  += '</a><div class="pull-left projectNameContainer">' +
                                                '<a href="/admin/projectfiles/'+records[i]['ProjectFile']['project_folder']+'">'+records[i]['ProjectFile']['title']+'</a>';

                                                <?php if($this->Html->user('role_id') == 1 || $this->Html->user('role_id') == 2) { ?>
                                                    output  += '<p><a href="#" class="addThumBtn" id="'+records[i]['ProjectFile']['title']+'" data-toggle="modal" data-target="#myModal">Add Thumbnail</a></p>';
                                                <?php } ?>
                                output  += '</div></div>' +
                                            '<div class="col-xs-4">' +
                                                '<p>'+records[i]['ProjectFile']['last_project_added_date']+'</p>' +
                                            '</div>';

                                            <?php if($this->Html->user('role_id') == 1 || $this->Html->user('role_id') == 2) { ?>
                                            output += '<div class="col-xs-2">' +
                                                '<a href="/admin/projectfiles/add/project:'+records[i]['ProjectFile']['project_folder']+'" data-toggle="tooltip" title="edit" class="lbl"><img src="/img/edit.png" alt=""> Edit</a>' +                       
                                            
                                                '<a onclick="if (confirm(&quot;Are you sure you want to delete?&quot;)) { return true; } return false;" href="projectfiles/deleteall/'+records[i]['ProjectFile']['project_folder']+'" data-toggle="tooltip" title="delete" class="lbl"><img src="/img/delete.png" alt=""> Delete</a>' +
                                            '</div>';
                                            <?php } ?>
                                        '</li>';
                            }
                            $(".ui-sortable").append(output); //append received data into the element
                            //hide loading image
                            $('.animation_image').hide(); //hide loading image once data is received                    
                            track_load++; //loaded group increment
                            loading = false;
                        }
						}
                    });
                	
			}
        });
        
        //for search filter on dashboard page
        var storeData = $(".ui-sortable").html();
        $('#searchFilter').keyup(function(){
            var valThis = $(this).val().toLowerCase();            
            if(valThis !== '') {
                $.ajax({
                    type: "GET",
                    url: '/admin/projectfiles/search/'+valThis,
                    success: function(data) {
                        var records = JSON.parse(data);
                        var output = '';
						//console.log(records);
                        for(i=0; i<records.length; i++) {
                            output += '<li id="recordsArray_'+records[i]['ProjectFile']['id']+'">' +
                                        '<div class="col-xs-6">' +
                                            '<a href="/admin/projectfiles/'+records[i]['ProjectFile']['project_folder']+'" class="ThumBtn" id="'+records[i]['ProjectFile']['title']+'">';
                                                if(records[i]['ProjectFile']['thumbnail'] !== '') {
                                                    output += '<img src="/files/groupthumb/'+records[i]['ProjectFile']['thumbnail']+'" alt="">';
                                                } else {
                                                    output += '<img src="/img/thumb.jpg" alt="">';
                                                }
												
                            output  += '</a><div class="pull-left projectNameContainer">' +
                                            '<a href="/admin/projectfiles/'+records[i]['ProjectFile']['project_folder']+'">'+records[i]['ProjectFile']['title']+'</a>';
                                            <?php if($this->Html->user('role_id') == 1 || $this->Html->user('role_id') == 2) { ?>
                                                output  += '<p><a href="#" class="addThumBtn" id="'+records[i]['ProjectFile']['title']+'" data-toggle="modal" data-target="#myModal">Add Thumbnail</a></p>';
                                            <?php } ?>
                            output  += '</div></div>' +
                                        '<div class="col-xs-4">' +
                                            '<p>'+records[i]['ProjectFile']['last_project_added_date']+'</p>' +
                                        '</div>';

                                        <?php if($this->Html->user('role_id') == 1 || $this->Html->user('role_id') == 2) { ?>
                                        output += '<div class="col-xs-2">' +
                                            '<a href="/admin/projectfiles/add/project:'+records[i]['ProjectFile']['project_folder']+'" data-toggle="tooltip" title="edit" class="lbl"><img src="/img/edit.png" alt=""> Edit</a>' +                       
                                        
                                            '<a onclick="if (confirm(&quot;Are you sure you want to delete?&quot;)) { return true; } return false;" href="projectfiles/deleteall/'+records[i]['ProjectFile']['project_folder']+'" data-toggle="tooltip" title="delete" class="lbl"><img src="/img/delete.png" alt=""> Delete</a>' +
                                        '</div>';
                                        <?php } ?>
                                    '</li>';
                        }
                        $(".ui-sortable").html(output);
                    }
                });
            } else {
                $(".ui-sortable").html(storeData);
                track_load = 1; //reseting track load variable for scroll functionality above
            }
        });
		
		//Data filtering on tag basis
		var storeData = $(".ui-sortable").html();

        setTimeout(function()
        {

            $('.sort_rang, .multiselect-item').change(function(){ 
    			//console.log("HELLO");
    			track_load = 1;
                //var valThis = $(this).val().toLowerCase();
                var valThis = $('#tagsCont').val();            
                valThis = valThis.toString();

    			//console.log(">> YSI : ",valThis);
                
                if(valThis !== '') {
    				//var val = $("#search_form").serialize();
    				var tags = [];
                    
    				// $("input[name='tagcb[]']:checked").each(function ()
    				// {
    				// 	tags.push($(this).val());                    
    				// });
                    tags = ($('#tagsCont').val()).slice();
                    var encodeTagURI = encodeURIComponent(tags);   
    				//console.log(checked);
    				if(tags.length == 0)
    				{
    					window.location = '/admin';
    					exit;
    				}
    				
                    $.ajax({
                        type: "GET",
                        url: '/admin/projectfiles/tags/'+encodeTagURI,
    					//data: $("#search_form").serialize(),
                        success: function(data) {
                            //console.log(data);
    						var records = JSON.parse(data);
                            var output = '';
    						//console.log(data);
                            for(i=0; i<records.length; i++) {
                                output += '<li id="recordsArray_'+records[i]['ProjectFile']['id']+'">' +
                                            '<div class="col-xs-6">' +
                                                '<a href="/admin/projectfiles/'+records[i]['ProjectFile']['project_folder']+'" class="ThumBtn" id="'+records[i]['ProjectFile']['title']+'">';
                                                    if(records[i]['ProjectFile']['thumbnail'] !== '') {
                                                        output += '<img src="/files/groupthumb/'+records[i]['ProjectFile']['thumbnail']+'" alt="">';
                                                    } else {
                                                        output += '<img src="/img/thumb.jpg" alt="">';
                                                    }
    												
                                output  += '</a><div class="pull-left projectNameContainer">' +
                                                '<a href="/admin/projectfiles/'+records[i]['ProjectFile']['project_folder']+'">'+records[i]['ProjectFile']['title']+'</a>';
                                                <?php if($this->Html->user('role_id') == 1 || $this->Html->user('role_id') == 2) { ?>
                                                    output  += '<p><a href="#" class="addThumBtn" id="'+records[i]['ProjectFile']['title']+'" data-toggle="modal" data-target="#myModal">Add Thumbnail</a></p>';
                                                <?php } ?>
                                output  += '</div></div>' +
                                            '<div class="col-xs-4">' +
                                                '<p>'+records[i]['ProjectFile']['last_project_added_date']+'</p>' +
                                            '</div>';

                                            <?php if($this->Html->user('role_id') == 1 || $this->Html->user('role_id') == 2) { ?>
                                            output += '<div class="col-xs-2">' +
                                                '<a href="/admin/projectfiles/add/project:'+records[i]['ProjectFile']['project_folder']+'" data-toggle="tooltip" title="edit" class="lbl"><img src="/img/edit.png" alt=""> Edit</a>' +                       
                                            
                                                '<a onclick="if (confirm(&quot;Are you sure you want to delete?&quot;)) { return true; } return false;" href="projectfiles/deleteall/'+records[i]['ProjectFile']['project_folder']+'" data-toggle="tooltip" title="delete" class="lbl"><img src="/img/delete.png" alt=""> Delete</a>' +
                                            '</div>';
                                            <?php } ?>
                                        '</li>';
                            }
                            $(".ui-sortable").html(output);
                        }
                    });
                } else {
					/* ______________________________________
					Added for new design | when no category selected then load all data from database 
					| because after sorting , data order has been changed.
					*/
					
					window.location = '/admin';
    				exit;
					/*__________END ________*/
					
                    $(".ui-sortable").html(storeData);
                    track_load = 1; //reseting track load variable for scroll functionality above
                }
            });  // End brace of sort range change function

            $(".text-info").show();
            $("#selectTagsCont").show();
            $("#clear-filter").show();

        }, MULTI_SELECTBOX_POPULATE_TIMEOUT);


    });
</script>

<div id="main_wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="addproject">
                <div class="col-lg-12">
			<div class="search-section">
			                    <input type="text" placeholder="Search Project" id="searchFilter" autofocus>
			</div>
                    <?php if($this->Html->user('id') != '' && $this->Html->user('role_id') != 3 ){ ?>
                    	<a href="<?php echo $this->Html->url(array('controller' => 'projectfiles', 'action' => 'add')); ?>" class="btn btn-primary pull-right">
                        	<span class="glyphicon glyphicon-plus pull-left"></span><span class="pull-left mobile">New Project<span>
                        </a>
                    <?php } ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="row">
            <div class="projectlist">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <div class="resp">
								<div>								
                                    <div class="col-xs-12 filtercont">
    									<form id="search_form">
        									<div class="well">
            									<h4 class="text-info" style="display:none">Filter by Categories: </h4>
                                                <div id="selectTagsCont" style="display:none">
                                                    <select id="tagsCont" multiple="multiple" name="tagcb[]">
                                                         <?php 
                                                            foreach($tags as $tag)
                                                            {
                                                                $tagValue = $tag['tags']['title'];
                                                             //echo "<input value=".$tag['tags']['title']." class='sort_rang' name=tag[] type='checkbox'>".$tag['tags']['title']."  ";

                                                             echo '<option value="'.$tagValue.'" class="sort_rang">'.$tagValue.'</option>';
                                                            }
                                                            ?> 
                                                    </select> 
                                                </div>
                                                <span class="clear-selectedtags"><a id="clear-filter" style="display:none" href="#">Clear Filter</a></span>
                                              <!--   <script type="text/javascript">
                                                    populateTags();
                                                </script> -->
            									<!-- <?php 
                									// foreach($tags as $tag)
                									// {
                									// 	echo "<input value=".$tag['tags']['title']." class='sort_rang' name=tag[] type='checkbox'>".$tag['tags']['title']."  ";
                									// }
            									?> -->
        									</div>
    									</form>
									</div>                                
								</div>
                            <ul class="heading">
								
                                <li>
                                    <div class="col-xs-6">Project Name</div>
                                    <div class="col-xs-4">Last Edit Date</div>
                                    <?php if($this->Html->user('role_id') != 3) { ?>
                                        <div class="col-xs-2">Actions</div>
                                    <?php } ?>
                                    <div class="clearfix"></div>
                                </li>
                                <div class="clearfix"></div>
                            </ul>

                            <div id="contentLeft">
                                <ul class="ui-sortable">
                                    <?php foreach($projectFiles as $project): ?>
                                        <li id="<?php echo 'recordsArray_'.$project['ProjectFile']['id']; ?>">
                                            <div class="col-xs-6">
                                                <?php
                                                    if($project['ProjectFile']['thumbnail'] != '') {
                                                        echo '<a href="/admin/projectfiles/'.$project['ProjectFile']['project_folder'].'" class="ThumBtn">'.$this->Html->image('/files/groupthumb/'.$project['ProjectFile']['thumbnail']).'</a>';
                                                    } else {
                                                        echo '<a href="/admin/projectfiles/"'.$project['ProjectFile']['project_folder'].' class="ThumBtn">'.$this->Html->image('thumb.jpg').'</a>';
                                                    }
                                                    echo "<div class='pull-left projectNameContainer'>";
                                                        echo $this->Html->link($project['ProjectFile']['title'], array('controller' => 'projectfiles', 'action' => 'view', 'projectfolder' => $project['ProjectFile']['project_folder']));

                                                        if($this->Html->user('role_id') == 1 || $this->Html->user('role_id') == 2) {
                                                            echo '<p><a href="#" class="addThumBtn" id="'.$project['ProjectFile']['title'].'" data-toggle="modal" data-target="#myModal">Add Thumbnail</a></p>';
                                                        }
                                                    echo '</div>';
                                                ?>
                                            </div>
                                            <div class="col-xs-4">
                                                <p><?php 
													//echo $this->Html->beautifulDate($project['ProjectFile']['modified']); 
													echo $this->Html->beautifulDate($project['ProjectFile']['last_project_added_date']); 
													?>
												</p>
                                            </div>
                                            <?php if($this->Html->user('role_id') != 3) { ?>
                                                <div class="col-xs-2">
                                                    <a href="<?php echo $this->Html->url(array('controller' => 'projectfiles', 'action' => 'add', 'projectname' => $project['ProjectFile']['project_folder'])); ?>" title="edit" class="lbl"><?php echo $this->Html->image('edit-project.png'); ?> <span class="action-link">Edit</span> </a>
                                                    
                                                    <!-- <?php if($project['ProjectFile']['status_id'] == 1){ ?>
                                                        <a href="#" class="jquery_action_status" id = 'action_<?php echo $project['ProjectFile']['project_folder']; ?>_projectfiles' rel="2" data-toggle="tooltip" title="unpublish"><?php echo $this->Html->image('publish.png'); ?></a>
                                                    <?php } else { ?>
                                                        <a href="#" class="jquery_action_status" id = 'action_<?php echo $project['ProjectFile']['project_folder']; ?>_projectfiles' rel="1" data-toggle="tooltip" title="publish"><?php echo $this->Html->image('unpublish.png'); ?></a>
                                                    <?php } ?> -->                                   
                                                
                                                    <a onclick="if (confirm(&quot;Are you sure you want to delete?&quot;)) { return true; } return false;" href="projectfiles/deleteall/<?php echo $project['ProjectFile']['project_folder']; ?>"  title="delete" class="lbl"><?php echo $this->Html->image('delete-project.png'); ?><span class="action-link">Delete</span> </a>
                                                </div>
                                            <?php } ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                                <div class="animation_image" style="display:none" align="center">
                                    <?php echo $this->Html->image('ajax-loader.gif'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Thumbnail Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Thumbnail</h4>
      </div>
      <form id="projectThumb" action="thumbnails/add" method="post" enctype="multipart/form-data">
            <div class="modal-body">
          		<fieldset>
                	<input type="file" id="ThumbnailFilename" maxlength="250" name="data[Thumbnail][filename]" required="required">
                </fieldset>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">

    var tags_str = "", testArr = [];

    $(document).ready(function() {
        $('.projectlist a').tooltip();
        $("#tagsCont").multiselect({
            includeSelectAllOption: false,
            nonSelectedText:'Select Categories'        
        }); 
        $("#clear-filter").on('click', function() {
           $('#tagsCont').multiselect('clearSelection');
           $('.sort_rang, .multiselect-item').trigger('change');
        });

        $( "#selectTagsCont" ).mouseover(function() {
            $( ".btn-group" ).addClass("open");
            $( ".multiselect.dropdown-toggle" ).attr( "aria-expanded","true");
        });

        $( "#selectTagsCont" ).mouseout(function() {
            $( ".btn-group" ).removeClass("open");
            $( ".multiselect.dropdown-toggle" ).attr( "aria-expanded","false");
        });        
    }); 

</script>