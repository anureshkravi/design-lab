
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="../../css/awesomplete.css" />
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css">

<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="../../js/awesomplete.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>


<style>
    .ui-autocomplete { 
        cursor:pointer; 
        height:120px; 
        overflow-y:scroll;
    }    
    #tags{
        width:44.8%;  
        font-size: 18px; 
        line-height: 1.42857143;
        color: #555;
        font-family: roboto;         
    }
    .ui-menu-item{
        margin: 0 !important;
        border:1px solid #fff;
        font-size: 14px; 
        line-height: 1.42857143;
        color: #555;
        font-family: roboto; 
    }
    .ui-menu-item.ui-state-focus {            
        margin: 0 !important;
        font-size: 14px; 
        line-height: 1.42857143;        
        font-family: roboto;
    }
    #multiple-values label {
        float: left;
        width: 100%;
    }
    #tagsInput{
        width:100%;
    }
</style>
<script>
    
    var availableTags, objRef;
    var aTagArr,sArr;

    $(function() {

        objRef = this;

        availableTags = [<?php echo $finalProjects; ?>];

        //console.log(">>> availableTags _________ : "+availableTags);
		/*___ Added new design || to populate tag on the basis of project selection
			I have added the code in this function just compare || check ajax call
		*/
        $( "#ProjectFileTitle" ).autocomplete({
            source: availableTags,
            minLength: 2,
			select: function( event , ui ) {
				var projectName = encodeURIComponent(ui.item.value);
				if(projectName.length>0){
					jQuery.ajax({
						type: "POST",
						url: '/admin/projectfiles/add',
						data: {'action':'gettags','projectName':projectName},
						success:function(res){
							var res = JSON.parse(res);
							var tagHtml = "";
							var availtags = [];
							if(res.tags.length>0){
								for(var i=0;i<res.tags.length;i++){
									availtags.push(res.tags[i]);
									//tagHtml += '<li class="select2-selection__choice" title="'+res.tags[i]+'"><span class="select2-selection__choice__remove" role="presentation">x</span>'+res.tags[i]+'</li>';
								}
								$progControl.val(availtags).trigger("change");
							}
						},
						error:function(jqXHR, textStatus, errorThrown){
							console.log(jqXHR);
							console.log(textStatus);
							console.log(errorThrown);
						},
					});
				}
			}
			
        });

        //  Select2 component, show assigned tag to the current showing project
        var tagarr = <?php echo json_encode($tags); ?>;
        var tagarrAll = <?php echo json_encode($allTags); ?>;
        var availtags =[];
        var availtagsall =[];
          //console.log(tagarrAll);
        for(i=0; i<tagarrAll.length; i++) {
            availtagsall.push(tagarrAll[i]['tags']['title']);
        }
       
        for(i=0; i<tagarr.length; i++) {
            availtags.push(tagarr[i]['tags']['title']);
        }
            
        //console.log(" >> YSI : "+availtagsall+" >> availtags_____ : "+availtags);
       
        //Select2
        var $progControl =  $(".limitedTagsSelect2").select2({
            //maximumSelectionLength: 2,
            placeholder: "Selected Tags",
            data: availtagsall,
            tags: true,
            tokenSeparators: [',',';',' '] 
        });   

        $('.limitedTagsSelect2').select2().on("change", function(e) {
            // mostly used event, fired to the original element when the value changes
            setValToHiddenField();
        });
		
		$progControl.val(availtags).trigger("change");
		       
        function setValToHiddenField(){

            // Require in project edit/add submit button
            aTagArr = $(".limitedTagsSelect2").select2('data');
            sArr = [];
            $.each(aTagArr, function(key, value) {     
                sArr.push(value.text);
            });
            
            sArr = sArr.toString();
            //var encodeTagURI = encodeURIComponent(sArr);  
            //console.log("--setting value : "+sArr);
            $("#tagsInput1").attr('value',sArr);
        }
       
        // var pid = < php echo $pid >;
        //console.log(pid);       

        /*
        $("#save-btn").click(function(){
            var aTagArr = $(".limitedTagsSelect2").select2('data');
            var sArr = [];
            $.each(aTagArr, function(key, value) {     
             sArr.push(value.text);
            });
            
            sArr = sArr.toString();
            var encodeTagURI = encodeURIComponent(sArr);  
            console.log(">> sArr __ : "+sArr);
                jQuery.ajax({
                        type: "GET",
                        url: '/admin/projectfiles/tagupdate/'+sArr+'/'+pid,
                        success:function(data){
                                 // do stuff with json (in this case an array)
                                 console.log(data);
                                 //alert("Success");
                             },
                             error:function(){
                                 alert("Error");
                             },
                    });
        });
        */
        



    });
	
    function splitstr( val ) {
        return val.split( /,\s*/ );
    }

    function extractLaststr( term ) {
        return splitstr( term ).pop();
    }

    $(document).ready(function() {
        BindControls();
    });

    function BindControls() 
    {
        // START: PrevComp Code ----------------------------------------------------------- 

        /*
		var tagarr = <php echo json_encode($tags) >;
		var availtags = [];
		for(i=0; i<tagarr.length; i++) {
			availtags.push(tagarr[i]['tags']['title']);
		}

        availtags = availtags.toString();
        $("#tagsInput").attr('data-list',availtags);

        function populateTagComp(){
            var tagsMultipleComp = new Awesomplete('input[data-multiple]', {
                filter: function(text, input) {
                    return Awesomplete.FILTER_CONTAINS(text, input.match(/[^,]*$/)[0]);
                },

                replace: function(text) {
                    var before = this.input.value.match(/^.+,\s*|/)[0];
                    this.input.value = before + text + ", ";
                }
            });            
        }

        $("#tagsInput").keyup(function(){
            $("#tagsInput1").attr('value',$("#tagsInput").val());            
        });

        populateTagComp();
        */

        // END: PrevComp Code ----------------------------------------------------------- 

		//console.log(availtags);
        
        // $('#tags').autocomplete({
        //     source: availtags,
        //     minLength: 0,
        //     scroll: true
        // }).focus(function() {
        //     $(this).autocomplete("search", "");
        // });

        /*    
        $('#tagsInput')
            // don't navigate away from the field on tab when selecting an item
            .on( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                    $( this ).autocomplete( "instance" ).menu.active ) {
                  event.preventDefault();
                }
            })
            .autocomplete({
                
                minLength: 0,
                scroll: true,
                source: function( request, response ) {
                  // delegate back to autocomplete, but extract the last term
                  response( $.ui.autocomplete.filter( availtags, extractLaststr( request.term ) ) );
                },               
                focus: function() {
                  // prevent value inserted on focus
                  $(this).autocomplete("search", "");
                  //return false;
                },
                select: function( event, ui ) {
                  var terms = splitstr( this.value );
                  // remove the current input
                  terms.pop();
                  // add the selected item
                  terms.push( ui.item.value );
                  // add placeholder to get the comma-and-space at the end
                  terms.push( "" );
                  this.value = terms.join( ", " );
                  return false;
                }               
          });
        */    

    }

</script>


<div id="main_wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="add-new-project">
                <div class="col-lg-12">                    
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $this->Html->url(array('controller' => 'dashboard', 'action' => 'index')); ?>">Dashboard</a></li>
                        <li>Add New Project</li>
                    </ol>
                    <h2>Add New Project</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="add-new">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <?php echo $this->Form->create('ProjectFile', array('type' => 'file')); ?>

                        <?php
							if(isset($this->params->projectname)){
								echo $this->Form->input('title', array('class' => 'form-control', 'label' => 'Project Name', 'div' => 'false', 'value' => $title));
							} else {
								echo $this->Form->input('title', array('class' => 'form-control', 'label' => 'Project Name', 'div' => 'false', 'placeholder' => 'Add Project Name'));
							}
                            echo $this->Form->input('project_brief', array('class' => 'form-control full-description', 'label' => 'Full Description', 'div' => 'false', 'placeholder' => 'Write your description here'));
						?>

                        <!--<label>Select your file</label>-->
                            <label>Select your file</label>
                        <div class="file-area">
                            <span class="drag-file">Drag .zip file here</span>
                            <div class="drop-area">
                                <?php echo $this->Form->input('filename', array('type' => 'file', 'label' => false, 'div' => false)); ?>
                            </div>
							
							<!-- Tags section for tagging of projects  -->                                    
                            <section id="multiple-values">                                
                                <label>Tags</label>
                                <!-- <input id="tagsInput" data-list="" data-multiple data-minchars="1" /> -->
                                
                                <select class="limitedTagsSelect2" multiple="true"></select>
                                <!-- <input id="save-btn" class="btn btn-primary btn-group-lg saveBtn" type="button" value="Save"> -->

                                <!-- <input name="customtagname" type="" value="hiddentext" /> -->
                            </section>
                            <label>Category</label>
                            <?php echo $this->Form->input('tags', array('type'=>'hidden','id'=>'tagsInput1')); ?>

							</br>
                                
                            <div class="btn-group">
                                <div class="row">
								
                                    <?php 
									//Debugger::dump($tags);
									
									foreach ($categories as $category): ?>
                                        <div class="col-lg-3 col-xs-6 col-sm-3">
                                            <input type="radio" name="data[ProjectFile][category_id]" id="ProjectFileCategoryId<?php echo $category['Category']['id']; ?>" class="css-checkbox" value="<?php echo $category['Category']['id']; ?>" />
                                            <label for="ProjectFileCategoryId<?php echo $category['Category']['id']; ?>" class="css-label"><?php echo $category['Category']['title']; ?></label>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
							
                        </div>
                        <?php
                            $options = array(
                                'class' => 'btn btn-primary btn-group-lg pull-left',
                                'label' => false,
                                'div' => array(
                                    'class' => 'form-group submit',
                                )
                            );
                            echo $this->Form->end($options);
                        ?>
                </div>
<!--                <div class="col-lg-6">
                                                                
                </div>-->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    
    $(document).ready(function() {

        //echo $this->Form->input('tags', array('class' => 'form-control', 'type' => 'text', 'id' => 'tags', 'label' => 'Tags', 'div' => 'false')    );

        //var tagarr = <?php  ?>;
        //array('class' => 'form-control', 'type' => 'text', 'id' => 'tags', 'label' => 'Tags', 'div' => 'false')

        
    });


</script>