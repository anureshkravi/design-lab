<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script> -->

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700">
<link rel="stylesheet" href="/css/jquery-ui.css">
<link rel="stylesheet" href="/css/select2.min.css">

<!-- <script src="http://code.jquery.com/jquery-1.10.2.js"></script> -->
<script src="/js/jquery-1.10.2.js"></script>
<!-- <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->
<script src="/js/jquery-ui.min.js"></script>
<!-- <script src="http://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script> -->
<script src="/js/select2.min.js"></script>


<!-- 
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.6.0/clipboard.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-md5/2.7.0/js/md5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/crypto-js.min.js"></script>
-->

<script src="/js/clipboard.min.js"></script>
<script src="/js/md5.js"></script>
<script src="/js/copyme.js"></script>

<script>
  $(function() {
  
    var isPublicUrlVisible = false, fieldId = "";
    var availableTags = [<?php echo $finalTags; ?>];

    $( "#projectTags" ).autocomplete({
      source: availableTags
    });

    function split( val ) {
      return val.split( /,\s*/ );
    }

    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( "#projectTags" )
      // don't navigate away from the field on tab when selecting an item
      .bind( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        minLength: 0,
        source: function( request, response ) {
          // delegate back to autocomplete, but extract the last term
          response( $.ui.autocomplete.filter(
            availableTags, extractLast( request.term ) ) );
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
      });
        
      //var availableProjTags = ["pVideos","Web Portal","Web Application"];
	 
      var tagarr = <?php echo json_encode($tags) ?>;
      var tagarrAll = <?php echo json_encode($allTags) ?>;
      var availtags =[];
      var availtagsall =[];
      //console.log(tagarrAll);
      for(i=0; i<tagarrAll.length; i++) {
            availtagsall.push(tagarrAll[i]['tags']['title']);
        }
   
      for(i=0; i<tagarr.length; i++) {
            availtags.push(tagarr[i]['tags']['title']);
        }
    
        //availtags = availtags.toString();
        //console.log(" >> YSI : "+availtagsall+" >> availtags_____ : "+availtags);

        //assignedTags
        //<span>Wireframes</span><span>Web Application</span>
        var $progControl;
        var divstr = "", combostr = "";
        for(var j=0; j<availtags.length; j++){
            divstr += '<span>'+availtags[j]+'</span>';

            if(j < (availtags.length-1)) {
                combostr += availtags[j] + ",";
            } else {
                combostr += availtags[j];
            }
        }

        $("#assignedTags").html(divstr);  
        
        document.cookie = "cookie_assignedtags="+combostr;

        $("#editTagsBtn").on('click', function(e){
            
            $("#assignedTags").hide();
            configSelect2Box();   
            $("#editTagsBtn").hide();        
            $("#saveTagsBtn").show();        
            $("#cancelTagsBtn").show();
            e.preventDefault();        
        });

        $("#cancelTagsBtn").on('click', function(){
            
            $("#projTagsHolder .select2-container").hide();
            $("#assignedTags").show();            
            $("#saveTagsBtn").hide();        
            $("#cancelTagsBtn").hide();       
            $("#editTagsBtn").show();          
        });

        function configSelect2Box() {        
            //Select2
            $progControl =  $(".limitedTagsSelect").select2({
                //maximumSelectionLength: 2,
                placeholder: "Selected Tags",
                data: availtagsall,
                tags: false            
            });   
            $progControl.val(availtags).trigger("change");   
            $(".limitedTagsSelect").show();   
            $("#projTagsHolder .select2-container").show();
        }; 

      

		var pid = <?php echo $pid ?>;
        var aTagArr, sArr, encodeTagURI, projectIdNum, projectViewUrl, idnum, pwd, tempurl;         
		//console.log(pid);        
        
        $("#saveTagsBtn").click(function(){

            aTagArr = $(".limitedTagsSelect").select2('data');
            sArr = [];
            $.each(aTagArr, function(key, value) {     
                sArr.push(value.text);
            });
			
            sArr = sArr.toString();
			encodeTagURI = encodeURIComponent(sArr);  
            
			jQuery.ajax({
                type: "GET",
                url: '/admin/projectfiles/tagupdate/'+sArr+'/'+pid,
                success:function(data){
					// do stuff with json (in this case an array)
					//console.log(data);
				   //alert("Success");
                    location.reload();
				},
				error:function(){
				  console.log("Error in save");
                  $("#cancelTagsBtn").trigger('click');
				}
            });
        });

        // Modal radio function 24_01_2017
        

        $('.modal-body .radiobtn').click(function(){                  
            
            var c = $(".modal-body input:radio[name=abc]:checked").val();

            if(c=='ab1') {               

                $("#sharePopupSaveBtn").show();    
                $(this).parents('.modal-body').find('#publicCont').show();
                $(this).parents('.modal-body').find('#pwdCont').hide();             
                isPublicUrlVisible = true;
                $('#btnCopyToClipboard, #btnCopyToClipboardCont').removeAttr('disabled');
                $('#emailLink').removeAttr('disabled');
                //removeSharePassword();                 
                //setPublicShare();                                  

                //setTimeout(function(){
                //    $("#sid_"+projectIdNum).html("<img src='/img/share.png' width='17' height='17' alt=''>");
                //    setSharedProjectColor(true);
                //}, 500);                    

            } else if(c=='ab2') {              

                $("#sharePopupSaveBtn").show();    
                $(this).parents('.modal-body').find('#pwdCont').show();
                $(this).parents('.modal-body').find('#publicCont').hide();                
                isPublicUrlVisible = false;
                $('#btnCopyToClipboard, #btnCopyToClipboardCont').removeAttr('disabled');
                $('#emailLink').removeAttr('disabled'); 
                $("#pswdBlockPassword").val('');
                //removeSharePassword();
                //setSharedProjectColor(false);

            } else if(c=='ab3') {              

                $("#sharePopupSaveBtn").show();    
                $(this).parents('.modal-body').find('#publicCont').hide();
                $(this).parents('.modal-body').find('#pwdCont').hide(); 
                isPublicUrlVisible = false;
                $('#btnCopyToClipboard, #btnCopyToClipboardCont').attr('disabled');
                $('#emailLink').attr('disabled');         
                //removeSharePassword();
                //setSharedProjectColor(false);

            } else {                

                $("#sharePopupSaveBtn").show();    
                $(this).parents('.modal-body').find('#publicCont').hide();
                $(this).parents('.modal-body').find('#pwdCont').hide(); 
                isPublicUrlVisible = false;
                $('#btnCopyToClipboard, #btnCopyToClipboardCont').attr('disabled');
                $('#emailLink').attr('disabled');         
                //removeSharePassword();
                //setSharedProjectColor(false);
            }
        });

        function resetShareBoxOnOpen(){           
            
			$('#pwdCont').hide();    
            $('#publicCont').hide();    
            $("#success-alert").hide();
            $("#errorpwd").hide();
            $($("#shareModal").find('input[name="abc"]')).attr('checked',false);
        };

        function getSharedProjectRecord()
        {
            //$res_price = $conn -> query("SELECT price FROM list WHERE q=1");  
            //$row_price = mysqli_fetch_assoc($res_price);
            //echo $row_price['price'];            
        };


        $("#sharePopupSaveBtn").on('click', function(e){

            var rb = $(".modal-body input:radio[name=abc]:checked").val();

            if(rb == 'ab1') {   

                //YSI_08052017 : Check whether the project have previously set password, 
                //if any then remove that password and set public share. 
                removeSharePassword(); 
                setTimeout(function(){ setPublicShare(); }, 100);                                

            } else if(rb == 'ab2') {   
                
                //YSI_08052017 : Check whether the project have previously set password, 
                //if any then show previous set password for this project. User can reset that password again. 
                requestSetPassword();                          

            } else if(rb == 'ab3') {

                removeSharePassword(); 
            }            
        });

        function hideShareModalBox(){

            $('#shareModal').hide();
            $('.modal-backdrop').hide();
        }

        function removeShareIcon(bln) {

            //echo "<div class='shareiconcont'><span id='sid_$sid' style='opacity: 1;' class='projectShareIcon'><img src='/img/share.png' width='12' height='12' alt=''></span></div>";

            if(bln){                
                //Hide share icon
                $("#sid_"+projectIdNum).css('opacity','0');
            } else {
                //Show share icon
                $("#sid_"+projectIdNum).css('opacity','1');
                // setTimeout(function(){
                //     $("#sid_"+projectIdNum).html("<img src='/img/share.png' width='17' height='17' alt=''>");
                // }, 100);                
            }
        };

        function requestSetPassword()
        {
            if($('#pswdBlockPassword').val()=="") {                            
                
                $('.errorpwd').hide();
                $('.errorentpwd').show();                    
                $('#pswdBlockPassword').addClass('error');    

            } else {
            
                var id = projectIdNum;
                var pwd = $('#pswdBlockPassword').val();
                var pwd = CryptoJS.MD5(pwd).toString();
                
                setUrl = '/admin/projectfiles/setpwd/'+ id + '/1/'+pwd;
                
                $.ajax({
                    type: "POST",
                    url: setUrl,
                    success: function(data) {
                        if( data == 'SUCCESS' ) 
                        {                                            
                            $('.errorpwd').show();
                            $('.errorentpwd').hide(); 
                            $('#pswdBlockPassword').removeClass('error');
                            blinkStatusMessage(".errorpwd");             
                            $.cookie('shared_pid', id, { expires: 365, path: '/'});                           
                            removeShareIcon(false);
                            //setSharedProjectColor(true);     

                            hideShareModalBox();
                        } 
                        else {                    
                            //console.log('FAIL');
                        }
                    }  
                });
            }
        }

        $(".shareProjectFiles").on('click', function(e){
            
            if(e.currentTarget == undefined) return;
            projectIdNum = $(e.currentTarget).attr('id');

            tempurl = ($("#elemassetid_"+projectIdNum).attr('href')).replace("/admin", "");
            projectViewUrl = window.location.protocol + "//"+ window.location.hostname + tempurl;

            $("#shareURL").val(projectViewUrl);
            $("#pswdBlockShareURL").val(projectViewUrl);
			
			resetShareBoxOnOpen();
        });


        $("#btnCopyToClipboard, #btnCopyToClipboardCont").on('click', function(e){
            // Run Clipboard                          
                      
            if($('.publicContent').css('display') == 'block') {
                //console.log("shareUrl");
                $('.shareurl').copyme();
                blinkStatusMessage("#success-alert");  
            }
            else {
                //console.log("pwdurl");
                $('.pwdurl').copyme();                
            }
        });  

        function blinkStatusMessage(field_id)
        {
            setTimeout(function(){
                $(field_id).hide();    
            }, 8000);
        };

        // Email information function     
    	$('#emailLink').on('click', function (event) {

            event.preventDefault();
            var sUrl =  $(".urlInputBox").val();
            var spwd =  $("#pswdBlockPassword").val();        
            var email = 'yatender.saini@espire.com';
            var subject = 'Tesing Url and Password'; 
            var ebody = 'Url: ' + sUrl + '%0D%0A';
            var ebodypwd = 'Url: ' + sUrl + '%0D%0A' + 'Password: ' + spwd;             
            if($('.publicContent').css('display') == 'block') {
                window.location = 'mailto:' + email + '?subject=' + subject + '&body=' + ebody;
            }
            else {
                window.location = 'mailto:' + email + '?subject=' + subject + '&body=' + ebodypwd;
            } 
        });

        function removeSharePassword(){

            var pwd = "";                        
            setUrl = '/admin/projectfiles/setpwd/'+ projectIdNum + '/0/'+pwd;

            $.ajax({
                type: "POST",
                url: setUrl,
                success: function(data) {   
                    if( data == 'SUCCESS' ) 
                    {         
                        //$("#sid_"+projectIdNum).html("");       
                        //setSharedProjectColor(false);
                        removeShareIcon(true);    
                        hideShareModalBox();
                    } else {
                        // Problem in removing password of project 
                    } 
                }
            });
        }

    	$('.btn-setPassword').click( function()
        {
            //var buttonId = $(this).attr('id'),
		    //var buttonId = $(e.currentTarget).attr('id'); 
            //d = buttonId.split('_'),
			var id = projectIdNum;
			var pwd = $('#pswdBlockPassword').val();
			var pwd = CryptoJS.MD5(pwd).toString();
			//console.log(pwd);
			setUrl = '/admin/projectfiles/setpwd/'+ id + '/1/'+pwd;
            //console.log(setUrl);           

            $.ajax({
                type: "POST",
                url: setUrl,
                success: function(data) {
                    if( data == 'SUCCESS' ) 
                    {                    
                        if($('#pswdBlockPassword').val()=="") {
                            $('.errorentpwd').show();
                            $('.errorpwd').hide();
                            $('#pswdBlockPassword').addClass('error');                           

                        } else {
                            $('.errorpwd').show();
                            $('.errorentpwd').hide(); 
                            $('#pswdBlockPassword').removeClass('error');
                            blinkStatusMessage(".errorpwd");             
                            $.cookie('shared_pid', id, { expires: 365, path: '/'});
                            $("#sid_"+projectIdNum).html("<img src='/img/share.png' width='17' height='17' alt=''>");
                            setSharedProjectColor(true);
                        }
                    } else {                    
                            //console.log('FAIL');
                        }
                    }  
            });
            return false;
        });

        $("#pswdBlockPassword").on('keyup', function(e){    
            if($("#pswdBlockPassword").val() != ""){
                if( $("#pswdBlockPassword").hasClass('error')){
                    $("#pswdBlockPassword").removeClass('error'); 
                    $('.errorentpwd').hide();                   
                }
            }   
        });


        function setPublicShare(){
            
            var pwd = "";   
            var setUrl = '/admin/projectfiles/setpwd/'+ projectIdNum + '/2/'+pwd;

            $.ajax({
                type: "POST",
                url: setUrl,
                success: function(data) {   
                    if( data == 'SUCCESS' ) 
                    {         
                        //$("#sid_"+projectIdNum).html(""); 
                        removeShareIcon(false);
                        //setSharedProjectColor(true);                                
                    } 
                }
            });
        };

        function setSharedProjectColor(bln){

            // if(bln){
            //     if($("#sid_"+projectIdNum).prev().is('a')){                                        
            //         $("#sid_"+projectIdNum).prev().addClass('shared-project');
            //     }
            // } else {
            //     if($("#sid_"+projectIdNum).prev().is('a')){                    
            //         if($("#sid_"+projectIdNum).prev().hasClass('shared-project'))
            //         {
            //             $("#sid_"+projectIdNum).prev().removeClass('shared-project');
            //         }
            //     }
            // }
        };


        $("#editrecord td").on('click', function(e){ 
        
            //console.log("pmodel ", $(e.target).attr('ap-category'), " : ", $(e.currentTarget).attr('ap-category'));
            $("#anProjTitleCategory").text($(e.target).attr('ap-category'));

           
                var cat_val = $("#anProjTitleCategory").text().toLowerCase(), sel_index = 0;
                if(cat_val == "wireframes"){
                    sel_index = 0;
                } else if(cat_val == "creatives"){
                    sel_index = 1;
                } else if(cat_val == "html"){
                    sel_index = 2;
                } else if(cat_val == "assets"){
                    sel_index = 3;
                } 
                $($("#addNewProjectModal select")[0]).prop('selectedIndex', sel_index);     
                $( "#addNewProjectModal input:file" ).val(null);
                $("#an-projbrief").val('');
           



            //var myVar=$('#anProjTitleCategory').text(); //"Hello";
            //$.post('admin_view.ctp', myVar);

            // $('#addNewProjectModal').show();            
            // $('.modal-backdrop').show();
            // if($('#addNewProjectModal').hasClass('fade')){
            //     $('#addNewProjectModal').removeClass('fade').addClass('fadein');    
            // }            

        });

        

  });
</script>

<div id="main_wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="project-preview">
                <div class="col-lg-12">
                    <h2>Project Preview</h2>
                    <ol class="breadcrumb pull-left">
                        <li><a href="<?php echo $this->Html->url(array('controller' => 'dashboard', 'action' => 'index')); ?>">Dashboard</a></li>
                        <li>Project Preview</li>
                    </ol>
                </div>
            </div>
        </div>
        <!--project-preview div end-->
        <!--project-thumbnail div start-->
        <div class="row">
            <div class="project-thumbnail">
                <div class="project-thumbnail-logo">
                    <?php
                        if(!empty($projectFile['ProjectFile']['thumbnail'])){
                            echo $this->Html->image('/files/groupthumb/'.$projectFile['ProjectFile']['thumbnail'], array('class' => 'img-responsive'));
                        } else {
                            echo $this->Html->image('thumb.jpg', array('class' => 'img-responsive'));
                        }
                    ?>
                </div>
                <div class="project-thumbnail-heading">
                    <h1 style="margin:0px;"><?php echo $projectFile['ProjectFile']['title']; ?></h1>
                    
                    <?php echo $this->Form->create(null, array('url' => array('controller' => 'projectfiles', 'action' => 'changeTitle'), 'id' => 'ProjectFileAdminChangeTitle')); ?>
                        <div class="form-group">
                            <input type="hidden" value="<?php echo $projectFile['ProjectFile']['project_folder']; ?>" name="data[oldfolder]">
                            <input type="text" name="data[ProjectFile][title]" id="ProjectFileEditTitle" value="<?php echo $projectFile['ProjectFile']['title']; ?>" class="form-control">
                        </div>
                    <?php echo $this->Form->end(); ?>

                   
                </div>
                
                <div class="project-tags-list">
                    <div id="projTagsHolder" class="well">
                        <h4 class="text-info">Assigned Tags: </h4>
                        <div id="assignedTags" class="assignedTags"></div>
                        <select class="limitedTagsSelect" style="display:none;" multiple="true"></select>
                        <a id="editTagsBtn" class="tagBtn" href="#" data-toggle="tooltip" title="Edit">Edit</a>
                        <a id="saveTagsBtn" class="tagBtn" style="display:none;" href="#">Save</a>
                        <a id="cancelTagsBtn" class="tagBtn" style="display:none;" href="#">Cancel</a>
                    </div>
                    
                    <!-- <select class="limitedTagsSelect2" multiple="true"></select>
                    <input id="save-btn" class="btn btn-primary btn-group-lg saveBtn" type="button" value="Save"> -->
                </div>
              </div>


            </div>
      
        <!--project-thumbnail div start-->

        <!--project detail start-->
        <div class="row">
            <div class="project-detail">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="table-responsive projectTable" style="min-height:250px;">
                        <div class="resp">
                            <table class="table" style="margin-bottom:0px; min-width:850px;">
                                <tr>
                                    <?php
                                        foreach($categories as $value){
                                            echo "<th class='category-icon'>".$value."</th>";
                                        }
                                    ?>
                                </tr>
                                <tr id="editrecord">
                                    <td><button ap-category="wireframes" data-toggle="modal" data-target="#addNewProjectModal">Add New</button></td>
                                    <td><button ap-category="creatives" data-toggle="modal" data-target="#addNewProjectModal">Add New</button></td>
                                    <td><button ap-category="html" data-toggle="modal" data-target="#addNewProjectModal">Add New</button></td>
                                    <td><button ap-category="assets" data-toggle="modal" data-target="#addNewProjectModal">Add New</button></td>                                                                       
                                </tr>
                            </table>

                            <div style="min-width:850px;">
                                <?php
                                    foreach($categories as $key => $value)
                                    {
                                        if($key == 2) {
                                            $class = 'wireFramLatestPro';
                                        } else if($key == 3) {
                                            $class = 'creativeLatestPro';
                                        } else if($key == 1) {
                                            $class = 'htmlLatestPro oldHistoryPro';
                                        } else {
                                            $class = 'assetsLatestPro';
                                        }

                                        echo "<div class='col-lg-3 col-sm-3 col-xs-3 space'><table class='table ".$class."'>";

                                        //echo count($projects[$value]);

                                        if(count($projects[$value]) > 0)
                                        {
                                            foreach($projects[$value] as $project)
                                            {                                                
                                                echo "<tr><td>";
                                                //echo "SID_".$project['ProjectFile']['status_id']."   ";

                                                if($project['ProjectFile']['status_id'] == 1)
                                                {                                                 

                                        /*YSI_07*/    /* YSI_30_01_2017 */ // echo $this->Html->link($this->Html->modifyDate($project['ProjectFile']['date_folder']), array('controller' => 'projectfiles', 'action' => 'viewdetails', 'projectfolder' => $project['ProjectFile']['project_folder'], 'categoryfolder' => $project['Category']['category_folder'], 'datefolder' => $project['ProjectFile']['date_folder']), array('target' => '_blank', 'class' => 'active', 'id' => 'elemassetid_'.$project['ProjectFile']['id'] ));

                                                    $sid = $project['ProjectFile']['id'];

                                                    if($project['ProjectFile']['is_password'] == 0){    
                                                        $isSharedPswd = "";                                                               
                                                    } else {
                                                        $isSharedPswd = "shared-project";   
                                                    }                                                    

                                                    if($project['ProjectFile']['is_password'] == 1 || $project['ProjectFile']['is_password'] == 2){                                                        
                                                        echo "<div class='shareiconcont'><span id='sid_$sid' style='opacity: 1;' class='projectShareIcon'><img src='/img/share.png' width='12' height='12' alt=''></span></div>";
                                                    } else {                                                        
                                                        echo "<div class='shareiconcont'><span id='sid_$sid' style='opacity: 0;' class='projectShareIcon'><img src='/img/share.png' width='12' height='12' alt=''></span></div>";
                                                    }

                                                    echo $this->Html->link($this->Html->modifyDate($project['ProjectFile']['date_folder']), array('controller' => 'projectfiles', 'action' => 'viewdetails', 'projectfolder' => $project['ProjectFile']['project_folder'], 'categoryfolder' => $project['Category']['category_folder'], 'datefolder' => $project['ProjectFile']['date_folder']), array('target' => '_blank', 'class' => 'active '.$isSharedPswd, 'id' => 'elemassetid_'.$project['ProjectFile']['id'] ));    

                                                    echo "<p>".$project['ProjectFile']['project_brief']."</p>";

                                                    //for user clients we are hiding publish and edit options
                                                    //for publish
                                                    if($this->Html->user('role_id') != 3) 
                                                    {
                                                        echo '<div class="pop">';
                                                            echo '<a href="#" rel="2" class="jquery_action_status" id="publish_'.$project["ProjectFile"]["id"].'_projectfiles">'.$this->Html->image('publish.png', array('width' => '17', 'height' => '17')).' Unpublish</a>';
                                                            //for edit
                                                            echo '<a href="#" radio="'.$project["ProjectFile"]["class"].'" projectname="'.$this->params->project_folder.'" categoryname="" id="'.$project["ProjectFile"]["id"].'" tags="'.$project["ProjectFile"]["tags"].'" desc="'.$project["ProjectFile"]["description"].'" title="'.$project["ProjectFile"]["publish_title"].'" url="'.$project["ProjectFile"]["url"].'" class="editProject" data-toggle="modal" data-target="#publishModal">'.$this->Html->image('edit.png', array('width' => '17', 'height' => '17')).' Edit Publish</a>';

                                                            echo '<a href="#" class="editProjectFiles" id="'.$project["ProjectFile"]["id"].'" brief="'.$project["ProjectFile"]["project_brief"].'" data-toggle="modal" data-target="#editModal">'.$this->Html->image('edit-publish.png', array('width' => '17', 'height' => '17'))." <span>Edit</span></a>";

                                                            echo '<a href="'.$this->Html->url(array('controller' => 'projectfiles', 'action' => 'delete', $project['ProjectFile']['id'], $this->params->pass[0])).'" class="deleteProject hide">'.$this->Html->image('delete.png', array('width' => 17, 'height' => 17)).' Delete</a>';

                                                            echo '<a href="#" rel="4" class="jquery_action_status hide" id="protected_'.$project["ProjectFile"]["id"].'_projectfiles">'.$this->Html->image('unpublish.png', array('width' => '17', 'height' => '17')).' Protected</a>';
                                                        echo '</div>';
                                                    }

                                                } 
                                                else 
                                                {

                                                    $sid = $project['ProjectFile']['id'];

                                                    //if assets then url will be changes
                                                    if($project['ProjectFile']['category_id'] == 5) {

                                                        if($project['ProjectFile']['is_password'] == 1 || $project['ProjectFile']['is_password'] == 2){
                                                            echo "<div class='shareiconcont'><span id='sid_$sid' style='opacity: 1;' class='projectShareIcon'><img src='/img/share.png' width='12' height='12' alt=''></span></div>";
                                                        } else {
                                                            echo "<div class='shareiconcont'><span id='sid_$sid' style='opacity: 0;' class='projectShareIcon'><img src='/img/share.png' width='12' height='12' alt=''></span></div>";
                                                        }   

                      /*YSI_07*/  /* YSI_30_01_2017 */  echo '<a id='.'elemassetid_'.$project['ProjectFile']['id'].' href="/files/'.$project['ProjectFile']['project_folder'].'/'.$project['Category']['category_folder'].'/'.$project['ProjectFile']['date_folder'].'/'.$project['ProjectFile']['filename'].'">'.$this->Html->modifyDate($project['ProjectFile']['date_folder']).'</a>';
                                                    
                                                    } else if($project['ProjectFile']['status_id'] == 4) {
                                                        
                                                        //for protected projects

                                        /*YSI_07*/  /* YSI_30_01_2017 */ //echo $this->Html->link($this->Html->modifyDate($project['ProjectFile']['date_folder']), array('controller' => 'projectfiles', 'action' => 'viewdetails', 'projectfolder' => $project['ProjectFile']['project_folder'], 'categoryfolder' => $project['Category']['category_folder'], 'datefolder' => $project['ProjectFile']['date_folder']), array('target' => '_blank', 'class' => 'error', 'id' => 'elemassetid_'.$project['ProjectFile']['id'] ));
                                                        
                                                        if($project['ProjectFile']['is_password'] == 0){    
                                                            $isSharedPswd = "";                                                               
                                                        } else {
                                                            $isSharedPswd = "shared-project";   
                                                        }                                                        

                                                        if($project['ProjectFile']['is_password'] == 1 || $project['ProjectFile']['is_password'] == 2){                                                            
                                                            echo "<div class='shareiconcont'><span id='sid_$sid' style='opacity: 1;' class='projectShareIcon'><img src='/img/share.png' width='12' height='12' alt=''></span></div>";
                                                        } else {                                                            
                                                            echo "<div class='shareiconcont'><span id='sid_$sid' style='opacity: 0;' class='projectShareIcon'><img src='/img/share.png' width='12' height='12' alt=''></span></div>";
                                                        }

                                                        echo $this->Html->link($this->Html->modifyDate($project['ProjectFile']['date_folder']), array('controller' => 'projectfiles', 'action' => 'viewdetails', 'projectfolder' => $project['ProjectFile']['project_folder'], 'categoryfolder' => $project['Category']['category_folder'], 'datefolder' => $project['ProjectFile']['date_folder']), array('target' => '_blank', 'class' => 'error '.$isSharedPswd, 'id' => 'elemassetid_'.$project['ProjectFile']['id'] ));

                                                   
                                                    } else {
                                                      
                                        /*YSI_07*/  /* YSI_30_01_2017 */  //echo $this->Html->link($this->Html->modifyDate($project['ProjectFile']['date_folder']), array('controller' => 'projectfiles', 'action' => 'viewdetails', 'projectfolder' => $project['ProjectFile']['project_folder'], 'categoryfolder' => $project['Category']['category_folder'], 'datefolder' => $project['ProjectFile']['date_folder']), array('target' => '_blank', 'id' => 'elemassetid_'.$project['ProjectFile']['id']  ));

                                                        if($project['ProjectFile']['is_password'] == 0){    
                                                            $isSharedPswd = "";                                                               
                                                        } else {
                                                            $isSharedPswd = "shared-project";   
                                                        }

                                                        if($project['ProjectFile']['is_password'] == 1 || $project['ProjectFile']['is_password'] == 2){                                                            
                                                            echo "<div class='shareiconcont'><span id='sid_$sid' style='opacity: 1;' class='projectShareIcon'><img src='/img/share.png' width='12' height='12' alt=''></span></div>";
                                                        } else {                                                            
                                                            echo "<div class='shareiconcont'><span id='sid_$sid' style='opacity: 0;' class='projectShareIcon'><img src='/img/share.png' width='12' height='12' alt=''></span></div>";
                                                        }

                                                        echo $this->Html->link($this->Html->modifyDate($project['ProjectFile']['date_folder']), array('controller' => 'projectfiles', 'action' => 'viewdetails', 'projectfolder' => $project['ProjectFile']['project_folder'], 'categoryfolder' => $project['Category']['category_folder'], 'datefolder' => $project['ProjectFile']['date_folder']), array('target' => '_blank', 'class' => $isSharedPswd, 'id' => 'elemassetid_'.$project['ProjectFile']['id']  ));                                                         
                                                    }                                            
                                                    
                                                    echo "<p>".$project['ProjectFile']['project_brief']."</p>";


                                                    //for user (role) clients we are hiding unpublish option
                                                    if($this->Html->user('role_id') != 3) 
                                                    {
                                                        echo '<div class="pop">';
                                                            if($project['ProjectFile']['category_id'] != 5) {

                                                                /*
                                                                echo '<a href="#" class="publishProject" id="'.$project["ProjectFile"]["id"].'" projectname="'.$this->params->projectfolder.'" categoryname="" data-toggle="modal" data-target="#publishModal">'.$this->Html->image('unpublish.png', array('width' => '17', 'height' => '17'))." Publish</a>"; 
                                                                */
                                                            }

                                                            echo '<a href="#" class="shareProjectFiles" id="'.$project["ProjectFile"]["id"].'" brief="'.$project["ProjectFile"]["project_brief"].'" data-toggle="modal" data-target="#shareModal">'.$this->Html->image('share.png', array('width' => '17', 'height' => '17'))." Share</a>";


                                                            /* YSI_24_01_2017  */
                                                            //for status protected/unprotected
                                                            if($project['ProjectFile']['status_id'] == 4) {
                                                                echo '<a href="#" rel="2" class="jquery_action_status" id="protected_'.$project["ProjectFile"]["id"].'_projectfiles">'.$this->Html->image('publish.png', array('width' => '17', 'height' => '17')).' Unprotected</a>';
                                                            } else {
                                                                echo '<a href="#" rel="4" class="jquery_action_status" id="protected_'.$project["ProjectFile"]["id"].'_projectfiles">'.$this->Html->image('unpublish.png', array('width' => '17', 'height' => '17')).' Protected</a>'; 
                                                            }


                                                            echo '<a href="#" class="editProjectFiles" id="'.$project["ProjectFile"]["id"].'" brief="'.$project["ProjectFile"]["project_brief"].'" data-toggle="modal" data-target="#editModal">'.$this->Html->image('edit-publish.png', array('width' => '17', 'height' => '17'))." Edit</a>";


                                                            echo '<a href="'.$this->Html->url(array('controller' => 'projectfiles', 'action' => 'delete', $project['ProjectFile']['id'], $this->params->pass[0])).'" class="deleteProject">'.$this->Html->image('delete.png', array('width' => 17, 'height' => 17)).' Delete</a>';
                                                            


                                                        echo '</div>';
                                                    }
                                                }

                                                echo "</td></tr>";
                                            }
                                        } else {
                                            echo "<tr><td></td></tr>";
                                        }
                                        echo "</table></div>";
                                    }
                                ?>
                            </div>




                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<!-- Publish Modal -->   <!--  YSI_24_01_2017  -->
<div class="modal fade" id="publishModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><strong>Publish Project</strong></h4>
      </div>

      <form id="" action="/admin/projectfiles/publish" method="post" enctype="multipart/form-data">
          <div class="modal-body">
                <div class="false required">
                    <label>Title</label>
                    <input type="text" id="projectPublishTitle" class="form-control" name="data[ProjectFile][publish_title]" required="required">
                </div>

                <div class="false required">
                    <label>Thumbnail</label>
                    <input type="file" class="form-control" name="data[ProjectFile][thumbnail]">
                </div>

                <div class="false required">
                    <label class="radio-inline">
                        <input type="radio" name="data[ProjectFile][class]" id="inlineRadio1" class="thumbRadio" value="filter_portfolio filter_uncategorized width-and-small isotope-item  brick-with-img all_cat">
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="data[ProjectFile][class]" id="inlineRadio2" class="thumbRadio" value="filter_blog filter_branding filter_portfolio filter_uncategorized width-and-height isotope-item  brick-with-img all_cat1"> 
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="data[ProjectFile][class]" id="inlineRadio3" class="thumbRadio" value="filter_blog filter_branding filter_portfolio filter_uncategorized width-and-long isotope-item  brick-with-img all_cat2">
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="data[ProjectFile][class]" id="inlineRadio4" class="thumbRadio" value="filter_blog filter_photography-2 filter_portfolio filter_slider filter_uncategorized width-and-big isotope-item  brick-with-img all_cat3">
                    </label>
                </div> <br>

                <div class="false required">
                    <label>Cover Image</label>
                    <input type="file" class="form-control" name="data[ProjectFile][coverimage]">
                </div>

                <div class="false required">
                    <label>Description</label>
                    <textarea class="form-control" id="projectDescription" name="data[ProjectFile][description]"></textarea>
                </div>

                <div class="false required tagContainer">
                    <label>URL</label>
                    <input type="text" id="projectUrl" name="data[ProjectFile][url]" class="form-control">
                </div>

                <div class="false required tagContainer">
                    <label>Tags</label>
                    <input type="text" id="projectTags" required="required" name="data[Tag][title]" class="form-control" data-role="tagsinput" value="">
                </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-primary" value="Submit">
          </div>
      </form>
    </div>
  </div>
</div>


<!-- Share modal Html start here --> <!--  24_01_2017 : START -->
<div id="shareModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="shareTitle" class="modal-title">Share</h4>
            </div>
            <div class="modal-body">
                <div class="form-row share-option">                   
                    <label class="radioLabel share-option-label"><input class="radiobtn" type="radio" name="abc" value="ab1"/>Public</label>
                </div>                
                <div class="form-row share-option"> 
                    <label class="radioLabel share-option-label"><input class="radiobtn" type="radio" name="abc" value="ab2"/>Password Protected</label> 
                </div>
                <div class="form-row"> 
                    <label class="radioLabel share-option-label"><input class="radiobtn" type="radio" name="abc" value="ab3"/>Not Shared</label> 
                </div>    
                <div id="publicCont" class="publicContent">
                    <ul class="radioBlock">
                        <li>             
                            <label class="labelfield">URL:</label>
                            <input id="shareURL" class="form-control urlInputBox shareurl"  type="text" value="http://google.com " name="url" >                            
                        </li>                        
                    </ul>
                    <button type="button" id="btnCopyToClipboard" data-clipboard-action="copy" data-clipboard-target="#post-shortlink" disabled class="btn btn-default btn-copyToClipboard">Copy to Clipboard</button>
                </div>  
                <div id="pwdCont" class="publicContent">    
                   <ul class="radioBlock">
                        <li>             
                            <label class="labelfield">URL:</label>
                            <input id="pswdBlockShareURL"  class="form-control urlInputBox pwdurl" type="text" placeholder="http://jsfiddle.net/" name="url" >
                            <button type="button" id="btnCopyToClipboardCont" data-clipboard-action="copy" data-clipboard-target="#post-shortlink" disabled class="btn btn-default btn-copyToClipboard">Copy to Clipboard</button>
                        </li>
                        <li>              
                            <label class="labelfield">Password:</label>
                            <input id="pswdBlockPassword" class="form-control" type="password" placeholder="Password" name="password" >                       
                            <!-- <button type="button" id="btnSetPassword" class="btn btn-default btn-setPassword">Set Password</button> -->
                            <span class="errorentpwd" style="display: none;">Please enter a password.</span>
                            <span class="errorpwd" style="display: none;">Password has been set.</span>
                            
                        </li>
                    </ul>
                </div>      
            </div>
           <div class="modal-footer">                
                <!-- <button type="button" id="emailLink" disabled class="btn btn-default">Email</button> -->
                <button id="sharePopupSaveBtn" data-dismiss="modal" type="button" style="display:none;" class="btn btn-primary jquery_action_pwd">Ok</button>
                <!-- <a href="#" id="cancelSharePopupBtn" class="cancelSharing" data-dismiss="modal">Cancel</a> -->
            </div>  
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal --> <!--  24_01_2017 : END -->
<!-- Share modal Html ends here -->




<!-- Edit Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><strong>Edit Project</strong></h4>
      </div>

      <form id="" action="/admin/projectfiles/edit" method="post" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="false">
                    <label>Select file to upload</label>
                    <input type="file" name="data[ProjectFile][filename]" id="ProjectFileFilename" style="min-height: 90px;">
                </div>

                <div class="false">
                    <label>Brief Description</label>
                    <input name="data[ProjectFile][project_brief]" class="form-control" maxlength="500" type="text" id="ProjectFileProjectBrief">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>
      </form>
    </div>
  </div>
</div>

<!-- YSI: Add new project element updated on 28_04_2017 -->
<div id="addNewProjectModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">

    <div class="modal-content">        
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="anProjectModalLabel"><strong>Add new project in </strong><span id="anProjTitleCategory" style="
    font-weight: bold;"></span></h4>
        </div>      

        <div class="modal-body">
            <?php
                echo $this->Form->create(null, array('url' => array('controller' => 'projectfiles', 'action' => 'add'), 'type' => 'file')); 
				$phpVar =  $_COOKIE['cookie_assignedtags'];                

                $anprojecttitle = $projectFile['ProjectFile']['title'];                            
                $ancategoryid = '';                 
                $anprojecttag = $phpVar;   
                $pagerequesttype = "projectinnerpage";
                $projecttitletxt = str_replace(" ","-",$anprojecttitle);
                
                echo $this->Form->input('title',        array('value'=>$anprojecttitle, 'default'=>$anprojecttitle, 'class' => 'anproj-title', 'label' => 'Project Title: ', 'div' => 'true', 'placeholder' => $anprojecttitle));
                echo $this->Form->input('category_id',  array('value'=>$ancategoryid, 'default'=>$ancategoryid, 'class' => 'anproj-categoryid', 'label' => 'Category: ', 'div' => 'true', 'placeholder' => $ancategoryid));
                
				echo $this->Form->input('projinnertag',         array('value'=>$anprojecttag, 'default'=>$anprojecttag, 'class' => 'anproj-tag', 'label' => 'Tags: ', 'div' => 'true', 'placeholder' => $anprojecttag));            
                echo $this->Form->input('prequesttype', array('value'=>$pagerequesttype, 'default'=>$pagerequesttype, 'class' => 'anproj-reqtype', 'label' => 'RequestType: ', 'div' => 'true', 'placeholder' => $pagerequesttype));
                echo $this->Form->input('titleproj',    array('value'=>$projecttitletxt, 'default'=>$projecttitletxt, 'class' => 'anproj-titleproj', 'label' => 'Project TitleTxt: ', 'div' => 'true', 'placeholder' => $projecttitletxt));
                                
                echo "<script>$('.anproj-title').hide(); $('.anproj-categoryid').hide(); $('.anproj-tag').hide();  $('.anproj-reqtype').hide(); $('.anproj-titleproj').hide();
                    $($('#addNewProjectModal .modal-body form div')[1]).hide();
                    $($('#addNewProjectModal .modal-body form div')[2]).hide();
                    $($('#addNewProjectModal .modal-body form div')[3]).hide();
                    $($('#addNewProjectModal .modal-body form div')[4]).hide();
                    $($('#addNewProjectModal .modal-body form div')[5]).hide();
                </script>"                                  
            ?>
            <div class="false">
                <label>Select file to upload</label>
                    <?php echo $this->Form->input('filename', array('type' => 'file', 'id' => 'anfilenameinp', 'label' => '', 'div' => 'true', 'style'=>'min-height:90px;' )); ?>
            </div>
            <div class="false">
                <label>Brief Description</label>
                <?php echo $this->Form->input('project_brief', array('type'=>'text', 'maxlength'=>'500', 'size'=>'75', 'class' => 'anproj-brief form-control', 'id' => 'an-projbrief', 'label' => false, 'div' => 'true')); ?>   
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input class="btn btn-primary btn-group-lg pull-right" type="submit" value="Submit">         
        </div>
        
    </div>   <!-- End brace of class 'modal-content'  -->

    <?php echo $this->Form->end(); ?>

  </div>
</div>


<div class="overLay">
    <!-- Globally Button top of the overlay -->
    <div class="container-fluid">
        <div class="col-md-12">

            <div class="row">
                <p><button type="button" id="skip" class="skip pull-right" >Skip <img src="/img/skip.png" alt=""  /></button></p>
            </div>
            
            <div class="row">
                <p>
                    <button type="button" id="replay" class="tranBtn tran radius2 yellowBtn pull-right" style="display:none;" >
                    Replay <img src="/img/replay.png" alt=""/>
                    </button>
                    <button type="button" id="next" class="tranBtn tran radius2 orgBtn pull-right" > Next
                        <img src="/img/next.png" alt=""/>
                    </button>

                    <button type="button" id="Prev" class="tranBtn tran radius2 orgBtn pull-right" style="display:none;" > 
                        <img src="/img/next.png" alt="" class="flip"/> Previous
                    </button>

                    <button type="button" id="gotit" class="tranBtn tran radius2 greenBtn pull-right" style="margin-right:35px;" > Ok got it!
                        <img src="/img/ok.png" alt=""/>
                    </button>
                </p>
            </div>
        </div>
    </div>

    <!--start Slide 1-->
    <div id="slide1" class="slidecontainer" >
        <div class="mainContainer">
            <div class="wireFrame framePosit">

                <div class="project-detail" style="margin:0;">
                    <table style="margin-bottom:0px;" class="table">
                        <tbody>
                            <tr>
                                <th>Wireframes</th>
                            </tr>
                        </tbody>
                    </table>
                </div> <!-- End wire frame head -->

                <div class="project-detail" style="margin:0;">
                    <table class="table wireframLatestproApp" style="margin-bottom:0px; display:none;">
                        <tbody>
                            <tr>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- End wire frame latest Pro -->
            </div>

            <div class="topwire" >
                <img src="/img/top-arrow.png" alt="" />
                <h3>Your wireframe section</h3>
                <div class="clearfix"></div>
                <div class="topwireLeft" style="margin: 32px 0px 0px; display:none;" >
                    <img src="/img/top-arrow.png" alt="" />
                    <h3>Here is your latest version<br>of wire frame</h3>
                </div> <!-- End wire frame latest Pro arrow -->
            </div> <!-- End wire frame head arrow -->

            
        </div> <!-- end main container -->
    </div><!--end Slide 1-->

    <!--start Slide 2-->
    <div id="slide2" class="slidecontainer" style="display:none;">
        <div class="mainContainer">
            <div class="cretFrame framePosit">
                <div class="project-detail" style="margin:0;">
                    <table style="margin-bottom:0px; " class="table">
                        <tbody>
                            <tr>
                                <th>Creatives</th>
                            </tr>
                        </tbody>
                    </table>
                </div> <!-- End creative frame head -->

                <div class="project-detail" style="margin:0;">
                    <table class='table creativeLatestProApp' style="margin-bottom:0; display:none;">
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                </div> <!-- End creative frame Latest -->
            </div>
            
            <div class="topwire" style="margin:0 0 0 52%;" >
                <img src="/img/top-arrow.png" alt="" />
                <h3>Your creatives section</h3>

                <div class="topwireLeft" style="margin:32px 0 0 0; display:none;">
                    <img src="/img/top-arrow.png" alt="" />
                    <h3>Here is your latest version<br>of creatives</h3>
                </div> <!-- End creative frame Latest arrow -->

            </div> <!-- End creative frame head arrow-->


        </div> <!-- end main container -->
    </div><!--end Slide 2-->

    <!--start Slide 3-->
    <div id="slide3" class="slidecontainer" style="display:none;">
        <div class="mainContainer">
            

            <div class="htmlMockupText">
                <div class="htmlMockupHead">
                    <h3>Your html section</h3>
                    <img src="/img/top-arrow.png" alt="" class="flip" />
                </div>
                <div class="htmlMockupChild" style="display:none;" >
                    <h3>Here is your latest version of html</h3>
                    <img src="/img/top-arrow.png" alt="" class="flip" />
                </div>
            </div><!-- End Html frame head arrow-->

            <div class="htmlFrame framePosit">
                            
                            <div class="project-detail" style="margin:0;">
                                <table style="margin-bottom:0px; " class="table">
                                    <tbody>
                                        <tr>
                                            <th>HTML</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> <!-- End Html frame head -->
                            
                            <div class="project-detail" style="margin:0;">
                                <table class="table htmlLatestProApp" style="margin-bottom:0px; display:none;">
                                    <tbody>
                                        <tr>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> <!-- End Html letest Pro -->
            </div>
            
        </div> <!-- end main container -->
    </div><!--end Slide 3-->

    <!--start Slide 4-->
    <div id="slide4" class="slidecontainer" style="display:none;">
        <div class="mainContainer">

            <div class="assestMockupText">
                <div class="assestMockupHead">
                    <h3>Your assets section</h3>
                    <img src="/img/top-arrow.png" alt="" class="flip" />
                </div>
                <div class="assestMockupChild" style="display:none;" >
                    <h3>Here is your latest version<br>of assets</h3>
                    <img src="/img/top-arrow.png" alt="" class="flip" />
                </div>
            </div><!-- End Html frame head arrow-->

             <div class="asetFrame framePosit">
                
                <div class="project-detail" style="margin:0;">
                    <table style="margin-bottom:0px; " class="table">
                        <tbody>
                            <tr>
                                <th>Assets</th>
                            </tr>
                        </tbody>
                    </table>
                </div> <!-- End Assets Head --> 

                <div class="project-detail" style="margin:0;">
                    <table class="table assetsLatestProApp" style="margin-bottom:0px; display:none;">
                        <tbody>
                            <tr>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div> <!-- End Assests Latest Pro -->
            </div>
        </div> <!-- end main container -->
    </div><!--end Slide 4-->

    <!--start Slide 5-->
    <div id="slide5" class="slidecontainer" style="display:none;">
        <div class="mainContainer" style="margin-top:170px;">

            <div class="htmlMockupText">
                <div class="oldMockupChild" style="display:none;" >
                     <h3>Here is your version history</h3>
                    <img src="/img/top-arrow.png" alt="" class="flip" />
                </div>
            </div><!-- End Html frame head arrow-->

            <div class="framePosit oldhist">
                <div class="project-detail">
                    <table class="table oldHistoryProApp" style="margin:0; display:none;"></table>
                </div> <!-- End Old History -->
            </div>

        </div> <!-- end main container -->
    </div><!--End  Slide 5-->   
</div><!--end Overlay-->

<?php echo $this->Html->script(array('jquery.cookie', 'overlay-project')); ?>