<div id="main_wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="project-preview">
                <div class="col-lg-12">
                    <h2>Project Preview</h2>
                </div>
            </div>
        </div>
        <!--project-preview div end-->
        <!--project-thumbnail div start-->
        <div class="row">
            <div class="project-thumbnail">
                <div class="col-lg-3 col-sm-3" style="margin-bottom:25px; max-height:290px;overflow:hidden;">
                    <?php
                        if(!empty($projectFile['ProjectFile']['thumbnail'])){
                            echo $this->Html->image('/files/groupthumb/'.$projectFile['ProjectFile']['thumbnail'], array('class' => 'img-responsive'));
                        } else {
                            echo $this->Html->image('thumb.jpg', array('class' => 'img-responsive'));
                        }
                    ?>
                </div>
                <div class="col-lg-9 col-sm-9">
                    <h1 style="margin:0px;"><?php echo $projectFile['ProjectFile']['title']; ?></h1>
                </div>
            </div>
        </div>
        <!--project-thumbnail div start-->
        <!--project detail start-->
        <div class="row">
            <div class="project-detail">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="table-responsive projectTable">
                        <table class="table" style="margin-bottom:0px;">
                            <tr>
                                <?php
                                    foreach($categories as $value){
                                        echo "<th>".$value."</th>";
                                    }
                                ?>
                            </tr>
                        </table>

                        <?php
                            foreach($categories as $value){
                                echo "<div class='col-lg-12 col-sm-12 space'><table class='table'>";
                                if(count($projects[$value]) > 0){
                                    foreach($projects[$value] as $project){
                                        echo "<tr><td>";
                                        if($project['ProjectFile']['category_id'] == 5) {
                                            echo '<a href="/files/'.$project['ProjectFile']['project_folder'].'/'.$project['Category']['category_folder'].'/'.$project['ProjectFile']['date_folder'].'/'.$project['ProjectFile']['filename'].'">'.$this->Html->modifyDate($project['ProjectFile']['date_folder']).'</a>';
                                        } else {
                                            echo $this->Html->link($this->Html->modifyDate($project['ProjectFile']['date_folder']), array('controller' => 'projectfiles', 'action' => 'viewdetails', 'projectfolder' => $project['ProjectFile']['project_folder'], 'categoryfolder' => $project['Category']['category_folder'], 'datefolder' => $project['ProjectFile']['date_folder']), array('target' => '_blank'));
                                        }
                                        echo "<p>".$project['ProjectFile']['project_brief']."</p>";
                                        echo "</td></tr>";
                                    }
                                } else {
                                    echo "<tr><td>&nbsp;</td></tr>";
                                }
                                echo "</table></div>";
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>