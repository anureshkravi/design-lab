<div id="main_wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="projectlist">
                <div class="col-xs-12">

                    <a href="<?php echo $this->Html->url(array('controller'=>'projectfiles', 'action' => 'deleteAllfiles')); ?>" onclick="if (confirm(&quot;This will delete all the projects permanently!! Are you sure you want to continue?&quot;)) { return true; } return false;" class="btn btn-danger pull-right" style="color: #fff; margin:15px 0px;">Delete All</a>

                    <div class="table-responsive">
                        <div class="resp">
                            <ul class="heading" style="padding-top:50px;">
                                <li>
                                    <div class="col-xs-3">Project Name</div>
                                    <div class="col-xs-2">Category</div>
                                    <div class="col-xs-2">Folder Name</div>
                                    <div class="col-xs-3">Project Brief</div>
                                    <div class="col-xs-2">Action</div>
                                    <div class="clearfix"></div>
                                </li>
                                <div class="clearfix"></div>
                            </ul>

                            <div id="contentLeft">
                                <ul class="ui-sortable">
                                    <?php foreach($projects as $project): ?>
                                        <li>
                                            <div class="col-xs-3"><?php echo $project['ProjectFile']['title']; ?></div>
                                            <div class="col-xs-2"><?php echo $project['Category']['title']; ?></div>
                                            <div class="col-xs-2">
                                                <?php
                                                    if($project['ProjectFile']['category_id'] == 5) {
                                                        echo '<a href="/files/'.$project['ProjectFile']['project_folder'].'/'.$project['Category']['category_folder'].'/'.$project['ProjectFile']['date_folder'].'/'.$project['ProjectFile']['filename'].'">'.$this->Html->modifyDate($project['ProjectFile']['date_folder']).'</a>';
                                                    } else {
                                                        echo $this->Html->link($this->Html->modifyDate($project['ProjectFile']['date_folder']), array('controller' => 'projectfiles', 'action' => 'viewdetails', 'projectfolder' => $project['ProjectFile']['project_folder'], 'categoryfolder' => $project['Category']['category_folder'], 'datefolder' => $project['ProjectFile']['date_folder']), array('target' => '_blank'));
                                                    }
                                                ?>
                                            </div>
                                            <div class="col-xs-3" style="padding-top :7px; line-height: 25px;"><?php echo $project['ProjectFile']['project_brief']; ?></div>
                                            <div class="col-xs-2">
                                                <a onclick="if (confirm(&quot;Are you sure you want to delete the project permanentaly? After this it won't be possible for you to recover the project.&quot;)) { return true; } return false;"  href="<?php echo $this->Html->url(array('controller' => 'projectfiles', 'action' => 'deletefiles', 'id' => $project['ProjectFile']['id'])); ?>">Delete</a> /

                                                <a href="<?php echo $this->Html->url(array('controller' => 'projectfiles', 'action' => 'recover', 'id' => $project['ProjectFile']['id'])); ?>">Recover</a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>