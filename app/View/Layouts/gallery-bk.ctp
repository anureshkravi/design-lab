<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php
            if(count($this->params->pass) > 0){
                echo SITE_NAME;
                foreach($this->params->pass as $value){
                    echo ' :: '.Inflector::humanize($value, '-');
                }
            } else {
                echo SITE_NAME;
            }
        ?>
    </title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css(array('bootstrap.min', 'elastislide'));
        echo $this->Html->script(array('bootstrap.min', 'jquery.tmpl.min', 'jquery.easing.1.3', 'jquery.elastislide', 'gallery', 'custom-jquery'));
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]--> 
</head>
<body class="page-body">
<?php echo $this->Session->flash(); ?>
<?php echo $this->fetch('content'); ?>
</body>
</html>