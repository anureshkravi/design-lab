<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="noindex, nofollow">
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php
            if(count($this->params->pass) > 0){
                echo SITE_NAME;
                foreach($this->params->pass as $value){
                    echo ' :: '.Inflector::humanize($value, '-');
                }
            } else {
                echo SITE_NAME;
            }
        ?>
    </title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <?php echo $this->Html->script(array('jquery.cookie', 'comments', 'overlay-gallery')); ?>

	 <?php
			$role_id = $this->Html->user('role_id');

            if(!empty($role_id) && $role_id !== null) {
                $var_userRoleId = 'var userRoleId = '.$role_id.';';
            } else {
                $var_userRoleId = 'var userRoleId;';
            }
	?>
    <script>
        <?php
			 echo $var_userRoleId;
        ?>
        if($.cookie('cookie_id') === undefined) {
            <?php echo ' var cookie_id = "'.$this->Html->uniqueId().'";'; ?>
            $.cookie('cookie_id', cookie_id, {expires: 365, path: '/'});
        }
    </script>
    <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css(array('bootstrap.min', 'elastislide', 'overlay', 'jquery-ui'));
        echo $this->Html->script(array('bootstrap.min', 'jquery.tmpl.min', 'jquery.easing.1.3', 'jquery.elastislide', 'gallery', 'custom-jquery', 'jquery-ui.min'));
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!-- Added for Create hotspot || Image linking -->
<?php
	$userRoleId =  !empty($this->Html->user('role_id'))?$this->Html->user('role_id'):0;
	if($userRoleId >0){
		$defaultClassBasedOnUser = ' image-linking-active preview-mode';
	} else{
		$defaultClassBasedOnUser = ' image-linking-active preview-mode';
	}
	
?>
<!-- END -->
<body class="page-body  <?php echo $defaultClassBasedOnUser;?>">
    <?php echo $this->Session->flash(); ?>
    <?php echo $this->fetch('content'); ?>
</body>
</html>