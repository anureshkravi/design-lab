<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="robots" content="noindex, nofollow">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo $this->Html->charset(); ?>    
    <title><?php echo SITE_NAME; ?></title>
</head>
<body class="page-body">
<?php echo $this->fetch('content'); ?>
</body>
</html>