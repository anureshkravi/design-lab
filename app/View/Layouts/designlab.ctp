<!Doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo SITE_NAME; ?></title>
        <?php
            echo $this->Html->css(array('bootstrap.min', 'layout', 'screen', 'style1', 'theme-style', 'bootstrap-select'));
        	echo $this->Html->script(array('jquery.min', 'bootstrap.min', 'bootstrap-select', 'jquery-migrate.min', 'pagebuilder', 'custom'));
        ?>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div id="wrap">
            <?php echo $this->element('sidebar'); ?>
            <div id="alertmaincontainer">
                <div id="alertcontainer">
                    <?php echo $this->Session->flash(); ?>
                </div>
            </div>
            <?php echo $this->fetch('content'); ?>
            <div id="hot-close-sidebar-touch"></div>
        </div>

        <?php if($this->params->controller != 'tickets') { ?>
            <div class="hidden-phone" id="float-bar">
                <div id="float-bar-triggler"><i class="float-bar-triggler-inn"></i></div>
            </div>
        <?php } ?>
    </body>
</html>