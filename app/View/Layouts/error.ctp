<!Doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo SITE_NAME; ?></title>
        <?php
            echo $this->Html->css(array('bootstrap.min', 'layout', 'screen', 'style1', 'theme-style'));
        	echo $this->Html->script(array('jquery-migrate.min', 'pagebuilder', 'custom'));
        ?>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div id="wrap">
            <?php echo $this->element('error-sidebar'); ?>
            <div style="margin-left:100px; padding-top:20px;"><?php echo $this->fetch('content'); ?></div>
        </div>
        <div id="hot-close-sidebar-touch"></div>   
        <div class="hidden-phone" id="float-bar">
            <div id="float-bar-triggler"><i class="float-bar-triggler-inn"></i></div>
        </div>
    </body>
</html>