<div class="tickets view">
<h2><?php echo __('Ticket'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($ticket['Ticket']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Assigned Id'); ?></dt>
		<dd>
			<?php echo h($ticket['Ticket']['assigned_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($ticket['User']['id'], array('controller' => 'users', 'action' => 'view', $ticket['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($ticket['Ticket']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($ticket['Ticket']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project'); ?></dt>
		<dd>
			<?php echo h($ticket['Ticket']['project']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Filename'); ?></dt>
		<dd>
			<?php echo h($ticket['Ticket']['filename']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Requested Time'); ?></dt>
		<dd>
			<?php echo h($ticket['Ticket']['requested_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Time Spent'); ?></dt>
		<dd>
			<?php echo h($ticket['Ticket']['time_spent']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo $this->Html->link($ticket['Status']['title'], array('controller' => 'statuses', 'action' => 'view', $ticket['Status']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($ticket['Ticket']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($ticket['Ticket']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ticket'), array('action' => 'edit', $ticket['Ticket']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ticket'), array('action' => 'delete', $ticket['Ticket']['id']), null, __('Are you sure you want to delete # %s?', $ticket['Ticket']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Tickets'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ticket'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Statuses'), array('controller' => 'statuses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Status'), array('controller' => 'statuses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Efforts'), array('controller' => 'efforts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Effort'), array('controller' => 'efforts', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Efforts'); ?></h3>
	<?php if (!empty($ticket['Effort'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Ticket Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Hours'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($ticket['Effort'] as $effort): ?>
		<tr>
			<td><?php echo $effort['id']; ?></td>
			<td><?php echo $effort['ticket_id']; ?></td>
			<td><?php echo $effort['user_id']; ?></td>
			<td><?php echo $effort['hours']; ?></td>
			<td><?php echo $effort['description']; ?></td>
			<td><?php echo $effort['created']; ?></td>
			<td><?php echo $effort['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'efforts', 'action' => 'view', $effort['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'efforts', 'action' => 'edit', $effort['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'efforts', 'action' => 'delete', $effort['id']), null, __('Are you sure you want to delete # %s?', $effort['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Effort'), array('controller' => 'efforts', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
