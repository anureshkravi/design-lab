<div class="container-fluid">
	<div class="row">
        <div class="add-new-project">
            <div class="col-lg-12">
                <h2>UI Tickets</h2>
                <ol class="breadcrumb">
                    <li><a href="<?php echo $this->Html->url(array('controller' => 'dashboard', 'action' => 'index')); ?>">Dashboard</a></li>
                    <li>UI Tickets</li>
                </ol>
            </div>
        </div>
    </div>
	
	<div class="projectlist assigned-projects">
		<div class="col-md-12 tickets-container">
			<div class="clearfix tickets-head">
				<div class="col-md-3">Projects</div>
				<div class="col-md-2">Time Spent</div>
				<div class="col-md-2">Status</div>
				<div class="col-md-5">Actions</div>
			</div>

			<div class="clearfix tickets">
				<div class="col-md-3">ONS</div>
				<div class="col-md-2">
					<a href="#" class="hours">20 hrs <span class="glyphicon glyphicon-chevron-down"></span></a>
				</div>
				<div class="col-md-2">In Progress</div>
				<div class="col-md-5">
					<a class="btn btn-success" href="#" role="button">Completed</a>
                	<a class="btn btn-danger" href="#" role="button">Re-assign</a>
                	<a class="btn btn-warning" href="#" role="button">Hold</a>
                	<a class="btn btn-primary" href="#" role="button">Start</a>
				</div>
				
				<div class="table-responsive col-md-8 col-md-push-1 hours-container">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td colspan="3">
									<form class="form-inline">
										<div class="form-group">
											<label>Add Hours</label>
											<input type="number" class="form-control">
										</div>

										<div class="form-group">
											<label>Description</label>
											<input type="text" class="form-control">
										</div>
										<button type="submit" class="btn btn-success">Update</button>
									</form>
								</td>
							</tr>

							<tr>
								<td><strong>Date</strong></td>
								<td><strong>Hours</strong></td>
								<td><strong>Description</strong></td>
							</tr>

							<tr>
								<td>Jun 23, 2015</td>
								<td>2 hrs</td>
								<td>Lorem ipsum dolor amit</td>
							</tr>

							<tr>
								<td>Jun 23, 2015</td>
								<td>2 hrs</td>
								<td>Lorem ipsum dolor amit</td>
							</tr>

							<tr>
								<td>Jun 23, 2015</td>
								<td>2 hrs</td>
								<td>Lorem ipsum dolor amit</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="clearfix tickets">
				<div class="col-md-3">ONS</div>
				<div class="col-md-2"><a href="#">20 hrs <span class="glyphicon glyphicon-chevron-down"></span></a></div>
				<div class="col-md-2">In Progress</div>
				<div class="col-md-5">
					<a class="btn btn-success" href="#" role="button">Completed</a>
                	<a class="btn btn-danger" href="#" role="button">Re-assign</a>
                	<a class="btn btn-warning" href="#" role="button">Hold</a>
                	<a class="btn btn-primary" href="#" role="button">Start</a>
				</div>
			</div>

			<div class="clearfix tickets">
				<div class="col-md-3">ONS</div>
				<div class="col-md-2"><a href="#">20 hrs <span class="glyphicon glyphicon-chevron-down"></span></a></div>
				<div class="col-md-2">In Progress</div>
				<div class="col-md-5">
					<a class="btn btn-success" href="#" role="button">Completed</a>
                	<a class="btn btn-danger" href="#" role="button">Re-assign</a>
                	<a class="btn btn-warning" href="#" role="button">Hold</a>
                	<a class="btn btn-primary" href="#" role="button">Start</a>
				</div>
			</div>

			<div class="clearfix tickets">
				<div class="col-md-3">ONS</div>
				<div class="col-md-2"><a href="#">20 hrs <span class="glyphicon glyphicon-chevron-down"></span></a></div>
				<div class="col-md-2">In Progress</div>
				<div class="col-md-5">
					<a class="btn btn-success" href="#" role="button">Completed</a>
                	<a class="btn btn-danger" href="#" role="button">Re-assign</a>
                	<a class="btn btn-warning" href="#" role="button">Hold</a>
                	<a class="btn btn-primary" href="#" role="button">Start</a>
				</div>
			</div>

			<div class="clearfix tickets">
				<div class="col-md-3">ONS</div>
				<div class="col-md-2"><a href="#">20 hrs <span class="glyphicon glyphicon-chevron-down"></span></a></div>
				<div class="col-md-2">In Progress</div>
				<div class="col-md-5">
					<a class="btn btn-success" href="#" role="button">Completed</a>
                	<a class="btn btn-danger" href="#" role="button">Re-assign</a>
                	<a class="btn btn-warning" href="#" role="button">Hold</a>
                	<a class="btn btn-primary" href="#" role="button">Start</a>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Assign Modal -->
<div class="modal fade" id="assignModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Assign Task</h4>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<select class="form-control">
							<option value="">Select User</option>
						</select>
					</div>
		        </form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Assign</button>
			</div>
		</div>
	</div>
</div>
<?php echo $this->Html->script(array('tickets-module')); ?>