<div class="container-fluid">
	<div class="row">
        <div class="add-new-project">
            <div class="col-lg-12">
                <h2>UX Requests</h2>
                <ol class="breadcrumb">
                    <li><a href="<?php echo $this->Html->url(array('controller' => 'dashboard', 'action' => 'index')); ?>">Dashboard</a></li>
                    <li>UX Requests</li>
                </ol>
            </div>
        </div>
    </div>
	
	<?php if($this->Html->user('role_id') == 1) { ?>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    	<?php $count=1; ?>
    	<?php foreach($newtickets as $key => $ticket): ?>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading<?php echo $count; ?>">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $count; ?>" aria-expanded="true" aria-controls="collapse<?php echo $count; ?>">
					  	Requests from <strong><?php echo $key; ?></strong> (<span class="ticketCount"><?php echo count($ticket); ?></span>)
						</a>
					</h4>
				</div>

				<div id="collapse<?php echo $count; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $count; ?>">
					<div class="panel-body table-responsive">
						<table class="table table-striped">
							<tr>
								<th>Project</th>
								<th width="25%">Description</th>
								<th width="20%">Assets</th>
								<th>Requested Time</th>
								<th width="30%">Actions</th>
							</tr>
							
							<?php foreach($ticket as $value): ?>
							<tr id="ticket_<?php echo $value['Ticket']['id']; ?>">
								<td><?php echo $value['Ticket']['project']; ?></td>
								<td><?php echo $value['Ticket']['project_description']; ?></td>
								<td>
									<?php
										if($value['Ticket']['filename']) {
											$assets = explode(',', $value['Ticket']['filename']);
											foreach($assets as $asset) {
												echo '<a href="/documents/'.$value["Ticket"]["project_folder"].'/'.$asset.'">'.$asset.'</a><br>';
											}
										}
									?>
								</td>

								<td><?php echo $value['Ticket']['requested_time']; ?> hrs (<?php if ($value['Ticket']['billable'] == 1) echo 'Billable'; else echo 'Non Billable'; ?>)</td>

								<td class="actions" id="action_<?php echo $value['Ticket']['id']; ?>">
									<?php if($value['Ticket']['status_id'] == 6) { ?>
										<a class="btn btn-success approve" href="#" role="button" data-id="<?php echo $value['Ticket']['id']; ?>">Approve</a>
										<a class="btn btn-danger decline" href="#" role="button" data-id="<?php echo $value['Ticket']['id']; ?>">Decline</a>
									<?php } else { ?>
										<div class="form-inline">
											<div class="form-group">
												<select class="users form-control" required>
													<option value="">Select User</option>
													<?php foreach($users as $id => $user): ?>
														<option value="<?php echo $id; ?>"><?php echo $user; ?></option>
													<?php endforeach; ?>
												</select>
											</div>

											<div class="form-group date-container">
								                <div class='input-group date datetimepicker'>
								                    <input type='text' class="form-control" />
								                    <span class="input-group-addon">
								                        <span class="glyphicon glyphicon-calendar"></span>
								                    </span>
								                </div>
								            </div>

											<button type="button " class="btn btn-success assign" data-id="<?php echo $value['Ticket']['id']; ?>">Assign</button>
										</div>
									<?php } ?>
								</td>
							</tr>
							<?php endforeach; ?>
						</table>
					</div>
				</div>
			</div>
			<?php $count++; ?>
		<?php endforeach; ?>
	</div>
	<?php } ?>

	<div class="ticket-search-container">
		<input type="text" placeholder="Search Requests" id="searchTicket">
	</div>
	
	<div class="tickets-container">
		<div class="clearfix assigned-projects-head">
			<div class="col-xs-2"><strong>Projects</strong></div>
	    	<div class="col-xs-2"><strong>Assets</strong></div>
	    	<div class="col-xs-2"><strong>Assigned</strong></div>
	    	<div class="col-xs-1"><strong>Requested Time</strong></div>
	    	<div class="col-xs-1"><strong>Time Spent</strong></div>
	    	<div class="col-xs-1"><strong>Status</strong></div>
	    	<div class="col-xs-3"><strong>Actions</strong></div>
	    </div>

		<div class="projectlist assigned-projects">
		</div>
	</div>
</div>
<div id="loader"></div>
<?php echo $this->Html->script(array('tickets-module')); ?>