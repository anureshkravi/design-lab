<div class="ColLeftRight">
    <div class="LeftCol"> 
        <div class="col-xs-12 col-sm-10 col-sm-push-1 main-form margin-top19-per" id="form_Ticket" style="display:none;">
            <div class="tickets form" > 
                <?php echo $this->Form->create('Ticket', array('type' => 'file')); ?>
                    <fieldset>
                        <legend><?php echo __('New Ticket'); ?></legend>
                        <?php
							echo $this->Form->input('name', array('label' => 'Name*', 'div' => 'name-col'));
							echo $this->Form->input('email', array('label' => 'Email*', 'div' => 'email-col'));
							echo $this->Form->input('project', array('label' => 'Project Name*'));
							echo $this->Form->input('project_description');
							echo '<div class="time-container clearfix">';
								echo $this->Form->input('requested_time', array('label' => 'Requested Time (Hrs)*', 'div' => false));
								echo $this->Form->input('time', array('label' => false, 'options' => array(1 => 'Hrs', 8 => 'Days', 40 => 'Week'), 'div' => false, 'class' => 'selectpicker'));
							echo '</div>';
						?>

						<div class="input radio clerafix">
							<div class="pull-left">
								<label for="TicketBillable1">
									<input type="radio" name="data[Ticket][billable]" id="TicketBillable1" value="1" checked="checked"> Billable
								</label>
							</div>

							<div class="nonbillable pull-left">
								<label for="TicketBillable0">
									<input type="radio" name="data[Ticket][billable]" id="TicketBillable0" value="0"> Non Billable
								</label>
							</div>
						</div>

						<?php
							echo $this->Form->input('filename', array('label' => 'Supporting documents', 'type' => 'file', 'multiple', 'name' => 'data[Ticket][filename][]'));
						?>
						<p>For billable tasks supporting documents (SOW, Proposals etc) For non-billable tasks, management approval is required.</p>
					</fieldset>

					<div class="clearfix">
						<?php echo $this->Form->input('Submit', array('type' => 'submit', 'div' => 'submit', 'label' => false)); ?>
						<?php echo $this->Form->button('Cancel', array('type' => 'button', 'id' => 'cancel_new_ticket')); ?>
					</div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>

        <div class="btn-main-top" id="sub_Req_Btn">
            <a type="button" class="btn blue-bc btn-main btn-block">
            	<?php echo $this->Html->image('plus.png'); ?>
            	Submit new request
        	</a>
        </div>
    </div>

    <div class="RightCol">
        <div id="right_login" style="display:none;">
            <?php echo $this->Html->image('comingsoon.png'); ?>
        </div>
        <div class="btn-main-top" id="Trace_Req_Btn">
            <a type="button" class="btn orange-bc btn-main btn-block">
            	<?php echo $this->Html->image('left-arrow.png'); ?>Trace previous requests 
        	</a>
        </div>
    </div>
</div>
