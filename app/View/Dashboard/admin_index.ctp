<div class="row">
    <div class="col-sm-3">
        <div class="tile-stats tile-red">
            <div class="icon"><i class="entypo-users"></i></div>
            <div data-delay="0" data-duration="1500" data-postfix="" data-end="83" data-start="0" class="num">0</div>                    
            <h3>Registered users</h3>
            <p>so far in our website.</p>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="tile-stats tile-green">
            <div class="icon"><i class="entypo-chart-bar"></i></div>
            <div data-delay="600" data-duration="1500" data-postfix="" data-end="135" data-start="0" class="num">135</div>                    
            <h3>Daily Visitors</h3>
            <p>this is the average value.</p>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="tile-stats tile-aqua">
            <div class="icon"><i class="entypo-mail"></i></div>
            <div data-delay="1200" data-duration="1500" data-postfix="" data-end="23" data-start="0" class="num">23</div>                    
            <h3>New Messages</h3>
            <p>messages per day.</p>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="tile-stats tile-blue">
            <div class="icon"><i class="entypo-rss"></i></div>
            <div data-delay="1800" data-duration="1500" data-postfix="" data-end="52" data-start="0" class="num">52</div>                    
            <h3>Subscribers</h3>
            <p>on our site right now.</p>
        </div>

    </div>
</div>