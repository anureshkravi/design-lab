<p>Dear <?php echo $manager; ?>,</p>

<p>Your request for <strong><?php echo $project; ?></strong> has been approved and assigned to <?php echo $userName; ?> (<?php echo $userEmail; ?>).</p>

<p>The task has been scheduled to be started from <strong><?php echo $this->Time->format($startDate, '%B %e, %Y %H:%M %p'); ?></strong></p>

<p>Regards,<br>
Espire Design Lab Team</p>

<p>Note: This is an auto generated mail, please do not reply to this email.</p>
