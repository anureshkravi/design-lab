<p>Dear <?php echo $user; ?>,</p>

<p>You have been assigned a new task in the Design Lab. Follow the <a href="http://designlab.espire.com/admin/tickets">link</a> to open the task.</p>


<p>Regards,<br>
Espire Design Lab Team</p>

<p>Note: This is an auto generated mail, please do not reply to this email.</p>