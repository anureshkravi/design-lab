<p>Dear user,</p>

<p>You have received new comments from <?php echo $clientName; ?> on <?php echo $projectName; ?>.</p>
<p>Follow this <a href="<?php echo $link; ?>">link</a> to view comments.</p>


<p>Regards,<br>
Espire Design Lab Team</p>

<p>Note: This is an auto generated mail, please do not reply to this email.</p>