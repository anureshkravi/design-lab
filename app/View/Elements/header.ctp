<header>
    <div class="container-fluid">
        <div class="row">
            <div class="logo-section pull-left">
                <?php if($this->params->prefix == 'admin') { ?>
                <a href="<?php echo $this->Html->url(array('controller' => 'dashboard', 'action' => 'index')); ?>" class="logo"><?php echo $this->Html->image('inner-logo.png'); ?></a>
                <?php } else { ?>
                    <a href="/" class="logo"><?php echo $this->Html->image('inner-logo.png'); ?></a>
                <?php } ?>
            </div>           

            <?php if( isset($this->params['admin']) && $this->params['admin'] ){ ?>
            <div class="account-section pull-right" style="position: relative;">

                <!-- if admin or team only then show ux requests button-->
                <!-- 'UX Requests button removed, updated on 07-03-2017' --> 
                <!-- 'For Reference code check in google docs project or in backup' -->               

                <a href="#" class="dropdown dropdown-toggle pull-right" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-user"></span>
                    <span class="user-name"><?php echo $this->Html->user('username'); ?></span>
                    <b class="caret"></b>
                </a>

                <ul class="dropdown-menu">
                    <!-- only for admin -->
					<li class="user-name-text"><?php echo $this->Html->user('name'); ?><li>
                    <?php if($this->Html->user('role_id') == 1) { ?>
                            <li><a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'index')); ?>">Users</a></li>
                            <li><a href="<?php echo $this->Html->url(array('controller' => 'categories', 'action' => 'index')); ?>">Categories</a></li>
                            <li><a href="<?php echo $this->Html->url(array('controller' => 'roles', 'action' => 'index')); ?>">Roles</a></li>
                            <li><a href="<?php echo $this->Html->url(array('controller' => 'privileges', 'action' => 'index')); ?>">Privileges</a></li>
                    <?php } ?>

                    <!-- if admin or team only then show ux requests and change password links-->
                    <?php if($this->Html->user('role_id') == 1 || $this->Html->user('role_id') == 2) { ?>
                        <li><a href="<?php echo $this->Html->url(array('controller' => 'tickets', 'action' => 'index')); ?>">UX Requests</a></li>

                        <li><a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'change_password')); ?>">Change Password</a></li>
                    <?php } ?>

                    <hr style="margin: 5px 0 6px 0px;">
                    
                    <?php if($this->Html->user('role_id') == 1) { ?>
                        <li><a href="<?php echo $this->Html->url(array('controller' => 'projectfiles', 'action' => 'archives')); ?>">Archives</a></li>
                        <hr style="margin: 5px 0 6px 0px;">
                    <?php } ?>

                    <li><a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'logout')); ?>">Logout</a></li>
                </ul>
            </div>
            <?php } ?>
        </div>
    </div>
</header>