<aside class="sidebar_hide" id="sidebar">
    <div id="sidebar-trigger">
        <div class="menu-icon">
        	<a href=""><?php echo $this->Html->image('menu.png'); ?></a>
        </div>
    </div>
    <!--End sidebar-trigger-->
    <div class="sidebar-main">
        <!--Logo-->
        <h1 id="logo">
            <a title="Espire-Logo" href="<?php echo $this->Html->url(array('controller' => 'portfolio', 'action' => 'index')); ?>">
            	<?php echo $this->Html->image('inner-logo.png'); ?>
            </a>
        </h1>
        <div class="sidebar-bottom-wrap">
            <div class="copyright">Copyright Espire Design Lab.</div>
            <!--End copyright-->
            <!--WPML-->
        </div>
        <!--END sidebar-bottom-wrap-->
    </div>
    <!--end sidebar-main-->
</aside>