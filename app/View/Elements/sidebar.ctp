<aside class="sidebar_hide" id="sidebar">
    <div id="sidebar-trigger">
        <div class="menu-icon">
        	<a href=""><?php echo $this->Html->image('menu.png'); ?></a>
        </div>
    </div>
    <!--End sidebar-trigger-->
    <div class="sidebar-main">
        <!--Logo-->
        <h1 id="logo">
            <a title="Espire-Logo" href="<?php echo $this->Html->url(array('controller' => 'portfolio', 'action' => 'index')); ?>">
            	<?php echo $this->Html->image('inner-logo.png'); ?>
            </a>
        </h1>
        <!--Menu-->
        <nav class="clearfix" id="navi">
            <div class="menu-demo-menu-container" id="navi_wrap">
                <ul class="menu">
                    <li><a href="<?php echo $this->Html->url(array('controller' => 'portfolio', 'action' => 'index')); ?>" <?php if(!isset($this->params->tag)) echo 'class="active";'; ?>>All</a></li>
                    <?php
                        foreach($tags as $value){
                            echo "<li><a class='";
                            if($this->params->tag == $value['Tag']['title']){
                                echo "active";
                            }
                            echo "' href='".$this->Html->url(array('controller' => 'portfolio', 'action' => 'view', 'tag' => $value['Tag']['title']))."'>".Inflector::humanize($value['Tag']['title']).'</a></li>';
                        }
                    ?>
                </ul>

                <hr>

                <a href="/admin/users/login" class="btn btn-default front-btn">Login</a>
                <a href="/tickets/add" class="btn btn-default ticket-btn">UX Tickets</a>
            </div>
            <!--End #navi_wrap-->
        </nav>
        <div class="sidebar-bottom-wrap">
            <div class="copyright">Copyright Espire Design Lab.</div>
            <!--End copyright-->
            <!--WPML-->
        </div>
        <!--END sidebar-bottom-wrap-->
    </div>
    <!--end sidebar-main-->
</aside>