<div class="efforts view">
<h2><?php echo __('Effort'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($effort['Effort']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ticket'); ?></dt>
		<dd>
			<?php echo $this->Html->link($effort['Ticket']['name'], array('controller' => 'tickets', 'action' => 'view', $effort['Ticket']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($effort['User']['id'], array('controller' => 'users', 'action' => 'view', $effort['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hours'); ?></dt>
		<dd>
			<?php echo h($effort['Effort']['hours']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($effort['Effort']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($effort['Effort']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($effort['Effort']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Effort'), array('action' => 'edit', $effort['Effort']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Effort'), array('action' => 'delete', $effort['Effort']['id']), null, __('Are you sure you want to delete # %s?', $effort['Effort']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Efforts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Effort'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tickets'), array('controller' => 'tickets', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ticket'), array('controller' => 'tickets', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
