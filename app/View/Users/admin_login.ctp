<div class="container-fluid">
    <div class="row">

        <div class="col-xs-12 logo-icon">
            <img src="../../img/logo.png" alt="Espire Design Lab">
        </div>        
        <div class="login-box">
            <!--  <div class="col-lg-4 col-sm-4">
                  <div class="login-box_left">
                      <h1><img src="../../img/logo.png" alt="Espire Design Lab"></h1>
                  </div>
              </div> -->

            <div class="col-sm-12">
                <div class="login-box_right">
                    <h1><img src="../../img/logo-design-lab2.png" alt="Espire Design Lab"></h1>
                    <div class="error-msg"><?php
                        if($this->Session->check('Message.auth')){
                        echo $this->Session->flash('auth');
                        }
                        ?></div>
                    <?php echo $this->Form->create('User'); ?>
                    <div class="form-group">
                        <label>Username</label>
                        <?php echo $this->Form->input('username', array('class' => 'form-control', 'label' => false, 'div' => false, 'autofocus' => 'autofocus')); ?>
                        
                        <label>Password</label>
                        <?php echo $this->Form->input('password', array('class' => 'form-control', 'label' => false, 'div' => false)); ?>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Keep me signed in
                            </label>
                        </div>
                        <table width="100%">
                            <tr>
                                <td>
                                    <button type="submit" class="btn btn-primary">Login</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>