
<link rel="stylesheet" href="/css/image-picker.css">

<script src="/js/image-picker.min.js"></script>


<script type="text/javascript">

    $(document).ready(function () {

        var projFolderArr=[], isSelectAll = false;

        loadprojectsThumbnail();

        function loadprojectsThumbnail(){         

            var filename, projtitle, pfolder, container = $('#selectImage');

            var projectObj = <?php echo json_encode($projects); ?>;            

            var stackObj = <?php echo json_encode($stack); ?>;            
            
            //console.log(">>>>>> thumbObj ::::::::: ",projectObj);
            
            for (var i in stackObj) {

                //console.log(">> ProjectFolder __  : ",projectObj[i]["ProjectFile"]["project_folder"], " : ", projectObj[i]["ProjectFile"]["title"]);

                //console.log(">> PROJFOLDER__ : ",stackObj[i]["projfolder"]," >> TITLE____ : ",stackObj[i]["title"]);

                filename = stackObj[i]["filename"];
                projtitle = stackObj[i]["title"];
                pfolder = stackObj[i]["projfolder"];

                if(filename == ""){
                    filename = "/img/thumb.jpg";
                } else {
                    filename = "/files/groupthumb/"+filename;
                }

                if(projtitle.length > 20){
                    projtitle = projtitle.substring(0,20) + "...";
                }    

                projFolderArr.push(pfolder);    
                container.append($('<option data-img-src="'+filename+'" data-img-label="'+projtitle+'" value="'+pfolder+'">'));
            }

            container.imagepicker({               
                show_label  : true
            });
        }    


        function selectAllImages(isSelectAll) {            

            if(isSelectAll){
                $(".image-picker").val(projFolderArr);        
                for(var k=0; k<=projFolderArr.length; k++){
                    $($(".image_picker_selector li")[k]).find('.thumbnail').addClass('selected');
                    $($(".image_picker_selector li")[0]).find('.thumbnail p').html('Unselect all');
                }    
            } else {
                $(".image-picker").val('');        
                for(var k=0; k<=projFolderArr.length; k++){
                    $($(".image_picker_selector li")[k]).find('.thumbnail').removeClass('selected');
                    $($(".image_picker_selector li")[0]).find('.thumbnail p').html('Select all');
                }    
            }
        }

        $($(".image_picker_selector li")[0]).find('.thumbnail').on('click', function(){            
            if(!isSelectAll){
                selectAllImages(true);
                isSelectAll = true;
            } else {
                selectAllImages(false);
                isSelectAll = false;
            }
        })


    });

</script>    


<div id="main_wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="add-new-project">
                <div class="col-lg-12">
                    <h2>Add New User</h2>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $this->Html->url(array('controller' => 'dashboard', 'action' => 'index')); ?>">Dashboard</a></li>
                        <li>Add New User</li>
                    </ol>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="add-new">
                <div class="col-lg-12">
                    <?php echo $this->Form->create('User'); ?>
                        <?php echo $this->Form->input('name', array('class' => 'form-control', 'div' => 'false')); ?>
                        <?php echo $this->Form->input('username', array('class' => 'form-control', 'div' => 'false')); ?>
                        <?php echo $this->Form->input('password', array('class' => 'form-control', 'div' => 'false')); ?>
                        <?php echo $this->Form->input('role_id', array('empty' => 'Select Role', 'class' => 'form-control', 'div' => 'false')); ?>
                        
                        <div class="false required">                            
                            <label>Projects</label>                                                        
                        </div>

                        <select id="selectImage" name="projects[]" multiple="multiple" class="image-picker show-labels show-html">                           
                            <option data-img-label="Select all" data-img-src="/img/select-all.png"        value="selectall">All</option>
                            <!-- <option data-img-label="Awww2" data-img-src="/files/groupthumb/1461666538_logo.png"               value="3">Book</option>
                            <option data-img-label="Awww3" data-img-src="/files/groupthumb/1408536073_au.jpg"                 value="4">Book</option>                            
                            <option data-img-src="/files/groupthumb/evo-logo.jpg"                      value="31">Book</option>   -->
                        </select>

                        <?php
    						$options = array(
        						'class' => 'btn btn-primary btn-group-lg pull-right',
        					    'label' => false,
        					    'div' => array(
        					        'class' => 'form-group submit'
        					    )
    						);
                            
    				    echo $this->Form->end($options);
    				?>
                </div>
            </div>
        </div>
    </div>
</div>

