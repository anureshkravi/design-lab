<div class="container-fluid">
    <div class="row">
        <?php
            if($this->Session->check('Message.auth')){
                echo $this->Session->flash('auth');
            }
        ?>
                
        <div class="login-box">
            <div class="col-lg-4 col-sm-4">
                <div class="login-box_left">
                    <h1><img src="../../img/logo.png" alt="Espire Design Lab"></h1>
                </div>
            </div>

            <div class="col-lg-8 col-sm-8">
                <div class="login-box_right">
                    <h1>Login to Espire Design Lab</h1>
                    <?php echo $this->Form->create('User'); ?>
                        <div class="form-group">
                            <?php echo $this->Form->input('username', array('class' => 'form-control', 'placeholder' => 'Email', 'label' => false, 'div' => false)); ?>
                            <?php echo $this->Form->input('password', array('class' => 'form-control', 'placeholder' => 'Password', 'label' => false, 'div' => false)); ?>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <button type="submit" class="btn btn-primary pull-right">Sign In&nbsp;<img src="../../img/signin-icon.png"></button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>