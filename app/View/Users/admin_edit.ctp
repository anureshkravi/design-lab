
<!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/image-picker/0.3.0/image-picker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/image-picker/0.3.0/image-picker.min.js"></script> -->
<link rel="stylesheet" href="/css/image-picker.css">
<script src="/js/image-picker.min.js"></script>

<script type="text/javascript">

    $(document).ready(function () {

        var projDirectoryArr=[], isAllSelected = false;

        loadProjectsThumbs();

        function loadProjectsThumbs()
        {         
            //console.log(" ----  loadProjectsThumbs -- ");
            var filename, projtitle, pfolder, container = $('#selectImageEdit'), tmpArr=[];
            var projectObj = <?php echo json_encode($projects); ?>;         
            var stackObj = <?php echo json_encode($stacklist); ?>;         
            var customProjObj = <?php echo json_encode($customUserProjects); ?>;         
            
            //console.log(">>>>>> customProjObj ::::::::: ",customProjObj);          

            for (var i in stackObj) {

                //console.log(">> ProjectFolder __  : ",projectObj[i]["ProjectFile"]["project_folder"], " : ", projectObj[i]["ProjectFile"]["title"]);

                //console.log(">> PROJFOLDER__ : ",stackObj[i]["projfolder"]," >> TITLE____ : ",stackObj[i]["title"]);

                filename = stackObj[i]["filename"];
                projtitle = stackObj[i]["title"];
                pfolder = stackObj[i]["projfolder"];

                if(filename == ""){
                    filename = "/img/thumb.jpg";
                } else {
                    filename = "/files/groupthumb/"+filename;
                }
                if(projtitle.length > 20){
                    projtitle = projtitle.substring(0,20) + "...";
                }    
                projDirectoryArr.push(pfolder);    
                container.append($('<option data-img-src="'+filename+'" data-img-label="'+projtitle+'" value="'+pfolder+'">'));
            }

            container.imagepicker({               
                show_label  : true
            });

            // Show selection of projects which are already assigned to user
            if(customProjObj.length == 1){
                if(customProjObj[0].project_name == "all") {
                    isAllSelected = false;
                    $($(".image_picker_selector li")[0]).find('.thumbnail').trigger('click');
                    selectAllImages(true);
                    isAllSelected = true;
                }
            } else {    
                for(var n in stackObj) 
                {
                    for(var p in customProjObj)
                    {
                        if(customProjObj[p].project_name == stackObj[n]["projfolder"]) {
                            var id = Number(n) + 1;                        
                            $($(".image_picker_selector li")[id]).find('.thumbnail').addClass('selected');                        
                            tmpArr.push(id);                                               
                        }
                    }
                    if(tmpArr.length == customProjObj.length) break;
                }
            }
        };

        function selectAllImages(isAllSelected) {      
            if(isAllSelected){
                $(".image-picker").val(projDirectoryArr);        
                for(var k=0; k<=projDirectoryArr.length; k++){
                    $($(".image_picker_selector li")[k]).find('.thumbnail').addClass('selected');
                    $($(".image_picker_selector li")[0]).find('.thumbnail p').html('Unselect all');
                }    
            } else {
                $(".image-picker").val('');        
                for(var k=0; k<=projDirectoryArr.length; k++){
                    $($(".image_picker_selector li")[k]).find('.thumbnail').removeClass('selected');
                    $($(".image_picker_selector li")[0]).find('.thumbnail p').html('Select all');
                }    
            }
        };

        $($(".image_picker_selector li")[0]).find('.thumbnail').on('click', function(){            
            if(!isAllSelected){
                selectAllImages(true);
                isAllSelected = true;
            } else {
                selectAllImages(false);
                isAllSelected = false;
            }
        });

    });
</script>

<div id="main_wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="add-new-project">
                <div class="col-lg-12">
                    <h2>Edit User</h2>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $this->Html->url(array('controller' => 'dashboard', 'action' => 'index')); ?>">Dashboard</a></li>
                        <li>Edit User</li>
                    </ol>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="add-new">
                <div class="col-lg-12">
                    <?php echo $this->Form->create('User'); ?>
                    	<?php echo $this->Form->input('id'); ?>
                        <?php echo $this->Form->input('name', array('class' => 'form-control', 'div' => 'false')); ?>
                        <?php echo $this->Form->input('username', array('class' => 'form-control', 'div' => 'false')); ?>

                        <?php echo $this->Form->input('role_id', array('empty' => 'Select Role', 'class' => 'form-control', 'div' => 'false')); ?>

                        <div class="false required">
                            <label>Projects</label>                            
                            <select id="selectImageEdit" name="projects[]" multiple="multiple" class="image-picker show-labels show-html">
                                <option data-img-label="Select all" data-img-src="/img/select-all.png" value="selectall">All</option>
                            </select>
                        </div>
                    <?php
						$options = array(
							'class' => 'btn btn-primary btn-group-lg pull-right',
						    'label' => false,
						    'div' => array(
						        'class' => 'form-group submit',
						    )
						);
						echo $this->Form->end($options);
					?>
                </div>
            </div>
        </div>
    </div>
</div>


