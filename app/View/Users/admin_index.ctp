<div id="main_wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="addproject">
                <div class="col-lg-12"> 
                    <a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'add')); ?>" class="btn btn-primary pull-right" data-toggle="tooltip" title="New User" data-placement="left"><span class="glyphicon glyphicon-plus pull-left"></span><span class="pull-left mobile">New User</span></a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="projectlist">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class=" table-responsive">
                        <table class="table table-striped" id="mytooltip">
                            <tr>
                                <th>Name</th>
                                <th>Username</th>
                                <th>Role</th>
                                <th>Status</th>
                                <th>Created</th>
                                <th>Actions</th>
                            </tr>
                            <?php foreach ($users as $user): ?>
                            <tr>
                                <td><?php echo h($user['User']['name']); ?></td>
                                <td><?php echo h($user['User']['username']); ?></td>
                                <td><?php echo h($user['Role']['title']); ?></td>
                                <td id="change_<?php echo $user['User']['id']; ?>_status"><?php echo h($user['Status']['title']); ?></td>
                                <td><?php echo $this->Html->beautifulDate($user['User']['created']); ?></td>
                                <td>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'edit', $user['User']['id'])); ?>" data-toggle="tooltip" title="edit"><?php echo $this->Html->image('edit.png'); ?></a>
                                    <?php
                                        if($user['Status']['id'] == 1){
                                    ?>
                                        <a href="#" class="jquery_action_status" id = 'action_<?php echo $user['User']['id']; ?>_users' rel="2" data-toggle="tooltip" title="publish"><?php echo $this->Html->image('publish.png'); ?></a>
                                    <?php } else {
                                    ?>
                                        <a href="#" class="jquery_action_status" id = 'action_<?php echo $user['User']['id']; ?>_users' rel="1" data-toggle="tooltip" title="publish"><?php echo $this->Html->image('unpublish.png'); ?></a>
                                    <?php } ?>
                                    <a onclick="if (confirm(&quot;Are you sure you want to delete?&quot;)) { return true; } return false;" href="users/delete/<?php echo $user['User']['id']; ?>"><?php echo $this->Html->image('delete.png'); ?></a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.projectlist a').tooltip();
    }); 
</script>