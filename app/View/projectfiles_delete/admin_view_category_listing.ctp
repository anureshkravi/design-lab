<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
  $(function() {
    var availableTags = [<?php echo $finalTags; ?>];

    function split( val ) {
      return val.split( /,\s*/ );
    }

    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( "#projectTags" )
      // don't navigate away from the field on tab when selecting an item
      .bind( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        minLength: 0,
        source: function( request, response ) {
          // delegate back to autocomplete, but extract the last term
          response( $.ui.autocomplete.filter(
            availableTags, extractLast( request.term ) ) );
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
      });
  });
</script>

<div id="main_wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="project-preview">
                <div class="col-lg-12">
                    <h2>Project Preview</h2>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $this->Html->url(array('controller' => 'dashboard', 'action' => 'index')); ?>">Dashboard</a></li>
                        <li>Project Preview</li>
                    </ol>
                </div>
            </div>
        </div>
        <!--project-preview div end-->
        <!--project-thumbnail div start-->
        <div class="row">
            <div class="project-thumbnail">
                <div class="col-lg-3 col-sm-3" style="margin-bottom:25px; max-height:290px;overflow:hidden;">
                    <?php
                        if(!empty($projectFile['ProjectFile']['thumbnail'])){
                            echo $this->Html->image('/files/groupthumb/'.$projectFile['ProjectFile']['thumbnail'], array('class' => 'img-responsive'));
                        } else {
                            echo $this->Html->image('thumb.jpg', array('class' => 'img-responsive'));
                        }
                    ?>
                </div>
                <div class="col-lg-9 col-sm-9">
                    <h1 style="margin:0px;"><?php echo $projectFile['ProjectFile']['title']; ?>
                    <!-- <a href="<?php echo $this->Html->url('/webroot/files/'.$this->params->pass[0]); ?>" target="_blank" style="font-size:14px;">(View Repository)</a></h1> -->
                </div>
            </div>
        </div>
        <!--project-thumbnail div start-->
        <!--project detail start-->
        <div class="row">
            <div class="project-detail">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="table-responsive projectTable">
                        <table class="table" style="margin-bottom:0px;">
                            <tr>
                                <?php
                                    foreach($categories as $value){
                                        echo "<th>".$value."</th>";
                                    }
                                ?>
                            </tr>
                        </table>

                        <?php
                            foreach($categories as $value){
                                echo "<div class='col-lg-12 col-sm-12 space'><table class='table'>";
                                if(count($projects[$value]) > 0){
                                    foreach($projects[$value] as $project){
                                        echo "<tr><td>";
                                        if($project['ProjectFile']['status_id'] == 1){
                                            echo $this->Html->link($this->Html->modifyDate($project['ProjectFile']['date_folder']), array('controller' => 'projectfiles', 'action' => 'viewdetails', 'projectfolder' => $project['ProjectFile']['project_folder'], 'categoryfolder' => $project['Category']['category_folder'], 'datefolder' => $project['ProjectFile']['date_folder']), array('target' => '_blank', 'class' => 'active'));
                                            
                                            echo "<p>".$project['ProjectFile']['project_brief']."</p>";

                                            //for user clients we are hiding publish and edit options
                                            if($this->Html->user('role_id') != 3) {
                                                echo '<div class="pop">';
                                                    echo '<a href="#" rel="2" class="jquery_action_status" id="publish_'.$project["ProjectFile"]["id"].'_projectfiles">'.$this->Html->image('publish.png', array('width' => '17', 'height' => '17')).' Unpublish</a>';

                                                    echo '<a href="#" projectname="'.$this->params->projectfolder.'" categoryname="'.$this->params->categoryfolder.'" id="'.$project["ProjectFile"]["id"].'" tags="'.$project["ProjectFile"]["tags"].'" desc="'.$project["ProjectFile"]["description"].'" title="'.$project["ProjectFile"]["publish_title"].'" class="editProject" data-toggle="modal" data-target="#publishModal">'.$this->Html->image('edit.png', array('width' => '17', 'height' => '17')).' Edit Publish</a>';

                                                    echo '<a href="#" class="editProjectFiles" id="'.$project["ProjectFile"]["id"].'" brief="'.$project["ProjectFile"]["project_brief"].'" data-toggle="modal" data-target="#editModal">'.$this->Html->image('edit-publish.png', array('width' => '17', 'height' => '17'))." <span>Edit</span></a>";

                                                    echo '<a href="'.$this->Html->url(array('controller' => 'projectfiles', 'action' => 'delete', $project['ProjectFile']['id'], $this->params->pass[0], $this->params->pass[1])).'" class="deleteProject hide">'.$this->Html->image('delete.png', array('width' => 17, 'height' => 17)).' Delete</a>';

                                                    echo '<a href="#" rel="4" class="jquery_action_status hide" id="protected_'.$project["ProjectFile"]["id"].'_projectfiles">'.$this->Html->image('unpublish.png', array('width' => '17', 'height' => '17')).' Protected</a>';
                                                echo "</div>";
                                            }
                                        } else {
                                            if($project['ProjectFile']['category_id'] == 5) {
                                                echo '<a href="/files/'.$project['ProjectFile']['project_folder'].'/'.$project['Category']['category_folder'].'/'.$project['ProjectFile']['date_folder'].'/'.$project['ProjectFile']['filename'].'">'.$this->Html->modifyDate($project['ProjectFile']['date_folder']).'</a>';
                                            } else if($project['ProjectFile']['status_id'] == 4) {
                                                        //for protected projects
                                                        echo $this->Html->link($this->Html->modifyDate($project['ProjectFile']['date_folder']), array('controller' => 'projectfiles', 'action' => 'viewdetails', 'projectfolder' => $project['ProjectFile']['project_folder'], 'categoryfolder' => $project['Category']['category_folder'], 'datefolder' => $project['ProjectFile']['date_folder']), array('target' => '_blank', 'class' => 'error'));
                                            } else {
                                                echo $this->Html->link($this->Html->modifyDate($project['ProjectFile']['date_folder']), array('controller' => 'projectfiles', 'action' => 'viewdetails', 'projectfolder' => $project['ProjectFile']['project_folder'], 'categoryfolder' => $project['Category']['category_folder'], 'datefolder' => $project['ProjectFile']['date_folder']), array('target' => '_blank'));
                                            }
                                            echo "<p>".$project['ProjectFile']['project_brief']."</p>";

                                            //for user clients we are hiding unpublish option
                                            if($this->Html->user('role_id') != 3) {
                                                echo '<div class="pop">';
                                                    if($project['ProjectFile']['category_id'] != 5) {
                                                        echo '<a href="#" class="publishProject" id="'.$project["ProjectFile"]["id"].'" projectname="'.$this->params->projectfolder.'" categoryname="'.$this->params->categoryfolder.'" data-toggle="modal" data-target="#publishModal">'.$this->Html->image('unpublish.png', array('width' => '17', 'height' => '17'))." Publish</a>";
                                                    }

                                                    echo '<a href="'.$this->Html->url(array('controller' => 'projectfiles', 'action' => 'delete', $project['ProjectFile']['id'], $this->params->pass[0], $this->params->pass[1])).'" class="deleteProject">'.$this->Html->image('delete.png', array('width' => 17, 'height' => 17)).' Delete</a>';

                                                    echo '<a href="#" class="editProjectFiles" id="'.$project["ProjectFile"]["id"].'" brief="'.$project["ProjectFile"]["project_brief"].'" data-toggle="modal" data-target="#editModal">'.$this->Html->image('edit-publish.png', array('width' => '17', 'height' => '17'))." Edit</a>";

                                                    //for status protected/unprotected
                                                    if($project['ProjectFile']['status_id'] == 4) {
                                                        echo '<a href="#" rel="2" class="jquery_action_status" id="protected_'.$project["ProjectFile"]["id"].'_projectfiles">'.$this->Html->image('publish.png', array('width' => '17', 'height' => '17')).' Unprotected</a>';
                                                    } else {
                                                        echo '<a href="#" rel="4" class="jquery_action_status" id="protected_'.$project["ProjectFile"]["id"].'_projectfiles">'.$this->Html->image('unpublish.png', array('width' => '17', 'height' => '17')).' Protected</a>';
                                                    }
                                                echo "</div>";
                                            }
                                        }
                                        echo "</td></tr>";
                                    }
                                } else {
                                    echo "<tr><td>&nbsp;</td></tr>";
                                }
                                echo "</table></div>";
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Publish Modal -->
<div class="modal fade" id="publishModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><strong>Publish Project</strong></h4>
      </div>

      <form id="" action="/admin/projectfiles/publish" method="post" enctype="multipart/form-data">
          <div class="modal-body">
                <div class="false required">
                    <label>Title</label>
                    <input type="text" id="projectPublishTitle" class="form-control" name="data[ProjectFile][publish_title]" required="required">
                </div>

                <div class="false required">
                    <label>Thumbnail</label>
                    <input type="file" class="form-control" name="data[ProjectFile][thumbnail]">
                </div>

                <div class="false required">
                    <label class="radio-inline">
                      <input type="radio" name="data[ProjectFile][class]" id="inlineRadio1" value="filter_portfolio filter_uncategorized width-and-small isotope-item  brick-with-img all_cat" checked="checked"> 
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="data[ProjectFile][class]" id="inlineRadio2" value="filter_blog filter_branding filter_portfolio filter_uncategorized width-and-height isotope-item  brick-with-img all_cat1"> 
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="data[ProjectFile][class]" id="inlineRadio3" value="filter_blog filter_branding filter_portfolio filter_uncategorized width-and-long isotope-item  brick-with-img all_cat2">
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="data[ProjectFile][class]" id="inlineRadio3" value="filter_blog filter_photography-2 filter_portfolio filter_slider filter_uncategorized width-and-big isotope-item  brick-with-img all_cat3">
                    </label>
                </div> <br>

                <div class="false required">
                    <label>Cover Image</label>
                    <input type="file" class="form-control" name="data[ProjectFile][coverimage]">
                </div>

                <div class="false required">
                    <label>Description</label>
                    <textarea class="form-control" id="projectDescription" name="data[ProjectFile][description]"></textarea>
                </div>

                <div class="false required tagContainer">
                    <label>URL</label>
                    <input type="text" name="data[ProjectFile][url]" class="form-control">
                </div>

                <div class="false required tagContainer">
                    <label>Tags</label>
                    <input type="text" id="projectTags" required="required" name="data[Tag][title]" class="form-control">
                </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-primary" value="Submit">
          </div>
      </form>
    </div>
  </div>
</div>


<!-- Edit Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><strong>Edit Project</strong></h4>
      </div>

      <form id="" action="/admin/projectfiles/edit" method="post" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="false">
                    <label>Select file to upload</label>
                    <input type="file" name="data[ProjectFile][filename]" id="ProjectFileFilename" style="min-height: 90px;">
                </div>

                <div class="false">
                    <label>Brief Description</label>
                    <input name="data[ProjectFile][project_brief]" class="form-control" maxlength="500" type="text" id="ProjectFileProjectBrief">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>
      </form>
    </div>
  </div>
</div>