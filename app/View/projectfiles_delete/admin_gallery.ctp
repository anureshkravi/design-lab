<div class="needmeCreative">
    <a id="help" href="#" title="Help"><img alt="" src="/img/help.png"></a>
</div>
<div class="circleAniText">Whenever you need me again click here<img class="flip" alt="" src="/img/left-arrow.png"></div>
<div class="circleAni"></div>

<noscript>
	<style>
		.es-carousel ul{
			display:block;
		}
	</style>
</noscript>

<script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">	
	<div class="rg-image-wrapper">
		{{if itemsCount > 1}}
			<div class="rg-image-nav">
				<a href="#" class="rg-image-nav-prev">Previous Image</a>
				<a href="#" class="rg-image-nav-next">Next Image</a>
			</div>
		{{/if}}
		<div class="rg-image"></div>
		<div class="rg-loading"></div>
		<div class="rg-caption-wrapper">
			<div class="rg-caption" style="display:none;">
				<p></p>
			</div>
		</div>
	</div>
</script>

<div class="gallery-container">
	<div id="rg-gallery" class="rg-gallery">

		<div id="notification">
			<span class="CommentText">Send Notification</span>
			<span class="glyphicon glyphicon-envelope"></span>
		</div>
		<div id="downloadPDF" class="">
			<span class="CommentText">
			<?php echo $this->Html->link('Save as Pdf', array('controller' => 'projectfiles', 'action' => 'download_pdf',$projectfiles['ProjectFile']['project_folder'],$projectfiles['Category']['category_folder'], $projectfiles['ProjectFile']['date_folder']),array('target' => '_blank')); ?>
			</span>
		</div>
		<div class="commentsView">
			<div id="hideComments">
				<span class="CommentText">Hide Comments</span>
				<span class="glyphicon glyphicon-comment"></span>
			</div>

			<div id="showComments">
				<span class="CommentText">Show Comments</span>
				<span class="glyphicon glyphicon-comment"></span>
			</div>
		</div>

		<div class="rg-thumbs">
			<!-- Elastislide Carousel Thumbnail Viewer -->
			<div class="es-carousel-wrapper">
				<div class="es-nav">
					<span class="es-nav-prev">Previous</span>
					<span class="es-nav-next">Next</span>
				</div>
				<div class="es-carousel">
					<ul class="clearfix">
						<?php $count=1; ?>
						<?php foreach($files[1] as $image): ?>
							<?php if(substr(strrchr($image,'.'),1) == 'jpg' || substr(strrchr($image,'.'),1) == 'png') { ?> 
								<li><a href="#" id="image<?php echo $count; ?>" data-project="<?php echo $projectfiles['ProjectFile']['id']; ?>">
								<?php
									$dir = new Folder(WWW_ROOT.'/'.$path);
									$files = $dir->read();
									if(isset($files[0]) && in_array('thumbs', $files[0])) {
										echo $this->Html->image('/'.$path.'/thumbs/'.$image, array('data-large' => '/'.$path.'/'.$image, 'alt' => '', 'data-description' => '', 'width' => 65, 'height' => 65));
									} else {
										echo $this->Html->image('/'.$path.'/'.$image, array('data-large' => '/'.$path.'/'.$image, 'alt' => '', 'data-description' => '', 'width' => 65, 'height' => 65));
									}
									echo "<span>".$count."</span>";
								?>
								</a></li>
							<?php } ?>
							<?php $count++; ?>
						<?php endforeach; ?>
						<li></li>
					</ul>
				</div>
			</div>
			<!-- End Elastislide Carousel Thumbnail Viewer -->
		</div><!-- rg-thumbs -->
	</div><!-- rg-gallery -->
</div><!-- gallery-conatiner -->


<div class="name-container">
    <h3>Please provide your details:</h3>
    <p>We need your basic details to identify you. This will be your display name.</p>
    <form id="name-form">
        <div class="form-group">
            <label>Enter your name</label>
            <input type="text" id="name" required="required">
        </div>
        <div class="form-group">
            <label>Enter your email</label>
            <input type="email" id="email" required="required">
        </div>
        <input type="submit" value="Submit">
        <a href="#" id="cancelCommenting" class="cancelCommenting">Cancel</a>
    </form>
</div>

<div class="comments-container">
    <div class="user-comments">
    </div>

    <div class="comment-area">
        <form id="comment-form">
            <label>Enter your comments:</label>
                    
            <div id="commentusername"></div>

            <p class="hover-tooltip-msg">
                <a href="#" data-toggle="tooltip" data-placement="left" title="This is your display name. You can anytime edit it by clicking here." class="username-tooltip"></a> 
            </p>

            <!-- <button id="comment-box-tooltip" type="button" class="btn btn-default  btn-xs" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></button> -->


            <textarea id="comment" required="required"></textarea>
            <input type="submit" id="addComment" value="Save">
            <a href="#" id="cancelEmailCred" class="cancelCommenting">Cancel</a>
            <!-- <input href="#" type="button" id="usrCommentCancel" class="btn-hiddencancel btnCommentsCancel"  value="Cancel">  -->
            <?php echo $this->Html->image('close.png', array('id' => 'closeCommentBox')); ?>
        </form>
    </div>
</div>

<div class="notification-container">
	<?php echo $this->Html->image('close.png', array('id' => 'closeNotificationBox')); ?>
    <div class="notification-content">
    	<h3 class="text-success"></h3>
        <h4>Notify the project team when you are done commenting!</h4>
        <p class="notify-text">
        	<label>
        		<input type="checkbox" id="notifyStatus"> Notify me back when someone replies!
        	</label>
        </p>
        <button type="button" id="notify" class="btn btn-success" value="<?php echo $projectfiles['ProjectFile']['id']; ?>">Notify</button>
    </div>
    <?php echo $this->Html->image('arrow-down.png', array('class' => 'notify-arrow')); ?>
</div>

<!--START OVERLAY-->
<div class="overLay">
    <!--start slid 1 [Design Slide]-->
    <div id="slid1">
        <div class="creativeContainer">
            <div class="galleryinformation" >

                <h3>Your design slides</h3>
                <img src="/img/bottom-arrow.png" class="flip" alt="" />
            </div> <!-- End top left gallery Information --> 

            <div class="enterName">
                <img src="/img/circle.png" alt="Enter your name" />
            </div> <!-- End center image enterName-->

        </div>    
    </div><!--end Slid 1 -->


    <!--start Slid 2 [Comment Slide 1]-->
    <div id="slid2" >
        <div class="creativeContainer" style="display:none;">

            <div class="enterNamearrow" >
                <h3>Click anywhere on the screen &amp; enter your comments</h3>
                <img src="/img/top-arrow.png" class="flip" alt="" />
            </div> <!-- End left comment enterNamearrow --> 

            <div class="enterName">
                <img src="/img/enter_comment.png" alt="Enter your name" />
            </div> <!-- End center image enterName-->

            <div class="closeBox" >
                <img src="/img/top-arrow.png" alt="" />
                <h3>Click to close comment box</h3>
            </div> <!-- End right comment closeBox-->

        </div>    
    </div><!--end Slid 2-->

    <!--start Slid 3 [Comment Slide 2]-->
    <div id="slid3" >
            <div class="creativeContainer" style="display:none;">

                <div class="enterNamearrow" style="margin-top:206px;" >
                    <h3>As soon as you start entering your comment it will ask your name and email id for the first time. <br>
                        Please enter valid name and email address.</h3>
                    <img src="/img/top-arrow.png" class="flip" alt="" />
                </div> <!-- End left comment enterNamearrow -->  

                <div class="enterName">
                    <img src="/img/enter_name.png" alt="Enter your name" />
                </div> <!-- End center image enterName-->

            </div>
    </div><!--end Slid 3-->


    <!--start Slid 4 [Notify Slide]-->
    <div id="slid4" >
        <div class="creativeContainer" style="display:none;">

            <div class="notifyArrow" >
                <h3>Click to notify button when you are done commenting</h3>
                <img src="/img/top-arrow.png" class="flip" alt="" />
            </div><!-- End left comment notifyArrow -->  

            <div class="notifyImg">
                <img src="/img/notify.png" alt="Enter your name" />
            </div> <!-- End center image enterName-->

            <div id="notification" style="display: block;" class="active">
                <span class="CommentText">Send Notification</span>
                <span class="glyphicon glyphicon-envelope"></span>
            </div><!-- End bottom send Notification-->

        </div>
    </div><!--end Slid 4-->


    <!--start Slid 5 [View Comment Slide]-->
    <div id="slid5" >
        <div class="creativeContainer" style="display:none;">

            <div class="enterNamearrow" style="margin:286px 0 0;" >
                <img src="/img/top-arrow.png" class="flip" alt="" />
                <h3>Click to view your comments</h3>
            </div><!-- End left top comment veiw -->  

            <div class="enterNamearrow" style="margin:462px 0 0" >
                <img src="/img/top-arrow.png" class="flip" alt="" />
                <h3>Click to enter your new comments</h3>
            </div><!-- End left bottom comment veiw -->

            <div class="enterName">
                <img src="/img/add_comment.png" alt="Enter your name" />
            </div><!-- End center image enterName-->

        </div>
    </div><!--end Slid 5-->


    <!--start Slid 6 [Remove Comment Slide]-->
    <div id="slid6" >
        <div class="creativeContainer" style="display:none;">

            <div class="enterName">
                <img src="/img/remove_comment.png" alt="Enter your name" />
            </div><!-- End center image enterName-->

            <div class="closeBox" style="margin-top:366px;" >
                <img src="/img/top-arrow.png" alt="" />
                <h3>If you want to remove your comment hover over your comment and click "remove"</h3>
            </div><!-- End right remove close-->

        </div>
    </div><!--end Slid 6-->

    <!--start Slid 7 [Hide and Show Comment Slide]-->
    <div id="slid7" >
        <div class="creativeContainer" style="display:none;">

            <div class="hideCommtArrow" >
                <h3>Click to hide &amp; show comments</h3>
                <img src="/img/bottom-arrow.png"  alt="" />
            </div><!-- End top new comment --> 

            <div class="enterName">
                <div class="commentsView">
                    <div id="hideComments">
                        <span class="CommentText">Hide Comments</span>
                        <span class="glyphicon glyphicon-comment"></span>
                    </div>
                    <div id="showComments">
                        <span class="CommentText">Show Comments</span>
                        <span class="glyphicon glyphicon-comment"></span>
                    </div>
                </div>
            </div> <!-- End show hide bottome enterName -->

        </div>
    </div><!--end Slid 7-->

    <!--start Slid 8 [Hide and Show Gallery Frame Slide]-->
    <div id="slid8" >
        <div class="creativeContainer" style="display:none;">
            
            <div class="hideCommtArrow" style="right:112px;">
                <h3>Click to hide the thumbnail and preview the page without any interruption</h3>
                <img src="/img/bottom-arrow.png"  alt="" />
            </div> <!-- End top comment hidecommentArrow --> 

            <div class="galleryBox"></div><!-- End gallerArea --> 

            <div class="enterName">
                <div class="rg-view">
                    <span id="frameTextAppend">Hide frame</span>
                    <a class="rg-view-full" href="#"></a>
                    <a style="display:none;" class="rg-view-thumbs rg-view-selected" href="#"></a>
                </div>
            </div> <!-- End bottom hide frame enterName-->

        </div>
    </div><!--end Slid 8-->

    <!--start Slid 9 [Hiding NextButton & Showing ReplayButton]-->
    <div id="slid9">
        <div class="creativeContainer" style="display:none;">

            <div class="GalleryLeftRightBtn" >
                <h3>Press left and right arrow to scroll through all the designs</h3>
                <img src="/img/arrows.gif"  alt="" />
            </div><!-- End right top comment and image veiw -->    

            <div class="galleryImages" >
                <h3>Click to open full screen design</h3>
                <img src="/img/bottom-arrow.png"  alt="" />
            </div><!-- End left top comment veiw -->
            <div class="galleryBox"></div><!-- End galler Area--> 

        </div>
    </div><!--End  Slid 9-->   


    <!-- Globally Button top of the overlay -->
    <div class="container-fluid">
        <div class="col-md-12">

            <div class="row">
                <p><button type="button" id="skip" class="skip pull-right" >Skip <img src="/img/skip.png" alt=""  /></button></p>
            </div>
            
            <div class="row">
                <p>
                    <button type="button" id="ReplayButton" class="tranBtn tran radius2 yellowBtn pull-right" style="display:none;" >
                        Replay<img src="/img/replay.png" alt=""/>
                    </button>
                    <button type="button" id="NextButton" class="tranBtn tran radius2 orgBtn pull-right" > Next
                        <img src="/img/next.png" alt=""/>
                    </button>

                    <button type="button" id="PrevButton" class="tranBtn tran radius2 orgBtn pull-right" style="display:none;" > 
                        <img src="/img/next.png" alt="" class="flip"/> Previous
                    </button>

                    <button type="button" id="gotit" class="tranBtn tran radius2 greenBtn pull-right" style="margin-right:35px;" > Ok got it!
                        <img src="/img/ok.png" alt=""/>
                    </button>
                </p>
            </div>
        </div>
    </div>    
</div><!--END OVERLAY-->