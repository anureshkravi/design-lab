<div id="main_wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="add-new-project">
                <div class="col-lg-12">
                    <h2>Add New Project</h2>
                    <ol class="breadcrumb">
                        <li><a href="dashboard.html">Dashboard</a></li>
                        <li><a href="add-project.html">Add New Project</a></li>
                    </ol>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="add-new">
                <div class="col-lg-12">
                    <?php echo $this->Form->create('ProjectFile', array('type' => 'file')); ?>
                    	<?php echo $this->Form->input('id'); ?>
                        <?php echo $this->Form->input('title', array('class' => 'form-control', 'label' => 'Project Name', 'div' => 'false', 'placeholder' => 'Project name 001')); ?>

                        <label>Select File to Upload</label>
                        <div class="file-area">
                            <div class="drop-area">
                                <?php echo $this->Form->input('filename', array('type' => 'file', 'label' => false, 'div' => false)); ?>
                            </div>
                            <div class="btn-group">
                                <div class="row">
                                    <?php 
									if(isset($categories)) {
										foreach ($categories as $category): ?>
											<div class="col-lg-3 col-xs-6 col-sm-3">
												<input type="radio" name="data[ProjectFile][category_id]" id="ProjectFileCategoryId<?php echo $category['Category']['id']; ?>" class="css-checkbox" value="<?php echo $category['Category']['id']; ?>" <?php if($category['Category']['id'] == $this->request->data['ProjectFile']['category_id']) echo "checked"; ?> />
												<label for="ProjectFileCategoryId<?php echo $category['Category']['id']; ?>" class="css-label"><?php echo $category['Category']['title']; ?></label>
											</div>
										<?php 
										endforeach;
									}
									?>
                                </div>
                            </div>
                        </div>
                        <?php
                            $options = array(
                                'class' => 'btn btn-primary btn-group-lg pull-right',
                                'label' => false,
                                'div' => array(
                                    'class' => 'form-group submit',
                                )
                            );
                            echo $this->Form->end($options);
                        ?>
                </div>
            </div>
        </div>
    </div>
</div>
