<?php echo $this->Html->script(array('jquery-3.1.1.min', 'jquery-ui.min')); ?>
<script type="text/javascript">
    $(document).ready(function(){                                                 
        $(function() {
            $("#contentLeft ul").sortable({ opacity: 1, cursor: 'move', update: function() {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings'; 
                $.post("<?php echo $this->Html->url(array('controller' => 'projectfiles', 'action' => 'recordListing')); ?>", order, function(theResponse){
                    $("#contentRight").html(theResponse);
                });                                                           
            }                                 
            });
        });
    }); 
</script>

<div id="main_wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="add-new-project">
                <div class="col-lg-12">
                    <h2>Sort Project</h2>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $this->Html->url(array('controller' => 'dashboard', 'action' => 'index')); ?>">Dashboard</a></li>
                        <li>Sort Project</li>
                    </ol>
                    <div id="contentLeft">
                        <ul>
                            <?php foreach($projectFiles as $project): ?>
                                <li id="<?php echo 'recordsArray_'.$project['ProjectFile']['id']; ?>"><?php echo $project["ProjectFile"]["publish_title"]; ?></li>
                            <?php endforeach; ?>
                            <div class="clearfix"></div>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>