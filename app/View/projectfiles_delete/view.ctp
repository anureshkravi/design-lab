<div id="main_wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="project-preview">
                <div class="col-lg-12">
                    <h2>Project Preview</h2>
                </div>
            </div>
        </div>
        <!--project-preview div end-->
        <!--project-thumbnail div start-->
        <div class="row">
            <div class="project-thumbnail">
                <div class="col-lg-3 col-sm-3" style="margin-bottom:25px; max-height:290px;overflow:hidden;">
                	<?php
                		if(!empty($projectFile['ProjectFile']['thumbnail'])){
                			echo $this->Html->image('/files/groupthumb/'.$projectFile['ProjectFile']['thumbnail'], array('class' => 'img-responsive'));
                		} else {
                			echo $this->Html->image('thumb.jpg', array('class' => 'img-responsive'));
                		}
                	?>
                </div>
                <div class="col-lg-9 col-sm-9">
                    <h1 style="margin:0px;"><?php echo $projectFile['ProjectFile']['title']; ?></h1>
                </div>
            </div>
        </div>
        <!--project-thumbnail div start-->
        <!--project detail start-->
        <div class="row">
			<div class="project-detail">
			    <div class="col-lg-12 col-md-12 col-sm-12">
			        <div class="table-responsive projectTable">
                        <div class="resp">
    			            <table class="table" style="margin-bottom:0px; min-width:850px;">
    			                <tr>
    			                    <?php
    									foreach($categories as $value){
    										echo "<th>".$value."</th>";
    									}
    								?>
    			                </tr>
    			            </table>

                            <div style="min-width:850px;">
        			            <?php
        							foreach($categories as $key => $value){
                                        if($key == 2) {
                                            $class = 'wireFramLatestPro';
                                        } else if($key == 3) {
                                            $class = 'creativeLatestPro';
                                        } else if($key == 1) {
                                            $class = 'htmlLatestPro oldHistoryPro';
                                        } else {
                                            $class = 'assetsLatestPro';
                                        }

        								echo "<div class='col-lg-3 col-sm-3 col-xs-3 space'><table class='table ".$class."'>";
                                        if(count($projects[$value]) > 0){
                                            foreach($projects[$value] as $project){
                                                echo "<tr><td>";
                                                if($project['ProjectFile']['category_id'] == 5) {
                                                    echo '<a href="/files/'.$project['ProjectFile']['project_folder'].'/'.$project['Category']['category_folder'].'/'.$project['ProjectFile']['date_folder'].'/'.$project['ProjectFile']['filename'].'">'.$this->Html->modifyDate($project['ProjectFile']['date_folder']).'</a>';
                                                } else {
                                                    echo $this->Html->link($this->Html->modifyDate($project['ProjectFile']['date_folder']), array('controller' => 'projectfiles', 'action' => 'viewdetails', 'projectfolder' => $project['ProjectFile']['project_folder'], 'categoryfolder' => $project['Category']['category_folder'], 'datefolder' => $project['ProjectFile']['date_folder']), array('target' => '_blank'));
                                                }
                                                echo "<p>".$project['ProjectFile']['project_brief']."</p>";
                                                echo "</td></tr>";
                                            }
                                        } else {
                                            echo "<tr><td></td></tr>";
                                        }
        								echo "</table></div>";
        							}
        						?>
                            </div>
                        </div>
			        </div>
			    </div>
			</div>
        </div>
    </div>
</div>

<div class="overLay">
    <!-- Globally Button top of the overlay -->
    <div class="container-fluid">
        <div class="col-md-12">

            <div class="row">
                <p><button type="button" id="skip" class="skip pull-right" >Skip <img src="/img/skip.png" alt=""  /></button></p>
            </div>
            
            <div class="row">
                <p>
                    <button type="button" id="replay" class="tranBtn tran radius2 yellowBtn pull-right" style="display:none;" >
                    Replay <img src="/img/replay.png" alt=""/>
                    </button>
                    <button type="button" id="next" class="tranBtn tran radius2 orgBtn pull-right" > Next
                        <img src="/img/next.png" alt=""/>
                    </button>

                    <button type="button" id="Prev" class="tranBtn tran radius2 orgBtn pull-right" style="display:none;" > 
                        <img src="/img/next.png" alt="" class="flip"/> Previous
                    </button>

                    <button type="button" id="gotit" class="tranBtn tran radius2 greenBtn pull-right" style="margin-right:35px;" > Ok got it!
                        <img src="/img/ok.png" alt=""/>
                    </button>
                </p>
            </div>
        </div>
    </div>

    <!--start Slide 1-->
    <div id="slide1" class="slidecontainer" >
        <div class="mainContainer">
            <div class="wireFrame framePosit">

                <div class="project-detail" style="margin:0;">
                    <table style="margin-bottom:0px; " class="table">
                        <tbody>
                            <tr>
                                <th>Wireframes</th>
                            </tr>
                        </tbody>
                    </table>
                </div> <!-- End wire frame head -->

                <div class="project-detail" style="margin:0;">
                    <table class="table wireframLatestproApp" style="margin-bottom:0px; display:none;">
                        <tbody>
                            <tr>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- End wire frame latest Pro -->
            </div>

            <div class="topwire" >
                <img src="/img/top-arrow.png" alt="" />
                <h3>Your wireframe section</h3>
                <div class="clearfix"></div>
                <div class="topwireLeft" style="margin: 32px 0px 0px; display:none;" >
                    <img src="/img/top-arrow.png" alt="" />
                    <h3>Here is your latest version<br>of wire frame</h3>
                </div> <!-- End wire frame latest Pro arrow -->
            </div> <!-- End wire frame head arrow -->

            
        </div> <!-- end main container -->
    </div><!--end Slide 1-->

    <!--start Slide 2-->
    <div id="slide2" class="slidecontainer" style="display:none;">
        <div class="mainContainer">
            <div class="cretFrame framePosit">
                <div class="project-detail" style="margin:0;">
                    <table style="margin-bottom:0px; " class="table">
                        <tbody>
                            <tr>
                                <th>Creatives</th>
                            </tr>
                        </tbody>
                    </table>
                </div> <!-- End creative frame head -->

                <div class="project-detail" style="margin:0;">
                    <table class='table creativeLatestProApp' style="margin-bottom:0; display:none;">
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                </div> <!-- End creative frame Latest -->
            </div>
            
            <div class="topwire" style="margin:0 0 0 52%;" >
                <img src="/img/top-arrow.png" alt="" />
                <h3>Your creatives section</h3>

                <div class="topwireLeft" style="margin:32px 0 0 0; display:none;">
                    <img src="/img/top-arrow.png" alt="" />
                    <h3>Here is your latest version<br>of creatives</h3>
                </div> <!-- End creative frame Latest arrow -->

            </div> <!-- End creative frame head arrow-->


        </div> <!-- end main container -->
    </div><!--end Slide 2-->

    <!--start Slide 3-->
    <div id="slide3" class="slidecontainer" style="display:none;">
        <div class="mainContainer">
            

            <div class="htmlMockupText">
                <div class="htmlMockupHead">
                    <h3>Your html section</h3>
                    <img src="/img/top-arrow.png" alt="" class="flip" />
                </div>
                <div class="htmlMockupChild" style="display:none;" >
                    <h3>Here is your latest version of html</h3>
                    <img src="/img/top-arrow.png" alt="" class="flip" />
                </div>
            </div><!-- End Html frame head arrow-->

            <div class="htmlFrame framePosit">
                            
                            <div class="project-detail" style="margin:0;">
                                <table style="margin-bottom:0px; " class="table">
                                    <tbody>
                                        <tr>
                                            <th>HTML</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> <!-- End Html frame head -->
                            
                            <div class="project-detail" style="margin:0;">
                                <table class="table htmlLatestProApp" style="margin-bottom:0px; display:none;">
                                    <tbody>
                                        <tr>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> <!-- End Html letest Pro -->
            </div>
            
        </div> <!-- end main container -->
    </div><!--end Slide 3-->

    <!--start Slide 4-->
    <div id="slide4" class="slidecontainer" style="display:none;">
        <div class="mainContainer">

            <div class="assestMockupText">
                <div class="assestMockupHead">
                    <h3>Your assets section</h3>
                    <img src="/img/top-arrow.png" alt="" class="flip" />
                </div>
                <div class="assestMockupChild" style="display:none;" >
                    <h3>Here is your latest version<br>of assets</h3>
                    <img src="/img/top-arrow.png" alt="" class="flip" />
                </div>
            </div><!-- End Html frame head arrow-->

             <div class="asetFrame framePosit">
                
                <div class="project-detail" style="margin:0;">
                    <table style="margin-bottom:0px; " class="table">
                        <tbody>
                            <tr>
                                <th>Assets</th>
                            </tr>
                        </tbody>
                    </table>
                </div> <!-- End Assets Head --> 

                <div class="project-detail" style="margin:0;">
                    <table class="table assetsLatestProApp" style="margin-bottom:0px; display:none;">
                        <tbody>
                            <tr>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div> <!-- End Assests Latest Pro -->
            </div>
        </div> <!-- end main container -->
    </div><!--end Slide 4-->

    <!--start Slide 5-->
    <div id="slide5" class="slidecontainer" style="display:none;">
        <div class="mainContainer" style="margin-top:170px;">

            <div class="htmlMockupText">
                <div class="oldMockupChild" style="display:none;" >
                     <h3>Here is your version history</h3>
                    <img src="/img/top-arrow.png" alt="" class="flip" />
                </div>
            </div><!-- End Html frame head arrow-->

            <div class="framePosit oldhist">
                <div class="project-detail">
                    <table class="table oldHistoryProApp" style="margin:0; display:none;"></table>
                </div> <!-- End Old History -->
            </div>

        </div> <!-- end main container -->
    </div><!--End  Slide 5-->   
</div><!--end Overlay-->

<?php echo $this->Html->script(array('jquery.cookie', 'overlay-project')); ?>