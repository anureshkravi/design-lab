<div id="main_wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="addproject">
                <div class="col-lg-12"> 
                    <a href="<?php echo $this->Html->url(array('controller' => 'roles', 'action' => 'add')); ?>" class="btn btn-primary pull-right" data-toggle="tooltip" title="New Role" data-placement="left"><span class="glyphicon glyphicon-plus pull-left"></span><span class="pull-left mobile">New Role</span></a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="projectlist">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class=" table-responsive">
                        <table class="table table-striped" id="mytooltip">
                            <tr>
                                <th>Role</th>
                                <th>Actions</th>
                            </tr>
                            <?php foreach ($roles as $role): ?>
                            <tr>
                                <td width="90%"><?php echo h($role['Role']['title']); ?></td>
                                <td>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'roles', 'action' => 'edit', $role['Role']['id'])); ?>" data-toggle="tooltip" title="edit"><?php echo $this->Html->image('edit.png'); ?></a>
                                    <?php
                                        if($role['Status']['id'] == 1){
                                    ?>
                                        <a href="#" class="jquery_action_status" id = 'action_<?php echo $role['Role']['id']; ?>_roles' rel="2" data-toggle="tooltip" title="publish"><?php echo $this->Html->image('publish.png'); ?></a>
                                    <?php } else {
                                    ?>
                                        <a href="#" class="jquery_action_status" id = 'action_<?php echo $role['Role']['id']; ?>_roles' rel="1" data-toggle="tooltip" title="publish"><?php echo $this->Html->image('unpublish.png'); ?></a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.projectlist a').tooltip();
    }); 
</script>
