<div id="main_wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="add-new-project">
                <div class="col-lg-12">
                    <h2>Edit Role</h2>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $this->Html->url(array('controller' => 'dashboard', 'action' => 'index')); ?>">Dashboard</a></li>
                        <li>Edit Role</li>
                    </ol>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="add-new">
                <div class="col-lg-12">
                    <?php echo $this->Form->create('Role'); ?>
                        <?php 
                        	echo $this->Form->input('id');
                        	echo $this->Form->input('title', array('class' => 'form-control', 'div' => 'false')); 
                        ?>
                    <?php
						$options = array(
							'class' => 'btn btn-primary btn-group-lg pull-right',
						    'label' => false,
						    'div' => array(
						        'class' => 'form-group submit',
						    )
						);
						echo $this->Form->end($options);
					?>
                </div>
            </div>
        </div>
    </div>
</div>
