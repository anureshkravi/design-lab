<div id="main-wrap">
    <div class="row-fluid">
        <div class="" id="content_wrap">
            <div class="content-wrap-inn ">
                <div class="pagebuilder-wrap">
                    <div class="fullwrap_moudle">
                        <div class="row-fluid">
                            <div data-speed="19" style="background-position: 50% 0px;" class="fullwidth-wrap bg-theme-color-10 parallax " id="">
                                <div class="row-fluid">
                                    <div style="" class="span12 moudle   bottom-space-no ">
                                        <!--Portfolio isotope-->
                                        <div class="row-fluid">
                                            <div class="container-isotope ">
                                                <div id="isotope-load" style="display: none;">
                                                    <div class="ux-loading"></div>
                                                    <div class="ux-loading-transform">
                                                        <div class="loading-dot1">&nbsp;</div>
                                                        <div class="loading-dot2">&nbsp;</div>
                                                    </div>
                                                </div>
                                                <div data-size="brick" data-space="1px" class="isotope masonry  isotope_fade me">
                                                    <?php foreach($projects as $value): ?>
                                                        <div class="<?php echo $value['ProjectFile']['class']; ?>" >
                                                            <div class="inside brick-inside post-bgcolor-default sm">
                                                                <a class="brick-link" title="" href="<?php echo $this->Html->url(array('controller' => 'portfolio', 'action' => 'viewdetail', 'name' => strtolower(Inflector::slug($value['ProjectFile']['publish_title'], $replacement = '-')), 'id' => $value['ProjectFile']['id'])); ?>">
                                                                    <div class="brick-hover-mask brick-hover">
                                                                        <h3 class="brick-title"><?php echo $value['ProjectFile']['publish_title']; ?></h3>
                                                                        <p class="brick-title1"><?php echo $this->Text->truncate($value['ProjectFile']['description'], 22, array('ellipsis' => '...','exact' => false)); ?></p>
                                                                    </div>
                                                                    <div class="brick-content">
                                                                        <?php echo $this->Html->image('/files/thumbnails/'.$value['ProjectFile']['thumbnail']); ?>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <!--End inside-->
                                                        </div>
                                                    <?php endforeach; ?>                
                                                </div>
                                            </div>
                                            <!--End container-isotope-->
                                        </div>
                                        <!--End row-fluid-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End content-wrap-inn-->
        </div>
        <!--end content_wrap-->
    </div>
</div>