<div id="main-wrap">
    <div id="wrapper">
        <section>
            <div class="banner-container">
                <div class="cover">
                    <?php 
                        if($project['ProjectFile']['coverimage'] != '') {
                            echo $this->Html->image('/files/coverimages/'.$project['ProjectFile']['coverimage']);
                        }
                    ?>
                </div>
                <div class="project_title">
                    <div class="fixed">
                        <a href="<?php echo $this->Html->url(array('controller' => 'portfolio', 'action' => 'index')); ?>" class="btn btn-primary">
                        <span class="glyphicon glyphicon-chevron-left"></span>&nbsp;Back</a>    
                    </div>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $this->Html->url(array('controller' => 'portfolio', 'action' => 'index')); ?>">Portfolio</a></li>
                        <li>Project Detail</li>
                    </ol>
                    <?php
                        if($project['ProjectFile']['thumbnail'] != '') {
                            $imageSize = getimagesize('http://'.$_SERVER['HTTP_HOST'].'/files/thumbnails/'.$project['ProjectFile']['thumbnail']);
                        }
                    ?>
                    <div class="imageContainer <?php if(isset($imageSize) && $imageSize[0] <= 400 && $imageSize[1] > 350) {echo 'increaseHeight';} ?>">
                        <?php 
                            if($project['ProjectFile']['thumbnail'] != '') {
                                echo $this->Html->image('/files/thumbnails/'.$project['ProjectFile']['thumbnail']);
                            }
                        ?>
                    </div>
                    <h1><?php echo $project['ProjectFile']['publish_title']; ?></h1>

                    <?php
                        if($project['ProjectFile']['url'] != ''){
                            echo "<a href='".$project['ProjectFile']['url']."' class='white' target='_blank'>Preview</a>";
                        } else {
                            echo "<a href='".$this->Html->url(array('controller' => 'portfolio', 'action' => 'preview', 'projectfolder' => $project['ProjectFile']['project_folder'], 'categoryfolder' => $project['Category']['category_folder'], 'datefolder' => $project['ProjectFile']['date_folder']))."' class='white' target='_blank'>Preview</a>";
                        }
                    ?>
                </div>
            </div>
        </section>

        <div class="project_description">
            <h2><?php echo $project['ProjectFile']['publish_title']; ?></h2>
            <p><?php echo $project['ProjectFile']['description']; ?></p>
        </div>
        <div class="push"></div>
    </div>
</div>