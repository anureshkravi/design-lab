<div id="main_wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="add-new-project">
                <div class="col-lg-12">
                    <h2>Add New Privilege</h2>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $this->Html->url(array('controller' => 'dashboard', 'action' => 'index')); ?>">Dashboard</a></li>
                        <li>Add New Privilege</li>
                    </ol>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="add-new">
                <div class="col-lg-12">
                    <form role="form" id="PrivilegeAdminEditForm" method="post" class="validate" action="/admin/privileges/edit/<?php echo $this->request->data['Privilege']['id']; ?>">
						<input type="hidden" name="data[Privilege][id]" value="<?php echo $this->request->data['Privilege']['id']; ?>">			
						<div class="form-group">
							<label class="control-label">Role</label>					
							<select class="form-control" name="data[Privilege][role_id]" required="required">
								<option value="">Select Role</option>
								<?php
									foreach( $roles as $key => $value ){
										echo "<option value='".$key."'";
										if( $this->request->data['Privilege']['role_id'] == $key ){
											echo "selected";
										}
										echo ">".$value."</option>";
									}
								?>
							</select>
						</div>

						<div class="form-group">
							<label class="control-label">Controller</label>					
							<select class="form-control" name="data[Privilege][controller]" id="PrivilegeController" required="required">
								<option value="">Select Controller</option>
								<?php
									foreach( $controllers as $key => $value ){
										echo "<option value='".$key."'";
										if( $this->request->data['Privilege']['controller'] == $key ){
											echo "selected";
										}
										echo ">".$value."</option>";
									}
								?>
							</select>
						</div>

						<div class="form-group">
							<label class="control-label">Action</label>					
							<select class="form-control" name="data[Privilege][action]" id="PrivilegeAction" required="required">
								<option value="">Select Action</option>
								<?php
									foreach( $dropdown_action as $key => $value ){
										echo "<option value='".$key."'";
										if( $this->request->data['Privilege']['action'] == $key ){
											echo "selected";
										}
										echo ">".$value."</option>";
									}
								?>
							</select>
						</div>
						
						<div class="form-group">
							<input type="submit" class="btn btn-success" value="Submit">
							<input type="reset" class="btn" value="Reset">
						</div>			
					</form>
                </div>
            </div>
        </div>
    </div>
</div>
