<div id="main_wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="addproject">
                <div class="col-lg-12"> 
                    <a href="<?php echo $this->Html->url(array('controller' => 'privileges', 'action' => 'add')); ?>" class="btn btn-primary pull-right" data-toggle="tooltip" data-placement="left" title="New Privilege"><span class="glyphicon glyphicon-plus pull-left"></span><span class="pull-left mobile">New Privilege</span></a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="projectlist">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class=" table-responsive">
                        <table class="table table-striped" id="mytooltip">
                            <tr>
								<th>Role</th>
								<th>Controller</th>
								<th>Permissions</th>
								<th>Action</th>
                            </tr>
                            <?php foreach ($privileges as $privilege): ?>
                            <tr>
                                <td><?php echo h($privilege['Role']['title']); ?></td>
                                <td><?php echo h($privilege['Privilege']['controller']); ?></td>
								<td><?php echo h($privilege['Privilege']['action']); ?></td>
                                <td>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'privileges', 'action' => 'edit', $privilege['Privilege']['id'])); ?>" data-toggle="tooltip" title="edit"><?php echo $this->Html->image('edit.png'); ?></a>
                                    <?php
                                        if($privilege['Status']['id'] == 1){
                                    ?>
                                        <a href="#" class="jquery_action_status" id = 'action_<?php echo $privilege['Privilege']['id']; ?>_privileges' rel="2" data-toggle="tooltip" title="publish"><?php echo $this->Html->image('publish.png'); ?></a>
                                    <?php } else {
                                    ?>
                                        <a href="#" class="jquery_action_status" id = 'action_<?php echo $privilege['Privilege']['id']; ?>_privileges' rel="1" data-toggle="tooltip" title="publish"><?php echo $this->Html->image('unpublish.png'); ?></a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.projectlist a').tooltip();
    }); 
</script>
