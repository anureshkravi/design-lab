<div id="main_wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="addproject">
                <div class="col-lg-12"> 
                    <a href="<?php echo $this->Html->url(array('controller' => 'categories', 'action' => 'add')); ?>" class="btn btn-primary pull-right" data-toggle="tooltip" title="New Category" data-placement="left"><span class="glyphicon glyphicon-plus pull-left"></span><span class="pull-left mobile">New Category</span></a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="projectlist">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class=" table-responsive">
                        <table class="table table-striped" id="mytooltip">
                            <tr>
                                <th>Title</th>
								<!-- <th>Status</th>
								<th>Created</th>
								<th>Action</th> -->
                            </tr>
                            <?php foreach ($categories as $category): ?>
                            <tr>
                                <td><?php echo h($category['Category']['title']); ?></td>
                                <!-- <td id="change_<?php echo $category['Category']['id']; ?>_status"><?php echo h($category['Status']['title']); ?></td>
                                <td><?php echo $this->Html->beautifulDate($category['Category']['created']); ?></td>
                                <td>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'categories', 'action' => 'edit', $category['Category']['id'])); ?>" data-toggle="tooltip" title="edit"><img src="img/edit.png" alt="edit"></a>
                                    <?php
                                        if($category['Status']['id'] == 1){
                                    ?>
                                        <a href="#" class="jquery_action_status" id = 'action_<?php echo $category['Category']['id']; ?>_categories' rel="2" data-toggle="tooltip" title="publish"><img src="img/publish.png" alt="publish"></a>
                                    <?php } else {
                                    ?>
                                        <a href="#" class="jquery_action_status" id = 'action_<?php echo $category['Category']['id']; ?>_categories' rel="1" data-toggle="tooltip" title="publish"><img src="img/unpublish.png" alt="unpublish"></a>
                                    <?php } ?>
                                    <a onclick="if (confirm(&quot;Are you sure you want to delete?&quot;)) { return true; } return false;" href="categories/delete/<?php echo $category['Category']['id']; ?>"><img src="img/delete.png" alt="delete"></a>
                                </td> -->
                            </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.projectlist a').tooltip();
    }); 
</script>