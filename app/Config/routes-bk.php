<?php
	Router::connect('/', array('controller' => 'portfolio', 'action' => 'index'));

	Router::connect(
		'/portfolio/:tag',
		array('controller' => 'portfolio', 'action' => 'view'),
		array('pass' => array('tag'))
	);

	Router::connect(
		'/portfolio/:name/:id',
		array('controller' => 'portfolio', 'action' => 'viewdetail'),
		array('pass' => array('name', 'id'))
	);

	Router::connect(
		'/portfolio/:projectfolder/:categoryfolder/:datefolder',
		array('controller' => 'portfolio', 'action' => 'preview'),
		array('pass' => array('projectfolder', 'categoryfolder', 'datefolder'))
	);

	//Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

	Router::connect('/admin', array('controller' => 'dashboard', 'action' => 'index', 'admin' => 1));
	
	Router::connect('/admin/projectfiles/thumb', array('controller' => 'projectfiles', 'action' => 'thumb', 'admin' => 1));

	Router::connect('/admin/projectfiles/publish', array('controller' => 'projectfiles', 'action' => 'publish', 'admin' => 1));
	
	Router::connect('/admin/projectfiles/add', array('controller' => 'projectfiles', 'action' => 'add', 'admin' => 1));

	Router::connect(
		'/admin/projectfiles/add/project::projectname',
		array('controller' => 'projectfiles', 'action' => 'add', 'admin' => 1),
		array('named' => array('projectname'))
	);

	Router::connect(
		'/admin/projectfiles/recover/:id',
		array('controller' => 'projectfiles', 'action' => 'recover', 'admin' => 1),
		array('pass' => array('id'))
	);

	Router::connect(
		'/admin/projectfiles/deletefiles/:id',
		array('controller' => 'projectfiles', 'action' => 'deletefiles', 'admin' => 1),
		array('pass' => array('id'))
	);

	Router::connect('/admin/projectfiles/edit', array('controller' => 'projectfiles', 'action' => 'edit', 'admin' => 1));

	Router::connect('/admin/projectfiles/delete/*', array('controller' => 'projectfiles', 'action' => 'delete', 'admin' => 1));

	Router::connect('/admin/projectfiles/deleteall/*', array('controller' => 'projectfiles', 'action' => 'deleteall', 'admin' => 1));
	
	Router::connect('/admin/projectfiles/setstatus/*', array('controller' => 'projectfiles', 'action' => 'setstatus', 'admin' => 1));

	Router::connect('/admin/projectfiles/recordListing', array('controller' => 'projectfiles', 'action' => 'recordListing', 'admin' => 1));

	Router::connect('/admin/projectfiles/sorting', array('controller' => 'projectfiles', 'action' => 'sorting', 'admin' => 1));

	Router::connect('/admin/projectfiles/scroll/*', array('controller' => 'projectfiles', 'action' => 'scroll', 'admin' => 1));

	Router::connect('/admin/projectfiles/search/*', array('controller' => 'projectfiles', 'action' => 'search', 'admin' => 1));

	Router::connect('/admin/projectfiles/archives', array('controller' => 'projectfiles', 'action' => 'archives', 'admin' => 1));

	Router::connect(
		'/admin/projectfiles/:projectfolder',
		array('controller' => 'projectfiles', 'action' => 'view', 'admin' => 1),
		array('pass' => array('projectfolder'))
	);

	Router::connect(
		'/projectfiles/:projectfolder',
		array('controller' => 'projectfiles', 'action' => 'view'),
		array('pass' => array('projectfolder'))
	);

	Router::connect(
		'/admin/projectfiles/:projectfolder/:categoryfolder',
		array('controller' => 'projectfiles', 'action' => 'viewCategoryListing', 'admin' => 1),
		array('pass' => array('projectfolder', 'categoryfolder'))
	);

	Router::connect(
		'/projectfiles/:projectfolder/:categoryfolder',
		array('controller' => 'projectfiles', 'action' => 'viewCategoryListing'),
		array('pass' => array('projectfolder', 'categoryfolder'))
	);
	
	Router::connect(
		'/admin/projectfiles/:projectfolder/:categoryfolder/:datefolder',
		array('controller' => 'projectfiles', 'action' => 'viewdetails', 'admin' => 1),
		array('pass' => array('projectfolder', 'categoryfolder', 'datefolder'))
	);

	Router::connect(
		'/projectfiles/:projectfolder/:categoryfolder/:datefolder',
		array('controller' => 'projectfiles', 'action' => 'viewdetails'),
		array('pass' => array('projectfolder', 'categoryfolder', 'datefolder'))
	);

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';

